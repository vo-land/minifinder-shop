<?php
// Text
$_['text_address']       = 'Klart: Betaladress är registrerad!';
$_['text_method']        = 'Klart: Betalsätt är registrerad!';

// Error
$_['error_permission']   = 'Varning: Du har inte tillåtelse att ändra i API!!';
$_['error_firstname']    = 'Förnamn måste vara mellan 1-12 tecken.';
$_['error_lastname']     = 'Efternamn måste vara mellan 1-12 tecken.';
$_['error_address_1']    = 'Adress måste vara mellan 3-128 tecken.!';
$_['error_city']         = 'Stad måste vara mellan 3-128 tecken.';
$_['error_postcode']     = 'Postnummer måste vara mellan 2-10 tecken.!';
$_['error_country']      = 'Välj ett land!';
$_['error_zone']         = 'Välj ett län!';
$_['error_custom_field'] = '%s Krävs!';
$_['error_address']      = 'Varning: Betaladress krävs!';
$_['error_method']       = 'Varning: Betalsätt krävs!';
$_['error_no_shipping']  = 'Varning: Inget betalningsalternativ finns.';
?>