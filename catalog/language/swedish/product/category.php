<?php
// Text
$_['text_refine']       = 'Förfina sökningen';
$_['text_product']      = 'Produkter';
$_['text_error']        = 'Kategori hittades inte!';
$_['text_empty']        = 'Det finns inga produkter att visa i denna kategori.';
$_['text_quantity']     = 'Antal:';
$_['text_manufacturer'] = 'Tillverkare:';
$_['text_model']        = 'Artikelnummer:'; 
$_['text_points']       = 'Bonuspoäng:'; 
$_['text_price']        = 'Pris:'; 
$_['text_tax']          = 'Exkl. moms:'; 
$_['text_compare']      = 'Produktjämförelse (%s)'; 
$_['text_sort']         = 'Sortera efter:';
$_['text_default']      = 'Förvald';
$_['text_name_asc']     = 'Namn (A - Ö)';
$_['text_name_desc']    = 'Namn (Ö - A)';
$_['text_price_asc']    = 'Pris (Lågt &gt; högt)';
$_['text_price_desc']   = 'Pris (Högt &gt; lågt)';
$_['text_rating_asc']   = 'Betyg (Lägsta)';
$_['text_rating_desc']  = 'Betyg (Högsta)';
$_['text_model_asc']    = 'Artikelnummer (A - Ö, 1-99999)';
$_['text_model_desc']   = 'Artikelnummer (Ö - A, 99999-1)';
$_['text_limit']        = 'Visa:';
?>