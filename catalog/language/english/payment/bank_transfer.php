<?php
// Text
$_['text_title']				= 'Bank Transfer';
$_['text_instruction']			= 'Bank Transfer Instructions';
$_['text_description']			= 'Please transfer the total amount to the following bank account.';
$_['text_payment']				= 'Your order will not ship until we receive your payment.';

// Extra mail text - Custom
$_['text_payment_mail']  = 'Please use orderID as payment reference. Replay on this e-mail if you have any questions.';
