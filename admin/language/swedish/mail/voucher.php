<?php
// Text
$_['text_subject']  = 'Du har fått ett presentkort från %s';
$_['text_greeting'] = 'Grattis, du har emottagit ett presentkort värt %s';
$_['text_from']     = 'Detta presentkort är en gåva till dig från %s';
$_['text_message']  = 'Med följande meddelande';
$_['text_redeem']   = 'För att använda detta presentkort, anteckna denna kod: <b>%s</b> klicka därefter på länken nedan och välj den produkt du önskar i butiken. Du kan sedan ange presentkortskoden i varukorgen innan du klickar på "Till Kassan".';
$_['text_footer']   = 'Svara på detta epostmeddelande om du har några frågor.';
?>