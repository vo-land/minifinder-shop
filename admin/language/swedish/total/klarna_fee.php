<?php
// Heading
$_['heading_title']    = 'Klarna Avgift';

// Text
$_['text_total']       = 'Ordertotal';
$_['text_success']     = 'Klart: Du har nu ändrat Klarna avgiften!';
$_['text_edit']        = 'Ändra i Klarna Avgift';
$_['text_sweden']      = 'Sverige';
$_['text_norway']      = 'Norge';
$_['text_finland']     = 'Finland';
$_['text_denmark']     = 'Danmark';
$_['text_germany']     = 'Tyskland';
$_['text_netherlands'] = 'Holland';

// Entry
$_['entry_total']      = 'Ordertotal:';
$_['entry_fee']        = 'Faktura avgift:';
$_['entry_tax_class']  = 'Momsklass:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sorteringsordning:';

// Error
$_['error_permission'] = 'Varning: Du har ingen rättighet att ändra Klarna Avgiften!';
?>