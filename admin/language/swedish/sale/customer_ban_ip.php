<?php
// Heading
$_['heading_title']    = 'Svartlistade kunder / IP';

// Text
$_['text_success']     = 'Klart: Du har ändrat uppgifterna!';
$_['text_list']        = 'Svartlistade kunders IP Lista';
$_['text_add']         = 'Lägg till en Svartlistad IP';
$_['text_edit']        = 'Ändra i en Svartlistad IP';

// Column
$_['column_ip']        = 'IP';
$_['column_customer']  = 'Kunder';
$_['column_action']    = 'Välj';

// Entry
$_['entry_ip']         = 'IP-adress:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra!';
$_['error_ip']         = 'IP-adress måste innehålla mellan 1 och 15 tecken!';
?>