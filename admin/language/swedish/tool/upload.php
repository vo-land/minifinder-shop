<?php
// Heading
$_['heading_title']     = 'Uploads';

// Text
$_['text_success']      = 'Klart: Du har ändra i Upload!';
$_['text_list']         = 'Upload Lista';

// Column
$_['column_name']       = 'Upload Namn';
$_['column_filename']   = 'Filnamn';
$_['column_date_added'] = 'Datum';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_name']        = 'Upload Namn';
$_['entry_filename']    = 'Filnamn';
$_['entry_date_added'] 	= 'Datum';

// Error
$_['error_permission']  = 'Varning: Du har inte tillåtelse att ändra.';
?>