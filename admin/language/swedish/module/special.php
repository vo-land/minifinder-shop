<?php
// Heading
$_['heading_title']       = 'Kampanj';

// Text
$_['text_module']         = 'Moduler';
$_['text_success']        = 'Klart: Du har ändrat i modulen!';
$_['text_edit']           = 'Ändra Kampanjen.';

// Entry
$_['entry_name']          = 'Modul Namn';
$_['entry_limit']         = 'Max antal:';
$_['entry_width']         = 'Bredd';
$_['entry_height']        = 'Höjd';
$_['entry_status']        = 'Status:';

// Error
$_['error_permission']    = 'Varning: Du har inte behörighet att ändra i modulen!';
$_['error_name']       = 'Modul Namn måste vara mellan 3-64 tecken.!';
$_['error_width']      = 'Bredd krävs!';
$_['error_height']     = 'Höjd krävs!';
?>