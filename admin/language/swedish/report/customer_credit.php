<?php
// Heading
$_['heading_title']         = 'Rapport Krediter';

// Column
$_['text_list']             = 'Kunders Krediter Lista';
$_['column_customer']       = 'Kund';
$_['column_email']          = 'E-post';
$_['column_customer_group'] = 'Kundgrupp';
$_['column_status']         = 'Status';
$_['column_total']          = 'Summa';
$_['column_action']         = 'Ändra';

// Entry
$_['entry_date_start']      = 'Startdatum:';
$_['entry_date_end']        = 'Slutdatum:';
?>