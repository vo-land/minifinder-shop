<?php 
/*
 * This is custom file that is used to display which GPRS information
 * should be used to set GPRS on a specific tracker
 */
$getTracker = isset( $_GET['tracker'] ) ? $_GET['tracker'] : NULL;
$getPlatform = isset( $_GET['platform'] ) ? $_GET['platform'] : NULL;

function viewPico()
{
	?>
	<h2>Get firmware version och IMEI</h2>
	<p><input value="version" /></p>
	<br />
	
	<h2>Activate Assist GPS</h2>
	<p><input value="AGPS1" /></p>
	<br />
	
	<h2>GPRS inställningar</h2>
	<p><strong>S1,APN,username,password</strong></p>
	<p><input value="S1,online.telia.se" /></p>
	<p><input value="S1,halebop.telia.se" /></p>
	<p><input value="S1,internet.telenor.se" /></p>
	<p><input value="S1,internet.tele2.se" /></p>
	<p>S0 = inaktiverar GPRS. S2 = återansluter GPRS.</p>
	<br />
	
	<h2>Ställ in IP och Port</h2>
	<p>IP1,IP,port</p>
	<p>Tracktor: <input value="IP1,94.242.255.121,20790" /></p>
	<p>Smart-Tracking: <input value="IP1,103.21.211.11,5050" /></p>
	<p>Trace Orange: <input value="IP1,193.193.165.166,20790" /></p>
	<br />
	
	<h2>Ställ in GPRS tidsintervall</h2>
	<p>TIxxS/M/H</p>
	<p>Exempel: <input value="TI30S" /> eller <input value="TI01M" /></p>
	<br />
	
	<h2>Kontrollera inställningarna</h2>
	<p><input value="G1" /></p>
	<br />
	
	<h2>Starta om enheten</h2>
	<p><input value="REBOOT" /></p>
	<br />
	
	<h2>Activate Power Saving Mode</h2>
	<p><input value="PS1" /></p>
	<br />
	
	<h2>Activate Deep Sleep Mode</h2>
	<p><input value="DS1" /></p>
	<br />
	
	<h2>Återställ till fabriksinställningar</h2>
	<p><input value="RESET!" /></p>
	<br />
	<?php
}

function viewGM7()
{
	?>
	<p>Avsluta sleep mode 1 or 2</p>
	<p>$WP+PSMT=0000,0,0,0,0</p>
	<p>Alternativt ställa om enheten till sleep mode 3 (nytt)<br />
	Endast G-sensor kan väcka enheten<p>
	<p>$WP+PSMT=0000,3,0,3,0,0,0,0</p>
	
	<p>Kommentar:</p>
	<p>0000,mode,sleeping-interval,report-action,sms-vip,timer1,timer2,timer3<br />
	3 = Första trean står för sleep mode 3 (gps- &amp; gsm-antenn off vid ej rörelse)<br />
	3 = Andra trean står för logging + polling</p>
	
	<h2>1. Set Up APN and GPRS server</h2>

	<p>Telia:<br />
	$WP+COMMTYPE=0000,4,,,online.telia.se,,,tracktor.se,20379,30</p>
	
	<p>Telenor:<br />
	$WP+COMMTYPE=0000,4,,,internet.telenor.se,,,tracktor.se,20379,30</p>
	
	<p>Halebop<br />
	$WP+COMMTYPE=0000,4,,,halebop.telia.se,,,94.242.255.121,20379,30</p>
	
	<p>Telenor IP:<br />
	$WP+COMMTYPE=0000,4,,,internet.telenor.se,,,94.242.255.121,20379,30</p>
	
	<p>Telia IP:<br />
	$WP+COMMTYPE=0000,4,,,online.telia.se,,,94.242.255.121,20379,30</p>
	
	<p>Another format:<br />
	$WP+COMMTYPE=0000,4,“”,“”,online.telia.se,“”,“”,94.242.255.121,20379,30,</p>
	
	<p>Kommentar:<br />
	4  = GPRS over TCP (1 = GSM SMS communication)<br />
	30 = Keep Alive Packet<br />
	20379 = Port (this case Wilon hosting)<br />
	94.242.255.121 = Tracktor @ server.lu<br />
	<br />
	To clear COMMTYPE and SWITCH back to GSM/SMS communication:<br />
	$WP+COMMTYPE=0000,1,“”,“”,“”,“”,“”,0.0.0.0,1000,0</p>
	<br /><br />
	
	<h2>2. SET UP UNIQUE DEVICE ID</h2>
	<p>$WP+UNCFG=0000,3000XXXXXX</p>
	<p>Kommentar:
	Standard device ID is 3000000001. However that ID is probably used by someone else in a public tracking system so we have to make a new unique.
	Max 10 digits and must begin with nr. 3.<br />
	DeviceID should be entered as IMEI/UniqueNr in a tracking system.</p>
	<br /><br />
	
	<h2>3. Enable GPRS Roamin function</h2>

	<p>$WP+ROAMING=0000,1</p>
	
	<br />
	
	<h2>4. TRACKING BY TIME INTERVAL</h2>
	
	<p>$WP+TRACK=0000,1,S,,N,0,4</p>
	<p>S = whole number in seconds (min 5, max 65535)<br />
	N = number of times (0 means continuosly).<br />
	0 = send only if gps fixed (1 send even if gps not fixed)<br />
	4 = TCP</p>
	
	<br />
	
	<p>Example: Continuosly tracking every 30s<br />
	$WP+TRACK=0000,1,30,,0,0,4</p>
	
	<p>Example: Every 5 sec<br />
	$WP+TRACK=0000,1,5,,0,0,4</p>
	
	<p>Example: Every 10 sec<br />
	$WP+TRACK=0000,1,10,,0,0,4</p>
	
	<p>Example: Every 1 minute<br />  
	$WP+TRACK=0000,1,60,,0,0,4</p>
	
	<p>Example: Every 10 minutes<br /> 
	$WP+TRACK=0000,1,600,,0,0,4</p>
	
	<p>Example: CLOSE position uploading<br /> 
	$WP+TRACK=0000,0</p>
	
	<br />
	
	<h2>5. LED ON EVEN IF DETACHED (Function key pressed)</h2>
	<p>$WP+ELED=0000,1</p>
	
	<br /><br />
	<?php
}

// Functions
function viewMVT_1()
{
	?>
	<h2>Get Firmware Version and SN</h2>

	<p>SMS Set: 0000,E91<br/>
	SMS Get: IMEI, E91,version,SN</p>
	
	<p>Begär position med: 0000,A00</p>


	<h2>1. Get IMEI number for use at registration (Initialization F11)</h2>
	<p>SMS Set: 0000,F11<br/>
	SMS Get: IMEI,F11,OK</p>
	<p>Description: Set all parameters, except for the password, back to factory default.</p>
	
	
	<h2>2b. Create account</h2>
	<a href="http://mtr.gpser.se/ad/">mtr.gpser.se</a>
	<p>Device ID = IMEI<br/>
	Password = 0000</p>
	
	
	<h2>3. Ställ in GPRS</h2>
	<p>
	<strong>Telia:</strong><br />
	SMS Set: 0000,A21,1,mtr.gpser.se,8500,online.telia.se,,<br/>
	SMS Set: 0000,A21,1,tracktor.se,XXXXX,online.telia.se,,<br/><br/>
	
	<strong>Telenor:</strong><br />
	SMS Set: 0000,A21,1,mtr.gpser.se,8500,internet.telenor.se,,<br/>
	SMS Set: 0000,A21,1,tracktor.se,XXXXX,internet.telenor.se,,<br/><br/>
	SMS Get: IMEI,A21,OK<br/><br />
	
	Tracktor.se specific: IP: 94.242.255.121, PORT: [20710 = TC68] [20757 = T1] [20261 = MVT380] [20523 = MT90]<br/>
	
	</p>
	<p></p>
	
	
	<h2>4. Track by Time Interval (GPRS) - A12</h2>
	<p>SMS Set: 0000,A12,interval,times<br/>
	SMS Get: IMEI,A12,OK</p>
	<p>Desctiprion: Interval is in unit of 10 seconds. 
	<br />Interval = 0, stop tracking by time interval. <br />
	Times = 0, track by interval continuously; times = [1,65535]</p>
	
	<h2>Exempel:</h2>
	<p>SMS Set: 0000,A12,6,0<br/>
	SMS Get: IMEI,A12,OK</p>
	
	<p>Kommando ovan kommer att rapportera koordinater till karttjänsten varje minut.</p>
	
	
	<p>No data uploaded from this device</p>
	<p>Warm Note:</p>
	<p>Please confirm correct setting for connection, instrucions as follow:</p>
	<ul>
		<li>Please use Meitrack Manager, set GPRS connection items</li>
		<li>Open connection types,select UPD/TCP</li>
		<li>Select port 8500, set IP:67.203.13.26</li>
	</ul>
	<?php
}


function viewTK102()
{	
	?>
	<h2>TK102-2 APN SETUP</h2>
	
	<h2>Initiera enheten</h2>
	<p>1. begin123456</p>
	<br />
	<h2>Ställ in APN</h2>
	<p>2. apn123456 online.telia.se</p>
	<p>2. apn123456 halebop.telia.se</p>
	<p>2. apn123456 internet.telenor.se</p>
	<p>2. apn123456 internet.tele2.se</p>
	<br />
	
	<h2>Ställ in IP-adress & Port</h2>
	<p>3. adminip123456 87.96.157.27 20157 (home)</p>
	<p>3. adminip123456 94.242.255.121 20157 (server.lu)</p>
	<br />
	
	<h2>Autospårning</h2>
	<p>4. t060s***n123456</p>
	
	<h2>Autospårning (Coban TK102B)</h2>
	<p>4. GPRS123456</p>
	<p>4. fix060s***n123456</p>
	
	<p>You can use the Tlimit  command  together with T030S***N  command<br />
		For example <strong>Tlimit123456 500</strong> means  if car move more than 500 meters<br />
		,it'll send data each 30 seconds, if don't move ,then stop send data each 30 seconds.</p>

	<?php
}

function viewWP99()
{
	?>
	<h2>Spåra via gprs med WP99</h2>
	
	<h2>1. Auktorisera ditt mobilnummer</h2>
	<p>Facid,123456,authorize,1=ditt-modilnr;</p>
	
	
	<h2>2. Ställ in GPRS-inställningar</h2>
	<p><strong>Telia:</strong><br/>
	Facid,123456,gprs,addr=tracktor.se,port=20704,name=,pass=,APN=online.telia.se,ID=,mode=0;</p>
	
	<p><strong>Tenor:</strong><br/>
	Facid,123456,gprs,addr=tracktor.se,port=20704,name=,pass=,APN=internet.telenor.se,ID=,mode=0;</p>
	
	<p>IP-address to tracktor.se: 94.242.255.121</p>
	
	<p><strong>Förklaring av kommandon:</strong><br />
	addr = IP-nummer till GPRS-servern<br />
	port = port på GPRS-servern<br />
	name = APN-användarnamn<br />
	pass = APN-lösenord<br />
	APN = APN-adress för ditt mobilabonemang<br />
	ID = Ett unikt nummer upp till 19 tecken.<br />
	Mode = 0 är TCP och 1 står för UDP.</p>
	
	
	<h2>3. Ställ in intervall</h2>
	<p>Facid,123456,loc,i=30,t=999,l=0;<br />
	<p>Ovanstående exempel kommer skicka koordinater var 30:e sekund (i=30) oändligt många gånger (t=999) till karttjänsten.</p>
	<?php
}

?>


<!DOCTYPE html>
<head>
	<title>Tracker configuration tool</title>
	<meta charset="UTF-8" />
</head>
<body style="height:100%; font-family: Calibri, Candara, Segoe, Optima, Arial, sans-serif;">
	<div style="margin:auto;width:800px;">
	<h1>Set up GPRS on the tracker</h1>
	
	<form action="?" method="get">
	<select name="tracker">
		<option value="0" <?php echo ($getTracker == 0) ? 'selected' : NULL; ?>>Choose a tracker</option>
		<option value="1" <?php echo ($getTracker == 1) ? 'selected' : NULL; ?>>MVT380 &amp; MT90</option>
		<option value="2" <?php echo ($getTracker == 2) ? 'selected' : NULL; ?>>TK102-2</option>
		<option value="3" <?php echo ($getTracker == 3) ? 'selected' : NULL; ?>>WP99</option>
		<option value="4" <?php echo ($getTracker == 4) ? 'selected' : NULL; ?>>GM7</option>
		<option value="5" <?php echo ($getTracker == 5) ? 'selected' : NULL; ?>>Minifinder Pico</option>
	</select>
	<br />
	<br />
	
	<input type="submit" name="view" value="view" />
	</form>
	<hr/>
	<?php 
	
	
	
	// MVT380 & MT90 - Tracktor.se
	if ( $getTracker == 1) {
		viewMVT_1();
	}
	
	// TK102-2
	if ( $getTracker == 2 ) {
		viewTK102();
	}
	
	// WP99
	if ( $getTracker == 3 ) {
		viewWP99();
	}
	
	// GM7
	if ( $getTracker == 4 ) {
		viewGM7();
	}
	
	// MiniFinder Pico
	if ( $getTracker == 5 ) {
		viewPico();
	}
	?>
	</div>
</body>