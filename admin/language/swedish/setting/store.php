<?php
// Heading
$_['heading_title']            = 'Butik';

// Text
$_['text_settings']                    = 'Inställningar';
$_['text_success']             = 'Klart: Du har ändrat i Butik!';
$_['text_list']                        = 'Butik Lista';
$_['text_add']                         = 'Lägg till Butik';
$_['text_edit']                        = 'Ändra i Butik';
$_['text_items']               = 'Produkter';
$_['text_tax']                 = 'Moms';
$_['text_account']             = 'Konto';
$_['text_checkout']            = 'Kassa';
$_['text_stock']               = 'Lager';
$_['text_shipping']            = 'Leveransadress';
$_['text_payment']             = 'Betalningsadress';

// Column
$_['column_name']               = 'Butiksnamn';
$_['column_url']	            = 'Butiks-URL';
$_['column_action']             = 'Ändra';

// Entry
$_['entry_url']                 = 'Butiks-URL:';
$_['entry_ssl']                 = 'SSL-URL:';
$_['entry_name']                = 'Butiknamn:';
$_['entry_owner']               = 'Butiksägare:';
$_['entry_address']             = 'Adress:';
$_['entry_email']               = 'E-post:';
$_['entry_geocode']                    = 'Geocode';
$_['entry_telephone']           = 'Mobiltelefon:';
$_['entry_fax']                 = 'Telefon:';
$_['entry_image']                      = 'Bild';
$_['entry_open']                       = 'Öppettider';
$_['entry_comment']                    = 'Kommentar';
$_['entry_location']                   = 'Butikens plats';
$_['entry_meta_title']                 = 'Meta Title';
$_['entry_meta_description']           = 'Meta Tag Beskrivning:';
$_['entry_meta_keyword']               = 'Meta Tag Keywords';
$_['entry_layout']              = 'Standardlayout:';
$_['entry_template']            = 'Mall/Template:';
$_['entry_country']             = 'Land:';
$_['entry_zone']                = 'Län:';
$_['entry_language']            = 'Språk:';
$_['entry_currency']            = 'Valuta:';
$_['entry_product_limit'] 	    = 'Normalt antal objekt per sida.';
$_['entry_product_description_length'] = 'Lista beskrivning gräns i katalog.';
$_['entry_tax']                 = 'Visa priser inkl moms:';
$_['entry_tax_default']         = 'Använd butikens skatteadress:';
$_['entry_tax_customer']        = 'Använd kundens skatteadress:';
$_['entry_customer_group']      = 'Kundgrupp:';
$_['entry_customer_group_display'] = 'Kund Grupper lista:';
$_['entry_customer_price']      = 'Logga in för att se priser:';
$_['entry_account']             = 'Kontovillkor:';
$_['entry_cart_weight']         = 'Visa vikt i varukorgen:';
$_['entry_checkout_guest']      = 'Gäst kassa';
$_['entry_checkout']            = 'Köpvillkor:';
$_['entry_stock_display']       = 'Visa lagersaldo:';
$_['entry_stock_checkout']      = 'Tillåt alltid köp:';
$_['entry_order_status']        = 'Orderstatus:';
$_['entry_logo']                = 'Butikslogo:';
$_['entry_icon']                = 'Ikon:';
$_['entry_image_category']      = 'Storlek för kategoribild:';
$_['entry_image_thumb']         = 'Storlek Produkt-mini-bild:';
$_['entry_image_popup']         = 'Storlek Produkt-popup-bild:';
$_['entry_image_product']       = 'Storlek bild i produktlista:';
$_['entry_image_additional']    = 'Bildstorlek under "Fler bilder":';
$_['entry_image_related']       = 'Bildstorlek i "Relaterade produkter":';
$_['entry_image_compare']       = 'Bildstorlek i "Jämför":';
$_['entry_image_wishlist']      = 'Bildstorlek i Önskelista:';
$_['entry_image_cart']          = 'Bildstorlek i varukorgen:';
$_['entry_image_location']             = 'Bildstorlek butiken storlek';
$_['entry_width']                      = 'Bredd';
$_['entry_height']                     = 'Höjd';
$_['entry_secure']                     = 'Använd SSL:';

// Help
$_['help_url']                         = 'Ange full URL till din butik. Ange som: http://www.yourdomain.com/path/<br /><br /> Använd inte kataloger för att skapa ny butik. Du anger ny hos din host för tex sub-domäner.';
$_['help_ssl']                         = 'SSL URL till din butik. Ange som: http://www.yourdomain.com/path/<br /><br /> Använd inte kataloger för att skapa ny butik. Du anger ny hos din host för tex sub-domäner.';
$_['help_geocode']                     = 'Ange din butiksplats län manuellt.';
$_['help_open']                        = 'Ange dina öppettider.';
$_['help_comment']                     = 'I detta fält kan du skriva och ange sånt som inte ryms inom Opencart.';
$_['help_location']                    = 'Butiksplatsen som ska synas i kontakta oss. Om du vill ha olika platser.';
$_['help_currency']                    = 'Ändrar standard valutan. Töm din webbläsare om det inte visas.';
$_['help_product_limit'] 	           = 'Bestämmer hur många produkter som ska visas.';
$_['help_product_description_length']  = 'I listan ange en kort beskrivning av produkten.';
$_['help_tax_default']                 = 'Använd butikens adress för att räkna ut skatt och moms.';
$_['help_tax_customer']                = 'Använd kundens adress för att räkna ut skatt och moms.';
$_['help_customer_group']              = 'Standard kundgrupp.';
$_['help_customer_group_display']      = 'Visa kundgrupper nya kunder kan välja på.';
$_['help_customer_price']              = 'Visa endast priser när kunder är inloggade.';
$_['help_account']                     = 'Låt kunderna godkänna villkoren innan kontot skapas.';
$_['help_checkout_guest']              = 'Tillåt att kunder handlar utan att skapa konto. Går ej för nedladdningsfiler.';
$_['help_checkout']                    = 'Låt kunderna godkänna villkoren innan köpet godkänns.';
$_['help_order_status']                = 'Standard status vid ny order.';
$_['help_stock_display']               = 'Visa lagerstatus på produkten.';
$_['help_stock_checkout']              = 'Tillåt kunder att köpa produktten fastän den ej finns i lager.';
$_['help_icon']                        = 'Bilden ska vara en PNG som är 16px x 16px.';
$_['help_secure']                      = 'För att använda SSL kolla med din host så de godkänner det. Krävs för att nå config filerna.';

// Error
$_['error_warning']             = 'Varning: Kontrollera meddelanden och rättat till fel!';
$_['error_permission']          = 'Varning: Du har inte behörighet att ändra!';
$_['error_name']                = 'Butiksnamnet måste innehålla mellan 3 och 32 tecken!';
$_['error_owner']               = 'Butiksägare måste innehålla mellan 3 och 64 tecken!';
$_['error_address']             = 'Butiksadressen måste innehålla mellan 10 och 256 tecken!';
$_['error_email']               = 'E-postadressen är inte giltig!';
$_['error_telephone']           = 'Telefon måste innehålla mellan 3 och 32 tecken!';
$_['error_url']                 = 'Butiks-URL krävs!';
$_['error_meta_title']                 = 'Titeln måste innehålla mellan 3 och 32 tecken!';
$_['error_limit']               = 'Antal krävs!';
$_['error_customer_group_display'] = 'Du måste inkludera kundgrupper om du ska använda detta!';
$_['error_image_thumb']         = 'Storlek för bild krävs!';
$_['error_image_popup']         = 'Storlek för bild krävs!';
$_['error_image_product']       = 'Storlek för bild krävs!';
$_['error_image_category']      = 'Storlek för bild krävs!';
$_['error_image_additional']    = 'Storlek för bild krävs!';
$_['error_image_related']       = 'Storlek för bild krävs!';
$_['error_image_compare']       = 'Storlek för bild krävs!';
$_['error_image_wishlist']      = 'Storlek för bild krävs!';
$_['error_image_cart']          = 'Storlek för bild krävs!';
$_['error_image_location']      = 'Storlek för bild krävs!';
$_['error_default']             = 'Varning: Du kan inte radera din standardbutik!';
$_['error_store']               = 'Varning: Denna butik kan inte raderas eftersom den är kopplad till %s ordrar!';
?>