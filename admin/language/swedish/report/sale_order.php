<?php
// Heading
$_['heading_title']     = 'Rapport försäljning';

// Text
$_['text_list']         = 'Försäljnings Lista';
$_['text_year']         = 'År';
$_['text_month']        = 'Månader';
$_['text_week']         = 'Veckor';
$_['text_day']          = 'Dagar';
$_['text_all_status']   = 'Alla statusar';

// Column
$_['column_date_start'] = 'Startdatum';
$_['column_date_end']   = 'Slutdatum';
$_['column_orders']     = 'Antal ordrar';
$_['column_products']   = 'Antal produkter';
$_['column_tax']        = 'Moms';
$_['column_total']      = 'Summa';

// Entry
$_['entry_date_start']  = 'Startdatum:';
$_['entry_date_end']    = 'Slutdatum:';
$_['entry_group']       = 'Gruppera efter:';
$_['entry_status']      = 'Orderstatus:';
?>