<?php
// Heading 
$_['heading_title']         = 'Orderhistorik';

// Text
$_['text_account']          = 'Konto';
$_['text_order']            = 'Orderinformation';
$_['text_order_detail']     = 'Orderdetaljer';
$_['text_invoice_no']       = 'Fakturanummer.:';
$_['text_order_id']         = 'Ordernummer:';
$_['text_date_added']       = 'Datum:';
$_['text_shipping_address'] = 'Leveransadress';
$_['text_shipping_method']  = 'Leveranssätt:';
$_['text_payment_address']  = 'Faktureringsadress';
$_['text_payment_method']   = 'Betalsätt:';
$_['text_comment']          = 'Orderkommentarer';
$_['text_history']          = 'Orderhistorik';
$_['text_success']          = 'Klart! Du har lagt till produkterna från order-ID #%s i din varukorg!';
$_['text_empty']            = 'Du har inga tidigare beställningar!';
$_['text_error']            = 'Den begärda beställningen kunde inte hittas!';

// Column
$_['column_order_id']       = 'Ordernummer';
$_['column_product']        = 'Antal produkter';
$_['column_customer']       = 'Kund';
$_['column_name']           = 'Produktnamn';
$_['column_model']          = 'Artikelnummer';
$_['column_quantity']       = 'Antal';
$_['column_price']          = 'Pris';
$_['column_total']          = 'Totalt';
$_['column_action']         = 'Ändra';
$_['column_date_added']     = 'Datum';
$_['column_status']         = 'Status';
$_['column_comment']        = 'Kommentarer';

// Error
$_['error_reorder']         = '%s är just nu ej tillgängligt att spara.';
?>
