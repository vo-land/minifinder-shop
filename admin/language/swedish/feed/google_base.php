<?php
// Heading
$_['heading_title']    = 'Google Base';

// Text   
$_['text_feed']        = 'Produktfeed';
$_['text_success']     = 'Klart: Du har ändrat i Google Base feed!';
$_['text_list']        = 'Layout Lista';
$_['text_edit']        = 'Ändra i Google Base';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_data_feed']  = 'Datafeed Url:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i Google Base feed!';
?>