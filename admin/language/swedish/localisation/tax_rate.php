<?php
// Heading
$_['heading_title']        = 'Momssatser';

// Text
$_['text_success']         = 'Klart: Du har ändrat i momssatser!';
$_['text_list']            = 'Layout Lista';
$_['text_add']             = 'Lägg till momssats';
$_['text_edit']            = 'Ändra i momssats';
$_['text_percent']         = 'Procent';
$_['text_amount']          = 'Fast summa';

// Column
$_['column_name']          = 'Namn';
$_['column_rate']          = 'Momssats';
$_['column_type']          = 'Typ';
$_['column_geo_zone']      = 'Geozone';
$_['column_date_added']    = 'Tillagd, datum';
$_['column_date_modified'] = 'Ändrad, datum';
$_['column_action']        = 'Ändra';

// Entry
$_['entry_name']           = 'Namn:';
$_['entry_rate']           = 'Momssats:';
$_['entry_type']           = 'Typ:';
$_['entry_customer_group'] = 'Kundgrupp:';
$_['entry_geo_zone']       = 'Geozone:';

// Error
$_['error_permission']     = 'Varning: Du har inte behörighet!';
$_['error_tax_rule']       = 'Varning: Momssatsen kan inte raderas eftersom den är kopplad till %s Skatter!';
$_['error_name']           = 'Namn måste vara mellan 3 och 32 tecken!';
$_['error_rate']           = 'Momssats krävs!';
?>
