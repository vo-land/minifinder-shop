<?php
// Heading
$_['heading_title']       = 'Konto';

$_['text_module']         = 'Moduler';
$_['text_success']        = 'Klart: Du har ändrat i Konto!';
$_['text_edit']           = 'Ändra i kontot.';

// Entry
$_['entry_status']        = 'Status:';

// Error
$_['error_permission']    = 'Varning: Du har inte behörighet att ändra i Konto!';
?>