<?php
// Heading 
$_['heading_title']   = 'Glömt ditt lösenord?';

// Text
$_['text_account']    = 'Konto';
$_['text_forgotten']  = 'Glömt lösenord?';
$_['text_your_email'] = 'Din e-postadress';
$_['text_email']      = 'Ange den e-postadress som är kopplad till ditt konto. Klicka på skicka för att få ditt lösenord e-postat till dig.';
$_['text_success']    = 'Klart: Ett nytt lösenord har skickats till din e-postadress.';

// Entry
$_['entry_email']     = 'E-postadress:';

// Error
$_['error_email']     = 'Fel: Den angivna e-postadressen hittades inte i våra register. Var vänlig försök igen!';
?>