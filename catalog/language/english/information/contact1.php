<?php
// Heading
$_['heading_title']  = 'Contact Us';

// Text
$_['text_location']  = 'Our Location';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Address';
$_['text_tax']       = 'Holds F-tax';
$_['text_telephone'] = 'Telephone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Opening Times';
$_['text_hours']     = 'Phone Hours';
$_['text_hours_work']       = 'Working days: 09 - 17';
$_['text_hours_lunch']      = 'Closed for lunch: 12 - 13';
$_['text_hours_weekend']    = 'Weekends: <span>closed</span>';
$_['text_email']     = 'Mail us';
$_['text_email_enquiries']  = 'Enquiries:';
$_['text_email_support']    = 'Support:';
$_['text_email_orders']     = 'Orders:';
$_['text_email_economy']    = 'Economy:';
$_['text_email_market']     = 'Market:';
$_['text_comment']   = 'Comments';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';

// Entry
$_['entry_name']     = 'Your Name';
$_['entry_email']    = 'E-Mail Address';
$_['entry_enquiry']  = 'Enquiry';
$_['entry_captcha']  = 'Enter the code in the box below';

// Email
$_['email_subject']  = 'Enquiry %s';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';
$_['error_captcha']  = 'Verification code does not match the image!';
