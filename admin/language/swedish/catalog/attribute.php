<?php
// Heading
$_['heading_title']     = 'Specifikationer';

// Text
$_['text_success']           = 'Klart: Du har ändrat i modulen Specifikationer!';
$_['text_list']              = 'Alternativ grupplista';
$_['text_add']               = 'Lägg till Specifikation';
$_['text_edit']              = 'Ändra i Specifikation';

// Column
$_['column_name']            = 'Namn';
$_['column_attribute_group'] = 'Specifikationsgrupp';
$_['column_sort_order']      = 'Sorteringsordning';
$_['column_action']          = 'Ändra';

// Entry
$_['entry_name']            = 'Namn:';
$_['entry_attribute_group'] = 'Specifikationsgrupp:';
$_['entry_sort_order']      = 'Sorteringsordning:';

// Error
$_['error_permission']      = 'Varning: Du har inte behörighet att ändra!';
$_['error_name']            = 'Attributnamnet måste innehålla mellan 3 och 64 tecken!';
$_['error_product']         = 'Varning: Denna specifikation kan inte raderas eftersom det är kopplat till %s produkter!';
?>