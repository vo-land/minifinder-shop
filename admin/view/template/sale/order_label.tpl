<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $language; ?>" xml:lang="<?php echo $language; ?>">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<style type="text/css">
		div.print {
			width:110mm;height:150mm;border:0px solid black;font-family:Verdana,Arial,sans-serif;font-size:12px;
		}
		p { margin:0;padding:0 0 0.5em; }
		table.etikett { width:108mm;border:1px solid #666; padding:1mm; margin-top:7mm; border-spacing:0; }
		table.etikett tr td { vertical-align:top; padding:7px; }
		
		table.etikett tr.row1 td { font-size:13px; color:#333; }
		
		table.etikett tr.row1 td.col1 {}
		
		table.etikett tr.row2 td { font-size:18px; }
		table.etikett tr.row2 td.col1 { border-top:1px solid #666; border-bottom:1px solid #666; border-left:1px solid #666; padding-top:20px; }
		table.etikett tr.row2 td.col2 { border-top:1px solid #666; border-bottom:1px solid #666; border-right:1px solid #666; padding-top:20px; padding-bottom:20px;  }
		
		table.etikett td.col1 { width:10mm; vertical-align:top;}
		table.etikett td.line { height:10mm; }
		table.etikett td.line div { height:1px; border-top:1px solid #666; margin-top:5mm }
		
		span.city { text-transform:uppercase; }
	</style>
</head>
<body style="height:180mm">
<?php foreach ($orders as $order) { ?>
  
  <div class="print">
	<table class="etikett">
		<tr class="row1">
			<td class="col1">Från:</td>
			<td class="col2">
				<p>GPSER Sweden AB</p>
				<p>Honnörsgatan 6</p>
				<p>352 36 VÄXJÖ</p>
			</td>
		</tr>
		
		
		<tr class="row2">
			<td class="col1">Till:</td>
			<td class="col2">
				<?php 
				if ( !empty ( $order['shipping_company'] ) ) {
				?>
				<p><?php echo $order['shipping_company']; ?></p>
				<?php
				} 
				?>
				<p><?php echo $order['shipping_firstname']; ?> <?php echo $order['shipping_lastname']; ?></p>
				<p><?php echo $order['shipping_address_1']; ?></p>
				<?php 
				if ( !empty ( $order['shipping_address_2'] ) ) {
				?>
				<p><?php echo $order['shipping_address_2']; ?></p>
				<?php
				} 
				?> 
				<p><?php echo $order['shipping_postcode']; ?> <span class="city"><?php echo $order['shipping_city']; ?></span></p>
			</td>
		</tr>
		
	</table>
	</div>	
  
 
 
 
<?php } ?>

<script type="text/javascript">
	window.print();
</script>
</body>
</html>