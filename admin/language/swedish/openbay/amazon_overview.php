<?php

$_['lang_title']                    = 'OpenBay Pro for Amazon US';
$_['lang_heading']                  = 'Amazon Översikt';
$_['lang_overview']                 = 'Amazon Översikt';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_heading_settings']         = 'Settings';
$_['lang_heading_account']          = 'Mitt Konto';
$_['lang_heading_links']            = 'Produktlänkar';
$_['lang_heading_bulk_listing']     = 'Bulk Lista';
$_['lang_heading_register']         = 'Registrera';
$_['lang_heading_stock_updates']    = 'Produktuppdatering';
$_['lang_heading_saved_listings']   = 'Sparad Lista';
$_['lang_heading_bulk_linking']     = 'Bulk Länka';
?>