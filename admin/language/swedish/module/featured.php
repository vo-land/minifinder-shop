<?php
// Heading
$_['heading_title']       = 'Utvalda';

// Text
$_['text_module']         = 'Moduler';
$_['text_success']        = 'Klart: Du har ändrat i Utvalda!';
$_['text_edit']           = 'Ändra i Utvalda.';

// Entry
$_['entry_name']          = 'Modul Namn';
$_['entry_product']       = 'Produkter:';
$_['entry_limit']         = 'Max antal:';
$_['entry_width']         = 'Bredd';
$_['entry_height']        = 'Höjd';
$_['entry_status']        = 'Status:';

// Help
$_['help_product']     = '(Autocomplete)';

// Error 
$_['error_permission']    = 'Varning: Du har itne behörighet att ändra i Utvalda!';
$_['error_name']       = 'Modul Namn måste vara mellan 3-64 tecken.!';
$_['error_width']      = 'Bredd krävs!';
$_['error_height']     = 'Höjd krävs!';
?>