<?php

$_['heading_title'] = 'OpenBay Pro Amazon | Bulk Lista';

$_['text_openbay']      = 'OpenBay Pro';
$_['text_overview']     = 'Amazon Översikt';
$_['text_bulk_listing'] = 'Bulk Lista';
$_['text_searching']    = 'Sök';
$_['text_finished']     = 'Klart';
$_['text_marketplace']  = 'Marknadsplats';
$_['text_de'] = 'Tyskland';
$_['text_fr'] = 'Frankrike';
$_['text_es'] = 'Spanien';
$_['text_it'] = 'Italien';
$_['text_uk'] = 'England';
$_['text_filter_results'] = 'Filter results';
$_['text_dont_list']      = 'Visa inte listan';
$_['text_listing_values'] = 'Listans värde';
$_['text_condition']      = 'Villkor';
$_['text_condition_note'] = 'Villkor notering';
$_['text_start_selling']  = 'Starta försäljning';
$_['text_new']            = 'Ny';
$_['text_used_like_new']   = 'Använd - gillar ny';
$_['text_used_very_good']  = 'Använd - mycket bra';
$_['text_used_good']       = 'Använd - bra';
$_['text_used_acceptable'] = 'Använd - okej';
$_['text_collectible_like_new']   = 'Fått - gillar ny';
$_['text_collectible_very_good']  = 'Fått - mycket bra';
$_['text_collectible_good']       = 'Fått - bra';
$_['text_collectible_acceptable'] = 'Fått - okej';
$_['text_refurbished']            = 'Renoverad';

$_['column_name']    = 'Namn';
$_['column_image']   = 'Bild';
$_['column_model']   = 'Modell';
$_['column_status']  = 'Status';
$_['column_matches'] = 'Matchar';
$_['column_result']  = 'Resultat';

$_['button_return'] = 'Returnera';
$_['button_list']   = 'Lista';
$_['button_search'] = 'Sök';

$_['error_product_sku']                   = 'Produkten måste ha ett id SKU';
$_['error_product_no_searchable_fields']  = 'Produkten måste ha ISBN, EAN, UPC eller JAN fält';
$_['error_bulk_listing_not_allowed']      = 'Bulk lista är inte tillåten. ditt konto måste vara Medium nivå.';

?>