<?php
// Text
$_['text_success']     = 'Klart: Du har ändrat i varukorgen!';

// Error
$_['error_permission'] = 'Varning: Du har inte tillåtelse att ändra i API!!';
$_['error_stock']      = 'Produkter med *** är inte tillgängliga i lager just nu!';
$_['error_minimum']    = 'Minimum ordervärde för %s är %s!';
$_['error_store']      = 'Produkter kan inte köpas från butiken!';
$_['error_required']   = '%s krävs!';
?>