<?php
// Heading
$_['heading_title']      = 'Extended Banners';

// Text
$_['text_success']       = 'Success: You have modified banners!';
$_['text_list']          = 'Banner List';
$_['text_add']           = 'Add Banner';
$_['text_edit']          = 'Edit Banner';
$_['text_default']       = 'Default';
$_['text_left']             = 'Left: <span class="left_symbols error">%s</span>';
$_['text_max_length']       = 'Max length %s symbols';

// Column
$_['column_name']        = 'Banner Name';
$_['column_status']      = 'Status';
$_['column_action']      = 'Action';

// Entry
$_['entry_name']         = 'Banner Name';
$_['entry_title']        = 'Title';
$_['entry_button_ena']   = 'Button Enabled';
$_['entry_link']         = 'Link';
$_['entry_button_title'] = 'Button Title';
$_['entry_button_link']  = 'Button Link';
$_['entry_description']  = 'Description:';
$_['entry_description_ena'] = 'Description Enabled';
$_['entry_image']        = 'Image';
$_['entry_status']       = 'Status';
$_['entry_sort_order']   = 'Sort Order';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify banners!';
$_['error_name']         = 'Banner Name must be between 3 and 64 characters!';
$_['error_title']        = 'Banner Title must be between 2 and 64 characters!';
$_['error_button_title'] = 'Banner Button Title must be between 2 and 64 characters!';
$_['error_button_link']  = 'Banner Button Link should not be empty!';
$_['error_description']  = 'Banner Description must be between %s and %s characters!';