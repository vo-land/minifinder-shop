<?php
// Heading
$_['heading_title']      = 'Dibs (Q)';

// Text 
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified the account details!';
$_['text_development']   = '<span style="color: green;">Ready</span>';

// Entry
$_['entry_status']       = 'Status:';
$_['entry_geo_zone']     = 'Geo Zone:';
$_['entry_order_status'] = 'Final Order Status:';
$_['entry_mid']          = 'Merchant ID:';
$_['entry_key']          = 'MD5Key1:<br/><span class="help">Leave empty to not use MD5Key mode. Be sure that you set your Dibs account setting for this as well.</span>';
$_['entry_key2']         = 'MD5Key2:<br/><span class="help">Leave empty to not use MD5Key mode. Be sure that you set your Dibs account setting for this as well.</span>';
$_['entry_sort_order']   = 'Sort Order:';
$_['entry_ajax']   		 = 'Ajax Pre-Confirm';
$_['entry_test']   		 = 'Test Mode';
$_['entry_debug']   	 = 'Debug Mode';

// Help
$_['help_ajax']          = 'Ajax Pre-Confirm will set the order to "pending" before sending to the gateway. This is only needed if there are server communication problems with the gateway and orders are being lost. By pre-confirming the order, the order starts in a pending state, then upon gateway return, the order is updated. (Recommended: DISABLED)';
$_['help_mid']           = '';
$_['help_pass']          = '';
$_['help_key']           = '';
$_['help_debug']         = 'Debug mode is used when there are payment or order update issues to help track down where the problem is. This usually includes saving logs or sending emails to the store email with extra information. (Recommended: DISABLED)';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify this payment module!';
$_['error_mid']          = 'Field Required!'; 
$_['error_key']          = 'Field Required!'; 
$_['error_key2']         = 'Field Required!'; 
?>