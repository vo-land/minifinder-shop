<?php
// Heading
$_['heading_title']    = 'Google Sitemap';

// Text 
$_['text_feed']        = 'Produktfeed';
$_['text_success']     = 'Klart: Du har ändrat i Google Sitemap feed!';
$_['text_list']        = 'Layout Lista';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_data_feed']  = 'Datafeed-Url:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i Google Sitemap feed!';
?>