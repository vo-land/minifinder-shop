<?php
// Heading
$_['heading_title']        = 'Beräkna frakt &amp; avgifter';

// Text
$_['text_success']         = 'Klart: Din fraktrutt har blivit registrerad!';
$_['text_shipping']        = 'Ange din destination för att få ett pris.';
$_['text_shipping_method'] = 'Ange fraksätt som du önskar använda.';

// Entry
$_['entry_country']        = 'Land';
$_['entry_zone']           = 'Län';
$_['entry_postcode']       = 'Postnummer';

// Error
$_['error_postcode']       = 'Postnummer måste vara mellan 2-10 tecken!';
$_['error_country']        = 'Välj ett land!';
$_['error_zone']           = 'Välj ett län!';
$_['error_shipping']       = 'Varning: Fraktsätt krävs!';
$_['error_no_shipping']    = 'Varning: Inget fraktalternativ finns. Kontakta oss för en offert!';
?>