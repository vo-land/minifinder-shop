<?php
// Heading
$_['heading_title']    = 'Returer';

// Text
$_['text_success']     = 'Klart: Du har ändrat i returer!';
$_['text_list']        = 'Layout Lista';
$_['text_add']         = 'Lägg till Returer';
$_['text_edit']        = 'Ändra i Returer';

// Column
$_['column_name']      = 'Returer';
$_['column_action']    = 'Ändra';

// Entry
$_['entry_name']       = 'Returer:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i Returer!';
$_['error_name']       = 'Namnet måste vara mellan 3 och 32 tecken!';
$_['error_return']     = 'Varning: Denna kan inte raderas eftersom den är kopplad till %s returnerade produkter!';
?>