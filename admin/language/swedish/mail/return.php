<?php
// Text
$_['text_subject']       = '%s - Returärende, uppdatering %s';
$_['text_return_id']     = 'Retur-ID:';
$_['text_date_added']    = 'Returdatum:';
$_['text_return_status'] = 'Ditt returärende har uppdaterats till följande status:';
$_['text_comment']       = 'Kommentarer till ditt returärende:';
$_['text_footer']        = 'Svara på detta meddelande om du har några frågor.';
?>