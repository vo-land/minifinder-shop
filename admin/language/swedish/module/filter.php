<?php
// Heading
$_['heading_title']       = 'Filter'; 

// Text
$_['text_module']         = 'Modul';
$_['text_success']        = 'Klart: Du har ändrat i Filter!';
$_['text_edit']           = 'Ändra i Filter Modulen';

// Entry
$_['entry_status']        = 'Status:';

// Error
$_['error_permission']    = 'Varning: Du har inte behörighet att ändra i modulen filter!';
?>