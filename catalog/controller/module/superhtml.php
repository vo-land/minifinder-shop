<?php  
class ControllerModuleSuperhtml extends Controller {
	public function index($setting) {
		static $module = 0;
	
		if(isset($setting['group'][$this->config->get('config_language_id')])) {
			$groupid = $setting['group'][$this->config->get('config_language_id')];
		} else {
			$groupid = false;
		}
		$this->document->addStyle('catalog/view/javascript/superhtml/superhtml.css');
		if ($setting['carousel']) {
		$this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');
		}
		$data['columns'] = array();
		$data['title'] = false;
		$data['nocol'] = $setting['cols'][$this->config->get('config_language_id')];
		$data['gtitle'] = $setting['gtitle'][$this->config->get('config_language_id')];
		$data['ctitle'] = $setting['ctitle'][$this->config->get('config_language_id')];
		$data['min_width'] = $setting['min_width'];
		$data['carousel'] = $setting['carousel'];
		$data['car_itm_desk'] = $setting['car_itm_desk'];
		$data['car_itm_tab'] = $setting['car_itm_tab'];
		$data['car_itm_mob'] = $setting['car_itm_mob'];
		$data['car_speed'] = $setting['car_speed'];
		$data['carousel_autoplay'] = $setting['carousel_autoplay'];
		//customization
		$data['sh_h1_font'] = $this->config->get('superhtml_sh_h1_font');
		if ($data['sh_h1_font']) {
			$data['sh_h1_fontl'] = str_replace('+', " ", $data['sh_h1_font']);
			$data['sh_h1_fontl'] = str_replace(':', "", $data['sh_h1_fontl']);
			$data['sh_h1_fontl'] = preg_replace('/\d{3}/', '', $data['sh_h1_fontl']);
		} else {
			$data['sh_h1_fontl'] = false;
		}
		$data['sh_h1_font_size'] = $this->config->get('superhtml_sh_h1_font_size');
		$data['sh_h1_color'] = $this->config->get('superhtml_sh_h1_color');
		$data['sh_h1_border_color'] = $this->config->get('superhtml_sh_h1_border_color');
		$data['sh_h1_bg_color'] = $this->config->get('superhtml_sh_h1_bg_color');
		$data['sh_h2_font'] = $this->config->get('superhtml_sh_h2_font');
		if ($data['sh_h2_font']) {
			$data['sh_h2_fontl'] = str_replace('+', " ", $data['sh_h2_font']);
			$data['sh_h2_fontl'] = str_replace(':', "", $data['sh_h2_fontl']);
			$data['sh_h2_fontl'] = preg_replace('/\d{3}/', '', $data['sh_h2_fontl']);
		} else {
			$data['sh_h2_fontl'] = false;
		}
		$data['sh_h2_font_size'] = $this->config->get('superhtml_sh_h2_font_size');
		$data['sh_h2_color'] = $this->config->get('superhtml_sh_h2_color');
		$data['content_h2_font'] = $this->config->get('superhtml_content_h2_font');
		if ($data['content_h2_font']) {
			$data['content_h2_fontl'] = str_replace('+', " ", $data['content_h2_font']);
			$data['content_h2_fontl'] = str_replace(':', "", $data['content_h2_fontl']);
			$data['content_h2_fontl'] = preg_replace('/\d{3}/', '', $data['content_h2_fontl']);
		} else {
			$data['content_h2_fontl'] = false;
		}
		$data['sh_content_font_size'] = $this->config->get('superhtml_sh_content_font_size');
		$data['sh_content_color'] = $this->config->get('superhtml_sh_content_color');
		
		if ($groupid || $groupid == 0) {
			$group_data = $this->config->get('superhtml_groups');
			$group_data = $group_data[$groupid];
			$data['title'] = $group_data['name'];
			$group_columns = $group_data['column'];
			foreach ($group_columns as $column) {
				$content = $this->config->get($column['id']);
				if ($content['youtube']) {
					$yt = (strpos($content['youtube'], 'http://') !== 0 && strpos($content['youtube'], '//') !==0) ? 'http://'.$content['youtube'] : $content['youtube'];
				} else {
					$yt = false;
				}
				if ($content['url']) {
					$url = (strpos($content['url'], 'http://') !== 0 && strpos($content['url'], '//') !==0) ? 'http://'.$content['url'] : $content['url'];
				} else {
					$url = false;
				}
				$data['columns'][] = array(
					'name'    => $content['name'],
					'url'     => $url,
					'youtube' => $yt,
					'image'   => $content['image'] ? 'image/'.$content['image'] : false,
					'text'    => html_entity_decode($content['description'], ENT_QUOTES, 'UTF-8')
				);
			}
		
		}
		$data['module'] = $module++; 

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/superhtml.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/superhtml.tpl', $data);
		}	 else {
				return $this->load->view('default/template/module/superhtml.tpl', $data);
		}
	}
}
?>