<?php
// Heading
$_['heading_title']     = 'Sök';
$_['heading_tag']		= 'Tag - ';
 
// Text
$_['text_search']       = 'Produkter motsvarande dina sökkriterier';
$_['text_keyword']      = 'Nyckelord';
$_['text_category']     = 'Alla kategorier';
$_['text_sub_category'] = 'Sök i underkategorier';
$_['text_empty']        = 'Ingen produkt matchar dina sökkriterier.';
$_['text_quantity']     = 'Antal:';
$_['text_manufacturer'] = 'Tillverkare:';
$_['text_model']        = 'Produktkod:'; 
$_['text_points']       = 'Bonuspoäng:'; 
$_['text_price']        = 'Pris:'; 
$_['text_tax']          = 'Exkl moms:'; 
$_['text_reviews']      = 'Baserad på %s recensioner.'; 
$_['text_compare']      = 'Produktjämförelse (%s)'; 
$_['text_sort']         = 'Sortera efter:';
$_['text_default']      = 'Standard';
$_['text_name_asc']     = 'Namn (A - Ö)';
$_['text_name_desc']    = 'Namn (Ö - A)';
$_['text_price_asc']    = 'Pris (Lågt &gt; Högt)';
$_['text_price_desc']   = 'Pris (Högt &gt; Lågt)';
$_['text_rating_asc']   = 'Betyg (Lägsta)';
$_['text_rating_desc']  = 'Betyg (Högsta)';
$_['text_model_asc']    = 'Artikelnummer (A - Ö, 1-99999)';
$_['text_model_desc']   = 'Artikelnummer (Ö - A, 99999-1)';
$_['text_limit']        = 'Visa:';

// Entry
$_['entry_search']      = 'Sök:';
$_['entry_description'] = 'Sök i produktbeskrivningar';
?>