<?php
// Text
$_['text_success']           = 'Du har nu ändrat i order';

// Error
$_['error_permission']       = 'Varning: Du har inte tillåtelse att ändra i API!!';
$_['error_customer']         = 'Kundinställningar måste anges!';
$_['error_payment_address']  = 'Betaladress krävs!';
$_['error_payment_method']   = 'Betalningsalternativ krävs!';
$_['error_shipping_address'] = 'Fraktadress krävs!';
$_['error_shipping_method']  = 'Fraktalternativ krävs!';
$_['error_stock']            = 'Produkter med *** är inte tillgängliga i lager just nu!';
$_['error_minimum']          = 'Minimum ordervärde för %s är %s!';
$_['error_not_found']        = 'Varning: Ordern kunde inte hittas.';
?>