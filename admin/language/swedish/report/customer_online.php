<?php
// Heading
$_['heading_title']     = 'Kundrapport Online';

// Text 
$_['text_list']         = 'Kunder Online Lista';
$_['text_guest']        = 'Gäst';
 
// Column
$_['column_ip']         = 'IP';
$_['column_customer']   = 'Kund';
$_['column_url']        = 'Sista sida visad';
$_['column_referer']    = 'Referenser';
$_['column_date_added'] = 'Senaste klick';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_ip']          = 'IP';
$_['entry_customer']    = 'Kunder';
?>