<?php
// Heading 
$_['heading_title']     = 'Min kontoinformation';

// Text
$_['text_account']      = 'Återförsäljarkonto';
$_['text_edit']         = 'Redigera information';
$_['text_your_details'] = 'Personuppgifter';
$_['text_your_address'] = 'Adressuppgifter';
$_['text_success']      = 'Klart: Ditt konto har uppdaterats.';

// Entry
$_['entry_firstname']   = 'Förnamn:';
$_['entry_lastname']    = 'Efternamn:';
$_['entry_email']       = 'E-post:';
$_['entry_telephone']   = 'Telefon:';
$_['entry_fax']         = 'Fax:';
$_['entry_company']     = 'Företag:';
$_['entry_website']     = 'Hemsida:';
$_['entry_address_1']   = 'Adress 1:';
$_['entry_address_2']   = 'Adress 2:';
$_['entry_postcode']    = 'Postnr:';
$_['entry_city']        = 'Stad:';
$_['entry_country']     = 'Land:';
$_['entry_zone']        = 'Län:';

// Error
$_['error_exists']      = 'Fel: E-postadressen är redan registrerad!';
$_['error_firstname']   = 'Förnamn måste bestå av minst 1 och högst 32 tecken!';
$_['error_lastname']    = 'Efternamn måste bestå av minst 1 och högst 32 tecken!';
$_['error_email']       = 'E-postadressen verkar inte vara giltig!';
$_['error_telephone']   = 'Telefonnummer måste bestå av minst 3 och högst 32 tecken!';
$_['error_address_1']   = 'Adress 1 måste bestå av minst 3 och högst 128 tecken!';
$_['error_city']        = 'Stad måste bestå av minst 2 och högst 128 tecken!';
$_['error_country']     = 'Var vänlig välj ett land!';
$_['error_zone']        = 'Var vänlig välj ett län!';
$_['error_postcode']    = 'Postnummer måste bestå av minst 2 och högst 10 tecken!';
?>