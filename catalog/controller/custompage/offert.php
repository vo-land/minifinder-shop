<?php  
class ControllerCustompageOffert extends Controller {
  
  private $error = array();	
	
  public function index() {
    
	$this->load->language('custompage/offert');

	$this->document->setTitle($this->language->get('heading_title'));
		
	$servtype = NULL;
		
	if (isset($this->request->post['servtype'][0])) {
		$servtype .= ' Körjournaler ';
	}	
	
	if (isset($this->request->post['servtype'][1])) {
		$servtype .= ' Fordonsspårning ';
	}
	
	if (isset($this->request->post['servtype'][2])) {
		$servtype .= ' Trygghetslarm ';
	} 
	
	if (isset($this->request->post['servtype'][3])) {
		$servtype .= ' Stöldskydd ';
	}
			
    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_host');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');			
			
			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->request->post['email']);
			$mail->setSender($this->request->post['name']);
			$mail->setSubject(sprintf($this->language->get('email_subject'), $this->request->post['name']));
			$mail_text = 'Org.nr: '.$this->request->post['vat']."\n";
			$mail_text .= 'Företag: '.$this->request->post['name']."\n";
			$mail_text .= 'Adress: '.$this->request->post['address']."\n";
			$mail_text .= 'Postnummer: '.$this->request->post['zip']."\n";
			$mail_text .= 'Stad: '.$this->request->post['city']."\n";
			$mail_text .= 'Telefon: '.$this->request->post['phone']."\n";
			$mail_text .= 'E-post: '.$this->request->post['email']."\n";
			$mail_text .= 'Produkter: '.$servtype."\n";
			$mail_text .= 'Antal enheter: '.$this->request->post['devicenum']."\n";
			$mail_text .= 'Meddelande: '.$this->request->post['enquiry']."\n \n";
			$mail_text .= 'IP: '.$_SERVER['REMOTE_ADDR']."\n";
			$mail_text .= 'Referer: '.$_SERVER['HTTP_REFERER']."\n";
			$mail_text .= 'User agent: '.$_SERVER['HTTP_USER_AGENT']."\n";
			$mail->setText($mail_text);
			$mail->send();

			$this->response->redirect($this->url->link('custompage/offert/success'));
	}
    
	// Errors
	if (isset($this->error['vat'])) {
		$data['error_vat'] = $this->error['vat'];
	} else {
		$data['error_vat'] = '';
	}

	if (isset($this->error['name'])) {
		$data['error_name'] = $this->error['name'];
	} else {
		$data['error_name'] = '';
	}
	
	if (isset($this->error['address'])) {
		$data['error_address'] = $this->error['address'];
	} else {
		$data['error_address'] = '';
	}
	
	if (isset($this->error['zip'])) {
		$data['error_zip'] = $this->error['zip'];
	} else {
		$data['error_zip'] = '';
	}
	
	if (isset($this->error['city'])) {
		$data['error_city'] = $this->error['city'];
	} else {
		$data['error_city'] = '';
	}
	
	if (isset($this->error['phone'])) {
		$data['error_phone'] = $this->error['phone'];
	} else {
		$data['error_phone'] = '';
	}	

	if (isset($this->error['email'])) {
		$data['error_email'] = $this->error['email'];
	} else {
		$data['error_email'] = '';
	}
	
	if (isset($this->error['servtype'])) {
		$data['error_servtype'] = $this->error['servtype'];
	} else {
		$data['error_servtype'] = '';
	}
	
	if (isset($this->error['devicenum'])) {
		$data['error_devicenum'] = $this->error['devicenum'];
	} else {
		$data['error_devicenum'] = '';
	}

	if (isset($this->error['enquiry'])) {
		$data['error_enquiry'] = $this->error['enquiry'];
	} else {
		$data['error_enquiry'] = '';
	}

	if (isset($this->error['captcha'])) {
		$data['error_captcha'] = $this->error['captcha'];
	} else {
		$data['error_captcha'] = '';
	}
	
	 // define children templates
	$data['column_left'] = $this->load->controller('common/column_left');
	$data['column_right'] = $this->load->controller('common/column_right');
	$data['content_top'] = $this->load->controller('common/content_top');
	$data['content_bottom'] = $this->load->controller('common/content_bottom');
	$data['footer'] = $this->load->controller('common/footer');
	$data['header'] = $this->load->controller('common/header');
	
	// Breadcrumbs
	$data['breadcrumbs'] = array();

	$data['breadcrumbs'][] = array(
		'text' => $this->language->get('text_home'),
		'href' => $this->url->link('common/home')
	);

	$data['breadcrumbs'][] = array(
		'text' => $this->language->get('breadcrumb_title'),
		'href' => $this->url->link('custompage/offert')
	);
	
	$data['heading_title'] = $this->language->get('heading_title');

    // set data to the variable
    $data['custom_offert_text'] = "This is my custom OFFERT page.";
	
	// Catch post variables
	if (isset($this->request->post['vat'])) {
		$data['vat'] = $this->request->post['vat'];
	} else {
		$data['vat'] = '';
	}

	if (isset($this->request->post['name'])) {
		$data['name'] = $this->request->post['name'];
	} else {
		$data['name'] = '';
	}
	
	if (isset($this->request->post['address'])) {
		$data['address'] = $this->request->post['address'];
	} else {
		$data['address'] = '';
	}
	
	if (isset($this->request->post['zip'])) {
		$data['zip'] = $this->request->post['zip'];
	} else {
		$data['zip'] = '';
	}

	if (isset($this->request->post['city'])) {
		$data['city'] = $this->request->post['city'];
	} else {
		$data['city'] = '';
	}

	if (isset($this->request->post['phone'])) {
		$data['phone'] = $this->request->post['phone'];
	} else {
		$data['phone'] = '';
	}

	if (isset($this->request->post['email'])) {
		$data['email'] = $this->request->post['email'];
	} else {
		$data['email'] = '';
	}
	
	if (isset($this->request->post['servtype'])) {
		$data['servtype'] = $this->request->post['servtype'];
	} else {
		$data['servtype'] = '';
	}
	
	if (isset($this->request->post['devicenum'])) {
		$data['devicenum'] = $this->request->post['devicenum'];
	} else {
		$data['devicenum'] = '';
	}
	
	if (isset($this->request->post['enquiry'])) {
		$data['enquiry'] = $this->request->post['enquiry'];
	} else {
		$data['enquiry'] = '';
	}

	if ($this->config->get('config_google_captcha_status')) {
		$this->document->addScript('https://www.google.com/recaptcha/api.js');

		$data['site_key'] = $this->config->get('config_google_captcha_public');
	} else {
		$data['site_key'] = '';
	}
	
   	// define template file
	if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/custompage/offert.tpl')) {
		$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/custompage/offert.tpl', $data));
	} else {
		$this->response->setOutput($this->load->view('default/template/custompage/offert.tpl', $data));
	}
     
  }

	public function success() {
		$this->load->language('custompage/offert');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('breadcrumb_title'),
			'href' => $this->url->link('custompage/offert')
		);

		$data['heading_title'] 		= $this->language->get('heading_title');
		$data['text_message'] 		= $this->language->get('text_success');
		$data['button_continue'] 	= $this->language->get('button_continue');
		$data['continue'] 			= $this->url->link('common/home');
		$data['column_left'] 		= $this->load->controller('common/column_left');
		$data['column_right'] 		= $this->load->controller('common/column_right');
		$data['content_top'] 		= $this->load->controller('common/content_top');
		$data['content_bottom'] 	= $this->load->controller('common/content_bottom');
		$data['footer'] 			= $this->load->controller('common/footer');
		$data['header'] 			= $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/custompage/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/custompage/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/custompage/success.tpl', $data));
		}
	}

	protected function validate() {
		
		if ((utf8_strlen($this->request->post['vat']) < 9) || (utf8_strlen($this->request->post['vat']) > 20)) {
			$this->error['vat'] = $this->language->get('error_vat');
		}	
			
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if ((utf8_strlen($this->request->post['address']) < 3) || (utf8_strlen($this->request->post['address']) > 40)) {
			$this->error['address'] = $this->language->get('error_address');
		}
		
		if ((utf8_strlen($this->request->post['zip']) < 3) || (utf8_strlen($this->request->post['zip']) > 10)) {
			$this->error['zip'] = $this->language->get('error_zip');
		}
		
		if ((utf8_strlen($this->request->post['city']) < 2) || (utf8_strlen($this->request->post['city']) > 30)) {
			$this->error['city'] = $this->language->get('error_city');
		}

		if ((utf8_strlen($this->request->post['phone']) < 6) || (utf8_strlen($this->request->post['phone']) > 17)) {
			$this->error['phone'] = $this->language->get('error_phone');
		}

		if (!preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}
		
		if ((utf8_strlen($this->request->post['devicenum']) < 1)) {
			$this->error['devicenum'] = $this->language->get('error_devicenum');
		}

		/*if ((utf8_strlen($this->request->post['enquiry']) < 10) || (utf8_strlen($this->request->post['enquiry']) > 3000)) {
			$this->error['enquiry'] = $this->language->get('error_enquiry');
		}

		if ($this->config->get('config_google_captcha_status')) {
			$recaptcha = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($this->config->get('config_google_captcha_secret')) . '&response=' . $this->request->post['g-recaptcha-response'] . '&remoteip=' . $this->request->server['REMOTE_ADDR']);

			$recaptcha = json_decode($recaptcha, true);

			if (!$recaptcha['success']) {
				$this->error['captcha'] = $this->language->get('error_captcha');
			}
		}*/

		return !$this->error;
	}

}
?>