<?php
// Heading 
$_['heading_title']     = 'Adressbok';

// Text
$_['text_account']      = 'Konto';
$_['text_address_book'] = 'Adresser';
$_['text_edit_address'] = 'Redigera adress';
$_['text_add']             = 'Din adress är tillagd.';
$_['text_edit']            = 'Din adress är ändrad';
$_['text_delete']          = 'Din adress är raderad';
$_['text_empty']           = 'Du har ingen adress i ditt konto.';

// Entry
$_['entry_firstname']   = 'Förnamn:';
$_['entry_lastname']    = 'Efternamn:';
$_['entry_company']     = 'Företag:';
$_['entry_address_1']   = 'Adress 1:';
$_['entry_address_2']   = 'Adress 2:';
$_['entry_postcode']    = 'Postnr:';
$_['entry_city']        = 'Stad:';
$_['entry_country']     = 'Land:';
$_['entry_zone']        = 'Län:';
$_['entry_default']     = 'Standardadress:';

// Error
$_['error_delete']      = 'Fel: Du måste ange minst en adress!';
$_['error_default']     = 'Fel: Du kan inte radera din standardadress!';
$_['error_firstname']   = 'Förnamn måste bestå av minst 1 och högst 32 tecken!';
$_['error_lastname']    = 'Efternamn måste bestå av minst 1 och högst 32 tecken!';
$_['error_vat']         = 'Momsnummer stämmer inte!';
$_['error_address_1']   = 'Adressen måste bestå av minst 3 och högst 128 tecken!';
$_['error_postcode']    = 'Förnamn måste bestå av minst 2 och högst 10 tecken!';
$_['error_city']        = 'Stad måste bestå av minst 2 och högst 128 tecken!';
$_['error_country']     = 'Var vänlig välj ett land!';
$_['error_zone']        = 'Var vänlig välj ett län!';
$_['error_custom_field']   = '%s krävs!';
?>