<?php
// Heading
$_['heading_title']     = 'Rapport Återförsäljare Aktivitet';

// Text
$_['text_list']         = 'Återförsäljare Aktivitet Lista';
$_['text_edit']         = '<a href="Återförsäljare=%d">%s</a> uppdaterade sina inställningar.';
$_['text_forgotten']     = '<a href="Återförsäljare=%d">%s</a> krävde ett nytt password.';
$_['text_login']        = '<a href="Återförsäljare=%d">%s</a> loggade in.';
$_['text_password']     = '<a href="Återförsäljare=%d">%s</a> uppdaterade sitt password.';
$_['text_payment']      = '<a href="Återförsäljare=%d">%s</a> uppdaterade sina betalningsinställningar.';
$_['text_register']     = '<a href="Återförsäljare=%d">%s</a> registrerad för ett nytt konto.';

// Column
$_['column_affiliate']  = 'Återförsäljare';
$_['column_comment']    = 'Kommentar';
$_['column_ip']         = 'IP';
$_['column_date_added'] = 'Datum';

// Entry
$_['entry_affiliate']   = 'Återförsäljare';
$_['entry_ip']          = 'IP';
$_['entry_date_start']      = 'Startdatum:';
$_['entry_date_end']        = 'Slutdatum:';
?>