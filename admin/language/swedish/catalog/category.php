<?php
// Heading
$_['heading_title']          = 'Kategorier';

// Text
$_['text_success']           = 'Klart: Du har ändrat i kategorier!';
$_['text_list']              = 'Alternativ grupplista';
$_['text_add']               = 'Lägg till Kategori';
$_['text_edit']              = 'Ändra i Kategori';
$_['text_default']           = 'Förvalt';

// Column
$_['column_name']            = 'Kategorinamn';
$_['column_sort_order']      = 'Sorteringsordning';
$_['column_action']          = 'Ändra';

// Entry
$_['entry_name']             = 'Kategorinamn:';
$_['entry_description']      = 'Beskrivning:';
$_['entry_meta_title'] 	     = 'Meta Tag Namn';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords:';
$_['entry_meta_description'] = 'Meta Tag Beskrivning:';
$_['entry_keyword']          = 'SEO Keyword';
$_['entry_parent']           = 'Överordnad kategori:';
$_['entry_filter']           = 'Filters:';
$_['entry_store']            = 'Butik:';
$_['entry_image']            = 'Bild:';
$_['entry_top']              = 'Top-meny:';
$_['entry_column']           = 'Kolumner:';
$_['entry_sort_order']       = 'Sorteringsordning:';
$_['entry_status']           = 'Status:';
$_['entry_layout']           = 'Layout Override:';

// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'Använda inte mellanslag. Använd - tecken mellan orden.';
$_['help_top']               = 'Visas i toppmenyn. Fungerar endast i toppmenyn.';
$_['help_column']            = 'Antal kolumner för att använda i de 3 kategorierna. Fungerat endast för toppkategorier.';

// Error 
$_['error_warning']          = 'Varning: Kontollera och fyll i obligatoriska uppgifter!';
$_['error_permission']       = 'Varning: Du har inte ehörighet att ändra i kategorier!';
$_['error_name']             = 'Kategorinamnet måste vara mellan 2 och 32 tecken!';
$_['error_meta_title']       = 'Meta Tag Namn måste vara mer än 3 tecken men mindre än 255 tecken.';
$_['error_keyword']          = 'SEO keyword används redan!';
?>