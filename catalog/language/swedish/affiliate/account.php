<?php
// Heading 
$_['heading_title']        = 'Mitt affiliaterekonto';

// Text
$_['text_account']         = 'Affiliaterekonto';
$_['text_my_account']      = 'Mitt Återförsäljarekonto';
$_['text_my_tracking']     = 'Mina spårningslänkar';
$_['text_my_transactions'] = 'Mina transaktioner';
$_['text_edit']            = 'Redigera kontoinformation';
$_['text_password']        = 'Ändra lösenord';
$_['text_payment']         = 'Ändra utbetalningsuppgifter';
$_['text_tracking']        = 'Skapa personlig spårningslänk';
$_['text_transaction']     = 'Visa transaktionshistorik';
?>