<?php
// Heading
$_['heading_title']  = 'Kontakta oss';

// Text 
$_['text_location']  = 'Kontaktuppgifter';
$_['text_store']     = 'Våra butiker';
$_['text_contact']   = 'Kontaktformulär';
$_['text_address']   = 'Adress:';
$_['text_tax']       = 'Innehar F-skatt';
$_['text_telephone'] = 'Telefon:';
$_['text_fax']       = 'Fax:';
$_['text_open']      = 'Butikens öppettider';
$_['text_hours']     = 'Telefontider';
$_['text_hours_work']       = 'Arbetsdagar: 09 - 17';
$_['text_hours_lunch']      = 'Lunchstängt: 12 - 13';
$_['text_hours_weekend']    = 'Helger: <span>stängd</span>';
$_['text_email']     = 'Maila Oss';
$_['text_email_enquiries']  = 'Förfrågningar:';
$_['text_email_support']    = 'Stöd:';
$_['text_email_orders']     = 'Order:';
$_['text_email_economy']    = 'Ekonomi:';
$_['text_email_market']     = 'Marknadsföra:';
$_['text_comment']   = 'Kommentar';
$_['text_success']   = '<p>Ditt meddelande har nu skickats till oss!<br />Vi besvarar din fråga så snart vi kan.</p>';

// Entry Fields
$_['entry_name']     = 'Ditt namn';
$_['entry_email']    = 'E-postadress';
$_['entry_enquiry']  = 'Meddelande';
$_['entry_captcha']  = 'Ange koden i rutan nedan:';

// Email
$_['email_subject']  = 'Meddelande: %s';

// Errors
$_['error_name']     = 'Namnet måste bestå av minst 3 och högst 32 tecken!';
$_['error_email']    = 'E-postadress verkar inte vara giltig!';
$_['error_enquiry']  = 'Förfrågan måste bestå av minst 10 och högst 3000 tecken!';
$_['error_captcha']  = 'Verifikationskoden matchar inte bilden!';
?>
