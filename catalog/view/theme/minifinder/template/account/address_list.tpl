<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php $last_breadcrumb = array_pop($breadcrumbs); ?>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
    <li><?php echo $last_breadcrumb['text']; ?></li>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $text_address_book; ?></h1>
      <?php if ($addresses) { ?>
      <div class="address_list">
        <?php foreach ($addresses as $result) { ?>
        <div class="address_item">
          <div class="address_info"><?php echo $result['address']; ?></div>
          <div class="address_btn">
            <a href="<?php echo $result['update']; ?>" class="black_white_btn">
              <span><?php echo $button_edit; ?></span>
              <span><?php echo $button_edit; ?></span>
            </a> &nbsp;
            <a href="<?php echo $result['delete']; ?>" class="red_white_btn">
              <span><?php echo $button_delete; ?></span>
              <span><?php echo $button_delete; ?></span>
            </a>
          </div>
        </div>
        <?php } ?>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <!--<div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>-->
        <div class="pull-left">
          <a href="<?php echo $add; ?>" class="red_button">
            <span><?php echo $button_new_address; ?></span>
            <span><?php echo $button_new_address; ?></span>
          </a>
        </div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>