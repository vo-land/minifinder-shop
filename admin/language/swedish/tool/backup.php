<?php
// Heading
$_['heading_title']    = 'Backup / Återställning';

// Text
$_['text_backup']      = 'Download Backup';
$_['text_success']     = 'Klart: Du har importerat din databas!';
$_['text_list']        = 'Backup Lista';

// Entry
$_['entry_restore']    = 'Återställ från säkerhetskopia:';
$_['entry_backup']     = 'Säkerhetskopiera:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra!';
$_['error_backup']     = 'Varning: Du måste välja ett alternativ att backupa!';
$_['error_empty']      = 'Varning: Filen du laddade upp var tom!';
?>