<?php
// Heading
$_['heading_title']                = 'Senaste Akrivitet';

// Text
$_['text_customer_address_add']    = '<a href="customer_id=%d">%s</a> lagt till ny adress.';
$_['text_customer_address_edit']   = '<a href="customer_id=%d">%s</a> uppdaterat deras adress.';
$_['text_customer_address_delete'] = '<a href="customer_id=%d">%s</a> raderat en adress.';
$_['text_customer_edit']           = '<a href="customer_id=%d">%s</a> uppdaterat deras kontoinställningar.';
$_['text_customer_forgotten']      = '<a href="customer_id=%d">%s</a> har begärt nytt password.';
$_['text_customer_login']          = '<a href="customer_id=%d">%s</a> logga in.';
$_['text_customer_password']       = '<a href="customer_id=%d">%s</a> uppdaterat deras password.';
$_['text_customer_register']       = '<a href="customer_id=%d">%s</a> skapat nytt konto.';
$_['text_customer_return_account'] = '<a href="customer_id=%d">%s</a> ansökt om en retur.';
$_['text_customer_return_guest']   = '%s ansökt om en produkt retur.';
$_['text_customer_order_account']  = '<a href="customer_id=%d">%s</a> datum <a href="order_id=%d">ny order</a>.';
$_['text_customer_order_guest']    = '%s created a <a href="order_id=%d">ny order</a>.';
$_['text_affiliate_edit']          = '<a href="affiliate_id=%d">%s</a> uppdaterat deras kontoinställningar.';
$_['text_affiliate_forgotten']     = '<a href="affiliate_id=%d">%s</a> begärt nytt password.';
$_['text_affiliate_login']         = '<a href="affiliate_id=%d">%s</a> logga in.';
$_['text_affiliate_password']      = '<a href="affiliate_id=%d">%s</a> uppdaterat deras password.';
$_['text_affiliate_payment']       = '<a href="affiliate_id=%d">%s</a> uppdaterat deras betalningsalternativ.';
$_['text_affiliate_register']      = '<a href="affiliate_id=%d">%s</a> akapat nytt konto.';
?>