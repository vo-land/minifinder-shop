<?php
// Heading
$_['heading_title']        = 'Tilläggsmoduler Installation';

// Text
$_['text_success']         = 'Klart: Du har nu ändrat i Tilläggsmoduler!';
$_['text_unzip']           = 'Packar upp filerna!';
$_['text_ftp']             = 'Kopierar filer!';
$_['text_sql']             = 'Kör SQL!';
$_['text_xml']             = 'Lägger till modificationer!';
$_['text_php']             = 'Kör PHP!';
$_['text_remove']          = 'Tar bort tillfälliga filer!';
$_['text_clear']           = 'Klart: Tillfälliga filer är borttagna!';

// Entry
$_['entry_upload']         = 'Ladda upp filer';
$_['entry_overwrite']      = 'Filer som kommer skrivas över';
$_['entry_progress']       = 'Progress';

// Help
$_['help_upload']          = 'Kräver en Zip-fil eller XML modifikation fil.';

// Error
$_['error_permission']     = 'Varning: Du saknar tillåtelse att ändra i Tilläggsmoduler!';
$_['error_temporary']      = 'Varning: Där är tillfälliga filer som du måste radera manuellt.';
$_['error_upload']         = 'Filen kunde inte laddas upp!';
$_['error_filetype']       = 'Ej godkänd fil eller typ.!';
$_['error_file']           = 'Filen kunde inte hittas!';
$_['error_unzip']          = 'Zip filen kunde inte öppnas!';
$_['error_code']           = 'Modifikation kräver en unik ID kod!';
$_['error_exists']         = 'Modifikation %s is använder redan den ID kod du försöker ladda upp.';
$_['error_directory']      = 'Mappen saknar filer.';
$_['error_ftp_status']     = 'FTP måste vara aktiverad i dina inställningar.';
$_['error_ftp_connection'] = 'Kunde inte ansluta till %s:%s';
$_['error_ftp_login']      = 'Kunde inte logga in som %s';
$_['error_ftp_root']       = 'Kunde inte skapa huvudmappen som %s';
$_['error_ftp_directory']  = 'Kunde inte ändra till mappen %s';
$_['error_ftp_file']       = 'Kunde inte ladda upp fil %s';
?>