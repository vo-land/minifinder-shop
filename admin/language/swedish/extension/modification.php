<?php
// Heading
$_['heading_title']     = 'Modifikationer';

// Text
$_['text_success']      = 'Klart: Du har nu ändrat i Modifikationer!';
$_['text_refresh']      = 'Installera / Avinstallera eller radera en Modifikation: klicka på uppdatera sida!';
$_['text_list']         = 'Layoutlista';

// Column
$_['column_name']       = 'Modifikationer Namn';
$_['column_author']     = 'Ägare';
$_['column_version']    = 'Version';
$_['column_status']     = 'Status';
$_['column_date_added'] = 'Datum';
$_['column_action']     = 'Klart';

// Error
$_['error_permission']  = 'Varning: Du saknar tillåtelse att ändra i Modifikationer!';
?>