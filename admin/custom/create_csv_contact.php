<?php 
header('Content-Type: text/html; charset=utf-8');
// VIEW ALL ERRORS
error_reporting(E_ALL);
ini_set('display_errors','On');

// OPENCART CONFIGURATION FILE
include '../../config.php';

// CONNECTING AND SELECTING DATABASE
$connect = mysqli_connect( DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE ) or die( 'DB_ERROR: No connection to the database! ' );
//mysqli_select_db( DB_DATABASE ) or die( 'DB_ERROR: while selecting database' );
mysqli_query($connect, "SET NAMES 'utf8'"); 

$oid = isset($_GET['oid']) ? (int)$_GET['oid'] : NULL;
$sql = "SELECT * FROM `oc_order` WHERE order_id = '".$oid."' LIMIT 1";
$que = mysqli_query($connect, $sql) or die( mysql_error() );
$row = mysqli_fetch_array($que);
	

$orderid 		= $row['order_id'];
$company 		= $row['payment_company']; 
$firstname 		= ucfirst( $row['payment_firstname'] );
$lastname 		= ucfirst( $row['payment_lastname'] );
$address 		= ucfirst( $row['payment_address_1'] );
$address_2 		= ucfirst( $row['payment_address_2'] ); 
$postcode 		= $row['payment_postcode'];
$city 			= mb_convert_case($row['payment_city'], MB_CASE_UPPER, "UTF-8");
$country 		= $row['payment_country'];
$telephone 		= $row['telephone'];
$email 			= $row['email'];

if (!empty($company)){
	$name = $company;
	$cont_pers_name = $firstname.' '.$lastname;
	$is_private = 'False';
} else {
	$name = $firstname.' '.$lastname;
	$cont_pers_name = NULL;
	$is_private = 'True';
}

	$list = array(
	
	array(
	'CustomerNumber',
	'Name',
	'InvoiceAddress1',
	'InvoiceAddress2',
	'InvoicePostalCode',
	'InvoiceCity',
	'InvoiceCountryCode',
	'CorporateIdentityNumber',
	'EdiGlnNumber',
	'VatNumber',
	'Note',
	'Telephone',
	'MobilePhone',
	'EmailAddress',
	'WwwAddress',
	'ContactPersonName',
	'ContactPersonEmail',
	'ContactPersonPhone',
	'ContactPersonMobile',
	'CurrencyCode',
	'TermsOfPaymentCode',
	'IsPrivatePerson'),
	
	array(
	$orderid,
	$name,
	$address,
	$address_2,
	$postcode,
	$city,
	'SE',
	'',
	'',
	'',
	'',
	'',
	$telephone,
	$email,
	'',
	$cont_pers_name,
	'',
	'',
	'',
	'SEK',
	20,
	$is_private)
	);


$fp = fopen($orderid.'_cust_export.csv', 'w');

foreach ($list as $fields) {
    fputcsv($fp, $fields, ";");
}

fclose($fp);

?>
<div>Ladda ned: <a href="<?php echo $orderid.'_cust_export.csv'; ?>"><?php echo $orderid.'_cust_export.csv'; ?></a>
<?php
echo '<pre>';
#print_r($row);
echo '</pre>';
?></div>