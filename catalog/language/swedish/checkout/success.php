<?php
// Heading
$_['heading_title'] = 'Tack för din order!';

// Text
$_['text_basket']   = 'Varukorg';
$_['text_checkout'] = 'Till kassan';
$_['text_success']  = 'Bekräftelse';
$_['text_customer'] = '<p>Din order har mottagits och en bekräftelse har skickats till din e-postadress.</p><p>Du kan se din orderhistorik genom att gå till <a href="%s">ditt konto</a> och klicka på <a href="%s">orderhistorik</a>.</p><p>Om du har några frågor <a href="%s">kontakta oss</a> gärna!</p><br/><p>Tack för din order och välkommen åter!</p>';
$_['text_guest']    = '<p>Din order har mottagits och en bekräftelse har skickats till din e-postadress.</p><p>Om du har några frågor <a href="%s">kontakta oss</a> gärna</a>.</p><p>Tack för din order och välkommen åter!</p>';