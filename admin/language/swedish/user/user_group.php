<?php
// Heading
$_['heading_title']     = 'Användargrupp';

// Text
$_['text_success']      = 'Klart: Du har ändrat i Användare!';
$_['text_list']         = 'Användargrupps Lista';
$_['text_add']          = 'Lägg till Användargrupp';
$_['text_edit']         = 'Ändra i Användargrupp';

// Column
$_['column_name']      = 'Namn';
$_['column_action']    = 'Ändra';

// Entry
$_['entry_name']       = 'Användargruppsnamn:';
$_['entry_access']     = 'Behörighet att se:';
$_['entry_modify']     = 'Behörighet att ändra:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i modulen!';
$_['error_name']       = 'Namnet måste innehålla mellan 3 och 64 tecken!';
$_['error_user']       = 'Varning: Denna grupp kan inte raderas eftersom den är kopplad till %s användare!';
?>
