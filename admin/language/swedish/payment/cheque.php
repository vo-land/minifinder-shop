<?php
// Heading
$_['heading_title']      = 'Check';

// Text 
$_['text_payment']       = 'Betalningar';
$_['text_success']       = 'Klart: Du har ändrat i modulen Check!';
$_['text_edit']          = 'Andra i Check modulen';

// Entry
$_['entry_payable']      = 'Utbetalningsbar till:';
$_['entry_total']        = 'Totalt:<br /><span class="help">Denna summa måste nås innan betalningsalternativet blir aktivt.</span>';
$_['entry_order_status'] = 'Orderstatus:';
$_['entry_geo_zone']     = 'GeoZon:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sorteringsordning:';

// Help
$_['help_total']		 = 'Den totala ordersumman måste nå denna summa innan alternativet visas.';

// Error
$_['error_permission']   = 'Varning: Du har inte behörighet att ändra!';
$_['error_payable']      = 'Checkmottagarnamn krävs!';
?>