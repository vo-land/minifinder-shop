<?php
// Text
$_['text_approve_subject']      = '%s - Ditt Återförsäljarekonto är nu aktiverat!';
$_['text_approve_welcome']      = 'Välkommen och tack för att du registrerade dig hos %s!';
$_['text_approve_login']        = 'Ditt konto är nu skapat och du kan logga in med din epostadress och lösenord genom att besöka vår hemsida på följande URL:';
$_['text_approve_services']     = 'Genom att logga in kan du generera spårningkoder, se dina provisioner och ändra dina kontouppgifter.';
$_['text_approve_thanks']       = 'Tack,';
$_['text_transaction_subject']  = '%s - Återförsäljningsprovision';
$_['text_transaction_received'] = 'Du har mottagit %s i provision!';
$_['text_transaction_total']    = 'Din totala provision uppgår nu till %s.';
?>