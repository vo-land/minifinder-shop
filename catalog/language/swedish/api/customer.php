<?php
// Text
$_['text_success']       = 'Du har ändrat i kunder';

// Error
$_['error_permission']   = 'Varning: Du har inte tillåtelse att ändra i API!!';
$_['error_firstname']    = 'Förnamn måste vara mellan 1-12 tecken.';
$_['error_lastname']     = 'Efternamn måste vara mellan 1-12 tecken.';
$_['error_email']        = 'E-Mail Adress är felaktig!';
$_['error_telephone']    = 'Telefonnummer måste vara mellan 3-32 tecken!';
$_['error_custom_field'] = '%s krävs!';
?>