<?php
// Heading
$_['heading_title']             = 'Återförsäljare';

// Text
$_['text_success']              = 'Klart: Du har ändrat i Återförsäljare!';
$_['text_approved']             = 'Du har godkänts %s konto!';
$_['text_list']                 = 'Layout Lista';
$_['text_add']                  = 'Lägg till Återförsäljare';
$_['text_edit']                 = 'Ändra i Återförsäljare';
$_['text_balance']              = 'Balans på kontot';
$_['text_cheque']               = 'Check';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Banköverföring';

// Column
$_['column_name']               = 'Återförsäljare Namn';
$_['column_email']              = 'E-Mail';
$_['column_code']               = 'Spårnings kod';
$_['column_balance']            = 'Balans på kontot';
$_['column_status']             = 'Status';
$_['column_approved']           = 'Godkänd';
$_['column_date_added']         = 'Datum';
$_['column_description']        = 'Beskrivning';
$_['column_amount']             = 'Summa';
$_['column_action']             = 'Ändra';

// Entry
$_['entry_firstname']           = 'Förnamn';
$_['entry_lastname']            = 'Efternamn';
$_['entry_email']               = 'E-Mail';
$_['entry_telephone']           = 'Telefon';
$_['entry_fax']                 = 'Fax';
$_['entry_status']              = 'Status';
$_['entry_password']            = 'Password';
$_['entry_confirm']             = 'Klart';
$_['entry_company']             = 'Företag';
$_['entry_website']             = 'Hemsida';
$_['entry_address_1']           = 'Adress 1';
$_['entry_address_2']           = 'Adress 2';
$_['entry_city']                = 'Stad';
$_['entry_postcode']            = 'Postnummer';
$_['entry_country']             = 'Land';
$_['entry_zone']                = 'Län';
$_['entry_code']                = 'Spårnings kod';
$_['entry_commission']          = 'Provision (%)';
$_['entry_tax']                 = 'Skatt ID';
$_['entry_payment']             = 'Betalningsalternativ';
$_['entry_cheque']              = 'Check namn';
$_['entry_paypal']              = 'PayPal Email adress';
$_['entry_bank_name']           = 'Bank Namn';
$_['entry_bank_branch_number']  = 'ABA/BSB number (Branch Number)';
$_['entry_bank_swift_code']     = 'SWIFT Code';
$_['entry_bank_account_name']   = 'Konto namn på företaget';
$_['entry_bank_account_number'] = 'Kontonummer';
$_['entry_amount']              = 'Summa';
$_['entry_description']         = 'Beskrivning';
$_['entry_name']                = 'Återförsäljare Namn';
$_['entry_approved']            = 'Godkänd';
$_['entry_date_added']          = 'Datum';

// Help
$_['help_code']                 = 'Spårnings koden som ska användas.';
$_['help_commission']           = 'Procent som återförsäljare får.';

// Error
$_['error_permission']          = 'Varning: Du har inte tillåtelse att ändra.!';
$_['error_exists']              = 'Varning: E-Mail Adress används redan!';
$_['error_firstname']           = 'Förnamn måste vara mellan 1-32 tecken.';
$_['error_lastname']            = 'Efternamn måste vara mellan 1-32 tecken.!';
$_['error_email']               = 'E-Mail Adressen är felaktig!';
$_['error_cheque']              = 'Check namn krävs!';
$_['error_paypal']              = 'PayPal Email Adress är inte giltig!!';
$_['error_bank_account_name']   = 'Ägaren på kontot krävs!';
$_['error_bank_account_number'] = 'Kontonummer krävs!';
$_['error_telephone']           = 'Telefon måste vara mellan 1-32 tecken!';
$_['error_password']            = 'Password måste vara mellan 1-20 tecken!';
$_['error_confirm']             = 'Password och password 2 stämmer inte!';
$_['error_address_1']           = 'Adress 1 måste vara mellan 3-128 tecken!';
$_['error_city']                = 'Stad måste vara mellan 3-128 tecken';
$_['error_postcode']            = 'Postnummer måste vara mellan 2-20 tecken!!';
$_['error_country']             = 'Välj ett land!';
$_['error_zone']                = 'Välj ett län!';
$_['error_code']                = 'Spårnings koden krävs!';
?>