<?php
// Heading  
$_['heading_title']   = 'Varukorg';

// Text
$_['text_success']  		 = 'Klart: Du har lagt till <a href="%s">%s</a> i din <a href="%s">varukorg</a>!';
$_['text_remove']            = 'Klart: Du har ändrat i varukorgen!';
$_['text_login']     		 = 'OBS: Du måste <a href="%s">logga in</a> eller <a href="%s">skapa ett konto</a> för att visa priser!';
$_['text_items']    	     = '%s st - %s';
$_['text_points']            = 'Bonuspoäng: %s';
$_['text_next']              = 'Vad vill du göra nu?';
$_['text_next_choice']       = 'Ange rabattkod eller en presentkortskod här nedan. Du kan också se fraktkostnaden för det du har i varukorgen.';
$_['text_empty']    		 = 'Din varukorg är tom!';
$_['text_day']               = 'dag';
$_['text_week']              = 'vecka';
$_['text_semi_month']        = 'halv-månad';
$_['text_month']             = 'månad';
$_['text_year']              = 'år';
$_['text_trial']             = '%s varje %s %s för %s betalning ';
$_['text_recurring']         = '%s varje %s %s';
$_['text_length']            = ' för %s betalningar';
$_['text_until_cancelled']   = 'fram tills avbruten';
$_['text_recurring_item']    	              = 'Återkommande produkter';
$_['text_payment_recurring']                  = 'Betalning Profil';
$_['text_trial_description'] 	              = '%s varje %d %s(s) för %d betalning(s)';
$_['text_payment_description'] 	              = '%s varje %d %s(s) för %d betalning(s)';
$_['text_payment_until_canceled_description'] = '%s varje %d %s(s) fram tills avbruten';

// Column
$_['column_image']   		 = 'Bild';
$_['column_name']    		 = 'Produktnamn';
$_['column_model']   		 = 'Artikelnummer';
$_['column_quantity']		  = 'Antal';
$_['column_price']  		  = 'Enhetspris';
$_['column_total']   	   	 = 'Totalsumma';

// Error
$_['error_stock']   		   = 'Produkter märkta med *** är inte tillgängliga i önskat antal!';
$_['error_minimum'] 	  	   = 'Minimiorder för %s är %s!';	
$_['error_required'] 	       = '%s krävs!';	
$_['error_product']            = 'Varning: Det är inga produkter i din varukorg.';	
$_['error_recurring_required'] = 'Välj en betalningsprofil!';
?>