<?php if (count($languages) > 1) { ?>
<div class="pull-left">
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language">
  <div class="btn-group langbar">
      <?php foreach ($languages as $language) { ?>
      <a href="<?php echo $language['code']; ?>" <?php if ($language['code'] == $code) { echo 'class="lang-current"'; } ?>><img src="image/flags_new/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" /></a>
      <?php } ?>
  </div>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
  <input type="hidden" name="only_theme" value="1" />
</form>
</div>
<?php } ?>
