<?php
// Heading
$_['heading_title']     = 'Betalningar';

// Text
$_['text_success']      = 'Klart: Du har nu ändrat i Betalningar!';
$_['text_list']         = 'Layoutlista';

// Column
$_['column_name']       = 'Betalningsmetod';
$_['column_status']     = 'Status';
$_['column_sort_order'] = 'Sorteringsorder';
$_['column_action']     = 'Ändra';

// Error
$_['error_permission']  = 'Varning: Du har inte behörighet att ändra i Betalningar!';
?>