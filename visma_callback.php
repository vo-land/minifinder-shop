<?php
	$vismaApiEndPoint = "https://eaccountingapi.vismaonline.com";  
    header("Access-Control-Allow-Origin: ".$vismaApiEndPoint);
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Expose-Headers: Access-Control-Allow-Credentials, Access-Control-Allow-Origin");	
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT');
    error_reporting(E_ALL);
    session_start();
    
	// --------- SET CUSTOMER DATA --------------// 
if(isset($_REQUEST['orderId'])){	
	$oderId = $_REQUEST['orderId'];
	$CorporateIdentityNumber = isset( $_REQUEST['cin'] ) ? $_REQUEST['cin'] : NULL; // Modified by Dean 2016-04-04 11:38 
	$cust_name = $_REQUEST['name'];
	$company = isset( $_REQUEST['c_name'] ) ? $_REQUEST['c_name'] : NULL; // Modified by Dean 2015-11-05 13:19
	$cust_group = isset( $_REQUEST['c_group'] ) ? $_REQUEST['c_group'] : 0;
	$address1 = $_REQUEST['add'];
	$address2 = $_REQUEST['add2'];
	$zip = $_REQUEST['zip'];
	$city = $_REQUEST['city'];
	$country = $_REQUEST['country'];
	$email = $_REQUEST['email'];
	$phone = $_REQUEST['phone'];

	//if(!isset($_SESSION['CustomerNumber'])){
		
	$_SESSION['ContactPersonName'] = $cust_name;
	$_SESSION['ContactPersonEmail'] = $email;
	$_SESSION['ContactPersonPhone'] = $phone;
	$_SESSION['CustomerNumber'] = $oderId;
	$_SESSION['EmailAddress'] = $email;
	$_SESSION['InvoiceAddress1'] = $address1;
	$_SESSION['InvoiceAddress2'] = $address2;
	$_SESSION['InvoiceCity'] = $city;
	$_SESSION['InvoiceCountryCode'] = $country;
	$_SESSION['InvoicePostalCode'] = $zip;
	$_SESSION['customergroup'] = $cust_group;
	$_SESSION['CorporateIdentityNumber'] = $CorporateIdentityNumber;
	$_SESSION['CompanyName'] = $company;
		
	
	  //echo "<pre>"; print_r($_REQUEST);
		echo "Please wait...";
		//exit();
	  //  echo $_SESSION['api_code'];die();
	  //  if(!isset($_SESSION['api_code'])){
	//  }
	  ?>
	<?php if(!isset($_SESSION['refresh_token'])){ ?>
	<script>
		 //window.location="https://auth.vismaonline.com/eaccountingapi/oauth/authorize?client_id=gpser&redirect_uri=https://www.gpser.se/callback.php&state=a234fd&scope=sales+accounting&response_type=code"
		 window.location="https://auth.vismaonline.com/eaccountingapi/oauth/authorize?client_id=gpser&redirect_uri=https://www.gpser.se/visma_callback.php&state=a234fd&scope=sales&response_type=code"
	</script>
	<?php }else { ?>
	<script>
		window.location="https://www.gpser.se/visma_callback.php?code=<?php echo $_SESSION['api_code'];?>";
	</script>
	<?php  } ?>
<?php
	 
	}
	else	
	{
		
    $redirect_url = "https://www.gpser.se/visma_callback.php";
    $client_id="gpser";
    $client_secret="fQPeHoMar6cQkQddbrMQPlMjPLlt66y41rBRpAlZfFY";
    $code= "";
    $url = "https://auth.vismaonline.com/eaccountingapi/oauth/token";
  
    if(!isset($_SESSION['api_code'])){
		$code=$_GET['code'];
		$_SESSION['api_code'] = $_GET['code'];
	}

	//echo "AUTH CODE: ".$_SESSION['api_code'];
    // GET TOKEN ACCESS
   
//if(!isset($_SESSION['access_key'])){		
		$curl = curl_init($url);
		curl_setopt( $curl, CURLOPT_POST, true );
		curl_setopt( $curl, CURLOPT_USERPWD, $client_id . ":" . $client_secret );
		curl_setopt( $curl, CURLOPT_POSTFIELDS, http_build_query(array(
																	   'code' => $_GET['code'],
																	   'redirect_uri' => $redirect_url,
																	   'grant_type' => 'authorization_code'
																	   ) ) );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);
		$auth = curl_exec( $curl );
		$secret = json_decode($auth);
		$access_key = isset($secret->access_token)?$secret->access_token:NULL;
		$refresh_token = isset($secret->refresh_token)?$secret->refresh_token:NULL;
		
		if(!isset($_SESSION['refresh_token'])){
		   $_SESSION['refresh_token'] = $refresh_token;
		}
	//	echo "<hr>ACCESS TOKEN: ".$access_key;
		//echo "ACCESS Key: ".$access_key;
	   //	 echo "<pre>"; print_r($auth); die();
//}	   
	   // GET REFRESH TOKEN
	   
	 //  echo "REFRESH Key: ".$_SESSION['refresh_token'];
	   

	   $ch = curl_init($url);
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_USERPWD, $client_id . ":" . $client_secret );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query(array(
																	   'refresh_token' => $_SESSION['refresh_token'],
																	   'redirect_uri' => $redirect_url,
																	   'grant_type' => 'refresh_token'
																	   ) ) );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
	 	$auth2 = curl_exec( $ch );
		$secret2 = json_decode($auth2);
		$access_key = $secret2->access_token;


	if(!isset($_SESSION['access_key'])){
	   $_SESSION['access_key'] = $access_key;
	}
	
	//echo "<hr>REFRESH TOKEN: ".$_SESSION['access_key'];
	//GET TERMS OF PAYMENT ID
		
//echo "<hr>";		
	//************** NEW CODE **************************//	
		$data =  array("type"=>"TermsOfPayment",
					"parameters"=> array("paramType"=>"path","name"=>"id","description"=>"","required"=>false,"type"=>"Guid"	
					));
   
		$data_string = json_encode($data);     
	
		$resultPID="";
		
	if(isset($_SESSION['access_key'])){
		
		//echo "Access Key:".$_SESSION['access_key'];
		
		$ckfile = tempnam ("/vqmod", "CURLCOOKIE");

		$ch = curl_init($vismaApiEndPoint.'/v1/termsofpayment');                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $ckfile);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $ckfile);                                                                    
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json; charset=utf-8',
			'Authorization: Bearer  '.$_SESSION['access_key'],
			'Content-Length:  ' . strlen($data_string) )                                                                       
		);                                                                                                                   
																															 
		 $resultPID = curl_exec($ch);
		 
		//	$docs = json_decode($resultPID);
		//	$TermsOfPaymentId = $docs->TermsOfPaymentId;
		
		///	$resultPID = str_replace(array('[',']'),array(''),$resultPID);	
		$resultPID;
		$secret3 = json_decode($resultPID);
	
		
		if($_SESSION['customergroup']=='Privat')
		{
			$TermsOfPaymentId = $secret3[1]->Id;
			$paymentName =  $secret3[1]->Name;
			$paymentEnglishName =  $secret3[1]->NameEnglish;
			$NumberOfDays =  $secret3[1]->NumberOfDays;
			$TermsOfPaymentTypeId =  $secret3[1]->TermsOfPaymentTypeId;
			$TermsOfPaymentTypeText =  $secret3[1]->TermsOfPaymentTypeText;
		}
		else
		{
			$TermsOfPaymentId = $secret3[2]->Id;
			$paymentName =  $secret3[2]->Name;
			$paymentEnglishName =  $secret3[2]->NameEnglish;
			$NumberOfDays =  $secret3[2]->NumberOfDays;
			$TermsOfPaymentTypeId =  $secret3[2]->TermsOfPaymentTypeId;
			$TermsOfPaymentTypeText =  $secret3[2]->TermsOfPaymentTypeText;
		}

	}
		
	
		
		//echo "<pre>". print_r($secret3);
	
//	die('<br>HTTP SET end');  

//******************** End new code ************************//	
	
   // SEND DATA TO VISMA
   
 $IsPrivatePerson = $_SESSION['customergroup'] == 'Privat' ? 1 : 0;
 
 $paymentArr = array("Id"=> $TermsOfPaymentId,
				"Name"=>$paymentName,
				"NameEnglish"=> $paymentEnglishName,
				"NumberOfDays"=> $NumberOfDays,
				"TermsOfPaymentTypeId"=> $TermsOfPaymentTypeId,
				"TermsOfPaymentTypeText"=> $TermsOfPaymentTypeText);
				
	//	echo "<pre>";print_r($paymentArr);
				
   $name = isset($_SESSION['CompanyName']) && $_SESSION['CompanyName']<>NULL && $_SESSION['CompanyName']<>"" ? $_SESSION['CompanyName'] : $_SESSION['ContactPersonName'];

if (  $IsPrivatePerson == 1 ) {
	$data =  array("CustomerNumber"=> $_SESSION['CustomerNumber'],
			"CorporateIdentityNumber"=> $_SESSION['CorporateIdentityNumber'],
			"MobilePhone" => $_SESSION['ContactPersonPhone'],
            "EmailAddress"=> $_SESSION['EmailAddress'],
            "InvoiceAddress1"=> $_SESSION['InvoiceAddress1'],
			"InvoiceAddress2"=> $_SESSION['InvoiceAddress2'],
            "InvoiceCity"=> $_SESSION['InvoiceCity'],
            "InvoiceCountryCode"=> $_SESSION['InvoiceCountryCode'],
            "InvoicePostalCode"=> $_SESSION['InvoicePostalCode'],
			"TermsOfPaymentId"=> $TermsOfPaymentId,
			"TermsOfPayment"=> $paymentArr,
			"Name"=> $name,
			"IsPrivatePerson"=> $IsPrivatePerson);
} else {
	$data =  array("CustomerNumber"=> $_SESSION['CustomerNumber'],
			"CorporateIdentityNumber"=> $_SESSION['CorporateIdentityNumber'],
            "ContactPersonEmail"=> $_SESSION['ContactPersonEmail'],
            "ContactPersonName"=> $_SESSION['ContactPersonName'],
            "ContactPersonPhone"=> $_SESSION['ContactPersonPhone'],
            "MobilePhone" => $_SESSION['ContactPersonPhone'],
            "EmailAddress"=> $_SESSION['EmailAddress'],
            "InvoiceAddress1"=> $_SESSION['InvoiceAddress1'],
			"InvoiceAddress2"=> $_SESSION['InvoiceAddress2'],
            "InvoiceCity"=> $_SESSION['InvoiceCity'],
            "InvoiceCountryCode"=> $_SESSION['InvoiceCountryCode'],
            "InvoicePostalCode"=> $_SESSION['InvoicePostalCode'],
			"TermsOfPaymentId"=> $TermsOfPaymentId,
			"TermsOfPayment"=> $paymentArr,
			"Name"=> $name,
			"IsPrivatePerson"=> $IsPrivatePerson);
}
   
			
$data_string = json_encode($data);         

//echo '<PRE>'. print_r($data_string);
//echo $data_string;            
	
	$result="";
	$returnVal = "";
	
	if(isset($_SESSION['access_key'])){
		
		$ckfile = tempnam ("/vqmod", "CURLCOOKIE");

		$ch = curl_init($vismaApiEndPoint.'/v1/customers');                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $ckfile);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $ckfile);                                                                    
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json; charset=utf-8',
			'Authorization: Bearer  '.$_SESSION['access_key'],
			'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
																															 
		 $result = curl_exec($ch);

	}
	//	echo "RESULT:: ".$result;
		$returnVal = json_decode($result,true);
		$addedCustomer =  isset($returnVal['CustomerNumber'])?$returnVal['CustomerNumber']:0;
	//	var_dump($returnVal);
		if($addedCustomer>0)
		{
			
			if (is_file('config.php')) {
				require_once('config.php');
			}
			$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			// Check connection
			if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
			}

			$sql = 'UPDATE '.DB_PREFIX.'order SET in_visma=1 where order_id='.$addedCustomer;
			$query = $conn->query($sql);

		//	if(!$query) echo "ERROR.".mysqli_error(); else echo "DONE";
				
			unset($_SESSION['CustomerNumber']);
			unset($_SESSION['CorporateIdentityNumber']); 
			unset($_SESSION['ContactPersonEmail']);
			unset($_SESSION['ContactPersonName']);
			unset($_SESSION['ContactPersonPhone']);
			unset($_SESSION['EmailAddress']);
			unset($_SESSION['InvoiceAddress1']);
			unset($_SESSION['InvoiceAddress2']);
			unset($_SESSION['InvoiceCity']);
			unset($_SESSION['InvoiceCountryCode']);
			unset($_SESSION['InvoicePostalCode']);
			unset($_SESSION['customergroup']);
			unset($_SESSION['CompanyName']); 
			
			?>
			<script>alert('Customer added successfully Visma!');
				 window.location="https://www.gpser.se/admin/index.php?route=sale/order/info&token=<?php echo $_SESSION['token'];?>&order_id=<?php echo $addedCustomer;?>"
			</script>
			<?php
		}
		else
		{
			?>
			<script>alert('This customer is already added in Visma!');
				window.location="https://www.gpser.se/admin/index.php?route=sale/order&token=<?php echo $_SESSION['token'];?>"
			</script>
			<?php
		}
	}
?>
