<?php
// header
$_['heading_title']  = 'Återställ ditt lösenord';

// Text
$_['text_reset']     = 'Återställ ditt lösenord!';
$_['text_password']  = 'Ange ditt nya lösenord.';
$_['text_success']   = 'Klart: Ditt lösenord är nu ändrat.';

// Entry
$_['entry_password'] = 'Lösenord:';
$_['entry_confirm']  = 'Bekräfta lösenordet:';

// Error
$_['error_password'] = 'Lösenordet måste vara mellan 5 och 20 tecken!';
$_['error_confirm']  = 'Lösenordet och bekräftelsen stämmer inte överens!';
?>