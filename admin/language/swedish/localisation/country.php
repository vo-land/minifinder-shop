<?php
// Heading
$_['heading_title']           = 'Länder';

// Text
$_['text_success']            = 'Klart: Du har ändrat i Länder!';
$_['text_list']               = 'Layout Lista';
$_['text_add']                = 'Lägg till Land';
$_['text_edit']               = 'Ändra i Land';

// Column
$_['column_name']             = 'Land';
$_['column_iso_code_2']       = 'ISO-kod (2)';
$_['column_iso_code_3']       = 'ISO-kod (3)';
$_['column_action']           = 'Ändra';

// Entry
$_['entry_name']              = 'Land:';
$_['entry_iso_code_2']        = 'ISO-kod (2):';
$_['entry_iso_code_3']        = 'ISO-kod (3):';
$_['entry_address_format']    = 'Adress Format';
$_['entry_postcode_required'] = 'Postnummer krävs';
$_['entry_status']            = 'Status';

// Help
$_['help_address_format']     = 'Förnamn = {Förnamn}<br />Efternamn = {Efternamn}<br />Företag = {Företag}<br />Adress 1 = {adress_1}<br />Adress 2 = {adress_2}<br />Stad = {Stad}<br />Postnummer = {Postnummer}<br />Län = {Län}<br />Länkod = {Länkod}<br />Land = {Land}';

// Error
$_['error_permission']        = 'Varning: Du har inte behörighet att ändra i Länder!';
$_['error_name']              = 'Landets namn måste innehålla mellan 3 och 128 tecken!';
$_['error_default']           = 'Varning: Detta land kan inte raderas eftersom det är kopplat till butikens förvalda land!';
$_['error_store']             = 'Varning: Detta land kan inte raderas eftersom det är kopplat till %s butiker!';
$_['error_address']           = 'Varning: Detta land kan inte raderas eftersom det är kopplat till %s addressbokskontakter!';
$_['error_affiliate']         = 'Varning: Detta land kan inte raderas eftersom det är kopplat till %s affiliates!';
$_['error_zone']              = 'Varning: Detta land kan inte raderas eftersom det är kopplat till %s zoner!';
$_['error_zone_to_geo_zone']  = 'Varning: Detta land kan inte raderas eftersom det är kopplat till %s zoner för geozoner!';
?>