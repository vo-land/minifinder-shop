<?php
// Heading
$_['heading_title']    = 'Längder';

// Text
$_['text_success']     = 'Klart: Du har ändrat i Längder!';
$_['text_list']        = 'Layout Lista';
$_['text_add']         = 'Lägg till Längder';
$_['text_edit']        = 'Ändra i Längder';

// Column
$_['column_title']     = 'Längder';
$_['column_unit']      = 'Enhet';
$_['column_value']     = 'Värde';
$_['column_action']    = 'Ändra';

// Entry
$_['entry_title']      = 'Längder:';
$_['entry_unit']       = 'Enhet:';
$_['entry_value']      = 'Värde:';

// Help
$_['help_value']       = 'Sätt 1.00000 om det är din standard längd.';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i Längder!';
$_['error_title']      = 'Namnet måste vara mellan 3 och 32 tecken!';
$_['error_unit']       = 'Enhet måste innehålla mellan 1 och 4 tecken!';
$_['error_default']    = 'Varning: Denna längdenhet kan inte raderas eftersom den är butikens förvalda!';
$_['error_product']    = 'Varning: Denna längdenhet kan inte raderas eftersom den är kopplad till %s produkter!';
?>