<?php
// Heading
$_['heading_title'] = 'Ditt affiliatekonto har skapats!';

// Text 
$_['text_message']  = '<p>Tack! Ditt nya konto är skapat!</p> <p>Du kan nu nyttja shopping med oss.</p> <p>Har du några frågor så kontakta oss.</p> <p>En bekräftelse har skickats till din mail.</p>';
$_['text_approval'] = '<p>Tack för att du registrerat ett affiliatekonto hos %s!</p><p>Du kommer att meddelas via e-post när ditt konto har aktiverats.</p><p><a href="%s">Kontakta oss</a> om du har några frågor eller funderingar angående affiliate-programmet.</p>';
$_['text_account']  = 'Återförsäljarekonto';
$_['text_success']  = 'Bekräftelse';
?>