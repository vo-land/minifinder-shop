<?php
// Heading  
$_['heading_title']     = 'Presentkort';

// Text
$_['text_success']      = 'Klart: Du har ändrat i Presentkort!';
$_['text_list']         = 'Presentkort Lista';
$_['text_add']          = 'Lägg till Presentkort';
$_['text_edit']         = 'Ändra i Presentkort';
$_['text_sent']         = 'Klart: Presentkortet är skickat!';

// Column
$_['column_name']       = 'Presentkortsnamn';
$_['column_code']       = 'Kod';
$_['column_from']       = 'Från';
$_['column_to']         = 'Till';
$_['column_theme']      = 'Tema';
$_['column_amount']     = 'Summa';
$_['column_status']     = 'Status';
$_['column_order_id']   = 'Orderid';
$_['column_customer']   = 'Kund';
$_['column_date_added'] = 'Skapad datum';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_code']        = 'Kod:';
$_['entry_from_name']   = 'Från namn:';
$_['entry_from_email']  = 'Från e-postadress:';
$_['entry_to_name']     = 'Till namn:';
$_['entry_to_email']    = 'Till e-postadress:';
$_['entry_theme']       = 'Tema:';
$_['entry_message']     = 'Meddelande:';
$_['entry_amount']      = 'Summa:';
$_['entry_status']      = 'Status:';

// Help
$_['help_code']         = 'Koden kunden anger för att aktivera presentkort.';

// Error
$_['error_selection']   = 'Varning: Inga presentkort är valda!';
$_['error_permission']  = 'Varning: Du har inte behörighet att ändra i Presentkort!';
$_['error_exists']      = 'Varning: Koden är redan använd!';
$_['error_code']        = 'Koden måste innehålla mellan 3 och 10 tecken!';
$_['error_to_name']     = 'Mottagarens namn måste innehålla mellan 1 och 64 tecken!';
$_['error_from_name']   = 'Ditt namn måste innehålla mellan 1 och 64 tecken!';
$_['error_email']       = 'E-postadressen är inte giltig!';
$_['error_amount']      = 'Beloppet måste vara större eller lika med 1!';
$_['error_order']       = 'Varning: Kan inte raderas, presentkortet är en del av <a href="%s">order</a>!';
?>