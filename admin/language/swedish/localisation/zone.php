<?php
// Heading
$_['heading_title']          = 'Zoner';

// Text
$_['text_success']           = 'Klart: Du har ändrat i Zoner!';
$_['text_list']              = 'Layout Lista';
$_['text_add']               = 'Lägg till Zoner';
$_['text_edit']              = 'Ändra i Zoner';

// Column
$_['column_name']            = 'Zoner';
$_['column_code']            = 'Kod';
$_['column_country']         = 'Land';
$_['column_action']          = 'Ändra';

// Entry
$_['entry_status']           = 'Status:';
$_['entry_name']             = 'Namn:';
$_['entry_code']             = 'Kod:';
$_['entry_country']          = 'Land:';

// Error
$_['error_permission']       = 'Varning: Du har inte behörighet att ändra i Zoner!';
$_['error_name']             = 'Namnet måste innehålla mellan 3 och 128 tecken!';
$_['error_default']          = 'Varning: Denna kan inte raderas eftersom den är butikens förvalda!';
$_['error_store']            = 'Varning: Denna kan inte raderas eftersom den är kopplad till %s butiker!';
$_['error_address']          = 'Varning: Denna kan inte raderas eftersom den är kopplad till %s addressbokskontakter!';
$_['error_affiliate']        = 'Varning: Denna kan inte raderas eftersom den är kopplad till %s affiliates!';
$_['error_zone_to_geo_zone'] = 'Varning: Denna kan inte raderas eftersom den är kopplad till %s zoner för geozoner!';
?>