<?php
// Heading
$_['heading_title'] = 'Sidan hittades inte!';

// Text
$_['text_not_found'] = 'Sidan du sökte kan inte hittas! Kontakta din administratör om felet kvarstår.';
?>
