<?php 
//Heading
$_['heading_title']                     = 'eBay';

//Install actions
$_['text_edit']                         = 'Ändra';
$_['text_install']                      = 'Installera';
$_['text_uninstall']                    = 'Avinstallera';

//Status
$_['text_enabled']                      = 'Aktivera';
$_['text_disabled']                     = 'Inaktivera';

//Messages
$_['error_category_nosuggestions']      = 'Kunde inte ladda kategorier.';
$_['lang_text_success']                 = 'Du har inte tillåtelse att ändra i eBay modulen';
$_['lang_error_retry']          		= 'Kunde inte connecta till OpenBay server. ';
?>