<?php
// Heading
$_['heading_title'] = 'Använd Presentkort';

// Text
$_['text_success']  = 'Klart: Ditt presentkort har blivit godkänt!';

// Entry
$_['entry_voucher'] = 'Ange din kod här';

// Error
$_['error_voucher'] = 'Varning: Presentkort är antingen ej rätt eller att summan är slut!';
$_['error_empty']   = 'Varning: Du måste ange din kod här!';
?>