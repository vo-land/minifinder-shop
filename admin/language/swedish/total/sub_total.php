<?php
// Heading
$_['heading_title']    = 'Delsumma';

// Text
$_['text_total']       = 'Ordertotal';
$_['text_success']     = 'Klart: Du har ändrat!';
$_['text_edit']        = 'Ändra Delsumma';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sorteringsordning:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra!';
?>