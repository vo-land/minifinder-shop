<?php
// Heading
$_['heading_title']    = 'Fast fraktkostnad';

// Text
$_['text_shipping']    = 'Frakt';
$_['text_success']     = 'Klart: Du har ändrat i fast Fraktkostnad!';
$_['text_edit']        = 'Ändra i Fast fraktkostnad';

// Entry
$_['entry_cost']       = 'Belopp:';
$_['entry_tax_class']  = 'Moms:';
$_['entry_geo_zone']   = 'Geozon:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sorteringsordning:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra!';
?>