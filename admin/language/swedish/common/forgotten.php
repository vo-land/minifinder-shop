<?php
// header
$_['heading_title']   = 'Glömt ditt lösenord?';

// Text
$_['text_forgotten']  = 'Glömt lösenordet?';
$_['text_your_email'] = 'Din epostadress';
$_['text_email']      = 'Ange epostadressen för ditt konto. Klicka därefter på "Fortsätt" så skickas en återställningslänk för ditt lösenord till din adress.';
$_['text_success']    = 'Ett epostmeddelande med en återställningslänk har skickats till din epostadress.';

// Entry
$_['entry_email']     = 'Epostadress:';
$_['entry_password']  = 'Nytt lösenord:';
$_['entry_confirm']   = 'Bekräfta lösenordet:';

// Error
$_['error_email']     = 'Fel: Epostadressen kunde inte hittas i registret, försök igen!';
$_['error_password']  = 'Lösenordet måste innehålla mellan 3 och 20 tecken!';
$_['error_confirm']   = 'Lösenordet och bekräftelsen överensstämmer inte!';
?>