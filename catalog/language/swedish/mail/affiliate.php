<?php
// Text
$_['text_subject']              = '%s - Återförsäljare programmet';
$_['text_welcome']              = 'Tack för din registrering som Återförsäljare hos %s!';
$_['text_login']                = 'Ditt konto är nu skapat. Använd din e-mail adress och password på denna URL:';
$_['text_approval']             = 'Ditt konto måste godkännas innan du kan logga in. När kontot har godkänts kan du logga in med din e-postadress och ditt lösenord via vår hemsida eller genom att klicka på följande länk:';
$_['text_services']             = 'När du loggat in, kan du generera spårningskoder, se din provision och ändra dina uppgifter.';
$_['text_thanks']               = 'Tack!';
$_['text_new_affiliate']        = 'Ny Återförsäljare ';
$_['text_signup']		        = 'Ny Återförsäljare är skapad:';
$_['text_store']		        = 'Butik:';
$_['text_firstname']	        = 'Förnamn:';
$_['text_lastname']		        = 'Efternamn:';
$_['text_company']		        = 'Företag:';
$_['text_email']		        = 'E-Mail:';
$_['text_telephone']	        = 'Telefon:';
$_['text_website']		        = 'Hemsida:';
$_['text_order_id']             = 'Order ID:';
$_['text_transaction_subject']  = '%s - Återförsäljareprovision';
$_['text_transaction_received'] = 'Du har fått %s i provision!';
$_['text_transaction_total']    = 'Totalt i provision.';
?>