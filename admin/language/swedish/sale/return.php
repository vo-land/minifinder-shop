<?php
// Heading
$_['heading_title']       = 'Produktreturer';

// Text
$_['text_success']        = 'Klart: Du har ändrat i Produktreturer!';
$_['text_list']           = 'Produktreturer Lista';
$_['text_add']            = 'Lägg till Produktretur';
$_['text_edit']           = 'Ändra i Produktretur';
$_['text_opened']         = 'Öppnad';
$_['text_unopened']       = 'Ej öppnad';
$_['text_order']          = 'Order Information';
$_['text_product']        = 'Produkt Information &amp; Anledning till retur';
$_['text_history']        = 'Lägg till i historiken.';

// Column
$_['column_return_id']     = 'Returid';
$_['column_order_id']      = 'Orderid';
$_['column_customer']      = 'Kund';
$_['column_product']       = 'Produkt';
$_['column_model']         = 'Artikelnummer';
$_['column_status']        = 'Status';
$_['column_date_added']    = 'Skapad datum';
$_['column_date_modified'] = 'Ändrad datum';
$_['column_comment']       = 'Kommentarer';
$_['column_notify']        = 'Kund meddelad';
$_['column_action']        = 'Ändra';

// Entry
$_['entry_customer']      = 'Kund:';
$_['entry_order_id']      = 'Orderid:';
$_['entry_date_ordered']  = 'Orderdatum:';
$_['entry_firstname']     = 'Förnamn:';
$_['entry_lastname']      = 'Efternamn:';
$_['entry_email']         = 'E-post:';
$_['entry_telephone']     = 'Telefon:';
$_['entry_product']       = 'Produkt:';
$_['entry_model']         = 'Artikelnummer:';
$_['entry_quantity']      = 'Antal:';
$_['entry_opened']        = 'Öppnad:';
$_['entry_comment']       = 'Kommenterer:';
$_['entry_return_reason'] = 'Retur Orsak';
$_['entry_return_action'] = 'Retur Ändra';
$_['entry_return_status'] = 'Returstatus:';
$_['entry_notify']        = 'Meddela kund:';
$_['entry_return_id']     = 'Retur ID';
$_['entry_date_added']    = 'Datum';
$_['entry_date_modified'] = 'Datum ändrat';

// Help
$_['help_product']        = '(Autocomplete)';

// Error
$_['error_warning']       = 'Varning: Kontrollera felmeddelanden och ändra!';
$_['error_permission']    = 'Varning: Du har inte behörighet att ändra!';
$_['error_order_id']      = 'Order-ID krävs!';
$_['error_firstname']     = 'Förnamnet måste innehålla mellan 1 och 32 tecken!';
$_['error_lastname']      = 'Efternamnet måste innehålla mellan 1 och 32 tecken!';
$_['error_email']         = 'E-postadressen är inte giltig!';
$_['error_telephone']     = 'Telefon måste innehålla mellan 3 och 32 tecken!';
$_['error_product']       = 'Produktnamnet måste innehålla mellan 3 och 255 tecken!';
$_['error_model']         = 'Artikelnummer måste innehålla mellan 3 och 64 tecken!';
?>