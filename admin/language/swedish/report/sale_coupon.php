<?php
// Heading
$_['heading_title']    = 'Rapport rabattkuponger';

// Text
$_['text_list']        = 'Kupong Lista';

// Column
$_['column_name']      = 'Kupong Namn';
$_['column_code']      = 'Kod';
$_['column_orders']    = 'Ordrar';
$_['column_total']     = 'Summa';
$_['column_action']    = 'Ändra';

// Entry
$_['entry_date_start'] = 'Startdatum:';
$_['entry_date_end']   = 'Slutdatum:';
?>