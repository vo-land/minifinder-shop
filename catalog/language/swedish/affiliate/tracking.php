<?php
// Heading 
$_['heading_title']    = 'Affiliatespårning';

// Text
$_['text_account']     = 'Affiliatekonto';
$_['text_description'] = 'För att säkerställa att du får betalt för de kunder du skickar till oss, spårar vi besökaren med hjälp av en spårningskod i länken som dirigerar besökaren till oss. Du kan använda verktyget nedan för att generera länkar med din spårningskod. Klistra sedan in länken på din hemsida eller i din blogg.';

// Entry
$_['entry_code']       = 'Din spårningskod:';
$_['entry_generator']  = 'Spårningslänksgenerator: Skriv i namnet på den produkt du vill länka till.';
$_['entry_link']       = 'Spårningslänk:';

// Help
$_['help_generator']  = 'Ange namn på produkten du vill länka till.';
?>