<?php
// Heading 
$_['heading_title']   = 'Nedladdningar';

// Text
$_['text_account']    = 'Konto';
$_['text_downloads']  = 'Nedladdningar';
$_['text_empty']      = 'Du har inga köpt några nedladdningsbara produkter!';

// Column
$_['column_order_id']   = 'Order ID';
$_['column_name']       = 'Namn';
$_['column_size']       = 'Storlek';
$_['column_date_added'] = 'Datum';
?>