<?php
// Heading
$_['heading_title'] = 'Använd Rabattkod';

// Text
$_['text_success']  = 'Klart: Din rabattkod har blivit godkänt!';
$_['text_coupon']   = 'Klart: Din rabattkod har blivit godkänt!';

// Entry
$_['entry_coupon']  = 'Ange din rabattkod här';

// Error
$_['error_coupon']  = 'Varning: Rabattkoden är antingen ej rätt eller att summan är slut!';
$_['error_empty']   = 'Varning: Du måste ange din rabattkod här!';
?>