<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php $last_breadcrumb = array_pop($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
        <li><?php echo $last_breadcrumb['text']; ?></li>
    </ul>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="well info_new_customer">
                        <h2><?php echo $text_new_customer; ?></h2>
                        <p class="text_customer"><?php echo $text_register; ?></p>
                        <p><?php echo $text_register_account; ?></p>
                        <a href="<?php echo $register; ?>" class="red_button">
                            <span><?php echo $button_continue; ?></span>
                            <span><?php echo $button_continue; ?></span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="well login_block">
                        <h2><?php echo $text_returning_customer; ?></h2>
                        <p class="text_customer"><?php echo $text_i_am_returning_customer; ?></p>
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form_login">
                            <div class="form-group">
                                <!--<label class="control-label" for="input-email"><?php echo $entry_email; ?></label>-->
                                <input type="text" name="email" value="<?php echo $email; ?>"
                                       placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <!--<label class="control-label" for="input-password"><?php echo $entry_password; ?></label>-->
                                <input type="password" name="password" value="<?php echo $password; ?>"
                                       placeholder="<?php echo $entry_password; ?>" id="input-password"
                                       class="form-control"/>
                            </div>
                            <div class="red_form_btn">
                                <input type="submit" value="<?php echo $button_login; ?>"/>
                            </div>
                            <a href="<?php echo $forgotten; ?>" class="link_forgotten"><?php echo $text_forgotten; ?></a>
                            <?php if ($redirect) { ?>
                            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>"/>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>