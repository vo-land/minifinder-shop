<?php
// Heading
$_['heading_title']       = 'Kuponger';

// Text
$_['text_success']        = 'Klart: Du har ändrat i Kuponger.';
$_['text_list']           = 'Layout Lista';
$_['text_add']            = 'Lägg till Kupong';
$_['text_edit']           = 'Ändra i Kupong';
$_['text_percent']        = 'Procent';
$_['text_amount']         = 'Fast summa';

// Column
$_['column_name']         = 'Kupong Namn';
$_['column_code']         = 'Kod';
$_['column_discount']     = 'Rabatt';
$_['column_date_start']   = 'Start Datum';
$_['column_date_end']     = 'Slut Datum';
$_['column_status']       = 'Status';
$_['column_order_id']     = 'Order ID';
$_['column_customer']     = 'Kunder';
$_['column_amount']       = 'Summa';
$_['column_date_added']   = 'Datum';
$_['column_action']       = 'Ändra';

// Entry
$_['entry_name']          = 'Kupong Namn';
$_['entry_code']          = 'Kod';
$_['entry_type']          = 'Typ';
$_['entry_discount']      = 'Rabatt';
$_['entry_logged']        = 'Kunder Login';
$_['entry_shipping']      = 'Fraktfritt';
$_['entry_total']         = 'Totalsumma';
$_['entry_category']      = 'Kategori';
$_['entry_product']       = 'Produkt';
$_['entry_date_start']    = 'Start Datum';
$_['entry_date_end']      = 'Slut Datum';
$_['entry_uses_total']    = 'Antal per kupong';
$_['entry_uses_customer'] = 'Antal per kund';
$_['entry_status']        = 'Status';

// Help
$_['help_code']           = 'Koden som kunden ska ange.';
$_['help_type']           = 'Procent eller Fast summa.';
$_['help_logged']         = 'Kunden måste loggga in för att använda kupongen.';
$_['help_total']          = 'Totalsumma som måste uppnås innan koden är giltig.';
$_['help_category']       = 'Välj alla produkter i vald kategori.';
$_['help_product']        = 'Välj en produkt som koden är giltig för.';
$_['help_uses_total']     = 'Max antal som en kupong kan användas av alla kunder. Lämnas blankt för obegränsat antal gånger.';
$_['help_uses_customer']  = 'Max antal som en kupong kan användas per kund. Lämnas blankt för obegränsat antal gånger';

// Error
$_['error_permission']    = 'Varning: Du har inte tillåtelse att ändra.!';
$_['error_exists']        = 'Warning: Kupongen är redan använd.';
$_['error_name']          = 'Kupong Namn måste vara mellan 3-128 tecken.';
$_['error_code']          = 'Koden måste vara mellan 3-10 tecken.';
?>