<?php
// Heading
$_['heading_title']    = 'Moduler';

// Text
$_['text_success']     = 'Klart: Du har nu ändrat i Moduler!';
$_['text_layout']      = 'Efter installation av en modul kan du välja layout <a href="%s" class="alert-link">här</a>!';
$_['text_add']         = 'Lägg till en Modul';
$_['text_list']        = 'Layoutlista';

// Column
$_['column_name']      = 'Modulnamn';
$_['column_action']    = 'Ändra';

// Entry
$_['entry_code']       = 'Moduler';
$_['entry_name']       = 'Modul Namn';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i Moduler!';
$_['error_name']       = 'Modul Namn måste vara mellan 3-64 tecken!';
$_['error_code']       = 'Extension krävs!';
?>