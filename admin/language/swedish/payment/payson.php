<?php
// Heading
$_['heading_title']      = 'Payson';

// Text 
$_['text_payment']       = 'Betalningar';
$_['text_success']       = 'Klart: Du har ändrat detaljer för Paysonkontot!';
$_['text_development']   = '<span style="color: green;">Ready</span>';

// Entry
$_['entry_status']       = 'Status:';
$_['entry_geo_zone']     = 'GeoZon:';
$_['entry_order_status'] = 'Orderstatus:';
$_['entry_email']        = 'Säljarens epostadress:';
$_['entry_key']   		 = 'MD5-Key:';
$_['entry_mid']		     = 'Agent ID:';
$_['entry_test']         = 'Testläge:';
$_['entry_sort_order']   = 'Sorteringsordning:';

// Help
$_['help_agentid'] 		 = 'Hämta uppgifterna härifrån: https://www.payson.se/account/agent/Default.aspx';
$_['help_md5'] 			 = 'Hämta uppgifterna härifrån: https://www.payson.se/account/agent/Default.aspx';

// Error
$_['error_permission']   = 'Varning: Du har inte behörighet att ändra i modulen Payson!';
$_['error_email']        = 'E-postadress krävs!'; 
$_['error_encryption']   = 'Encryption Key krävs!';
$_['error_security_code']= 'Säkerhetskod krävs! Läs README för detaljer om detta. ';
?>