<?php
// Heading
$_['heading_title']    = 'Bildhanterare';
 
// Text
$_['text_uploaded']    = 'Klart: Din fil är uppladdad!';
$_['text_directory']   = 'Klart: Din mapp är skapad!';
$_['text_delete']      = 'Klart: Din fil eller mapp är raderad!';

// Entry
$_['entry_search']     = 'Sök..';
$_['entry_folder']     = 'Ny mapp:';

// Error
$_['error_permission'] = 'Varning: Behörighet saknas!';
$_['error_filename']   = 'Varning: Filnamnet måste vara mellan 3 och 255 tecken!';
$_['error_folder']     = 'Varning: Mappen måste vara i 3 tecken.';
$_['error_exists']     = 'Varning: En fil eller mapp med samma namn finns redan!';
$_['error_directory']  = 'Varning: Välj en mapp!';
$_['error_filename']   = 'Varning: Filnamnet måste vara mellan 3 och 255 tecken!';
$_['error_upload']     = 'Varning: Filen gick inte att ladda upp. Kolla och prova igen.';
$_['error_delete']     = 'Varning: Du kan inte radera det här biblioteket!';
?>