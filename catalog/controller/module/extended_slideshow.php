<?php
class ControllerModuleExtendedSlideshow extends Controller {
    public function index($setting) {
        static $module = 0;

        $this->load->model('design/extended_banner');
        $this->load->model('tool/image');

        $this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
        $this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

        $data['banners'] = array();

        $results = $this->model_design_extended_banner->getBanner($setting['banner_id']);

        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $data['banners'][] = array(
                    'title'             => html_entity_decode($result['title']),
                    'link'              => $result['slide_link'],
                    'button_link'       => $result['button_link'],
                    'button_title'      => html_entity_decode($result['button_title']),
                    'description'       => html_entity_decode($result['description']),
                    'button_ena'        => $result['button_ena'],
                    'description_ena'   => $result['description_ena'],
                    'image'             => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
                );
            }
        }

        $data['module'] = $module++;
        $data['slider_move'] = true;
        $data['slider_after'] = 'header';

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/extended_slideshow.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/extended_slideshow.tpl', $data);
        } else {
            return $this->load->view('default/template/module/extended_slideshow.tpl', $data);
        }
    }
}