<?php 

$_['option_register_payment_address_title']  		= 'Fakturaadress';
$_['option_register_payment_address_description']  	= '';
$_['option_register_shipping_address_title']  		= 'Leveransadress';
$_['option_register_shipping_address_description']  = '';
$_['option_register_shipping_method_title']  		= 'Leveranssätt';
$_['option_register_payment_method_title'] 			= 'Betalsätt';
$_['option_register_cart_description'] 				= '';

$_['option_guest_payment_address_title']  			= 'Fakturaadress';
$_['option_guest_payment_address_description']  	= '';
$_['option_guest_shipping_address_title']  			= 'Leveransadress';
$_['option_guest_shipping_address_description'] 	= '';
$_['option_guest_shipping_method_title']  			= 'Leveranssätt';
$_['option_guest_payment_method_title'] 			= 'Betalsätt';
$_['option_guest_cart_description'] 				= '';

$_['option_logged_payment_address_title']  			= 'Adress';
$_['option_logged_payment_address_description']  	= '';
$_['option_logged_shipping_address_title']  		= 'Leveransadress';
$_['option_logged_shipping_address_description'] 	= '';
$_['option_logged_shipping_method_title']  			= 'Leveranssätt';
$_['option_logged_payment_method_title'] 			= 'Betalsätt';
$_['option_logged_cart_description'] 				= '';

$_['entry_email_confirm'] 							= 'Bekräfta E-mail';
$_['error_email_confirm'] 							= 'Email confirmation does not match email!';


$_['step_option_guest_desciption'] = 'Använd gästkassa om du inte vill skapa ett konto hos oss.';

$_['error_step_payment_address_fields_company'] = 'Company name required more the 3 and less the 128 characters';
$_['error_step_shipping_address_fields_company'] = 'Company name required more the 3 and less the 128 characters';

$_['error_step_confirm_fields_comment'] = 'Please fill in the comment to the order';


?>