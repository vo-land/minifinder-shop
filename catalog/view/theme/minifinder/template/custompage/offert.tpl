<?php 
    echo $header;
    echo $column_left;
    echo $column_right; ?>
    <div class="container">
		
		<ul class="breadcrumb">
		<?php $last_breadcrumb = array_pop($breadcrumbs); ?>
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
		<li><?php echo $last_breadcrumb['text']; ?></li>
		</ul>
  
    <div class="row">
    <div id="content" class="col-sm-12">
    	<h1>Offertförfrågan</h1>
    	
        <?php 
            echo $content_top;
            echo $content_bottom;
        ?>
    	
    	<div>Fyll i alla uppgifter som krävs i formuläret nedan för offertförfrågan. Vi kommer att behandla din förfrågan inom 12h - 48h.</div>
		<div>Du är även alltid välkommen att kontakta oss per telefon: <strong>0470-786833</strong> eller mail <a href="mailto:info@gpser.se">info@gpser.se</a>.</div>
        	
        	
        <div class="offertform" style="margin-top:20px;width:800px;">
        	<form action="index.php?route=custompage/offert" method="post" enctype="multipart/form-data" class="form-horizontal">
        		<fieldset id="offert">
        		<!--
        		<label for=""></label>
        		<input type="text" id="" class="form-control" placeholder="* Förnamn" />
        		<textarea name="comment" id="comment" class="form-control" placeholder="Meddelande"></textarea>
        		-->
        		<legend>Företagsinfo</legend>
        		
        		<div class="form-group required">
        			<label for="vat" class="col-sm-3 control-label">Org.nr:</label>
        			<div class="col-sm-7">
        				<input type="text" name="vat" id="vat" value="<?php echo $vat; ?>" class="form-control" placeholder="* Organisationsnummer" />
        				<?php if ($error_vat) { ?>
	              		<div class="text-danger"><?php echo $error_vat; ?></div>
	              		<?php } ?>
        			</div>
        		</div>
        		
        		<div class="form-group required">
        			<label for="name" class="col-sm-3 control-label">Företag:</label>
        			<div class="col-sm-7">
        				<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="form-control" placeholder="* Företagsnamn" />
	        			<?php if ($error_name) { ?>
	              		<div class="text-danger"><?php echo $error_name; ?></div>
	              		<?php } ?>
        			</div>
        		</div>
        		
        		<div class="form-group required">
        			<label for="address" class="col-sm-3 control-label">Adress:</label>
        			<div class="col-sm-7">
        				<input type="text" name="address" id="address" value="<?php echo $address; ?>" class="form-control" placeholder="* Adress" />
        				<?php if ($error_address) { ?>
	              		<div class="text-danger"><?php echo $error_address; ?></div>
	              		<?php } ?>
        			</div>
        		</div>
        		
        		<div class="form-group required">
        			<label for="zip" class="col-sm-3 control-label">Postnummer:</label>
        			<div class="col-sm-7">
        				<input type="text" name="zip" id="zip" value="<?php echo $zip; ?>" class="form-control" placeholder="* Postnummer" />
        				<?php if ($error_zip) { ?>
	              		<div class="text-danger"><?php echo $error_zip; ?></div>
	              		<?php } ?>
        			</div>
        		</div>
        		
        		<div class="form-group required">
        			<label for="city" class="col-sm-3 control-label">Stad:</label>
        			<div class="col-sm-7">
        				<input type="text" name="city" id="city" value="<?php echo $city; ?>" class="form-control" placeholder="* Stad" />
        				<?php if ($error_city) { ?>
	              		<div class="text-danger"><?php echo $error_city; ?></div>
	              		<?php } ?>
        			</div>
        		</div>
        		
        		<div class="form-group required">
        			<label for="phone" class="col-sm-3 control-label">Telefonnummer:</label>
        			<div class="col-sm-7">
        				<input type="text" name="phone" id="phone" value="<?php echo $phone; ?>" class="form-control" placeholder="* Telefonnummer" />
        				<?php if ($error_phone) { ?>
	              		<div class="text-danger"><?php echo $error_phone; ?></div>
	              		<?php } ?>
        			</div>
        		</div>
        		
        		<div class="form-group required">
        			<label for="email" class="col-sm-3 control-label">E-post:</label>
        			<div class="col-sm-7">
        				<input type="text" name="email" id="email" value="<?php echo $email; ?>" class="form-control" placeholder="* E-postadress" />
        				<?php if ($error_email) { ?>
	              		<div class="text-danger"><?php echo $error_email; ?></div>
	              		<?php } ?>
        			</div>
        		</div>
        		
        		<div class="form-group required">
	        		<legend>Produkter &amp; Tjänster</legend>
	        		<p>Offertförfrågan gäller någon eller några av nedanstående tjänster:</p>
	        		<?php
	        		// Catch POST variables
	        		for ( $i = 0; $i <= 3; $i++ ) { 
		        		if ( !empty( $_POST['servtype'][$i] ) ) {
		        			$servtype[$i] = 'checked="checked"';
		        		} else {
		        			$servtype[$i] = '';
		        		}
					}
	        		?>
		        	<label for="servtype" class="col-sm-3 control-label">Typ av tjänst:</label>
	        		<div class="col-sm-7">
		        		<div class="radio"><label><input type="checkbox" name="servtype[0]" value="1" <?php echo $servtype[0]; ?> /> Körjournaler</label></div>
		        		<div class="radio"><label><input type="checkbox" name="servtype[1]" value="2" <?php echo $servtype[1]; ?> /> Fordonsspårning</label></div>
		        		<div class="radio"><label><input type="checkbox" name="servtype[2]" value="3" <?php echo $servtype[2]; ?>/> Trygghetslarm</label></div>
		        		<div class="radio"><label><input type="checkbox" name="servtype[3]" value="4" <?php echo $servtype[3]; ?> /> Stöldskydd</label></div>
	        		</div>
	        	</div>
        		
        		<div class="form-group required">
        			<label for="devicenum" class="col-sm-3 control-label">Antal enheter:</label>
        			<div class="col-sm-7">
        				<select name="devicenum" id="devicenum" class="form-control">
        					<option value=""> --- Var god välj --- </option>
        					<?php
        					$postdevicenum = !empty( $_POST['devicenum'] ) ? $_POST['devicenum'] : NULL;
							
        					for ($i = 1; $i <= 100; $i++) {
        						if ( $postdevicenum == $i ) {
				        			$selected = 'selected="selected"';
				        		} else {
				        			$selected = '';
				        		}
        						?>
	        					<option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
        						<?php
        					} 
        					?>
        						<option value="101"> > 100</option>
        				</select>
        			</div>
        		</div>
        		
        		
        		<div class="form-group">
        			<label for="input-enquiry" class="col-sm-3 control-label">Meddelande:</label>
	        		<div class="col-sm-7"><textarea name="enquiry" rows="8" id="input-enquiry" class="form-control"></textarea></div>
        		</div>

							
		<div class="buttons">
          <div class="pull-right">

			  <div class="red_form_btn">
				  <input type="submit" value="Skicka offertförfrågan">
			  </div>

          </div>
        </div>


				

				<br />
				</fieldset>
        	</form>
        	
        	
        	
        </div>
    </div>
    </div>
    </div>
<?php echo $footer; ?>