<?php
// Heading
$_['heading_title']     = 'Språk';  

// Text
$_['text_success']      = 'Klart: Du har ändrat i Språk!'; 
$_['text_list']         = 'Layout Lista';
$_['text_add']          = 'Lägg till Språk';
$_['text_edit']         = 'Ändra i Språk';

// Column
$_['column_name']       = 'Språk';
$_['column_code']       = 'Kod';
$_['column_sort_order'] = 'Sorteringsordning';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_name']        = 'Språk:';
$_['entry_code']        = 'Kod:';
$_['entry_locale']      = 'Lokalt';
$_['entry_image']       = 'Bild:';
$_['entry_directory']   = 'Mapp:';
$_['entry_status']      = 'Status:';
$_['entry_sort_order']  = 'Sorteringsordning:';
$_['entry_currency']    = 'Valuta';

// Help
$_['help_code']         = 'Förslag: en. Do not change if this is your default language.';
$_['help_locale']       = 'Förslag: en_US.UTF-8,en_US,en-gb,en_gb,english';
$_['help_image']        = 'Förslag: gb.png';
$_['help_directory']    = 'Namn på språk mappen';
$_['help_currency']     = 'Valuta standard för språket';
$_['help_status']       = 'Göm/Visa i språk lista';

// Error
$_['error_permission']  = 'Varning: Du har inte behörighet att ändra i Språk!';
$_['error_name']        = 'Språkets namn måste innehålla mellan 3 och 32 tecken!';
$_['error_code']        = 'Koden måste innehålla minst 2 tecken!';
$_['error_locale']      = 'Uppgifter under Lokalt krävs!';
$_['error_image']       = 'Bildfilnamnet måste innehålla mellan 3 och 64 tecken!';
$_['error_directory']   = 'Målkällmapp krävs!';
$_['error_default']     = 'Varning: Detta språk kan inte raderas eftersom det är kopplat till butikens förvalda språk!';
$_['error_admin']       = 'Varning: Detta språk kan inte raderas eftersom det är kopplat till administrationsspråket!';
$_['error_store']       = 'Varning: Detta språk kan inte raderas eftersom det är kopplat till %s butiker!';
$_['error_order']       = 'Varning: Detta språk kan inte raderas eftersom det är kopplat till %s ordrar!';
?>