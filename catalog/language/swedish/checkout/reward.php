<?php
// Heading
$_['heading_title'] = 'Använd Dina Bonupoäng %s)';

// Text
$_['text_success']  = 'Klart: Dina Bonupoäng har blivit godkända!';

// Entry
$_['entry_reward']  = 'Bonupoäng att använda (Max %s)';

// Error
$_['error_reward']  = 'Varning: Ange hur många Bonupoäng du vill använda!!';
$_['error_points']  = 'Varning: Du har inte %s Bonupoäng!';
$_['error_maximum'] = 'Varning: Maximum Bonupoäng du kan använda här är %s!';
?>