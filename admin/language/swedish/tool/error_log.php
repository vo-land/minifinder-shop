<?php
// Heading
$_['heading_title'] = 'Fellogg';

// Text
$_['text_success']     = 'Klart: Du har ändrat i Fel-loggen!';
$_['text_list']        = 'Errors Lista';

// Error
$_['error_warning']			= 'Varning: Din error loggfil %s är %s!';
$_['error_permission']    	= 'Varning: Du har inte tillåtelse att tömma error filen!';
?>