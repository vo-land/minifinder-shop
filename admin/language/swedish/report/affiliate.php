<?php
// Heading
$_['heading_title']     = 'Rapport Återförsäljare Provision';

// Text
$_['text_list']         = 'Återförsäljare Provisions Lista';

// Column
$_['column_affiliate']  = 'Återförsäljare Namn';
$_['column_email']      = 'E-Mail';
$_['column_status']     = 'Status';
$_['column_commission'] = 'Provision';
$_['column_orders']     = 'Antal Orders';
$_['column_total']      = 'Totalt';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_date_start']      = 'Startdatum:';
$_['entry_date_end']        = 'Slutdatum:';
?>