<?php
// Heading 
$_['heading_title']    = 'Produktfeed';

// Text
$_['text_success']     = 'Klart: Du har nu ändrat i Produktfeed!';
$_['text_list']        = 'Layout Lista';

// Column
$_['column_name']      = 'Produktfeednamn';
$_['column_status']    = 'Status';
$_['column_action']    = 'Ändra';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i produktfeed!';
?>