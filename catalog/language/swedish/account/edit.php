<?php
// Heading 
$_['heading_title']     = 'Mitt konto';

// Text
$_['text_account']      = 'Konto';
$_['text_edit']         = 'Redigera information';
$_['text_your_details'] = 'Personuppgifter';
$_['text_success']      = 'Klart: Ditt konto har uppdaterats.';

// Entry
$_['entry_firstname']  = 'Förnamn:';
$_['entry_lastname']   = 'Efternamn:';
$_['entry_email']      = 'E-postadress:';
$_['entry_telephone']  = 'Mobiltelefon:';
$_['entry_fax']        = 'Telefon:';

// Error
$_['error_exists']     = 'Fel: E-postadressen är redan registrerad!';
$_['error_firstname']  = 'Förnamnet måste bestå av minst 1 och högst 32 tecken!';
$_['error_lastname']   = 'Efternamnet måste bestå av minst 1 och högst 32 tecken!';
$_['error_email']      = 'E-postadressen verkar inte vara giltig!';
$_['error_telephone']  = 'Telefonnumret måste bestå av minst 3 och högst 32 tecken!';
$_['error_custom_field'] = '%s krävs!';
?>