<?php
// Heading
$_['heading_title']     = 'Senaste Orders';

// Column
$_['column_order_id']   = 'Order ID';
$_['column_customer']   = 'Kund';
$_['column_status']     = 'Status';
$_['column_total']      = 'Total';
$_['column_date_added'] = 'Datum';
$_['column_action']     = 'Klart';
?>