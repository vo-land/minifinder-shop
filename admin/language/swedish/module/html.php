<?php
// Heading
$_['heading_title']     = 'HTML Innehåll';

// Text
$_['text_module']       = 'Moduler';
$_['text_success']      = 'Klart: Du har ändrat i HTML Innehåll!';
$_['text_edit']         = 'Ändra i HTML Innehåll';

// Entry
$_['entry_name']        = 'Modul Namn';
$_['entry_title']       = 'Rubrik Titeln';
$_['entry_description'] = 'Innehåll';
$_['entry_status']      = 'Status';

// Error
$_['error_permission']  = 'Varning: Du har inte tillåtelse att ändra i modulen.';
$_['error_name']        = 'Modul Namn måste vara mellan 3-64 tecken.';