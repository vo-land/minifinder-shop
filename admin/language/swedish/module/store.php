<?php
// Heading
$_['heading_title']       = 'Butik';

// Text
$_['text_module']         = 'Moduler';
$_['text_success']        = 'Klart: Du har ändrat i modulen Butik!';
$_['text_edit']           = 'Ändra i Butik modulen';

// Entry
$_['entry_admin']         = 'Endast adminanvändare:';
$_['entry_status']        = 'Status:';

// Error
$_['error_permission']    = 'Varning: Du har inte behörighet att ändra i modulen!';
?>