<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php $last_breadcrumb = array_pop($breadcrumbs); ?>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
    <li><?php echo $last_breadcrumb['text']; ?></li>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> success_error"><?php echo $content_top; ?>
      <h1><?php /*echo $heading_title;*/ ?>Ansökan skickad</h1>
      <?php /*echo $text_message;*/ ?>Tack för din ansökan!<br /> 
      Dina uppgifter kommer att behandlas snarast. Du kommer inom 12-48h få besked ifall du godkänns som återförsäljare.
      <div class="buttons">
        <a href="<?php echo $continue; ?>" class="red_button">
          <span>Ok</span>
          <span>Ok</span>
        </a>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>