<?php
// Heading
$_['heading_title']      = 'Offertförfrågan';

// Text
$_['breadcrumb_title']      = 'Offertförfrågan';
$_['text_success']   = '<p>Din offertförfrågan har nu skickats till oss!<br />Vi besvarar med en offert inom 12- 48h.</p>';

// E-mail
$_['email_subject']			= 'Offert: %s';

// Errors
$_['error_vat']     	= 'Org.nr. måste bestå av 10 tecken långt nummer enl. formatet: XXXXXX-XXXX';
$_['error_name']     	= 'Namnet måste bestå av minst 3 och högst 32 tecken!';
$_['error_address']     = 'Adressen måste bestå av minst 3 och högst 40 tecken!';
$_['error_zip']     	= 'Postnummer måste anges eller ej giltig!';
$_['error_city']     	= 'Stad måste anges!';
$_['error_phone']     	= 'Telefonnummer måste anges!';
$_['error_email']    	= 'E-postadress verkar inte vara giltig!';
$_['error_enquiry']  	= 'Förfrågan måste bestå av minst 10 och högst 3000 tecken!';
$_['error_captcha']  	= 'Verifikationskoden matchar inte bilden!';
?>