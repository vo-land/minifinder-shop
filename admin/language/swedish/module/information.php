<?php
// Heading
$_['heading_title']       = 'Information';

// Text
$_['text_module']         = 'Moduler';
$_['text_success']        = 'Klart: Du har ändrat i modulen information!';
$_['text_edit']           = 'Ändra i Informationen';

// Entry
$_['entry_status']        = 'Status:';

// Error
$_['error_permission']    = 'Varning: Du har inte behörighet att ändra i modulen information!';
?>