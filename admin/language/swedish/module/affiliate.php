<?php
// Heading
$_['heading_title']       = 'Återförsäljare';

$_['text_module']         = 'Moduler';
$_['text_success']        = 'Klart: Du har ändrat i modulen Återförsäljare!';
$_['text_edit']           = 'Ändra i återförsäljare modulen';

// Entry
$_['entry_status']        = 'Status:';

// Error
$_['error_permission']    = 'Varning: Du har inte behörighet att ändra i modulen Återförsäljare!';
?>