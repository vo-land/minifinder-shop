<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />

<!--link rel="alternate" hreflang="en" href="http://shop.minifinder.se/" />
<link rel="alternate" hreflang="de" href="http://shop.minifinder.se/de/" />
<link rel="alternate" hreflang="es" href="http://shop.minifinder.se/es/" />
<link rel="alternate" hreflang="nl" href="http://shop.minifinder.se/nl/" /-->

<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!--<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900,400italic|Armata|Aldrich' rel='stylesheet' type='text/css'>-->
<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Aldrich" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Armata" rel="stylesheet">
<link href="catalog/view/theme/minifinder/stylesheet/stylesheet.css?time=<?php echo time(); ?>" rel="stylesheet">
<script src="catalog/view/javascript/jquery.countdown-2.0.4/jquery.countdown.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/readmore.min.js" type="text/javascript"></script>
<!--script src="catalog/liveadmin/client.php?key=LFACEA5DEV23F5EAA7M9B78BAA" type="text/javascript"></script-->
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common1.js?time=<?php echo time(); ?>" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php echo $google_analytics; ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47466388-5', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
var google_replace_number="+46(0)470-78 68 33";
(function(a,e,c,f,g,b,d){var h={ak:"1016491089",cl:"_YbqCJ-3jGIQ0djZ5AM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[f]||(a[f]=h.ak);b=e.createElement(g);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(g)[0];d.parentNode.insertBefore(b,d);a._googWcmGet=function(b,d,e){a[c](2,b,h,d,null,new Date,e)}})(window,document,"_googWcmImpl","_googWcmAk","script");
</script>
</head>
<body class="<?php echo $class; ?>">
<nav id="top">
  <div class="container">
    <?php echo $language; ?>
    <?php echo $currency; ?>
      <div class="pull-left">
          <div class="tax_include">
              <label for="tax_include" class="control-label<?php echo ($tax_include ? ' checked' : ''); ?>">
                  <input type="checkbox" id="tax_include" class="styled"<?php echo ($tax_include ? ' checked="checked"' : ''); ?>>
                  <span class="text" title=""><?php echo $text_tax;?></span>
              </label>
            </div>
          <script>
              $('#tax_include').on('change', function () {
                  var tax = $(this).prop('checked');
                  $(this).parent().toggleClass('checked');
                  $.post('index.php?route=common/header/tax_toggle', {tax: tax}, function (data) {
                      data.success && (location = data.location);
                  }, 'json');
              });
          </script>
      </div>
	<div class="nav pull-left we_ship_country">
		 <ul class="list-inline we_ship">
		 	<li>
                <!--<i class="fa fa-paper-plane"></i> We ship to <span class="ship-country" title=""> Ukraine</span>-->
		   	<?php
			    $country = @geoip_country_name_by_name($_SERVER['REMOTE_ADDR']);
			if ( $country != 'Sweden' && $_SERVER['REMOTE_ADDR'] != '192.168.1.1') {
			    echo '<i class="fa fa-paper-plane"></i> We ship to: <span class="ship-country" title="">' . $country . '</span>';
			}
			?>
			</li>
		</ul>
	</div>
		
    <div id="top-links" class="nav pull-right">
      <ul class="list-inline">
      	<!--li><a href="index.php?route=custompage/offert" class="btn-link"><i class="fa fa-dot-circle-o"></i> <span class="hidden-xs hidden-sm">&nbsp;Offertförfrågan</span></a></li>
      	<li><a href="index.php?route=custompage/reseller" class="btn-link"><i class="fa fa-suitcase"></i> <span class="hidden-xs hidden-sm">&nbsp;Bli återförsäljare</span></a></li-->
        <li><a href="<?php echo $reseller; ?>" class="btn-link"><span class="hidden-xs hidden-sm"><?php echo $text_reseller; ?></span></a></li>
        <li><a href="<?php echo $contact; ?>"><!--<i class="fa fa-phone-square"></i>--> <span class="hidden-xs hidden-sm hidden-md">&nbsp; <?php echo $text_contact; ?></span></a></li>
      	<li class="hidden-xs hidden-sm hidden-md"><a href="<?php echo $terms; ?>" class="btn-link"><!--<i class="fa fa-file-text"></i>--> <span class="hidden-xs hidden-sm hidden-md">&nbsp;<?php echo $text_terms; ?></span></a></li>
        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle info_icon" data-toggle="dropdown"><!--<i class="fa fa-question-circle"></i>-->&nbsp; <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_help; ?></span> <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            
            <?php }?>
            <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
            <!-- li><a href="./how-it-works">How does it work?</a></li -->
            <!--<li><a href="./faq">FAQ</a></li>-->
            <li><a href="<?=$faq;?>"><?php echo $text_faq; ?></a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle login_icon" data-toggle="dropdown"><!--<i class="fa fa-user"></i>&nbsp;--> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <?php } ?>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-4 header_logo">
        <div id="logo"><?php if ($logo) { ?><a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
      <!--<div class="col-sm-2">--><!--div class="check-list"><img src="image/catalog/check_list.png" title="Säker e-handel" alt="Säker e-handel - GPSER Sweden AB" class="hidden-xs hidden-sm hidden-md img-responsive" /></div--><!-- </div>-->
      <!--div class="col-sm-1">
      	<div id="trygg-ehandel" class="hidden-xs hidden-sm hidden-md">  	
  			<a id="celink1" href="https://www.ehandelscertifiering.se/"><img id="ceimg1" alt="ehandelscertifiering" src="https://www.ehandelscertifiering.se/lv5/preload.png"></a>
  			<script src="https://www.ehandelscertifiering.se/lv5/bootstrap.php?size=65&amp;url=www.gpser.se&amp;lang=en&amp;autolang=no&amp;pos=centerbottom&amp;nr=1" defer="defer" type="text/javascript"></script>
		</div>
      </div -->
      <!--<div class="col-sm-3 hidden-xs"><?php echo $search; ?></div>-->
      <div class="header_cart pull-right"><?php echo $cart; ?></div>
      <div class="header_search hidden-xs pull-right"><?php echo $search; ?></div>
      <!--<div class="col-sm-3"><?php echo $cart; ?></div>-->

    </div>
  </div>
</header>
<?php if ($categories) { ?>
<!--div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div-->
<?php }
?>
