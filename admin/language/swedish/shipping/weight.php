<?php
// Heading
$_['heading_title']    = 'Viktbaserad frakt';

// Text
$_['text_shipping']    = 'Frakt';
$_['text_success']     = 'Klart: Du har ändrat i Viktbaserad frakt!';
$_['text_edit']        = 'Ändra i Viktbaserad frakt';

// Entry
$_['entry_rate']       = 'Prislista:<br /><span class="help">Exempel: 5:10.00,7:12.00 Vikt:pris,vikt:pris, osv..</span>';
$_['entry_tax_class']  = 'Moms:';
$_['entry_geo_zone']   = 'GeoZon:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sorteringsordning:';

// Help
$_['help_rate']        = 'Exempel: 5:10.00,7:12.00 Vikt:Kostnad,Vikt:Kostnad, etc..';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra!';
?>