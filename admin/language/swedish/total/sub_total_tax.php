<?php
// Heading
$_['heading_title']    = 'Delsumma inkl moms';

// Text
$_['text_total']       = 'Ordertotal';
$_['text_success']     = 'Klart: Du har modifierat delsumman!';
$_['text_edit']        = 'Ändra Delsumman';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sorteringsordning:';

// Error
$_['error_permission'] = 'Varning: Du kan inte modifiera delsumman!';
?>