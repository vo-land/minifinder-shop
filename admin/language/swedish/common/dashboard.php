<?php
// Heading
$_['heading_title']                = 'Information';

// Text
$_['text_order_total']             = 'Antal Orders';
$_['text_customer_total']          = 'Antal Kunder';
$_['text_sale_total']              = 'Omsättning försälning';
$_['text_online_total']            = 'Kunder Online';
$_['text_map']                     = 'World Map';
$_['text_sale']                    = 'Försäljnings Analys';
$_['text_activity']                = 'Senast Akrivitet';
$_['text_recent']                  = 'Senaste Orders';
$_['text_order']                   = 'Orders';
$_['text_customer']                = 'Kunder';
$_['text_day']                     = 'Idag';
$_['text_week']                    = 'Vecka';
$_['text_month']                   = 'Månad';
$_['text_year']                    = 'År';
$_['text_view']                    = 'Se mer...';

// Error
$_['error_install']                = 'Varning: Install mappen finns fortfarande kvar!';
?>