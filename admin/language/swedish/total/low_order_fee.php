<?php
// Heading
$_['heading_title']    = 'Avgift vid låg ordersumma';

// Text
$_['text_total']       = 'Ordertotal';
$_['text_success']     = 'Klart: Du har ändrat!';
$_['text_edit']        = 'Ändra i låg ordersumma';

// Entry
$_['entry_total']      = 'Summa:';
$_['entry_fee']        = 'Avgift:';
$_['entry_tax_class']  = 'Moms:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sorteringsordning:';

// Help
$_['help_total']       = 'Totalsumma måste uppgå till minimum summan innan köp godkänns.';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra!';
?>