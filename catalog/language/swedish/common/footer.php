<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Kundtjänst';
$_['text_extra']        = 'Övrigt';
$_['text_contact']      = 'Kontakta oss';
$_['text_return']       = 'Returer';
$_['text_sitemap']      = 'Sidkarta';
$_['text_manufacturer'] = 'Tillverkare';
$_['text_voucher']      = 'Presentkort';
$_['text_affiliate']    = 'Affiliate';
$_['text_special']      = 'Kampanjer';
$_['text_account']      = 'Mitt konto';
$_['text_order']        = 'Orderhistorik';
$_['text_wishlist']     = 'Önskelista';
$_['text_newsletter']   = 'Nyhetsbrev';
$_['text_powered']      = '<a href="https://shop.minifinder.se/" class="gpser-green">MiniFinder Sweden AB</a> (org.nr: 556909-5697)<br /> &copy; 2011 - '.date('Y');
$_['text_accepted_payment']      = 'VI ACCEPTERAR'; // custom
?>