<?php
// Heading
$_['heading_title']      = 'Geozoner - Specifika områden';

// Text
$_['text_success']       = 'Klart: Du har ändrat i geozoner!';
$_['text_list']          = 'Layout Lista';
$_['text_add']           = 'Lägg till Geozoner';
$_['text_edit']          = 'Ändra i Geozoner';

// Column
$_['column_name']        = 'Geozonnamn';
$_['column_description'] = 'Beskrivning';
$_['column_action']      = 'Ändra';

// Entry
$_['entry_name']         = 'Geozonenamn:';
$_['entry_description']  = 'Beskrivning:';
$_['entry_country']      = 'Land:';
$_['entry_zone']         = 'Zon:';

// Error
$_['error_permission']   = 'Varning: Du har inte behörighet att ändra i geozoner!';
$_['error_name']         = 'Geozonnamnet måste innehålla mellan 3 och 32 tecken!';
$_['error_description']  = 'Beskrivningen måste innehålla mellan 3 and 255 tecken!';
$_['error_tax_rate']     = 'Varning: Denna geozon kan inte raderas eftersom den är kopplad till en eller flera skatter!';
?>