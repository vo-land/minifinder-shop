<?php
// Heading
$_['heading_title']         = 'Extra fält';

// Text
$_['text_success']          = 'Klart: Du har nu ändrat i Extra fält!';
$_['text_list']             = 'Extra Fält Lista';
$_['text_add']              = 'Lägg till Extra Fält';
$_['text_edit']             = 'Ändra i Extra Fält';
$_['text_choose']           = 'Välj';
$_['text_select']           = 'Rullgardin';
$_['text_radio']            = 'Radioknapp';
$_['text_checkbox']         = 'Checkbox';
$_['text_input']            = 'Infoga';
$_['text_text']             = 'Text';
$_['text_textarea']         = 'Textfält';
$_['text_file']             = 'Fil';
$_['text_date']             = 'Datum';
$_['text_datetime']         = 'Datum &amp; Tid';
$_['text_time']             = 'Tid';
$_['text_account']          = 'Konto';
$_['text_address']          = 'Adress';

// Column
$_['column_name']           = 'Extra fält Namn';
$_['column_location']       = 'Plats';
$_['column_type']           = 'Typ';
$_['column_sort_order']     = 'Sorteringsordning';
$_['column_action']         = 'Ändra';

// Entry
$_['entry_name']            = 'Extra fält Namn';
$_['entry_location']        = 'Plats';
$_['entry_type']            = 'Typ';
$_['entry_value']           = 'Värde';
$_['entry_custom_value']    = 'Extra fält värde namn';
$_['entry_customer_group']  = 'Kundgrupp';
$_['entry_required']        = 'Krävs';
$_['entry_status']          = 'Status';
$_['entry_sort_order']      = 'Sorteringsordningr';

// Help
$_['help_sort_order']       = 'Använd minus för att räkna bakåt i sorteringslistan.';

// Error
$_['error_permission']      = 'Varning: Du har inte tillåtelse att ändra här!';
$_['error_name']            = 'Extra fält Namn måste ha mellan 1-128 tecken!';
$_['error_type']            = 'Varning: Extra fält värd namn krävs!';
$_['error_custom_value']    = 'Extra fält värd namn måste ha mellan 1-128 tecken!';
?>