<?php
// Heading
$_['heading_title']       = 'Karusell';

// Text
$_['text_module']         = 'Moduler';
$_['text_success']        = 'Klart: Du har ändrat i modulen karusell!';
$_['text_edit']           = 'Ändra i Karusellen.';

// Entry
$_['entry_name']       = 'Modul Namn';
$_['entry_banner']        = 'Banner:';
$_['entry_width']         = 'Bredd';
$_['entry_height']        = 'Höjd';
$_['entry_status']        = 'Status:';

// Error
$_['error_permission']   = 'Varning: Du har inte behörighet!';
$_['error_name']       = 'Modul Namn måste vara mellan 3-64 tecken.!';
$_['error_width']      = 'Bredd krävs!';
$_['error_height']     = 'Höjd krävs!';
?>