<?php
// Heading
$_['heading_title']       = 'Super Html Content Module';

// Text
$_['text_module']         = 'Module Info';
$_['text_success']        = 'Success: You have modified module Super Html Content Module!';
$_['text_success_groups'] = 'Success: You saved the SuperHtml groups and styles!';
$_['text_image_manager']  = 'Image Manager';
$_['text_browse']         = 'Change Image';
$_['text_clear']          = 'Clear Image';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_edit']           = 'Edit Content';
$_['tab_modules']         = 'Modules';
$_['tab_groups']          = 'Html Groups';
$_['button_group']        = '<strong style="font-size: 18px;">+</strong> Group';
$_['button_column']       = '<strong style="font-size: 18px;">+</strong> Column';
$_['button_add_module']   = 'Add module';
$_['group_name']          = 'Group Name:';
$_['group_columns']       = 'Group Columns:';
$_['column_name']         = 'Column Heading & Content:';
$_['column_content']      = 'Column Content:';
$_['text_h1_color']       = 'Module heading color:';
$_['text_h1_border_color']  = 'Module heading border color:';
$_['text_h1_bg_color']    = 'Module heading background color:';
$_['text_h1_font_size']   = 'Module heading font size:';
$_['text_h1_font']        = 'Module heading font:';
$_['text_h2_font_size']   = 'Column heading font size:';
$_['text_h2_font']        = 'Column heading font:';
$_['text_h2_color']       = 'Column heading color:';
$_['text_content_font_size'] = 'Content font size (default):';
$_['text_content_font']      = 'Content font (default):';
$_['text_content_color']     = 'Content color (default):';
$_['text_column_name']       = 'Name<span class="minitext"> (optional)</span>';
$_['text_column_image']      = 'Image<span class="minitext"> (optional)</span>';
$_['text_column_youtube']    = 'Youtube video url<span class="minitext"> (optional)</span>';
$_['text_column_url']        = 'Name link url<span class="minitext"> (optional)</span>';
$_['text_create_module']     = 'Create a SuperHtml module';
$_['text_edit_module']       = 'Edit this SuperHtml module';
$_['text_save_mod']          = 'Save Module';
$_['text_create_mod']        = 'Create Module';
$_['text_save_groups']       = 'Save Groups &amp; Styles';
$_['text_save_info']         = 'This page contains 2 forms, one with settings for the module instance and one with general module info so each section has its own save button that when clicked saves only the data from that section.';

// Entry
$_['entry_description']   = 'Content:';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
$_['entry_selectgroup']   = 'Html Group to display:';
$_['entry_nocol']  		  = 'Columns per row:';
$_['entry_gtitle']        = 'Display Group Title:';
$_['entry_ctitle']  	  = 'Display Column Title:';
$_['entry_make_carousel'] = 'Display in carousel';
$_['entry_min_width']     = 'Column minimum width <br /><span class="help">For responsive resizing</span>';
$_['entry_carousel_items_row'] = 'Items per row:';
$_['entry_carousel_desktop'] = 'Desktop:';
$_['entry_carousel_tablet'] = 'Tablet:';
$_['entry_carousel_mobile'] = 'Mobile:';
$_['entry_carousel_speed'] = 'Carousel Speed:';
$_['entry_carousel_autoplay'] = 'Autoplay:';
$_['entry_styles'] = 'Style Customization';
$_['entry_name']          = 'Module Name';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify the Super Html Content Module!';
?>