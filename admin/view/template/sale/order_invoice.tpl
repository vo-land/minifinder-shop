<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link href="../image/catalog/favicon.png" rel="icon" />
<script src="../catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
<script type="text/javascript">
	window.print();
</script>
</head>
<body>
<div class="container">
  <?php 
  // Controller
  if ( isset( $_GET['waybill'] ) ) {
  	$inv_title = 'FÖLJESEDEL';
  } else {
  	$inv_title = 'FAKTURA';
  }
  
  foreach ($orders as $order) { ?>
  <div style="page-break-after: always;margin-top:30px;">
    <!--h1><?php echo $text_invoice; ?> #<?php echo $order['order_id']; ?></h1-->
    <table class="table table-bordered">
      <thead>
        <tr>
        	<td style="border:0"><img src="/image/catalog/minifinder_logo.png" alt="MiniFinder Sweden AB" style="height:45px" /></td>
        	<td style="border:none;font-size:30px;color:#ddd !important;"><?php echo $inv_title; ?></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width: 50%;"><address>
            <strong><?php echo $order['store_name']; ?></strong><br />
            <?php echo $order['store_address']; ?><br />
            </address>
            <b><?php echo $text_telephone; ?></b> <?php echo $order['store_telephone']; ?><br />
            <?php if ($order['store_fax']) { ?>
            <b><?php echo $text_fax; ?></b> <?php echo $order['store_fax']; ?><br />
            <?php } ?>
            <b><?php echo $text_email; ?></b> <?php echo $order['store_email']; ?><br />
            <b><?php echo $text_website; ?></b> <?php echo $order['store_url']; ?></td>
          
          <td style="width: 50%;vertical-align: top;">
            <span style="border:1px solid #666;padding:4px;display: inline-block"><b><?php echo $text_order_id; ?></b> <?php echo $order['order_id']; ?><br /></span><br /><br />
          	<b><?php echo $text_date_added; ?></b> <?php echo $order['date_added']; ?><br />
            <b><?php echo $text_payment_method; ?></b> <?php echo $order['payment_method']; ?><br />
            <?php if ($order['shipping_method']) { ?>
            <b><?php echo $text_shipping_method; ?></b> <?php echo $order['shipping_method']; ?><br />
            <?php } ?></td>
        </tr>
      </tbody>
    </table>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td style="width: 50%;"><b><?php echo $text_to; ?></b></td>
          <td style="width: 50%;" class="address-pop"><b><?php echo $text_ship_to; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><address>
            <?php echo $order['payment_address']; ?>
            </address></td>
          <td><address>
            <?php echo $order['shipping_address']; ?>
            </address>
             <script type="text/javascript">
	      		$(document).ready(function() {
	      			
	      			$('.address-pop').click( function() {
	      				
	      				var display = $('.address-copy').css("display");
	      				if ( display == 'none' ) {
	      					$('.address-copy').css("display","block");	
	      				} else {
	      					$('.address-copy').css("display","none");
	      				}
	      			});
	      			
	      			$('.hideme').click( function() {
	      				$('.address-copy').css("display","none");
	      			});
	      			
	      		});
	      	</script>
		      <!-- adress pops up when "leveransadress" clicked -->
		      <div class="address-copy" style="display:none">
		      	<?php 
		      	if (!empty( $order['payment_company'] )) {
		      	?>
		      	<input type="text" size="25" name="company" onClick="this.select();" value="<?php echo $order['shipping_company']; ?>" /><br/>
		      	<?php
				} 
		      	?>
		      	<input type="text" size="25" name="name" onClick="this.select();" value="<?php echo $order['shipping_firstname'] . ' ' . $order['shipping_lastname']; ?>" /><br/>
		      	<input type="text" size="25" name="address1" onClick="this.select();" value="<?php echo $order['shipping_address_1']; ?>" /><br/>
		      	<?php 
		      	if (!empty( $order['shipping_address_2'] )) {
		      	?>
		      	<input type="text" size="25" name="address2" onClick="this.select();" value="<?php echo $order['shipping_address_2']; ?>" /><br/>
		      	<?php
				} 
		      	?>
		      	<input type="text" size="5" name="postcode" onClick="this.select();" value="<?php echo $order['shipping_postcode']; ?>" /> <input type="text" size="15" name="city" onClick="this.select();" value="<?php echo $order['shipping_city']; ?>" /><br/>
		      	<input type="text" size="20" name="phone" onClick="this.select();" value="<?php echo $order['telephone']; ?>" /><br />
		      </div>
            </td>
        </tr>
      </tbody>
    </table>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $column_product; ?></b></td>
          <td><b><?php echo $column_model; ?></b></td>
          <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
          <?php  if ( !isset( $_GET['waybill'] ) ) { ?>
          <td class="text-right"><b><?php echo $column_price; ?></b></td>
          <td class="text-right"><b><?php echo $column_total; ?></b></td>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr>
          <td><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
          <td><?php echo $product['model']; ?></td>
          <td class="text-right"><?php echo $product['quantity']; ?></td>
          <?php  if ( !isset( $_GET['waybill'] ) ) { ?>
          <td class="text-right"><?php echo $product['price']; ?></td>
          <td class="text-right"><?php echo $product['total']; ?></td>
          <?php } ?>
        </tr>
        <?php } ?>
        <?php 
        if ( !isset( $_GET['waybill'] ) ) {
        foreach ($order['voucher'] as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td class="text-right">1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['total'] as $total) { ?>
        <tr>
          <td class="text-right" colspan="4"><b><?php echo $total['title']; ?></b></td>
          <td class="text-right"><?php echo $total['text']; ?></td>
        </tr>
        <?php } 
		}
        ?>
      </tbody>
    </table>
    <?php if ($order['comment']) { 
    	// Do not print it - custom modified
    	?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $column_comment; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $order['comment']; ?></td>
        </tr>
      </tbody>
    </table>
    <?php } ?>
  </div>
  <?php } ?>
</div>
</body>
</html>