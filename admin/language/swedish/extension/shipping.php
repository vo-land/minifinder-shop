<?php
// Heading
$_['heading_title']     = 'Frakter';

// Text
$_['text_success']      = 'Klart: Du har nu ändrat i Frakter!';
$_['text_list']         = 'Layoutlista';

// Column
$_['column_name']       = 'Fraktmetod';
$_['column_status']     = 'Status';
$_['column_sort_order'] = 'Sorteringsordning';
$_['column_action']     = 'Ändra';

// Error
$_['error_permission']  = 'Varning: Du har inte behörighet att ändra i Frakt!';
?>