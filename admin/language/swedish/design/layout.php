<?php
// Heading
$_['heading_title']       = 'Layout';

// Text
$_['text_success']        = 'Klart: Du har ändrat i layout!';
$_['text_list']           = 'Layoutlista';
$_['text_add']            = 'Lägg till Layout';
$_['text_edit']           = 'Ändra layout';
$_['text_default']        = 'Förvalt';
$_['text_content_top']    = 'Topp';
$_['text_content_bottom'] = 'Botten';
$_['text_column_left']    = 'Vänster';
$_['text_column_right']   = 'Höger';

// Column
$_['column_name']       = 'Layoutnamn';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_name']          = 'Layoutnamn:';
$_['entry_store']         = 'Butik:';
$_['entry_route']         = 'Sökväg:';
$_['entry_module']        = 'Moduler';
$_['entry_position']      = 'Position';
$_['entry_sort_order']    = 'Ordning';

// Error
$_['error_permission']  = 'Varning: Du har inte behörighet att ändra i Layout!';
$_['error_name']        = 'Layoutnamnet måste vara mellan 3 och 64 tecken!';
$_['error_default']     = 'Varning: Denna layout kan inte raderas eftersom den är kopplad till butikens layout!';
$_['error_store']       = 'Varning: Denna layout kan inte raderas eftersom den är kopplad till %s butiker!';
$_['error_product']     = 'Varning: Denna layout kan inte raderas eftersom den är kopplad till %s produkter!';
$_['error_category']    = 'Varning: Denna layout kan inte raderas eftersom den är kopplad till %s kategorier!';
$_['error_information'] = 'Varning: Denna layout kan inte raderas eftersom den är kopplad till %s informationssidor!';
?>