<?php
// Heading
$_['heading_title']     = 'Annonsspårning';

// Text
$_['text_success']      = 'Klart: Du har nu ändrat i Försäljning Spårning!';
$_['text_list']         = 'Layout Lista';
$_['text_add']          = 'Lägg till en Försäljning Spårning';
$_['text_edit']         = 'Ändra i Försäljning Spårning';

// Column
$_['column_name']       = 'Kampanj';
$_['column_code']       = 'Kod';
$_['column_clicks']     = 'Antal klick';
$_['column_orders']     = 'Orders';
$_['column_date_added'] = 'Datum';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_name']        = 'Kampanj';
$_['entry_description'] = 'Kampanjbeskrivning';
$_['entry_code']        = 'Spårningskod';
$_['entry_example']     = 'Exempel';
$_['entry_date_added']  = 'Datum';

// Help
$_['help_code']         = 'Spårnings koden som ska användas för att söka på kampanjen.';
$_['help_example']      = 'Så att systemet kan lägga till koden i slutet på din URL.';

// Error
$_['error_permission']  = 'Varning: Du har inte tillåtelse att ändra.';
$_['error_name']        = 'Kampanj måste vara mellan 1-32 tecken!';
$_['error_code']        = 'Spårnings kod krävs!';
?>