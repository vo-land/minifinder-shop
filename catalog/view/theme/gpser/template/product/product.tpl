<?php echo $header; ?>

<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
  <?php 
  
  //if ( $_SERVER['REMOTE_ADDR'] == '192.168.1.1' ) {
  	if ( date('Y-m-d') < '2016-11-29' ) {
  	?>
  	<div class="black-friday-product" style="border-radius: 4px;background: #80b22c; font-size: 18px; color:white; padding:15px; margin-bottom:20px; ">
  		&raquo; CYBER MONDAY KAMPANJ! &laquo; <span style="color:#000;text-decoration: underline;font-weight:bold;">15% rabatt</span> i hela butiken. Kampanjen upphör om <span id="blackclock" style="color:#000;font-weight:bold;"></span>
  	</div>
		  
	<script type="text/javascript">
	/* Kampanj tom */
	$('#blackclock').countdown('2016/11/28 23:59', function(event) {
		$(this).html(event.strftime('%Dd %Ht %Mm %Ss'));
	});
	</script>
  	<?php
  	
  }
  ?>
  
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row">
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-8'; ?>
        <?php } ?>
        <div class="col-sm-12">
    	  <h1 style="margin-top:5px;"><?php echo $heading_title; ?></h1>
    	  <p class="prod-name2"><?php echo $heading_title2; ?></p>
    	</div>
        <div class="<?php echo $class; ?>">
          <?php if ($thumb || $images) { ?>
          <ul class="thumbnails">
            <?php if ($thumb) { ?>
            <li><a class="thumbnail headpopup" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" class="headthumb" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
            <?php } ?>
            <?php if ($images) { ?>
            <?php
            $i = 1; 
            foreach ($images as $image) { 
            $i++;
            ?>
            <li class="image-additional"><a class="thumbnail add-thumb" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $image['thumb']; ?>" data-thubig="<?php echo $image['thumb_big']; ?>" data-popup="<?php echo $image['popup']; ?>" class="add-thumbnail" id="thumbnail_<?php echo $i; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <!-- custom added -->
          <script type="text/javascript">
          	$('.add-thumbnail').mouseenter(function(){
          		var theID = $(this).attr('id');
          		$('.headthumb').attr('src',$('#'+theID).attr('data-thubig'));	
          		$('.headpopup').attr('href',$('#'+theID).attr('data-popup'));
          	});
          </script>
          <?php } ?>
          
          <hr style="margin-top:0" />
          
          <div class="pull-left" style="margin-right: 10px;">
          	<div class="fb-like" data-href="<?php echo $_SERVER['REQUEST_URI']; ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
          </div>
          
          <div class="pull-left" style="margin-right: 10px;">
          	<a href="https://twitter.com/share" class="twitter-share-button" data-via="gpserab" data-hashtags="gpserab">Tweet</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
          </div>
          
          <div class="pull-left" style="margin-right: 10px;">
			<a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-color="red"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_20.png" style="margin-left:4px; margin-top:2px;" /></a>
			<!-- Please call pinit.js only once per page -->
			<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
          </div>
          
          <div class="pull-left">
          	<!-- Place this tag in your head or just before your close body tag. -->
			<script src="https://apis.google.com/js/platform.js" async defer>
			  {lang: 'en-GB'}
			</script>
			
			<!-- Place this tag where you want the +1 button to render. -->
			<div class="g-plusone" data-size="medium"></div> 
          </div>
          
          <br />
          <hr style="margin-top:15px" />
            
            <!-- Product Tabs -->
	          <ul class="nav nav-tabs">
	            <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
	            <?php if ($attribute_groups) { ?>
	            <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
	            <?php } ?>
	            <?php if ($review_status) { ?>
	            <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
	            <?php } ?>
	          </ul>
	          <div class="tab-content">
	            <div class="tab-pane active" id="tab-description">
	            	<div class="article"><?php echo $description; ?></div>
	            	<script type="text/javascript">
	            	$('.article').readmore({
					  speed: 75,
					  collapsedHeight: 200,
					  moreLink: '<a href="#" class="btn btn-default btn-read-more">Visa mer [+]</a>',
					  lessLink: '<a href="#" class="btn btn-default btn-read-more">Visa mindre [-]</a>',
					  beforeToggle: function() {
					  	// remove gradiant css
					  	$( "div" ).remove( ".gradient" );
					  }
					});
					
					$('.article').prepend('<div class="gradient">&nbsp;</div>');
	            	</script>
	            
	            </div>
	            <?php if ($attribute_groups) { ?>
	            <div class="tab-pane" id="tab-specification">
	              <table class="table table-bordered">
	                <?php foreach ($attribute_groups as $attribute_group) { ?>
	                <thead>
	                  <tr>
	                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
	                  </tr>
	                </thead>
	                <tbody>
	                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
	                  <tr>
	                    <td><?php echo $attribute['name']; ?></td>
	                    <td><?php echo $attribute['text']; ?></td>
	                  </tr>
	                  <?php } ?>
	                </tbody>
	                <?php } ?>
	              </table>
	            </div>
	            <?php } ?>
	            <?php if ($review_status) { ?>
	            <div class="tab-pane" id="tab-review">
	              <form class="form-horizontal">
	                <div id="review"></div>
	                <h2><?php echo $text_write; ?></h2>
	                <?php if ($review_guest) { ?>
	                <div class="form-group required">
	                  <div class="col-sm-12">
	                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
	                    <input type="text" name="name" value="" id="input-name" class="form-control" />
	                  </div>
	                </div>
	                <div class="form-group required">
	                  <div class="col-sm-12">
	                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
	                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
	                    <div class="help-block"><?php echo $text_note; ?></div>
	                  </div>
	                </div>
	                <div class="form-group required">
	                  <div class="col-sm-12">
	                    <label class="control-label"><?php echo $entry_rating; ?></label>
	                    &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
	                    <input type="radio" name="rating" value="1" />
	                    &nbsp;
	                    <input type="radio" name="rating" value="2" />
	                    &nbsp;
	                    <input type="radio" name="rating" value="3" />
	                    &nbsp;
	                    <input type="radio" name="rating" value="4" />
	                    &nbsp;
	                    <input type="radio" name="rating" value="5" />
	                    &nbsp;<?php echo $entry_good; ?></div>
	                </div>
	                <?php if ($site_key) { ?>
	                  <div class="form-group">
	                    <div class="col-sm-12">
	                      <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
	                    </div>
	                  </div>
	                <?php } ?>
	                <div class="buttons clearfix">
	                  <div class="pull-right">
	                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
	                  </div>
	                </div>
	                <?php } else { ?>
	                <?php echo $text_login; ?>
	                <?php } ?>
	              </form>
	            </div>
	            <?php } ?>
	          </div>
              <!-- / Product Tabs -->
            
            
        </div>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-4'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>" style="margin-top:0px;border-top:1px solid #eee;padding-top:10px;">
          <ul class="list-unstyled">
            <?php if ($manufacturer) { ?>
            <li><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>" class="manufacturer"><?php echo $manufacturer; ?></a></li>
            <?php } ?>
            <li><?php echo $text_model; ?> <?php echo $model; ?></li>
            <?php 
            // EAN number
            if ( $ean ) { ?>
            <li style="margin-bottom:10px;">EAN: <?php echo $ean; ?></li>
            <?php }
            if ($reward) { ?>
            <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
            <?php } ?>
            <?php 
            // CUSTOM MADE - Stock coloring
	        if ( $stock <= 0 ) {
	        	$stock_color_class = 'red';
				$stock_qty = 0;
	        } else {
	        	$stock_color_class = 'green';
				$stock_qty = $stock;
	        }
			
			// CUSTOM MADE - Stock status graphics
			switch ($stock) {
				case ($stock >= 40):
					$stock_status = $text_stock_instock.'. '.$text_stock_delivery.' <i id="clock"></i>';
					#$stock_status = '<img src="catalog/view/theme/gpser/image/stock-status-4.png" alt="ca 50st i lager" title="ca 50st i lager"   />';
					break;
				case ($stock < 40 && $stock >= 30):
					$stock_status = $text_stock_instock.'. '.$text_stock_delivery.' <i id="clock"></i>';
					#$stock_status = '<img src="catalog/view/theme/gpser/image/stock-status-3.png" alt="ca 40st i lager" title="ca 40st i lager" />';
					break;
				case ($stock < 30 && $stock >= 20):
					#$stock_status = '<img src="catalog/view/theme/gpser/image/stock-status-2.png" alt="ca 30st i lager" title="ca 30st i lager"  />';
					$stock_status = $text_stock_instock.'. '.$text_stock_delivery.' <i id="clock"></i>';
					break;
				case ($stock < 20 && $stock >= 1):
					#$stock_status = '<img src="catalog/view/theme/gpser/image/stock-status-1.png" alt="ca 10st i lager" title="ca 10st i lager" />';
					$stock_status = $text_stock_instock.'. '.$text_stock_delivery.' <i id="clock"></i>';
					break;		
				default:
					$stock_status = $stock . ' ('.$text_stock_nostock.')';
					break;
			}
            ?>
            
            <li><span><?php echo $text_stock; ?></span> <span class="stock-color-<?php echo $stock_color_class; ?>"><?php echo $stock_status; ?></span></li>
          </ul>
          	<?php 
          	// Shipping time counter condition
          	// Detect the day of the week
          	// Add one or two days if weekend
			$date1 = date('Y/m/d 16:00');
			$dayN = date('N');
			$daysleft = NULL;
			
			if ( $dayN == 6 ) { // Lördag, +2 till mån
				
				$shiptime = date('Y/m/d 16:00',strtotime($date1 . "+2 days"));
				$daysleft = 2 . 'd ';
				
			} elseif ( $dayN == 7 ) { // Söndag +1 till mån
				
				if ( date('H') > '16' ) {
					$shiptime = date('Y/m/d 16:00',strtotime($date1));
				} else {
					$shiptime = date('Y/m/d 16:00',strtotime($date1 . "+1 days"));
					$daysleft = 1 . 'd ';
				}

				
			} else {
					
				// Check if 16:00 has passed	
				if ( date('H') >= '16') {
					$date1 = date('Y/m/d 16:00',strtotime("+1 days"));
				} else {
					$date1 = date('Y/m/d 16:00');
				}
				
				$shiptime = $date1;
				
			}
			
			
			
          	?>
          <script type="text/javascript">
          	/* Leverans inom */
    		$('#clock').countdown('<?php echo $shiptime; ?>', function(event) {
				$(this).html(event.strftime('<?php echo $daysleft; ?>%H<?php echo $text_counter_hour; ?> %Mm %Ss'));
			});
		  </script>
		
          <?php if ($price) { ?>
          <ul class="list-unstyled">
            <?php if (!$special) { ?>
            <li>
              <h2 class="prod-price" data-price="<?php echo $price; ?>" ><?php echo $price; ?></h2>
              	<?php
	        	// Custom added price2
	        	// RRP price - rek. pris added by dean 
	        	if ( $price2 > 0 ) {
	        	?>
	        	<span class="price-rec"><?php echo $text_price2; ?> <?php echo $price2; ?></span>
	        	<?php 
				}
	        	?>
            </li>
            <?php } else { ?>
            <li><span style="text-decoration: line-through;font-size:20px;color:grey">Ord. pris: <?php echo $price; ?></span></li>
            <li>
              <h2 class="prod-price" data-price="<?php echo $special; ?>" ><?php echo $special; ?></h2>
            </li>
            <?php } ?>
            <?php if ($tax) { ?>
            <li><?php echo $text_tax; ?> <?php echo $tax; ?></li>
            <?php } ?>
            <?php if ($points) { ?>
            <li><?php echo $text_points; ?> <?php echo $points; ?></li>
            <?php } ?>
            <?php if ($discounts) { ?>
            <li>
              <hr>
            </li>
            <?php foreach ($discounts as $discount) { ?>
            <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>	
		  <div class='clearfix'></div>
          <div id="product">
            <?php if ($options) { ?>
            <!--hr-->
				<script>
					var per_month = 0;
					var price = 0; 
				function reloadPrice(per_month, price ){
							if( per_month != 0 ||  price != 0){
						
								
								$.ajax({
									url: 'index.php?route=product/product/getReloadPrice',
									type: 'post',
									data: 'new-price='+ price+'&per_month='+per_month+'&product_id='+<?php echo $product_id;?>+'',
									dataType: 'html',
									beforeSend: function() {
										
									},
									complete: function() {
											
									},
									success: function(html) {
										$newPrice = html;
										$priceParent = $('.prod-price').parent().parent();
										
								
										if( $priceParent.hasClass('col-sm-6')){
												$priceParent.last().replaceWith($newPrice);
												//$priceParent.last().find('li:last-child').text('Subscription: '+ per_month + '');
										}else{
											$priceParent.addClass('col-sm-6');
											$priceParent.find('.prod-price').css('text-decoration','line-through');
											$priceParent.after(html);
											
										}
									},
								})
							event.stopImmediatePropagation();
						} else {
								$priceParent = $('.prod-price').parent().parent();
								
								if( $priceParent.hasClass('col-sm-6') && $priceParent.length == 2){
										$priceParent.last().remove();
										$priceParent.removeClass('col-sm-6');
										$priceParent.find('.prod-price').css('text-decoration','none');
										event.stopImmediatePropagation();
								}
						}
						// per_month = 0;
						 //price = 0; 
						// return per_month, price;
				}
			</script>
            <!--h3><?php // echo $text_option; ?></h3-->
            <?php foreach ($options as $option) { ?>
		
            <?php if ($option['type'] == 'select') { ?>
		
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>" >
                <h3><?php echo $option['name']; ?></h3>
                <div id="input-option<?php echo $option['product_option_id']; ?>">
				  <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
					<option value=""><?php echo $text_select; ?></option>
					<?php foreach ($option['product_option_value'] as $option_value) { ?>
					<option value="<?php echo $option_value['product_option_value_id']; ?>"   data-delta-price="<?php echo $option_value['price_prefix']; ?><?php echo $option_value['int_price_with']; ?>" data-current-price="<?php echo $option['int_price_without']; ?>" data-price_per_month="<?php echo $option_value['price_per_month']; ?>"><?php echo $option_value['name']; ?>
					<?php if ($option_value['price']) { ?>
					(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
					<?php } ?>
					</option>
					<?php } ?>
				  </select>
				</div>
			</div>
			<script>
				$(document).ready( function(){
					$("[ id *= 'input-option<?php echo $option['product_option_id']; ?>' ]").on('click', function(event){
								per_month = 0;
								 price = 0; 
								$(" input:checked, option:checked").each( function(){
									if(!isNaN(parseInt($(this).data('price_per_month')))){
										per_month += parseInt($(this).data('price_per_month'));
									}
									if(!isNaN(parseInt($(this).data('delta-price')))){ 
										price += parseInt($(this).data('delta-price'));
									}
										
								})
								reloadPrice(per_month, price );
					})
				});
			</script>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <h3><?php echo $option['name']; ?></h3>
            <div class="option_desc"><?php echo html_entity_decode($option['description'], ENT_QUOTES, 'UTF-8'); ?></div>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio">
                   <input type="radio" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"  data-delta-price="<?php echo $option_value['price_prefix']; ?><?php echo $option_value['int_price_with']; ?>" data-current-price="<?php echo $option['int_price_without']; ?>" data-price_per_month="<?php echo $option_value['price_per_month']; ?>"/>
                    <label  for="option-value-<?php echo $option_value['product_option_value_id']; ?>"  style="display: inline-block;">
					<?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
						<?php if ($option_value['price_per_month']) { ?>
						<span class="per_mon" ><?php echo $option_value['per_month_currency']; ?> / <?php echo $text_month_short; ?> </span>
				
						<?php } ?>
						<?php 
						// Option value description
						echo html_entity_decode($option_value['description'], ENT_QUOTES, 'UTF-8'); 
						?>
					</label>	
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
					<script>
				$(document).ready( function(){
					$("[ id *= 'input-option<?php echo $option['product_option_id']; ?>' ]").on('click','input', function(){
									per_month = 0;
									price = 0; 
								$(" input:checked, option:checked").each( function(){
										if(!isNaN(parseInt($(this).data('price_per_month')))){
										per_month += parseInt($(this).data('price_per_month'));
									}
									if( !isNaN(parseInt($(this).data('delta-price')))){ 
										price += parseInt($(this).data('delta-price'));
									}
										
										
								})
								reloadPrice(per_month, price );
					})
				});
			</script>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <h3><?php echo $option['name']; ?></h3>
			  <?php echo html_entity_decode($option['description'], ENT_QUOTES, 'UTF-8'); ?>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" data-delta-price="<?php echo $option_value['price_prefix']; ?><?php echo $option_value['int_price_with']; ?>" data-current-price="<?php echo $option['int_price_without']; ?>" data-price_per_month="<?php echo $option_value['price_per_month']; ?>"/>
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
						
                    <?php } ?>
					<?php if ($option_value['price_per_month']) { ?>
						<span class="per_mon" ><?php echo $option_value['per_month_currency']; ?> / <?php echo $text_month_short; ?> </span>
				
						<?php } ?>
						<?php echo html_entity_decode($option_value['description'], ENT_QUOTES, 'UTF-8'); ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
			<script>
				$(document).ready( function(){
					$("[ id *= 'input-option' ]").on('click','input', function(){
								per_month = 0;
								price = 0; 
								$(" input:checked, option:checked").each( function(){
										if(!isNaN(parseInt($(this).data('price_per_month')))){
										per_month += parseInt($(this).data('price_per_month'));
									}
									if( !isNaN(parseInt($(this).data('delta-price')))){ 
										price += parseInt($(this).data('delta-price'));
									}
										
										
								})
								reloadPrice(per_month, price );
					
					})
				});
			</script>
            <?php } ?>
            <?php if ($option['type'] == 'image') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label">
					<h3><?php echo $option['name']; ?></h3>
			  </label>
			  <?php echo html_entity_decode($option['description'], ENT_QUOTES, 'UTF-8'); ?>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio">
				<?php echo ""; ?>
                  <label>
				  <?php /*echo "<pre>"; print_r($option_value); echo "</pre>"; */ ?>
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"  data-delta-price="<?php echo $option_value['price_prefix']; ?><?php echo $option_value['int_price_with']; ?>" data-current-price="<?php echo $option['int_price_without']; ?>" data-price_per_month="<?php echo $option_value['price_per_month']; ?>"/>
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                    
					<div style="display: inline-block;">
						<?php echo $option_value['name']; ?>
						<?php if ($option_value['price']) { ?>
							(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
						<?php } ?>
						<?php if ($option_value['price_per_month']) { ?>
						<span class="per_mon" ><?php echo $option_value['per_month_currency']; ?> / <?php echo $text_month_short; ?> </span>
				
						<?php } ?>
						<?php echo html_entity_decode($option_value['description'], ENT_QUOTES, 'UTF-8'); ?>
					</div>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
			
			<script>
				$(document).ready( function(){
					$("[ id *= 'input-option' ]").on('click','input', function(){
									per_month = 0;
									price = 0; 
								$(" input:checked, option:checked").each( function(){
										if(!isNaN(parseInt($(this).data('price_per_month')))){
										per_month += parseInt($(this).data('price_per_month'));
									}
									if( !isNaN(parseInt($(this).data('delta-price')))){ 
										price += parseInt($(this).data('delta-price'));
									}
										
									
								})
									reloadPrice(per_month, price );
						
					
					})
				});
			</script>
            <?php } ?>
            <?php if ($option['type'] == 'text') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group date">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group datetime">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php if ($recurrings) { ?>
            <hr>
            <h3><?php echo $text_payment_recurring ?></h3>
            <div class="form-group required">
              <select name="recurring_id" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($recurrings as $recurring) { ?>
                <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                <?php } ?>
              </select>
              <div class="help-block" id="recurring-description"></div>
            </div>
            <?php } ?>
            <div class="form-group">
            	<div class="col-sm-5 prod-col-qty">
              		<!--standard -- label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
              		<input type="text" name="quantity1" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
              		<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" /-->
              		<!-- custom minus plus -->
		          <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
	              	<div class="input-group">
			          <span class="input-group-btn">
			              <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quantity">
			                  <span class="glyphicon glyphicon-minus"></span>
			              </button>
			          </span>
			          <input type="text" name="quantity" class="form-control input-number" id="input-quantity" value="1" min="1" max="50" size="2" />
			          <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
			          <span class="input-group-btn">
			              <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quantity">
			                  <span class="glyphicon glyphicon-plus"></span>
			              </button>
			          </span>
			      	</div>
			      <!-- / custom minus plus -->
		      
              	</div>
              	<div class="col-sm-6 prod-col-buybtn">
              		<button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
              	</div>
              	<div class="col-sm-4">&nbsp;</div>
              	<br /><br /><br />
              
            </div>
            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>
          </div>
          <?php if ($review_status) { ?>
          <div class="rating">
            <p>
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($rating < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } ?>
              <?php } ?>
              <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a></p>
            <hr>
            
          
          </div>
          <?php } ?>
          
          <!-- Whishlist, Compare -->
	      <div class="btn-group">
	        <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="fa fa-heart"></i></button>
	      	<button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-exchange"></i></button>
	      </div>
          
          <!-- Need help? -->
          <br /><br />
          <div class="need-help">
          	<h3>Behöver du hjälp?</h3>
			<p>Vänligen &nbsp;<i class="fa fa-envelope-o"></i><a href="/contact"> maila oss</a> eller ring  &nbsp;<i class="fa fa-phone-square"></i> 0470-786833
				<br />så hjälper vi dig. <br /><br />Vi tar emot beställningar även per mail och telefon.</p>
          </div>
          <br />
          	
        </div>
        
      </div>
      
      <?php if ($products) { ?>
      <h3><?php echo $text_related; ?></h3>
      <div class="row">
        <?php $i = 0; ?>
        <?php foreach ($products as $product) { ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-lg-4 col-md-4 col-sm-6 col-xs-12'; ?>
        <?php } else { ?>
        <?php $class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <div class="product-thumb transition">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div class="caption">
              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
              <p><?php echo $product['description']; ?></p>
              <?php /* if ($product['rating']) { ?>
              <div class="rating">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($product['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>
              <?php } */ ?>
            </div>
            
              <?php if ($product['price']) { ?>
              <div class="price-button-group">	
		        <div class="pull-left">	
			        <p class="price">
			          <?php if (!$product['special']) { ?>
			          <?php echo $product['price']; ?>
			          <?php } else { ?>
			          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
			          <?php } ?>
			          <?php if ($product['tax']) { ?>
			          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
			          <?php } ?>
			        </p>
		        </div>
		        <div class="pull-right">
		        	<button class="button-cart" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"> <?php echo $button_cart; ?></button>	
		        </div>
		      </div>
              <?php } ?>
            
          </div>
        </div>
        <?php if (($column_left && $column_right) && ($i % 2 == 0)) { ?>
        <div class="clearfix visible-md visible-sm"></div>
        <?php } elseif (($column_left || $column_right) && ($i % 3 == 0)) { ?>
        <div class="clearfix visible-md"></div>
        <?php } elseif ($i % 4 == 0) { ?>
        <div class="clearfix visible-md"></div>
        <?php } ?>
        <?php $i++; ?>
        <?php } ?>
      </div>
      <?php } ?>
      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');
			$('.cart-inactive').addClass('cart-active'); /* custom added */
			
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('#cart > button').html('<i class="fa fa-shopping-cart"></i> ' + json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
				
				$('#cart').addClass('open'); /* custom defined */
			}
		}
	});
});

/* custom defined 
$('#cart').mouseenter(function() {
	$('#cart').addClass('open'); 
});
*/

//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
  e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : ''),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>


<script type="text/javascript">
// quantity minus and plus
$(document).ready(function(){
	//http://jsfiddle.net/laelitenetwork/puJ6G/
	$('.btn-number').click(function(e){
	    e.preventDefault();
	    
	    fieldName = $(this).attr('data-field');
	    type      = $(this).attr('data-type');
	    var input = $("input[name='"+fieldName+"']");
	    var currentVal = parseInt(input.val());
	    if (!isNaN(currentVal)) {
	        if(type == 'minus') {
	            
	            if(currentVal > input.attr('min')) {
	                input.val(currentVal - 1).change();
	            } 
	            if(parseInt(input.val()) == input.attr('min')) {
	                $(this).attr('disabled', true);
	            }
	
	        } else if(type == 'plus') {
	
	            if(currentVal < input.attr('max')) {
	                input.val(currentVal + 1).change();
	            }
	            if(parseInt(input.val()) == input.attr('max')) {
	                $(this).attr('disabled', true);
	            }
	
	        }
	    } else {
	        input.val(0);
	    }
	});
	$('.input-number').focusin(function(){
	   $(this).data('oldValue', $(this).val());
	});
	$('.input-number').change(function() {
	    
	    minValue =  parseInt($(this).attr('min'));
	    maxValue =  parseInt($(this).attr('max'));
	    valueCurrent = parseInt($(this).val());
	    
	    name = $(this).attr('name');
	    if(valueCurrent >= minValue) {
	        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
	    } else {
	        alert('Sorry, the minimum value was reached');
	        $(this).val($(this).data('oldValue'));
	    }
	    if(valueCurrent <= maxValue) {
	        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
	    } else {
	        alert('Sorry, the maximum value was reached');
	        $(this).val($(this).data('oldValue'));
	    }
	    
	    
	});
	$(".input-number").keydown(function (e) {
	        // Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
	             // Allow: Ctrl+A
	            (e.keyCode == 65 && e.ctrlKey === true) || 
	             // Allow: home, end, left, right
	            (e.keyCode >= 35 && e.keyCode <= 39)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        // Ensure that it is a number and stop the keypress
		        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		            e.preventDefault();
		        }
		    });
  	});
</script>


<!-- Facebook like button -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php echo $footer; ?>