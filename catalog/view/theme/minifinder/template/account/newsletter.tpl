<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php $last_breadcrumb = array_pop($breadcrumbs); ?>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
    <li><?php echo $last_breadcrumb['text']; ?></li>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal form_register newsletter">
        <fieldset>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_newsletter; ?></label>
            <div class="col-sm-10">
              <?php if ($newsletter) { ?>
              <div class="radio-inline">
                <label>
                  <input type="radio" name="newsletter" value="1" checked="checked" />
                  <?php echo $text_yes; ?> </label>
              </div>
              <div class="radio-inline">
                <label>
                  <input type="radio" name="newsletter" value="0" />
                  <?php echo $text_no; ?></label>
              </div>
              <?php } else { ?>
              <div class="radio-inline">
                <label>
                  <input type="radio" name="newsletter" value="1" />
                  <?php echo $text_yes; ?> </label>
              </div>
              <div class="radio-inline">
                <label>
                  <input type="radio" name="newsletter" value="0" checked="checked" />
                  <?php echo $text_no; ?></label>
              </div>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <!--<div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>-->
          <div class="pull-left">
            <div class="red_form_btn">
              <input type="submit" value="<?php echo $button_new_save; ?>"/>
            </div>
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script><!--
  $('.radio-inline input[type="radio"]').each(function() {
    var state = $(this).prop('checked');
    if (state) {
      $(this).parent().addClass('checked');
    }
    else {
      $(this).parent().removeClass('checked');
    }
  });
  $(document).on('click', '.radio-inline input[type="radio"]', function(){
    var radio = $(this),
            parent = radio.parents('.form_register .col-sm-10');

    if (radio.prop('checked', true)){
      parent.find('label').removeClass('checked');
      radio.parent().addClass('checked');
    }
  });
  //--></script>
<?php echo $footer; ?>