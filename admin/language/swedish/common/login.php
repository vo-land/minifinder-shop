<?php
// header
$_['heading_title']  = 'Administration';

// Text
$_['text_heading']   = 'Administration';
$_['text_login']     = 'Ange dina inloggningsuppgifter';
$_['text_forgotten'] = 'Glömt lösenordet?';

// Entry
$_['entry_username'] = 'Användarnamn:';
$_['entry_password'] = 'Lösenord:';

// Button
$_['button_login']   = 'Logga in';

// Error
$_['error_login']    = 'Användarnamnet och/eller lösenordet finns inte.';
$_['error_token']    = 'Utgången/förfallen token-session. Logga in igen.';
?>
