<?php if ($products) {
	$cartclass = 'cart-active'; 
} else {
	$cartclass = 'cart-inactive';
}
?>
<div id="cart" class="btn-group btn-block">
  <button type="button" data-toggle="dropdown" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default <?php echo $cartclass; ?> btn-lg dropdown-toggle">
    <span><i class="fa fa-shopping-cart"></i> <span><?php echo $text_items; ?></span></span>
    <span><i class="fa fa-shopping-cart"></i> <span id="cart-total"><?php echo $text_items; ?></span></span>
  </button>
  <ul class="dropdown-menu pull-right">
    <?php if ($products || $vouchers) { ?>
    <li>
      <table class="table table-striped">
        <?php foreach ($products as $product) { ?>
        <tr>
          <td class="text-center img_prod_cart"><?php if ($product['thumb']) { ?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
            <?php } ?></td>
          <td class="text-left prod_option"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <?php if ($product['option']) { ?>
            <?php foreach ($product['option'] as $option) { ?>
            <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
            <?php } ?>
            <?php } ?>
            <?php if ($product['recurring']) { ?>
            <br />
            <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
            <?php } ?></td>
          <td class="text-left quantity_prod">x <?php echo $product['quantity']; ?></td>
          <td class="text-right total_price"><?php echo $product['total']; ?></td>
          <td class="text-center remove_prod"><button type="button" onclick="cart.remove('<?php echo $product['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
        </tr>
        <?php } ?>
        <?php foreach ($vouchers as $voucher) { ?>
        <tr>
          <td class="text-center"></td>
          <td class="text-left"><?php echo $voucher['description']; ?></td>
          <td class="text-right">x&nbsp;1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
        </tr>
        <?php } ?>
      </table>
    </li>
    <li>
      <div>
        <div class="pull-left totals">
          <?php foreach ($totals as $total) { ?>
          <div class="totals_row">
            <div class="total_title pull-left"><?php echo $total['title']; ?></div>
            <div><?php echo $total['text']; ?></div>
          </div>
          <?php } ?>
        </div>
        <div class="pull-right cart_btns">
        	<a href="<?php echo $cart; ?>" class="black_white_btn">
              <span><?php echo $text_cart; ?></span>
              <span><?php echo $text_cart; ?></span>
            </a>
        	<a href="<?php echo $checkout; ?>" class="checkout-button red_button">
              <span><?php echo $text_checkout; ?></span>
              <span><?php echo $text_checkout; ?></span>
            </a>
        </div>
      </div>
    </li>
    <?php } else { ?>
    <li>
      <p class="text-center"><?php echo $text_empty; ?></p>
    </li>
    <?php } ?>
  </ul>
</div>
