<?php
// Heading
$_['heading_title']    = 'Fri frakt';

// Text 
$_['text_shipping']    = 'Frakt';
$_['text_success']     = 'Klart: Du har ändrat i fri frakt!';
$_['text_edit']        = 'Ändra i Fri Frakt';

// Entry
$_['entry_total']      = 'Summa:';
$_['entry_geo_zone']   = 'GeoZon:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sorteringsordning:';

// Help
$_['help_total']       = 'Slutsumma som måste uppnås innan alternativet är giltigt.';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra!';
?>