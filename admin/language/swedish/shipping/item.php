<?php 
// Heading
$_['heading_title']     = 'Frakt per vara';

// Text
$_['text_shipping']    = 'Frakt';
$_['text_success']     = 'Klar: Du har ändrat i fraktmodulen Per vara!';
$_['text_edit']        = 'Änddra i frakt per vara';

// Entry
$_['entry_cost']       = 'Pris:';
$_['entry_tax_class']  = 'Moms:';
$_['entry_geo_zone']   = 'GeoZon:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sorteringsordning:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra!';
?>