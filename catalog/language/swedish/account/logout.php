<?php
// Heading 
$_['heading_title'] = 'Kontoutloggning';

// Text
$_['text_message']  = '<p>Du har loggats ut från ditt konto.</p><p>Din varukorg med tillagda produkter kommer att återställas nästa gång du loggar in.</p>';
$_['text_account']  = 'Konto';
$_['text_logout']   = 'Logga ut';
?>