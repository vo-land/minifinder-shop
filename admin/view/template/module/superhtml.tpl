<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
<script type="text/javascript" src="view/superhtml/jquery.miniColors.js"></script>
<link rel="stylesheet" type="text/css" href="view/superhtml/jquery.miniColors.css" />
<script type="text/javascript">
$(document).ready( function() {
	$(".colors").miniColors({
		change: function(hex, rgb) {
			$("#console").prepend('HEX: ' + hex + ' (RGB: ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')<br />');
		}
	});
});
</script>
<link rel="stylesheet" type="text/css" href="view/superhtml/fontselect.css" />
<script src="view/superhtml/jquery.fontselect.min.js"></script>
<script>
      $(function(){
        $('#h1_font_family').fontselect();
        $('#h2_font_family').fontselect();
        $('#content_font_family').fontselect();
      });
</script>
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button data-toggle="tooltip" onclick="alert('<?php echo $text_save_info; ?>');" class="btn btn-primary"><i class="fa fa-info"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $istemplate ? $text_create_module : $text_edit_module; ?></h3>
      </div>
      <div class="panel-body">
		  <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-mods" data-toggle="tab"><?php echo $tab_modules; ?></a></li>
            <li><a href="#tab-groups-styles" data-toggle="tab"><?php echo $tab_groups; ?> & <?php echo $entry_styles; ?></a></li>
          </ul>
		  <div class="tab-content">
              <div class="tab-pane active in" id="tab-mods">
                <div class="save-section">
                    <div class="save-section-heading"><button type="submit" form="form-sh" data-toggle="tooltip" title="<?php echo $istemplate ? $text_create_mod : $text_save_mod; ?>" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $istemplate ? $text_create_mod : $text_save_mod; ?></button></div>
                  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sh" class="form-horizontal"> 
                      <div class="form-group">
                          <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
                          <div class="col-sm-10">
                              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                            <?php if ($error_name) { ?>
                                <div class="text-danger"><?php echo $error_name; ?></div>
                            <?php } ?>
                          </div>
                      </div> 
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                          <div class="col-sm-10">
                              <select name="status" id="input-status" class="form-control">
                                <?php if ($status) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                              </select>
                          </div>
                      </div>
                      <ul class="nav nav-tabs" id="language">
                        <?php foreach ($languages as $language) { ?>
                            <li><a href="#tab-module-language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                        <?php } ?>
                      </ul>
		              <div class="tab-content">
                        <?php foreach ($languages as $language) { ?>
                          <div class="tab-pane" id="tab-module-language<?php echo $language['language_id']; ?>">
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <td><?php echo $entry_selectgroup; ?></td>
				                    <td>
                                        <select name="group[<?php echo $language['language_id']; ?>]">
					                       <?php foreach ($groups as $grp) { ?>
						                      <?php if (isset($group[$language['language_id']]) && $grp['key'] == $group[$language['language_id']]) { ?>
						                          <option value="<?php echo $grp['key']; ?>" selected="selected"><?php echo isset($grp['name']) ? $grp['name'] : 'Unnamed Group'; ?></option>
						                   <?php } else { ?>
						                          <option value="<?php echo $grp['key']; ?>"><?php echo isset($grp['name']) ? $grp['name'] : 'Unnamed Group'; ?></option>
                                              <?php } ?>
					                       <?php } ?>
					                   </select>
				                    </td> 
				                    <td><?php echo $entry_nocol; ?></td>
				                    <td><select name="cols[<?php echo $language['language_id']; ?>]">
                                        <?php for ($i = 1; $i < 6; $i++) { ?>
                                            <?php if (isset($cols[$language['language_id']]) && $i == $cols[$language['language_id']]) { ?>
                                                <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select></td>
                                    <td><?php echo $entry_gtitle; ?></td>
                                    <td><select name="gtitle[<?php echo $language['language_id']; ?>]">
                                        <?php if (isset($gtitle[$language['language_id']]) && $gtitle[$language['language_id']]) { ?>
                                            <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                            <option value="0"><?php echo $text_no; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_yes; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                        <?php } ?>
                                    </select></td>
                                    <td><?php echo $entry_ctitle; ?></td>
                                    <td><select name="ctitle[<?php echo $language['language_id']; ?>]">
                                        <?php if (isset($ctitle[$language['language_id']]) && $ctitle[$language['language_id']]) { ?>
                                            <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                            <option value="0"><?php echo $text_no; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_yes; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                        <?php } ?>
                                    </select></td>
                                </tr>
                              </table>
                          </div>
                        <?php } ?>
                      </div>
                      <table class="table table-striped table-bordered table-hover">
                          <tr>
                              <td><?php echo $entry_min_width; ?></td>
                              <td><input type="text" name="min_width" size="3" value="<?php echo $min_width; ?>" /></td>
                          </tr>
                          <tr>
                              <td><?php echo $entry_make_carousel; ?></td>
                              <td><table width="100%"><tr><td>
                                    <select class="selcar" name="carousel">
                                        <?php if ($carousel) { ?>
                                            <option class="yes" value="1" selected="selected"><?php echo $text_yes; ?></option>
                                            <option class="no" value="0"><?php echo $text_no; ?></option>
                                        <?php } else { ?>
                                            <option class="yes" value="1"><?php echo $text_yes; ?></option>
                                            <option class="no" value="0" selected="selected"><?php echo $text_no; ?></option>
                                        <?php } ?>
                                    </select>
                                    </td>
                                    <td class="ifcarousel"><?php echo $entry_carousel_items_row; ?>
                                        <ul style="list-style: none; margin: 0px; padding: 0px;">
                                            <li><?php echo $entry_carousel_desktop; ?> <input type="text" name="car_itm_desk" size="3" value="<?php echo $car_itm_desk; ?>" /></li>
                                            <li><?php echo $entry_carousel_tablet; ?> <input type="text" name="car_itm_tab" size="3" value="<?php echo $car_itm_tab; ?>" /></li>
                                            <li><?php echo $entry_carousel_mobile; ?> <input type="text" name="car_itm_mob" size="3" value="<?php echo $car_itm_mob; ?>" /></li>
                                        </ul>
				                    </td>
                                    <td class="ifcarousel">
                                        <ul style="list-style: none; margin: 0px; padding: 0px;">
                                            <li><?php echo $entry_carousel_speed; ?> <input type="text" name="car_speed" size="3" value="<?php echo $car_speed; ?>" /></li>
                                            <li><?php echo $entry_carousel_autoplay; ?> 
                                                <select name="carousel_autoplay">
                                                    <?php if ($carousel_autoplay) { ?>
                                                        <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                                        <option value="0"><?php echo $text_no; ?></option>
                                                    <?php } else { ?>
                                                        <option value="1"><?php echo $text_yes; ?></option>
                                                        <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </li>
                                        </ul>
                                    </td>
                                  </tr>
                                </table>
                            </td>
                          </tr>
                      </table>
                  </form>
                </div>
              </div>
              <div class="tab-pane fade" id="tab-groups-styles">
                <div class="save-section">
                    <div class="save-section-heading"><button type="submit" form="form-shs" data-toggle="tooltip" title="<?php echo $text_save_groups; ?>" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $text_save_groups; ?></button></div>
	                <?php $controller->load->model('tool/image'); ?>
                    <form action="<?php echo $action; ?>&settings_mode=1" method="post" enctype="multipart/form-data" id="form-shs" class="form-horizontal"> 
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-groups" data-toggle="tab"><?php echo $tab_groups; ?></a></li>
                            <li><a href="#tab-styles" data-toggle="tab"><?php echo $entry_styles; ?></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active in" id="tab-groups">
                                <table id="group" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left"><?php echo $group_name; ?></td>
                                            <td class="text-left"><?php echo $group_columns; ?></td>
                                            <td class="text-right"></td>
                                        </tr>
                                    </thead>
                                    <?php $group_row = 0; ?>
                                    <?php $column_row = 0; ?>
                                    <?php $biggest_column = 0; ?>
                                    <?php foreach ($groups as $group) { ?>
                                        <?php $group_row = $group['key']; ?>
                                            <tbody class="group-row" id="group-row<?php echo $group_row; ?>">
                                                <tr>
                                                    <td style="width: 120px;"><input type="text" name="superhtml_groups[<?php echo $group_row; ?>][name]" value="<?php echo isset($group['name']) ? $group['name'] : ''; ?>" /></td>
                                                    <td>
                                                        <table id="column<?php echo $group_row; ?>" class="table table-striped table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <td class="text-left"><?php echo $column_name; ?></td>
                                                                    <td class="text-right"></td>
                                                                </tr>
                                                            </thead>
                                                            <?php foreach ($group['column'] as $column) { ?>
                                                                <?php $column_row = $column['key']; ?>
                                                                <?php if ($column_row > $biggest_column) { $biggest_column = $column_row;  } ?>
                                                                <tbody class="column-row" id="column-row<?php echo $group_row; ?>-<?php echo $column_row; ?>">
                                                                    <tr>
                                                                        <td class="text-left"><input type="hidden" name="superhtml_groups[<?php echo $group_row; ?>][column][<?php echo $column_row; ?>][id]" value="<?php echo isset($column['id']) ? $column['id'] : "superhtml_groups_".$group_row."_column_".$column_row; ?>" />
                                                                            <?php $thisrow = $controller->config->get($column['id']); ?>
                                                                            <div class="editpreview">
                                                                                <?php echo isset($thisrow['description']) ? utf8_substr(strip_tags(html_entity_decode($thisrow['description'], ENT_QUOTES, 'UTF-8')), 0, 70) . '...' : 'No content'; ?>
                                                                                <a onclick="showup('hasdescription-<?php echo $group_row; ?>-<?php echo $column_row; ?>');"><?php echo $text_edit; ?></a>
                                                                            </div>
                                                                            <div class="containseditor" id="hasdescription-<?php echo $group_row; ?>-<?php echo $column_row; ?>" >
                                                                                <table class="nonborder" style="width: 100%; border: none !important;">
                                                                                    <tr>
                                                                                        <td style="width: 100px;"><?php echo $text_column_name; ?></td>
                                                                                        <td><input type="text" name="<?php echo $column['id']; ?>[name]" value="<?php echo isset($thisrow['name']) ? $thisrow['name'] : ''; ?>" /></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 100px;"><?php echo $text_column_url; ?></td>
                                                                                        <td><input type="text" name="<?php echo $column['id']; ?>[url]" value="<?php echo isset($thisrow['url']) ? $thisrow['url'] : ''; ?>" /></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 100px;"><?php echo $text_column_image; ?></td>
                                                                                        <td>
                                                                                            <div class="image">
                                                                                                <a href="" id="thumb-image<?php echo $column_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo ($thisrow['image']) ? $controller->model_tool_image->resize($thisrow['image'],50,50) : $no_image; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                                                                                <input type="hidden" name="<?php echo $column['id']; ?>[image]" value="<?php echo isset($thisrow['image']) ? $thisrow['image'] : ''; ?>" id="input-image<?php echo $column_row; ?>" />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 100px;"><?php echo $text_column_youtube; ?></td>
                                                                                        <td><input type="text" name="<?php echo $column['id']; ?>[youtube]" value="<?php echo isset($thisrow['youtube']) ? $thisrow['youtube'] : ''; ?>" /></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                </table>
                                                                                <textarea cols="70" rows="7" name="<?php echo $column['id']; ?>[description]" id="description-<?php echo $group_row; ?>-<?php echo $column_row; ?>"><?php echo isset($thisrow['description']) ? $thisrow['description'] : ''; ?></textarea>
                                                                            </div>
                                                                            <script type="text/javascript"><!--
                                                                                $('#description-<?php echo $group_row; ?>-<?php echo $column_row; ?>').summernote({
                                                                                    height: 300
                                                                                });
                                                                            //--></script> 
                                                                        </td>
                                                                        <td class="text-right"><button type="button" onclick="$('#column-row<?php echo $group_row; ?>-<?php echo $column_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                                                    </tr>
                                                            </tbody>
                                                    <?php } ?>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="1"></td>
                                                            <td class="text-right" style="width: 70px;">
                                                                <button type="button" onclick="addcolumn(<?php echo $group_row; ?>);" class="btn btn-primary"><?php echo $button_column; ?></button>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </td>
                                            <td class="text-right">
                                                <button type="button" onclick="$('#group-row<?php echo $group_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                <?php } ?>
                            <tfoot id="gtf">
                                <tr>
                                    <td colspan="2"></td>
                                    <td class="text-right" style="width: 60px;">
                                        <button type="button" onclick="addgroup();" class="btn btn-primary"><?php echo $button_group; ?></button>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="tab-styles">
	                   <table class="table table-striped table-bordered table-hover">
                           <tr>
                               <td> <?php echo $text_h1_font; ?></td>
                               <td><input id="h1_font_family" type="text" name="superhtml_sh_h1_font" value="<?php echo ($controller->config->get('superhtml_sh_h1_font')) ? $controller->config->get('superhtml_sh_h1_font') : ''; ?>" /></td>
                           </tr>
                           <tr>
                               <td> <?php echo $text_h1_font_size; ?></td>
                               <td><input type="text" name="superhtml_sh_h1_font_size" value="<?php echo ($controller->config->get('superhtml_sh_h1_font_size')) ? $controller->config->get('superhtml_sh_h1_font_size') : '23px'; ?>" /></td>
                           </tr>
                           <tr>
                               <td> <?php echo $text_h1_color; ?></td>
                               <td><input class="colors" type="text" name="superhtml_sh_h1_color" value="<?php echo ($controller->config->get('superhtml_sh_h1_color')) ? $controller->config->get('superhtml_sh_h1_color') : ''; ?>" /></td>
                           </tr>
                           <tr>
                               <td> <?php echo $text_h1_border_color; ?></td>
                               <td><input class="colors" type="text" name="superhtml_sh_h1_border_color" value="<?php echo ($controller->config->get('superhtml_sh_h1_border_color')) ? $controller->config->get('superhtml_sh_h1_border_color') : ''; ?>" /></td>
                           </tr>
                           <tr>
                               <td> <?php echo $text_h1_bg_color; ?></td>
                               <td><input class="colors" type="text" name="superhtml_sh_h1_bg_color" value="<?php echo ($controller->config->get('superhtml_sh_h1_bg_color')) ? $controller->config->get('superhtml_sh_h1_bg_color') : ''; ?>" /></td>
                           </tr>
                           <tr>
                               <td> <?php echo $text_h2_font; ?></td>
                               <td><input id="h2_font_family" type="text" name="superhtml_sh_h2_font" value="<?php echo ($controller->config->get('superhtml_sh_h2_font')) ? $controller->config->get('superhtml_sh_h2_font') : ''; ?>" /></td>
                           </tr>
                           <tr>
                               <td> <?php echo $text_h2_font_size; ?></td>
                               <td><input type="text" name="superhtml_sh_h2_font_size" value="<?php echo ($controller->config->get('superhtml_sh_h2_font_size')) ? $controller->config->get('superhtml_sh_h2_font_size') : '15px'; ?>" /></td>
                           </tr>
                           <tr>
                               <td> <?php echo $text_h2_color; ?></td>
                               <td><input class="colors" type="text" name="superhtml_sh_h2_color" value="<?php echo ($controller->config->get('superhtml_sh_h2_color')) ? $controller->config->get('superhtml_sh_h2_color') : ''; ?>" /></td>
                           </tr>
                           <tr>
                               <td> <?php echo $text_content_font; ?></td>
                               <td><input id="content_font_family" type="text" name="superhtml_content_h2_font" value="<?php echo ($controller->config->get('superhtml_content_h2_font')) ? $controller->config->get('superhtml_content_h2_font') : ''; ?>" /></td>
                           </tr>
                           <tr>
                               <td> <?php echo $text_content_font_size; ?></td>
                               <td><input type="text" name="superhtml_sh_content_font_size" value="<?php echo ($controller->config->get('superhtml_sh_content_font_size')) ? $controller->config->get('superhtml_sh_content_font_size') : '12px'; ?>" /></td>
                           </tr>
                           <tr>
                               <td> <?php echo $text_content_color; ?></td>
                               <td><input class="colors" type="text" name="superhtml_sh_content_color" value="<?php echo ($controller->config->get('superhtml_sh_content_color')) ? $controller->config->get('superhtml_sh_content_color') : ''; ?>" /></td>
                           </tr>
                        </table>
                            </div>
                        </div>
                    </form>
                  </div>
              </div>
          </div>
        </div>
      </div>
</div>
<script type="text/javascript"><!--
var group_row = <?php echo $group_row + 1; ?>;

function addgroup() {	
	html  = '<tbody class="group-row" id="group-row' + group_row + '">';
	html += '  <tr>';
	html += '    <td class="left" style="width: 120px;"><input type="text" name="superhtml_groups[' + group_row + '][name]" value="" /></td>';
	html += '    <td><table id="column' + group_row + '" class="table table-striped table-bordered table-hover"><thead><tr><td class="left"><?php echo $column_name; ?></td><td></td></tr></thead><tfoot><tr><td colspan="1"></td><td class="text-right" style="width: 70px;"><button type="button" onclick="addcolumn(' + group_row + ');" class="btn btn-primary"><?php echo $button_column; ?></button></td></tr></tfoot></table>';
	html += '&nbsp;</td>';
	html += '    <td class="text-right"><button type="button" onclick="$(\'#group-row' + group_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#group tfoot#gtf').before(html);
	group_row++;
}
var column_row = <?php echo $biggest_column + 1; ?>;
function addcolumn(group_row) {
	html  = '<tbody class="column-row" id="column-row' + group_row + '-' + column_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="hidden" name="superhtml_groups[' + group_row + '][column][' + column_row + '][id]" value="superhtml_groups_' + group_row + '_column_' + column_row + '" />';
	html += '    <table class="nonborder" style="width: 100%; border: none !important;"><tr><td style="width: 100px;"><?php echo $text_column_name; ?></td>';	
	html += '    <td><input type="text" name="superhtml_groups_' + group_row + '_column_' + column_row + '[name]" value="" /></td></tr>';	
	html += '    <tr><td style="width: 100px;"><?php echo $text_column_url; ?></td><td><input type="text" name="superhtml_groups_' + group_row + '_column_' + column_row + '[url]" value="" /></td></tr>';	
	html += '    <tr><td style="width: 100px;"><?php echo $text_column_image; ?></td><td><div class="image"><a href="" id="thumb-image' + column_row + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $no_image; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="superhtml_groups_' + group_row + '_column_' + column_row + '[image]" value="" id="input-image' + column_row + '" />	</div></td></tr>';							
	html += '    <tr><td style="width: 100px;"><?php echo $text_column_youtube; ?></td><td><input type="text" name="superhtml_groups_' + group_row + '_column_' + column_row + '[youtube]" value="" /></td></tr></table>';		
	html += '   <textarea cols="70" rows="7" name="superhtml_groups_' + group_row + '_column_' + column_row + '[description]" id="description-' + group_row + '-' + column_row + '"></textarea></td>';
	html += '    <td class="text-right"><button type="button" onclick="$(\'#column-row' + group_row + '-' + column_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#column' + group_row + ' tfoot').before(html);
	
	$('#description-' + group_row + '-' + column_row).summernote({
		height: 300
	});
	
	column_row++;
}
function showup(div) {
  if ($("#"+div).hasClass('deschis')) {
	$("#"+div).fadeOut('fast');
	$("#"+div).removeClass('deschis')
  }else {
	$("#"+div).fadeIn('fast');
	$("#"+div).addClass('deschis');
  }
}
//--></script> 

<script type="text/javascript"><!--
$('#language li:first-child a').tab('show');
//--></script>

<script type="text/javascript"><!--
$(".selcar").change(function() {
  var parent = $(this).parent().parent();
  if ($(this).find('.yes').is(":selected")) {
     parent.find('.ifcarousel').slideDown('fast');
  } else if ($(this).find('.no').is(":selected")) {
     parent.find('.ifcarousel').slideUp('fast');
  }
}).trigger('change');
//--></script> 
<style type="text/css">
    .save-section { padding: 10px; border: 2px solid #eee; }
    .save-section .save-section-heading { margin: -10px; margin-bottom: 10px; padding: 8px; background: #eee; text-align: right; }
</style>
</div><?php echo $footer; ?>