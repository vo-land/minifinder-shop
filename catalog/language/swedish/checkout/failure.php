<?php
// Heading
$_['heading_title'] = 'Betalningen Misslyckades!';

// Text
$_['text_basket']   = 'Varukorg';
$_['text_checkout'] = 'Kassa';
$_['text_failure']  = 'Betalningen misslyckades';
$_['text_message']  = '<p>Ett problem har uppstått och betalningen har ej gått igenom.</p>

<p>Försök gärna igen med ett annat betalningssätt.</p>

<p>Kvarstår problemet prova från annan dator eller kontakta oss.</p>
';
?>
