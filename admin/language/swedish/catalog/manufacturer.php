<?php
// Heading
$_['heading_title']      = 'Tillverkare';

// Text
$_['text_success']       = 'Klart: Du har ändrat i tillverkare!';
$_['text_list']          = 'Alternativ grupplista';
$_['text_add']           = 'Lägg till Tillverkare';
$_['text_edit']          = 'Ändra i Tillverkare';
$_['text_default']       = 'Förvalt';
$_['text_percent']       = 'Procent';
$_['text_amount']        = 'Fast summa';

// Column
$_['column_name']        = 'Tillverkare';
$_['column_sort_order']  = 'Sorteringsordning';
$_['column_action']      = 'Ändra';

// Entry
$_['entry_name']         = 'Tillverkare:';
$_['entry_store']        = 'Butiker:';
$_['entry_keyword']      = 'SEO-sökord:';
$_['entry_image']        = 'Bild:';
$_['entry_sort_order']   = 'Sorteringsordning:';
$_['entry_type']         = 'Typ:';

// Help
$_['help_keyword']       = 'Använd inte mellanslag. Använd istället - tecken mellan orden.';

// Error
$_['error_permission']   = 'Varning: Du har inte behörighet att ändra i tillverkare!';
$_['error_name']         = 'Tillverkarens namn måste innheålla mellan 3 och 64 tecken!';
$_['error_product']      = 'Varning: Den här tillverkaren kan inte raderas eftersom den är kopplad till %s products!';
$_['error_product']      = 'Varning: Du kan inte radera Tillverkare för den används på %s produkter!';
?>
