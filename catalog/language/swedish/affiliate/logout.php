<?php
// Heading 
$_['heading_title'] = 'Logga ut';

// Text
$_['text_message']  = '<p>Du har loggats ut från ditt konto.</p>';
$_['text_account']  = 'Affiliatekonto';
$_['text_logout']   = 'Logga ut';
?>