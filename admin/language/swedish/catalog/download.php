<?php
// Heading
$_['heading_title']    = 'Nerladdningar';

// Text
$_['text_success']     = 'Klart: Du har ändrat i nerladdningar!';
$_['text_list']        = 'Alternativ grupplista';
$_['text_add']          = 'Lägg till Nerladdningar';
$_['text_edit']         = 'Ändra i Nerladdningar';
$_['text_upload']      = 'Klart: Din fil är uploaded!';

// Column
$_['column_name']       = 'Benämning';
$_['column_date_added'] = 'Datum tillagd';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_name']       = 'Benämning:';
$_['entry_filename']   = 'Filnamn:';
$_['entry_mask']       = 'Mask:';

// Help
$_['help_filename']     = 'Du kan ladda upp via upload knappen eller via FTP och ange beskrivning nedan.';
$_['help_mask']         = 'Det rekommenderas att du har två olika namn så de inte kan länka direkt till dina downloads.';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i nerladdningar!';
$_['error_name']       = 'Namnet måste vara mellan 3 och 64 tecken!';
$_['error_upload']     = 'Upload krävs!';
$_['error_filename']   = 'Filnamnet måste vara mellan 3 och 128 tecken!';
$_['error_exists']     = 'Filen finns inte!';
$_['error_mask']       = 'Mask måste vara mellan 3 och 128 tecken!';
$_['error_filetype']   = 'Felaktigt filformat!';
$_['error_product']    = 'Varning: Den här nerladdninen kan inte raderas eftersom den är kopplad till %s produkter!';
?>