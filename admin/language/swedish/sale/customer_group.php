<?php
// Heading
$_['heading_title']    = 'Kundgrupp';

// Text
$_['text_success']     = 'Klart: Du har ändrat i kundgrupp!';
$_['text_list']         = 'Kundgrupps Lista';
$_['text_add']          = 'Lägg till Kundgrupp';
$_['text_edit']         = 'Ändra i Kundgrupp';

// Column
$_['column_name']        = 'Kundgruppsnamn';
$_['column_sort_order']  = 'Sorteringsordning';
$_['column_action']      = 'Ändra';

// Entry
$_['entry_name']                = 'Kundgruppsnamn:';
$_['entry_description']         = 'Beskrivning:';
$_['entry_approval']            = 'Godkän nya kunder:';
$_['entry_sort_order']          = 'Sorteringsordning:';

// Help
$_['help_approval']     = 'Kunder måste bli godkända först.';

// Error
$_['error_permission'] = 'Varning: Du har inte behörihet att ändra i Kundgrupper!';
$_['error_name']       = 'Kundgruppsnamnet måste innehålla mellan 3 och 64 tecken!';
$_['error_default']    = 'Varning: Denna grupp kan inte raderas eftersom den är kopplad till butikens förvalda kundgrupp!';
$_['error_store']      = 'Varning: Denna grupp kan inte raderas eftersom den är kopplad till %s butiker!';
$_['error_customer']   = 'Varning: Denna grupp kan inte raderas eftersom den är kopplad till %s kunder!';
?>