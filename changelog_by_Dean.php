<?php
/*
 * Change log made by Dean
 * ========================
 * 
 * Format: 
 * date
 * ----
 * * Change log
 */
?>

2017-01-04
----------
- Changed admin/template/common/header.php
	- Added shortcut link Return (row 60)
	- Added links to Project and MF Support (row 94)
	
- Modified admin/controller/sale/return.php
	- Changed date_format_short to date_format_long on line 367 and 368	to get even hours and minutes

2014-08-25
----------
- Sök på Allabolag
	- La till ett sökformulär på raden 180 till 188 i filen admin/sale/order_info.tpl


2013-06-03
----------
- Klarna julkampanj logo
	- product/product.tpl flash code at row 17 and 18 br,br
	- När julkapnjen är över skall du gå in på admin, klarna delbetalningar modulen och spara och uppdatera modulen för att julkampanjen skall försvinna.
	
- onecheckout/payment.tpl added image for klarna_specialcampaigns at line 31



2013-06-03
----------
Admin - added/hardcoded shortcut links to Order, Produkter, Kunder, Inställningar
	- Changed admin/view/template/common/header.tpl
	- Added div and links from line 77 to 84 


2013-06-03
----------
Removing län (Kronobergs län) from order mail
1. Modify: catalog/model/checkout/order.php
	- Changed places for {city} {postcode} to {postcode} {city} at the line 280 and line 314
	- Removed {zone} between city and country at the line 280 and line 314

2. Modification of order mail (orderbekräftelse)
	- Added some br in front of greeting text and betwwen powered and text_footer in catalog/view/theme/gpser/template/mail/order/
	- Translated text_new_powered in language file: catalog/languages/swedish/mail/order.php 


2013-06-03
----------
1. Modified file admin/view/template/sale/order_info.tpl
	- Added js code at the end of the file: $(document).ready(function(){ $('#invoice-create').click(); });
	- Function generates fakturanummer automatically



2013-04-09
----------
Implementerade double.net tracking kod i kassan.

1. Modified /checkout/simplified_checkout_confirm.tpl
	- Added $_SESSION['custom_order_total'] = $product['total']; at line 74

2. Modified /catalog/view/theme/gpser/template/common/success.tpl
	- Added tracking image code from double.net between rows 11 and 19
	

2013-02-13
----------
Added city in orderlist in Admin

1. Modified function getOrders($data = array()) in file /admin/model/sale/order.php
	- Added o.shipping_city field to $sql variable/query at line 308
	- Added array 'shipping_city' at line 336

2. Modified file /admin/controller/sale/order.php
	- Added array 'shipping_city' => $result['shipping_city'] at line 334

3. Modified file /admin/view/template/sale/order_list.tpl
	- Added varibale $order['shipping_city']; at line 92
	

2013-02-12
----------
Google Analytics Conversation and Facebook plugin HTML code added to:
/catalog/view/theme/gpser/template/common/footer.tpl
 

2013-02-11
-----------
Implemented on gpser
Functionality for saving and viewing internal comment on order info page in admin.

1. Created database field internal_comment (text) in order table

2. Modified file /admin/model/sale/order.php
  - Inside function "getOrder($order_id)" and "return array(" added:
  		- 'internal_comment'        => $order_query->row['internal_comment'], (line 237)
  - At the end of the file "addInternalComment($order_id, $data)" function added for saving data to the database

3. Modified file /admin/controller/sale/order.php
  - From line  1207 to 1212 added variable $this->data['internal_comment'] 
  - Ad the end of the file function public function "internal_comment()" added

4. View internal_comment and internal_comment form
   - Modified file /admin/view/template/sale/order_info.php
   - Added code block from around line 138 to 169. Marked with "Custom added html comment"
 

Functionality for viewing comment and internal comment in order list
   
1. Modified file /admin/model/sale/order.php, modified under function "getOrders($data = array())"
   	- Added o.internal_comment and o.comment in SQL query on line 307
   	- Added into array on line 334,335: o.internal_comment and o.comment 
2. Modified file /admin/controller/sale/order.php
	- Inside function "getList()" and array "$this->data['orders'][] = array" added "internal_comment" and "comment" arrays on line 334,335.
3. Modified file /admin/view/template/sale/order_info.php
	- After line 93 added variables $order['internal_comment'], $order['comment'] for viewing indication of comments if not empty

2012-10-02
----------
* Added hours and minutes on orders - date_added
* Modified /admin/controller/sale/order.php, on line 1278 and 1279 changed to function: date('Y-m-d H:i', strtotime( $order_info['date_added'] ) );

2012-04-27
----------

REFERER TRACKING AND USER AGENT
Implemented on gpser.se!
Originally created on monster.selfip.org/opencart152 
--------------------------------------------

* Database field "track_referer" and "user_agent" has been created, varchar(250) on table order

* Catalog - Files created: 
	- catalog/view/theme/mytemplate/template/common/track_referer.tpl

* Catalog - Files that has been modified:
	- catalog/view/theme/default/template/common/header.tpl
    - Code added: include_once 'track_referer.tpl';

	- catalog/model/checkout/order.php
	- catalog/model/onecheckout/checkout.php (row 70 - 80)
    - Variable $track_referer_que created. Read comments in the file!
    - Variable $user_agent created. 

* Admin - Files modified
	- File: admin/view/template/sale/order_info.tpl
	- Added code below $date_modified row:
	<!-- custom added  - track_referer -->
	<tr>
		<td><?php echo $text_track_referer; ?></td>
		<td><?php echo $track_referer; ?></td>
	</tr>
* Modified file: admin/model/sale/order.php
	- Added 'user_agent' => $order_query->row['user_agent']
	- Added	'track_referer' => $order_query->row['track_referer'] 

* Modified file: admin/controller/sale/order.php
	- Added $this->data['text_track_referer'] = $this->language->get('text_track_referer'); inside "function info()"
	- Added $this->data['track_referer'] = $order_info['track_referer']; inside "function info()" after variable "$this->data['lastname']"

* Modified file: admin/language/english/sale/order.php
	- Added $_['text_track_referer'] = 'Track referer:';

* Modified file: admin/model/sale/order.php
	- Added: 'track_referer' => $order_query->row['track_referer'] inside "function getOrder($order_id)" on line xxx below "date_modified"

-----------------------
END OF REFERER TRACKING
-----------------------




2012-03-15
----------
* Admin (1.5.2.1) adj - Dashboard - red text for items to approve
   - Added variables $customer_appr_class, $review_appr_class, $affiliate_appr_class


* Admin (1.5.2.1) adj - Feed list disabled status.
	- Added line 'extension_status' => $this->config->get($extension . '_status')  on line 90 in /admin/controller/extension/feed.php to pull out status from database
	- Added variable $status_class on line 32 in file /admin/view/template/extension/feed.tpl
	
* Admin (1.5.2.1) adj - Order total list disabled status.
	- Added line 'extension_status' => $this->config->get($extension . '_status')  on line 91 in /admin/controller/extension/total.php to pull out status from database
	- Added variable $status_class on line 35 in file /admin/view/template/extension/total.tpl

* Admin (1.5.2.1) adj - Payment list disabled status.
	- Added line 'extension_status' => $this->config->get($extension . '_status')  on line 100 in /admin/controller/extension/shipping.php to pull out status from database
	- Added variable $status_class on line 36 in file /admin/view/template/extension/payment.tpl
	
* Admin (1.5.2.1) adj - Shipping list disabled status.
	- Added line 'extension_status' => $this->config->get($extension . '_status')  on line 91 in /admin/controller/extension/shipping.php to pull out status from database
	- Added variable $status_class on line 35 in file /admin/view/template/extension/shipping.tpl
	
* Admin (1.5.2.1) adj - Modules list disabled status.
	- Added line $status_class on line 33 in /admin/view/template/extension/module.tpl

* Admin (1.5.2.1) adj - Information list disabled status.
	- Added line 'status' => $result['status']  on line 188 in /admin/controller/catalog/information.php to pull out status from database
	- Added variable $status_class on line 42 in file /admin/view/template/catalog/information_list.tpl

* Admin (1.5.2.1) adj - Category list disabled status.
	- Added line 'status' => $result['status'],  on line 105 in /admin/controller/catalog/category.php to pull out status from database
	- Added variable $status_class on line 35 in file /admin/view/template/catalog/category_list.tpl
	
* Admin (1.5.2.1) adj - Product list disabled status on row.
	- Added line 'prod_status' => $result['status'],  on line 105 in /admin/controller/catalog/product.php to pull out status integer from database
	- Added variable $status_class, on line 81 in file /admin/view/template/catalog/product_list.tpl
	
* Admin (1.5.2.1) adj - Review list disabled status on row.
	- Added line 'review_status' => $result['status'],  on line 189 in /admin/controller/catalog/review.php to pull out status integer from database
	- Added variable $status_class, on line 59 in file /admin/view/template/catalog/review_list.tpl	

2012-03-15
----------
* Admin (1.5.2.1) adj - Added links on all titles or names in lists

2012-03-14
----------
* Add classes for different color on every second row
	- .list tr:nth-child(even) td { background:#fafafa; } /* Coloring every second row - Custom added */
	- .list tr.row-on td, table.list tbody tr:hover td  { background:#ffffd8; } /* Highlight row color on hover - Custom added */


2012-03-05
----------
* Admin: Added js function that adds a message to the comment form with best regards message and kollinr only if "levererad" is chosen.


2012-02-29
----------
* Row hover functionality added on common/header.tpl
	<script type="text/javascript">
	/*
	 * Function for making hover on every table row
	 */	
	$(document).ready(function() {
		$("table.list tr").mouseenter(function() {
			catID = $(this).attr('id');
			$('tr#'+catID).addClass('row-on');
		});	
		$("table.list tr").mouseleave(function() {
			catID = $(this).attr('id');
			$('tr#'+catID).removeClass('row-on');
		});	
		
	});
	</script>
	- Add code in the header /admin/view/template/catalog/product_list.tpl
	- Every table row must have an ID - added on many rows $i variable
	- Add .list tr.row-on td { background:#fefeec; } class to admin/view/stylesheet/stylesheet.css

* Admin: Link categoris names in the list to edit
	- Added to line 40-43 in /admin/view/template/catalog/category_list.tpl - <?php foreach ($category['action'] as $action) { ?> <a href="<?php echo $action['href']; ?>"><?php echo $category['name']; ?></a> <?php } ?>

2012-02-28
----------
* Admin: Add .list tr:nth-child(even) td { background:#fafafa; } /* Added */ 
	- admin/view/stylesheet/stylesheet.css to make every second row light gray 
* Admin: Now it is possible to click on a productname to change it. You do not need to click on [Change] on the right
	- Added to line 87-90 in /admin/view/template/catalog/product_list.tpl <?php foreach ($product['action'] as $action) { ?> <a href="<?php echo $action['href']; ?>"><?php echo $product['name']; ?></a> <?php } ?>	

2012-02-17
----------
* Included latest jquery in header.tpl inactivated the old one


2012-01-10
----------
* Waybill (Fraktsedel)
	- Modified admin/controller/sale/order.php - from line 2183 - 2293 cause of Contry and Zone that was printed on waybill

2011-12-19
----------
* Waybill (fraktsedel) functionallity 
	- Changed in admin/languages/swedish/swedish.php & admin/languages/english/english.php	- $_['button_waybill'] added
	- Changed admin/controller/sale/order.php - Added $this->data['button_waybill'] = $this->language->get('button_waybill'); on lines 356 and 1128
	- Changed admin/controller/sale/order.php - Added $this->data['invoice'] = $this->url->link('sale/order/invoice', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], 'SSL');
	- Changed admin/controller/sale/order.php - Copied function invoice() and pasted as waybill() on row 2090  
	- Created button link in admin/view/template/sale/order_info.tpl - Added <a onclick="window.open('<?php echo $waybill; ?>');" class="button"><?php echo $button_waybill; ?></a> on line 12
	- Changed admin/language/swedish/sale/order.php and admin/language/english/sale/order.php - Added $_['text_waybill']
	- Created admin/view/template/sale/order_waybill.tpl - Just copied admin/view/template/sale/order_invoice.tpl


2011-11-28
----------
* Made cart to fall out when Buy button clicked
	- /catalog/view/javascript/common.js						- Inactivated .success... and added $('#cart > .heading a').click();
	- /catalog/view/theme/gpser/template/product/product.tpl	- added js at the end of the file inside function $('#button-cart').bind('click',... $('#cart > .heading a').click();



2011-11-08
----------
*  Added functionality for comment_private (Internal product comments)
*  Database - created 'comment_private' db field in the 'product' table
*  Admin
	- /admin/language/-lang-/catalog/product.php 			- $_['comment_private']  and $_['comment_private'] added 
 	- /admin/view/template/catalog/product_form.tpl 		- around line 92 or search for comment_private
 	- /admin/controller/catalog/product.php 				- search for the 'comment_private' and 'Added by'
 	- /admin/model/catalog/product.php 						- search for the 'comment_private' to find all added occurences
 	
2011-11-07
----------
* Added functionalit for name2
*  Database - created 'name2' db field in the 'product_description' table
	- /admin/view/template/catalog/product_form.tpl 			- line 37 ($entry_name2 trow) or search for the 'Added by Dean'
	- /admin/model/catalog/product.php 							- search for the word 'name2' to find all added occurences
	- /admin/controller/catalog/product.php 					- search for the 'name2' and 'Added by Dean'
	- /admin/language/-lang-/catalog/product.php 				- $_['entry_name2'] added
	
* Store front
	- /catalog/controller/product/product.php 					- heading_title2 added
	- /catalog/view/theme/gpser/template/product/product.tpl	- $heading_title2 added
	- 

2011-11-06
----------
*  Added functionality for price2
*  Database - created 'price2' db field in the 'product' table
*  Admin
	- /admin/language/-lang-/catalog/product.php 			- $_['entry_price2']  and $_['entry_name2'] added 
 	- /admin/view/template/catalog/product_form.tpl 		- line 80 or search for the 'Added by Dean'
 	- /admin/controller/catalog/product.php 				- search for the 'price2' and 'Added by'
 	- /admin/model/catalog/product.php 						- search for the 'price2' to find all added occurences
 	
* Store front
	- /catalog/controller/product/product.php 					- price2 added, search for word 'price2'
	- /catalog/view/theme/gpser/template/product/product.tpl 	- added $text_price2 and $price2 variables
	- /language/-lang-/product/product.php 						- added $_['text_stock_status'] and $_['text_price2'] 