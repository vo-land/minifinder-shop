<?php
// Text
$_['text_success']     = 'Klart: Din kupong är registrerad!';

// Error
$_['error_permission'] = 'Varning: Du har inte tillåtelse att ändra i API!!';
$_['error_coupon']     = 'Varning: Kupong är antingen fel eller är slut!';
?>