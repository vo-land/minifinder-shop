<?php
// Text
$_['text_title']	    = 'VISA / MASTERCARD';
$_['text_testmode']	    = 'ATTENTION!!! The Payment Gateway is in \'Test Mode\'. Your credit card will not be charged.';

// Error
$_['error_reject']	= 'Payment was rejected. Please try again.';
$_['error_no_order']	= 'No order data found';
?>