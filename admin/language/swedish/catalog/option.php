<?php
// Heading
$_['heading_title']       = 'Alternativ Produkter';

// Text
$_['text_success']        = 'Klart: Du har ändrat i alternativ ';
$_['text_list']           = 'Alternativ grupplista';
$_['text_add']            = 'Lägg till Alternativ';
$_['text_edit']           = 'Ändra i Alternativ';
$_['text_choose']         = 'Val';
$_['text_select']         = 'Rullgardin';
$_['text_radio']          = 'Radioknapp';
$_['text_checkbox']       = 'Checkbox';
$_['text_image']          = 'Bild';
$_['text_input']          = 'Infoga';
$_['text_text']           = 'Text';
$_['text_textarea']       = 'Textarea';
$_['text_file']           = 'Fil';
$_['text_date']           = 'Datum';
$_['text_datetime']       = 'Datum - Tid';
$_['text_time']           = 'Tid';

// Column
$_['column_name']         = 'Alternativnamn';
$_['column_sort_order']   = 'Sorteringsordning';
$_['column_action']       = 'Ändra';

// Entry
$_['entry_name']         = 'Alternativnamn:';
$_['entry_type']         = 'Typ:';
$_['entry_option_value'] = 'Alternativvärde namn:';
$_['entry_image']        = 'Bild:';
$_['entry_sort_order']   = 'Sorteringsordning:';

// Error
$_['error_permission']   = 'Varning: Du har inte behörighet att ändra i Alternativ!';
$_['error_name']         = 'Alternativnamnet måste innehålla mellan 1 och 128 tecken!';
$_['error_type']         = 'Varning: Alternativvärde krävs!';
$_['error_option_value'] = 'Alternativvärde måste innehålla mellan 1 och 128 tecken!';
$_['error_product']      = 'Varning: Detta Alternativ kan inte raderas eftersom det är kopplat till %s products!';
?>