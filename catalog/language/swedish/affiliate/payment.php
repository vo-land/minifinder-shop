<?php
// Heading 
$_['heading_title']             = 'Utbetalningssätt';

// Text
$_['text_account']              = 'Affiliatekonto';
$_['text_payment']              = 'Utbetalning';
$_['text_your_payment']         = 'Utbetalningsinformation';
$_['text_your_password']        = 'Ditt lösenord';
$_['text_cheque']               = 'Utbetalningskort';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Banköverföring';
$_['text_success']              = 'Klart: Ditt konto har uppdaterats.';

// Entry
$_['entry_tax']                 = 'Momsregistreringsnummer (VAT-nummer):';
$_['entry_payment']             = 'Utbetalningssätt:';
$_['entry_cheque']              = 'Mottagarens namn:';
$_['entry_paypal']              = 'PayPal-kopplad e-postadress:';
$_['entry_bank_name']           = 'Bankens namn:';
$_['entry_bank_branch_number']  = 'ABA/BSB-nummer (branschnummer):';
$_['entry_bank_swift_code']     = 'SWIFT-kod:';
$_['entry_bank_account_name']   = 'Kontonamn:';
$_['entry_bank_account_number'] = 'Kontonummer:';
?>