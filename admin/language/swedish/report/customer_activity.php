<?php
// Heading
$_['heading_title']       = 'Rapport Kunder Aktivitet';

// Text
$_['text_list']           = 'Kunder Aktivitet Lista';
$_['text_address_add']    = '<a href="Kund=%d">%s</a> skapade en ny adress.';
$_['text_address_edit']   = '<a href="Kund=%d">%s</a> uppdaterade sin adress.';
$_['text_address_delete'] = '<a href="Kund=%d">%s</a> raderade en adress.';
$_['text_edit']           = '<a href="Kund=%d">%s</a> uppdaterade sina inställningar.';
$_['text_forgotten']      = '<a href="Kund=%d">%s</a> ansökte om nytt password.';
$_['text_login']          = '<a href="Kund=%d">%s</a> loggade in.';
$_['text_password']       = '<a href="Kund=%d">%s</a> uppdaterade sitt password.';
$_['text_register']       = '<a href="Kund=%d">%s</a> uppdaterade för ett konto.';
$_['text_return_account'] = '<a href="Kund=%d">%s</a> ansökte om en retur.';
$_['text_return_guest']   = '%s vill göra en retur.';
$_['text_order_account']  = '<a href="Kund=%d">%s</a> skapade ett <a href="order_id=%d">ny order</a>.';
$_['text_order_guest']    = '%s skapade a <a href="order_id=%d">ny order</a>.';

// Column
$_['column_customer']     = 'Kunder';
$_['column_comment']      = 'Kommentar';
$_['column_ip']           = 'IP';
$_['column_date_added']   = 'Datum';

// Entry
$_['entry_customer']      = 'Kunder';
$_['entry_ip']            = 'IP';
$_['entry_date_start']      = 'Startdatum:';
$_['entry_date_end']        = 'Slutdatum:';
?>