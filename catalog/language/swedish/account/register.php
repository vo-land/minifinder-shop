<?php
// Heading 
$_['heading_title']        = 'Registrera konto';

// Text
$_['text_account']         = 'Konto';
$_['text_register']        = 'Registrera';
$_['text_account_already'] = 'Om du redan har ett konto hos oss, vänligen logga in <a href="%s">här</a>.';
$_['text_your_details']    = 'Kontaktuppgifter';
$_['text_your_address']    = 'Adress';
$_['text_newsletter']      = 'Nyhetsbrev';
$_['text_your_password']   = 'Lösenord';
$_['text_agree']           = 'Jag har läst och godkänner <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Kundgrupp:';
$_['entry_firstname']      = 'Förnamn:';
$_['entry_lastname']       = 'Efternamn:';
$_['entry_email']          = 'E-post:';
$_['entry_telephone']      = 'Mobiltelefon:';
$_['entry_fax']            = 'Telefon:';
$_['entry_company']        = 'Företag:';
$_['entry_address_1']      = 'Adress 1:';
$_['entry_address_2']      = 'Adress 2:';
$_['entry_postcode']       = 'Postnr:';
$_['entry_city']           = 'Stad:';
$_['entry_country']        = 'Land:';
$_['entry_zone']           = 'Län:';
$_['entry_newsletter']     = 'Prenumerera:';
$_['entry_password']       = 'Lösenord:';
$_['entry_confirm']        = 'Bekräfta lösenord:';

// Error
$_['error_exists']         = 'Fel: E-postadressen är redan registrerad!';
$_['error_firstname']      = 'Förnamnet måste bestå av minst 1 och högst 32 tecken!';
$_['error_lastname']       = 'Efternamnet måste bestå av minst 1 och högst 32 tecken!';
$_['error_email']          = 'E-postadressen verkar inte vara giltig!';
$_['error_telephone']      = 'Telefonnumret måste bestå av minst 3 och högst 32 tecken!';
$_['error_password']       = 'Lösenordet måste bestå av minst 4 och högst 20 tecken!';
$_['error_confirm']        = 'Lösenordsbekräftelsen stämmer inte med angivet lösenord!';
$_['error_vat']            = 'Företags Org.Nr är felaktigt!';
$_['error_address_1']      = 'Adress 1 måste bestå av minst 3 och högst 128 tecken!';
$_['error_city']           = 'Stad måste bestå av minst 2 och högst 128 tecken!';
$_['error_postcode']       = 'Postnr måste bestå av minst 2 och högst 10 tecken!';
$_['error_country']        = 'Var vänlig välj ett land!';
$_['error_zone']           = 'Var vänlig välj ett län!';
$_['error_agree']          = 'Fel: Du måste godkänna %s!';
?>