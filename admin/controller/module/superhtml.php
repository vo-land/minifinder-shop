<?php
class ControllerModuleSuperhtml extends Controller {
	private $error = array(); 

	public function index() {   

		$this->load->model('setting/setting');
        
        $this->load->model('extension/module');
		
		$this->load->model('tool/image');
		
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['controller'] = clone $this;

		$data['tab_module'] = $this->language->get('tab_module');
		$data += $this->language->load('module/superhtml');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validatesetting() && isset($this->request->get['settings_mode'])) {
			$this->model_setting_setting->editSetting('superhtml', $this->request->post);		

			$this->session->data['success'] = $this->language->get('text_success_groups');
            if (isset($this->request->get['module_id'])) {
			     $this->response->redirect($this->url->link('module/superhtml', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL'));
            } else {
			     $this->response->redirect($this->url->link('module/superhtml', 'token=' . $this->session->data['token'], 'SSL'));
            }
		}
        
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate() && !isset($this->request->get['settings_mode'])) {
			if (!isset($this->request->get['module_id'])) {
				$this->model_extension_module->addModule('superhtml', $this->request->post);
			} else {
				$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}


		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
        
		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}
        
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/superhtml', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);
        
        if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('module/superhtml', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('module/superhtml', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
		}

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        
        if (isset($this->request->get['module_id'])) {
            $data['istemplate'] = false;
        } else {
            $data['istemplate'] = true;
        }

		$data['no_image'] = $this->model_tool_image->resize('no_image.png',50,50);
		$data['placeholder'] = $data['no_image'];
		
		$data['token'] = $this->session->data['token'];
		
		
		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
		}
        
        if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}
        if (isset($this->request->post['group'])) {
			$data['group'] = $this->request->post['group'];
		} elseif (!empty($module_info)) {
			$data['group'] = isset($module_info['group']) ? $module_info['group'] : '';
		} else {
			$data['group'] = array();
		}
        if (isset($this->request->post['cols'])) {
			$data['cols'] = $this->request->post['cols'];
		} elseif (!empty($module_info)) {
			$data['cols'] = $module_info['cols'];
		} else {
			$data['cols'] = array();
		}
        if (isset($this->request->post['gtitle'])) {
			$data['gtitle'] = $this->request->post['gtitle'];
		} elseif (!empty($module_info)) {
			$data['gtitle'] = $module_info['gtitle'];
		} else {
			$data['gtitle'] = array();
		}
        if (isset($this->request->post['ctitle'])) {
			$data['ctitle'] = $this->request->post['ctitle'];
		} elseif (!empty($module_info)) {
			$data['ctitle'] = $module_info['ctitle'];
		} else {
			$data['ctitle'] = array();
		}
        if (isset($this->request->post['min_width'])) {
			$data['min_width'] = $this->request->post['min_width'];
		} elseif (!empty($module_info)) {
			$data['min_width'] = $module_info['min_width'];
		} else {
			$data['min_width'] = '150';
		}
        if (isset($this->request->post['carousel'])) {
			$data['carousel'] = $this->request->post['carousel'];
		} elseif (!empty($module_info)) {
			$data['carousel'] = $module_info['carousel'];
		} else {
			$data['carousel'] = '';
		}
        if (isset($this->request->post['car_itm_desk'])) {
			$data['car_itm_desk'] = $this->request->post['car_itm_desk'];
		} elseif (!empty($module_info)) {
			$data['car_itm_desk'] = $module_info['car_itm_desk'];
		} else {
			$data['car_itm_desk'] = '4';
		}
        if (isset($this->request->post['car_itm_tab'])) {
			$data['car_itm_tab'] = $this->request->post['car_itm_tab'];
		} elseif (!empty($module_info)) {
			$data['car_itm_tab'] = $module_info['car_itm_tab'];
		} else {
			$data['car_itm_tab'] = '2';
		}
        if (isset($this->request->post['car_itm_mob'])) {
			$data['car_itm_mob'] = $this->request->post['car_itm_mob'];
		} elseif (!empty($module_info)) {
			$data['car_itm_mob'] = $module_info['car_itm_mob'];
		} else {
			$data['car_itm_mob'] = '1';
		}
        if (isset($this->request->post['car_speed'])) {
			$data['car_speed'] = $this->request->post['car_speed'];
		} elseif (!empty($module_info)) {
			$data['car_speed'] = $module_info['car_speed'];
		} else {
			$data['car_speed'] = '2000';
		}
        if (isset($this->request->post['carousel_autoplay'])) {
			$data['carousel_autoplay'] = $this->request->post['carousel_autoplay'];
		} elseif (!empty($module_info)) {
			$data['carousel_autoplay'] = $module_info['carousel_autoplay'];
		} else {
			$data['carousel_autoplay'] = '1';
		}
        if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}
		
		if (isset($this->request->post['superhtml_groups'])) {
			$groups = $this->request->post['superhtml_groups']; 
			foreach ($groups as $index => $htmlgroup) {
			$column_data = array();
		   if (isset($htmlgroup['column'])) {
			foreach ($htmlgroup['column'] as $colindex => $htmlcolumn) {
				$column_data[] = array (
					'id'  => $htmlcolumn['id'],
					'key' => $colindex
				);
			}
		   }
			
			$data['groups'][] = array(
				'name'       => $htmlgroup['name'],
				'column'     => $column_data,
				'key'        => $index
			
			);
			$sortgroups[] = $index;
		  }
		  array_multisort($sortgroups,SORT_NUMERIC,$data['groups']);
		} elseif ($this->config->get('superhtml_groups')) { 
		  foreach ($this->config->get('superhtml_groups') as $index => $htmlgroup) {
			$column_data = array();
		   if (isset($htmlgroup['column'])) {
			foreach ($htmlgroup['column'] as $colindex => $htmlcolumn) {
				$column_data[] = array (
					'id'  => $htmlcolumn['id'],
					'key' => $colindex
				);
			}
		   }
			
			$data['groups'][] = array(
				'name'       => $htmlgroup['name'],
				'column'     => $column_data,
				'key'        => $index
			
			);
			$sortgroups[] = $index;
		  }
		  array_multisort($sortgroups,SORT_NUMERIC,$data['groups']);
		} else {
			$data['groups'] = array();
		}


		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/superhtml.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/superhtml')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
        
        if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}
        
		return !$this->error;	
	}
    protected function validatesetting() {
		if (!$this->user->hasPermission('modify', 'module/superhtml')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
        
		return !$this->error;	
	}
}
?>