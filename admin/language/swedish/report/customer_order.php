<?php
// Heading
$_['heading_title']         = 'Rapport - kundordrar';

// Text
$_['text_list']             = 'Kunders Orders Lista';
$_['text_all_status']       = 'Alla statusar';

// Column
$_['column_customer']       = 'Kundnamn';
$_['column_email']          = 'E-post';
$_['column_customer_group'] = 'Kundgrupp';
$_['column_status']         = 'Status';
$_['column_orders']         = 'Antal ordrar';
$_['column_products']       = 'Antal produkter';
$_['column_total']          = 'Summa';
$_['column_action']         = 'Ändra';

// Entry
$_['entry_date_start']      = 'Startdatum:';
$_['entry_date_end']        = 'Slutdatum:';
$_['entry_status']          = 'Orderstatus:';
?>