<?php
// Text
$_['text_title']       = 'Förskottsbetalning';
$_['text_instruction'] = 'Förskottsbetalning';
$_['text_description'] = 'Betalningsanvisningar';
$_['text_payment']     = 'Ordern levereras omgående after att betalningen har inkommit till vårt bankkonto. Tänk på att det tar ca 24h innan din betalning blir bokfört.';


// Extra mail text - Custom
$_['text_payment_mail']  = 'Vid betalning vänligen uppge ditt ordernummer som betalningsreferens.';

?> 