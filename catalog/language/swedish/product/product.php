<?php
// Text
$_['text_search']          = 'Sök';
$_['text_brand']           = 'Märke';
$_['text_manufacturer']    = 'Tillverkare:';
$_['text_model']           = 'Artikelnummer:';
$_['text_reward']          = 'Bonuspoäng:'; 
$_['text_points']          = 'Pris med bonuspoäng:';
$_['text_stock']           = 'Lagerstatus:';
$_['text_instock']         = 'I lager';
$_['text_tax']             = 'Exkl. moms:'; 
$_['text_discount']        = '%s eller mer %s';
$_['text_option']          = 'Tillgängliga alternativ';
$_['text_minimum']         = 'Denna produkt har ett krav på minimiantal som är %s';
$_['text_reviews']         = '%s recensioner'; 
$_['text_write']           = 'Skriv en recension';
$_['text_login']           = 'Vänligen <a href="%s">logga in</a> eller <a href="%s">skapa ett konto</a> för att skriva omdöme';
$_['text_no_reviews']      = 'Det finns inga recensioner för denna produkt.';
$_['text_note']            = '<span style="color: #FF0000;">OBS:</span> HTML översätts inte!';
$_['text_success']         = 'Tack för din recension! Den har skickats till butiken för godkännande.';
$_['text_related']         = 'Tillbehör';
$_['text_tags']            = 'Taggar:';
$_['text_error']           = 'Produkten kunde inte hittas!';
$_['text_payment_recurring']   = 'Betalnings Profiler';
$_['text_trial_description']   = '%s alla %d %s(s) för %d betalningar då';
$_['text_payment_description'] = '%s alla %d %s(s) för %d betalningar';
$_['text_payment_until_canceled_description'] = '%s alla %d %s(s) förrän avslutad';
$_['text_day'] = 'dag';
$_['text_week'] = 'vecka';
$_['text_semi_month'] = 'halv månad';
$_['text_month'] = 'månad';
$_['text_year'] = 'år';

// Entry
$_['entry_qty']         = 'Antal';
$_['entry_name']        = 'Ditt namn:';
$_['entry_review']      = 'Din recension:';
$_['entry_rating']      = 'Betyg:';
$_['entry_good']        = 'Utmärkt';
$_['entry_bad']         = 'Dålig';
$_['entry_captcha']     = 'Ange koden i rutan nedan:';

// Tabs
$_['tab_description']   = 'Beskrivning';
$_['tab_attribute']     = 'Specifikationer';
$_['tab_review']        = 'Recensioner (%s)';

// Error
$_['error_name']        = 'Fel: Recensionens namn måste bestå av minst 3 och högst 25 tecken!';
$_['error_text']        = 'Fel: Recensionstexten måste bestå av minst 25 och högst 1000 tecken!';
$_['error_rating']      = 'Fel: Var vänlig välj en betygskala!';
$_['error_captcha']     = 'Fel: Verifikationskoden matchar inte bilden!';

// Custom added
$_['text_month_short'] = 'mån'; // Custom added
$_['text_subscription'] = 'Abonnemang'; // Custom added
$_['text_price2']       = 'Rek. pris:'; // Custom added
$_['text_stock_status'] = 'Lagerstatus:'; // Custom added
$_['text_stock_delivery'] 	= 'Skickas om:'; // Custom added
$_['text_stock_instock'] = 'I lager'; // Custom added
$_['text_stock_nostock'] = 'Lev. om 5 - 10 dagar'; // Custom added
$_['text_counter_hour'] = 't'; // Custom added

?>