<?php
// Heading
$_['heading_title']     = 'Filters';

// Text
$_['text_success']      = 'Klart: Du har ändrat filters!';
$_['text_list']         = 'Alternativ grupplista';
$_['text_add']          = 'Lägg till Filter';
$_['text_edit']         = 'Ändra i Filter';

// Column
$_['column_group']      = 'Filter Grupp';
$_['column_sort_order'] = 'Sorteringsordning';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_group']       = 'Filter Grupp Namn:';
$_['entry_name']        = 'Filter Namn:';
$_['entry_sort_order']  = 'Sorteringsordning:';

// Error
$_['error_permission']  = 'Varning: Du har inte tillåtelse att ändra filters!';
$_['error_group']       = 'Filter Grupp Namn måste vara mellan 1 och 64 tecken!';
$_['error_name']        = 'Filter Namn måste vara mellan 1 och 64 tecken!';
?>