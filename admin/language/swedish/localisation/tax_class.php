<?php
// Heading
$_['heading_title']     = 'Momsklass';

// Text
$_['text_shipping']     = 'Leveransadress';
$_['text_payment']      = 'Faktura-/betalningsadress';
$_['text_store']        = 'Butikens adress';
$_['text_success']      = 'Klart: Du har ändrat i Momsklass!';
$_['text_list']         = 'Layout Lista';
$_['text_add']          = 'Lägg till Momsklass';
$_['text_edit']         = 'Ändra i Momsklass';

// Column
$_['column_title']      = 'Momsklass';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_title']       = 'Namn på Momsklass:';
$_['entry_description'] = 'Beskrivning:';
$_['entry_rate']        = 'Momsklass:';
$_['entry_based']       = 'Baserat på:';
$_['entry_geo_zone']    = 'Geozonw:';
$_['entry_priority']    = 'Sorteringsordning:';

// Error
$_['error_permission']  = 'Varning: Du har inte behörighet att ändra i Momsklass!';
$_['error_title']       = 'Namnet måste innehålla mellan 3 och 32 tecken!';
$_['error_description'] = 'Beskrivningen måste innehålla mellan 3 och 255 tecken!';
$_['error_product']     = 'Varning: Denna kan inte raderas eftersom den är kopplad till %s produkter!';
?>
