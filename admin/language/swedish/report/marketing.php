<?php
// Heading
$_['heading_title']    = 'Rapport Kampanjer';

// Text
$_['text_list']         = 'Kampanj Lista';
$_['text_all_status']   = 'Alla Statusar';

// Column
$_['column_campaign']  = 'Kampanj Namn';
$_['column_code']      = 'Kod';
$_['column_clicks']    = 'Antal klick';
$_['column_orders']    = 'Attal Orders';
$_['column_total']     = 'Totalt';

// Entry
$_['entry_date_start']      = 'Startdatum:';
$_['entry_date_end']        = 'Slutdatum:';
$_['entry_status']     = 'Order Status';
?>