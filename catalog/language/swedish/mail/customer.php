<?php
// CUSTOM - Pregenerated password at checkout page
$cust_pass = isset( $_SESSION['payment_address']['password'] ) ? $_SESSION['payment_address']['password'] : NULL;

// Text
$_['text_subject']        = '%s - Tack för din registrering';
$_['text_welcome']        = 'Välkommen och tack för din registrering hos oss på %s!';
$_['text_login']          = 'Ditt konto har nu skapats och du kan logga in med hjälp av din e-postadress och lösenord ('.$cust_pass.') via vår hemsida eller genom att klicka på följande länk:';
$_['text_approval']       = 'Ditt konto måste godkännas innan du kan logga in. När ditt konto har godkänts kan du logga in med hjälp av din e-postadress och lösenord via vår hemsida eller genom att klicka på följande länk:';
$_['text_services']       = 'När du loggat in kan du se din orderhistorik och ändra dina person- och adressuppgifter.';
$_['text_thanks']         = 'Tack!';
$_['text_new_customer']   = 'Ny kund';
$_['text_signup']         = 'Ny kund är registrerad:';
$_['text_website']        = 'Hemsida:';
$_['text_customer_group'] = 'Kundgrupp:';
$_['text_firstname']      = 'Förnamn:';
$_['text_lastname']       = 'Efternamn:';
$_['text_email']          = 'E-postadress:';
$_['text_telephone']      = 'Mobiltelefon:';
?>