<?php
// Heading
$_['heading_title']     = 'Ordertotal';

// Text
$_['text_success']      = 'Klart: Du har nu ändrat i Ordertotal!';
$_['text_list']         = 'Layoutlista';

// Column
$_['column_name']       = 'Ordertotal';
$_['column_status']     = 'Status';
$_['column_sort_order'] = 'Sorteringsordning';
$_['column_action']     = 'Ändra';

// Error
$_['error_permission']  = 'Varning: Du har inte behörighet att ändra i Ordertotal!';
?>