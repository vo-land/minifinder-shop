<?php
// Heading 
$_['heading_title']                = 'Kontoinloggning';

// Text
$_['text_account']                 = 'Konto';
$_['text_login']                   = 'Logga in';
$_['text_new_customer']            = 'Ny kund';
$_['text_register']                = 'Registrera konto';
$_['text_register_account']        = 'Genom att skapa ett konto kan du handla snabbare, se din orderstatus och få tillgång till din orderhistorik.';
$_['text_returning_customer']      = 'Återkommande kund';
$_['text_i_am_returning_customer'] = 'Jag är en återkommande kund';
$_['text_forgotten']               = 'Glömt lösenord?';

// Entry
$_['entry_email']                  = 'E-postadress';
$_['entry_password']               = 'Lösenord';

// Error
$_['error_login']                  = 'Fel: Den angivna e-postadressen och/eller lösenordet kunde inte hittas i våra register.';
$_['error_attempts']               = 'Varning: Ditt konto är stängt under 1 timme för att du försökt logga in flera gånger.';
$_['error_approved']               = 'Varning: Ditt konto behöver godkännande för att logga in.'; 
?>