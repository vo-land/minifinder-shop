<?php
$_['heading_title']           = 'Återkommande betalningar';
$_['button_continue']         = 'Fortsätt';
$_['button_view']             = 'Se';
$_['text_empty']              = 'Ingen återkommande betalningsprofil finns.';
$_['text_product']            = 'Produkt: ';
$_['text_order']              = 'Order: ';
$_['text_quantity']           = 'Antal: ';
$_['text_account']            = 'Konto';
$_['text_action']             = 'Ändra';
$_['text_recurring']          = 'Återkommande betalning';
$_['text_transactions']       = 'Transaktioner';
$_['button_return']           = 'Tillbaka';
$_['text_empty_transactions'] = 'Det finns ingen transaktion på denna profil.';

$_['column_created']          = 'Skapa';
$_['column_type']             = 'Typ';
$_['column_amount']           = 'Summa';
$_['column_status']           = 'Status';
$_['column_product']          = 'Produkt';
$_['column_action']           = 'Ändra';
$_['column_profile_id']       = 'Profil ID';

$_['text_recurring_detail']      = 'Detaljer för återkommande betalningar';
$_['text_recurring_id']          = 'Profil ID: ';
$_['text_payment_method']        = 'Betalningsalternativ: ';
$_['text_date_added']            = 'Skapa: ';
$_['text_recurring_description'] = 'Beskrivning: ';
$_['text_status']                = 'Status: ';
$_['text_ref']                   = 'Referenser: ';

$_['text_status_active']         = 'Aktiv';
$_['text_status_inactive']       = 'Inaktivera';
$_['text_status_cancelled']      = 'Avbruten';
$_['text_status_suspended']      = 'Avslutad';
$_['text_status_expired']        = 'Upphört';
$_['text_status_pending']        = 'Behandlas';

$_['text_transaction_created']             = 'Skapa';
$_['text_transaction_payment']             = 'Betalning';
$_['text_transaction_outstanding_payment'] = 'Ej betalt';
$_['text_transaction_skipped']             = 'Betalning skippad';
$_['text_transaction_failed']              = 'Betalning misslyckad';
$_['text_transaction_cancelled']           = 'Avbruen';
$_['text_transaction_suspended']           = 'Avslutad';
$_['text_transaction_suspended_failed']    = 'Avslutad för misslyckad betalning';
$_['text_transaction_outstanding_failed']  = 'Utstående betalning misslyckades';
$_['text_transaction_expired']             = 'Upphört';

$_['error_not_cancelled'] = 'Error: %s';
$_['error_not_found']     = 'Kunde inte avsluta profilen.';
$_['success_cancelled']   = 'Återkommande betalning har avslutats';
?>