<?php
// Heading 
$_['heading_title']      = 'Produktretur';

// Text
$_['text_account']       = 'Konto';
$_['text_return']        = 'Returinformation';
$_['text_return_detail'] = 'Return Details';
$_['text_description']   = '<p>Fyll i formuläret nedan för att begära ett returnummer.</p>';
$_['text_order']         = 'Orderinformation';
$_['text_product']       = 'Produktinformation och anledning till retur';
$_['text_message']       = '<p>Tack för din returförfrågan.</p><p> Status på din förfrågan kommer att meddelas via e-post.</p>';
$_['text_return_id']     = 'Returid:';
$_['text_order_id']      = 'Orderid:';
$_['text_date_ordered']  = 'Orderdatum:';
$_['text_status']        = 'Status:';
$_['text_date_added']    = 'Datum:';
$_['text_comment']       = 'Returkommentarer';
$_['text_history']       = 'Returhistorik';
$_['text_empty']         = 'Du har inte returnerat några produkter!';
$_['text_agree']         = 'Jag har läst och förstår <a href="%s" class="agree"><b>%s</b></a>';

// Column
$_['column_return_id']   = 'Retur ID';
$_['column_order_id']    = 'Order ID';
$_['column_status']      = 'Status';
$_['column_date_added']  = 'Datum';
$_['column_customer']    = 'Kund';
$_['column_product']     = 'Produktnamn';
$_['column_model']       = 'Artikelnummer';
$_['column_quantity']    = 'Antal';
$_['column_price']       = 'Pris';
$_['column_opened']      = 'Öppnad';
$_['column_comment']     = 'Kommentar';
$_['column_reason']      = 'Anledning';
$_['column_action']      = 'Åtgärd';

// Entry
$_['entry_order_id']     = 'Orderid:';
$_['entry_date_ordered'] = 'Orderdatum:';
$_['entry_firstname']    = 'Förnamn:';
$_['entry_lastname']     = 'Efternamn:';
$_['entry_email']        = 'E-post:';
$_['entry_telephone']    = 'Mobiltelefon:';
$_['entry_product']      = 'Produktnamn:';
$_['entry_model']        = 'Artikelnummer:';
$_['entry_quantity']     = 'Antal:';
$_['entry_reason']       = 'Anledning till retur:';
$_['entry_opened']       = 'Produkten är öppnad:';
$_['entry_fault_detail'] = 'Defekt eller andra detaljer:';
$_['entry_captcha']      = 'Ange koden i rutan nedan:';

// Error
$_['text_error']         = 'Returen som du försökte nå går ej att hitta!';
$_['error_order_id']     = 'Orderid krävs!';
$_['error_firstname']    = 'Förnamnet måste bestå av minst 1 och högst 32 tecken!';
$_['error_lastname']     = 'Efternamnet måste bestå av minst 1 och högst 32 tecken!';
$_['error_email']        = 'E-postadressen verkar inte vara giltig!';
$_['error_telephone']    = 'Telefon måste bestå av minst 3 och högst 32 tecken!';
$_['error_product']      = 'Du måste välja minst en produkt!';
$_['error_model']        = 'Artikelnummer måste bestå av minst 3 och högst 64 tecken!';
$_['error_reason']       = 'Du måste ange anledningen till din produktretur!';
$_['error_captcha']      = 'Verifikationskoden matchar inte bilden!';
$_['error_agree']        = 'Varning: Du måste godkänna %s!';
?>