<?php 
class ControllerFeedUksbGoogleMerchant extends Controller {
	private $error = array(); 
	
	public function index() {
		$this->load->language('uksb_licensing/uksb_licensing');
		
		$data['regerror_email'] = $this->language->get('regerror_email');
		$data['regerror_orderid'] = $this->language->get('regerror_orderid');
		$data['regerror_noreferer'] = $this->language->get('regerror_noreferer');
		$data['regerror_localhost'] = $this->language->get('regerror_localhost');
		$data['regerror_licensedupe'] = $this->language->get('regerror_licensedupe');
		$data['regerror_quote_msg'] = $this->language->get('regerror_quote_msg');
		$data['license_purchase_thanks'] = $this->language->get('license_purchase_thanks');
		$data['license_registration'] = $this->language->get('license_registration');
		$data['license_opencart_email'] = $this->language->get('license_opencart_email');
		$data['license_opencart_orderid'] = $this->language->get('license_opencart_orderid');
		$data['check_email'] = $this->language->get('check_email');
		$data['check_orderid'] = $this->language->get('check_orderid');
		$data['server_error_curl'] = $this->language->get('server_error_curl');
		
		$data['uksb_install_link'] = $this->url->link('feed/uksb_google_merchant/uksb_install', 'token=' . $this->session->data['token'], 'SSL');
		

		$this->load->language('feed/uksb_google_merchant');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('uksb_google_merchant', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['config_cron'] = $this->config->get('uksb_google_merchant_cron');
		$data['config_language'] = $this->config->get('config_language');
		$data['config_currency'] = $this->config->get('config_currency');

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_condition_new'] = $this->language->get('text_condition_new');
		$data['text_condition_used'] = $this->language->get('text_condition_used');
		$data['text_condition_ref'] = $this->language->get('text_condition_ref');
		$data['text_male'] = $this->language->get('text_male');
		$data['text_female'] = $this->language->get('text_female');
		$data['text_unisex'] = $this->language->get('text_unisex');
		$data['text_newborn'] = $this->language->get('text_newborn');
		$data['text_toddler'] = $this->language->get('text_toddler');
		$data['text_infant'] = $this->language->get('text_infant');
		$data['text_kids'] = $this->language->get('text_kids');
		$data['text_adult'] = $this->language->get('text_adult');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_location'] = $this->language->get('text_location');
		$data['text_gtin'] = $this->language->get('text_gtin');
		$data['text_mpn'] = $this->language->get('text_mpn');
		$data['text_sku'] = $this->language->get('text_sku');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_initialise_data'] = $this->language->get('text_initialise_data');
		$data['text_initialise_data_text'] = $this->language->get('text_initialise_data_text');
		
		$data['tab_general_settings'] = $this->language->get('tab_general_settings');
		$data['tab_google_settings'] = $this->language->get('tab_google_settings');
		$data['tab_google_feeds'] = $this->language->get('tab_google_feeds');
		$data['tab_bing_feeds'] = $this->language->get('tab_bing_feeds');
		$data['tab_utilities'] = $this->language->get('tab_utilities');
		$data['tab_videos'] = $this->language->get('tab_videos');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_condition'] = $this->language->get('entry_condition');
		$data['entry_mpn'] = $this->language->get('entry_mpn');
		$data['entry_gtin'] = $this->language->get('entry_gtin');
		$data['entry_gender'] = $this->language->get('entry_gender');
		$data['entry_age_group'] = $this->language->get('entry_age_group');
		$data['entry_characters'] = $this->language->get('entry_characters');
		$data['entry_split'] = $this->language->get('entry_split');
		$data['entry_cron'] = $this->language->get('entry_cron');
		$data['entry_site'] = $this->language->get('entry_site');
		$data['entry_google_category'] = $this->language->get('entry_google_category');
		$data['entry_choose_google_category'] = $this->language->get('entry_choose_google_category');
		$data['entry_choose_google_category_xml'] = $this->language->get('entry_choose_google_category_xml');
		$data['entry_info'] = $this->language->get('entry_info');
		$data['entry_data_feed'] = $this->language->get('entry_data_feed');
		$data['entry_cron_code'] = $this->language->get('entry_cron_code');
				
		$data['help_brand'] = $this->language->get('help_brand');
		$data['help_condition'] = $this->language->get('help_condition');
		$data['help_mpn'] = $this->language->get('help_mpn');
		$data['help_gtin'] = $this->language->get('help_gtin');
		$data['help_gender'] = $this->language->get('help_gender');
		$data['help_age_group'] = $this->language->get('help_age_group');
		$data['help_characters'] = $this->language->get('help_characters');
		$data['help_split'] = $this->language->get('help_split');
		$data['help_split_help'] = $this->language->get('help_split_help');
		$data['help_cron'] = $this->language->get('help_cron');
		$data['help_cron_code'] = $this->language->get('help_cron_code');
		$data['help_site'] = $this->language->get('help_site');
		$data['help_google_category'] = $this->language->get('help_google_category');
		$data['help_info'] = $this->language->get('help_info');
		
		$data['utilities1'] = $this->language->get('utilities1');
		$data['utilities2'] = $this->language->get('utilities2');
		$data['utilities3'] = $this->language->get('utilities3');
		$data['utilities4'] = $this->language->get('utilities4');
		$data['utilities5'] = $this->language->get('utilities5');
		$data['utilities6'] = $this->language->get('utilities6');
		$data['utilities7'] = $this->language->get('utilities7');
		$data['utilities8'] = $this->language->get('utilities8');
		$data['utilities9'] = $this->language->get('utilities9');
		$data['utilities_confirm'] = $this->language->get('utilities_confirm');
		
		$data['button_run'] = $this->language->get('button_run');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
 		if (isset($this->error['duplicate'])) {
			$data['error_duplicate'] = $this->error['duplicate'];
		} else {
			$data['error_duplicate'] = '';
		}
		
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_feed'),
			'href'      => $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL')
   		);
				
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('feed/uksb_google_merchant', 'token=' . $this->session->data['token'], 'SSL')
   		);
				
		$data['action'] = $this->url->link('feed/uksb_google_merchant', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['uksb_google_merchant_status'])) {
			$data['uksb_google_merchant_status'] = $this->request->post['uksb_google_merchant_status'];
		} else {
			$data['uksb_google_merchant_status'] = $this->config->get('uksb_google_merchant_status');
		}
		
		if (isset($this->request->post['uksb_google_merchant_mpn'])) {
			$data['uksb_google_merchant_mpn'] = $this->request->post['uksb_google_merchant_mpn'];
		} else {
			$data['uksb_google_merchant_mpn'] = $this->config->get('uksb_google_merchant_mpn');
		}
		
		if (isset($this->request->post['uksb_google_merchant_condition'])) {
			$data['uksb_google_merchant_condition'] = $this->request->post['uksb_google_merchant_condition'];
		} else {
			$data['uksb_google_merchant_condition'] = $this->config->get('uksb_google_merchant_condition');
		}
		
		if (isset($this->request->post['uksb_google_merchant_g_gtin'])) {
			$data['uksb_google_merchant_g_gtin'] = $this->request->post['uksb_google_merchant_g_gtin'];
		} else {
			$data['uksb_google_merchant_g_gtin'] = $this->config->get('uksb_google_merchant_g_gtin');
		}
		
		if (isset($this->request->post['uksb_google_merchant_gender'])) {
			$data['uksb_google_merchant_gender'] = $this->request->post['uksb_google_merchant_gender'];
		} else {
			$data['uksb_google_merchant_gender'] = $this->config->get('uksb_google_merchant_gender');
		}
		
		if (isset($this->request->post['uksb_google_merchant_age_group'])) {
			$data['uksb_google_merchant_age_group'] = $this->request->post['uksb_google_merchant_age_group'];
		} else {
			$data['uksb_google_merchant_age_group'] = $this->config->get('uksb_google_merchant_age_group');
		}
				
		if (isset($this->request->post['uksb_google_merchant_characters'])) {
			$data['uksb_google_merchant_characters'] = $this->request->post['uksb_google_merchant_characters'];
		} else {
			$data['uksb_google_merchant_characters'] = $this->config->get('uksb_google_merchant_characters');
		}
		
		if (isset($this->request->post['uksb_google_merchant_split'])) {
			$data['uksb_google_merchant_split'] = $this->request->post['uksb_google_merchant_split'];
		} else {
			$data['uksb_google_merchant_split'] = $this->config->get('uksb_google_merchant_split');
		}
		
		if (isset($this->request->post['uksb_google_merchant_cron'])) {
			$data['uksb_google_merchant_cron'] = $this->request->post['uksb_google_merchant_cron'];
		} else {
			$data['uksb_google_merchant_cron'] = $this->config->get('uksb_google_merchant_cron');
		}
		
		if (isset($this->request->post['uksb_google_merchant_google_category_gb'])) {
			$data['uksb_google_merchant_google_category_gb'] = $this->request->post['uksb_google_merchant_google_category_gb'];
			$data['uksb_google_merchant_google_category_us'] = $this->request->post['uksb_google_merchant_google_category_us'];
			$data['uksb_google_merchant_google_category_au'] = $this->request->post['uksb_google_merchant_google_category_au'];
			$data['uksb_google_merchant_google_category_fr'] = $this->request->post['uksb_google_merchant_google_category_fr'];
			$data['uksb_google_merchant_google_category_de'] = $this->request->post['uksb_google_merchant_google_category_de'];
			$data['uksb_google_merchant_google_category_it'] = $this->request->post['uksb_google_merchant_google_category_it'];
			$data['uksb_google_merchant_google_category_nl'] = $this->request->post['uksb_google_merchant_google_category_nl'];
			$data['uksb_google_merchant_google_category_es'] = $this->request->post['uksb_google_merchant_google_category_es'];
			$data['uksb_google_merchant_google_category_pt'] = $this->request->post['uksb_google_merchant_google_category_pt'];
			$data['uksb_google_merchant_google_category_cz'] = $this->request->post['uksb_google_merchant_google_category_cz'];
			$data['uksb_google_merchant_google_category_jp'] = $this->request->post['uksb_google_merchant_google_category_jp'];
			$data['uksb_google_merchant_google_category_dk'] = $this->request->post['uksb_google_merchant_google_category_dk'];
			$data['uksb_google_merchant_google_category_no'] = $this->request->post['uksb_google_merchant_google_category_no'];
			$data['uksb_google_merchant_google_category_pl'] = $this->request->post['uksb_google_merchant_google_category_pl'];
			$data['uksb_google_merchant_google_category_ru'] = $this->request->post['uksb_google_merchant_google_category_ru'];
			$data['uksb_google_merchant_google_category_sv'] = $this->request->post['uksb_google_merchant_google_category_sv'];
			$data['uksb_google_merchant_google_category_tr'] = $this->request->post['uksb_google_merchant_google_category_tr'];
		} else {
			$data['uksb_google_merchant_google_category_gb'] = $this->config->get('uksb_google_merchant_google_category_gb');
			$data['uksb_google_merchant_google_category_us'] = $this->config->get('uksb_google_merchant_google_category_us');
			$data['uksb_google_merchant_google_category_au'] = $this->config->get('uksb_google_merchant_google_category_au');
			$data['uksb_google_merchant_google_category_fr'] = $this->config->get('uksb_google_merchant_google_category_fr');
			$data['uksb_google_merchant_google_category_de'] = $this->config->get('uksb_google_merchant_google_category_de');
			$data['uksb_google_merchant_google_category_it'] = $this->config->get('uksb_google_merchant_google_category_it');
			$data['uksb_google_merchant_google_category_nl'] = $this->config->get('uksb_google_merchant_google_category_nl');
			$data['uksb_google_merchant_google_category_es'] = $this->config->get('uksb_google_merchant_google_category_es');
			$data['uksb_google_merchant_google_category_pt'] = $this->config->get('uksb_google_merchant_google_category_pt');
			$data['uksb_google_merchant_google_category_cz'] = $this->config->get('uksb_google_merchant_google_category_cz');
			$data['uksb_google_merchant_google_category_jp'] = $this->config->get('uksb_google_merchant_google_category_jp');
			$data['uksb_google_merchant_google_category_dk'] = $this->config->get('uksb_google_merchant_google_category_dk');
			$data['uksb_google_merchant_google_category_no'] = $this->config->get('uksb_google_merchant_google_category_no');
			$data['uksb_google_merchant_google_category_pl'] = $this->config->get('uksb_google_merchant_google_category_pl');
			$data['uksb_google_merchant_google_category_ru'] = $this->config->get('uksb_google_merchant_google_category_ru');
			$data['uksb_google_merchant_google_category_sv'] = $this->config->get('uksb_google_merchant_google_category_sv');
			$data['uksb_google_merchant_google_category_tr'] = $this->config->get('uksb_google_merchant_google_category_tr');
		}

		$this->load->model('feed/uksb_google_merchant');
		
		
		$data['data_feed'] = '';
		$data['data_bingfeed'] = '';
		if(!$this->config->get('uksb_google_merchant_cron')){
			if($this->config->get('uksb_google_merchant_split')>0){
				$split = $this->config->get('uksb_google_merchant_split');
				$totalproducts = $this->model_feed_uksb_google_merchant->getTotalProductsByStore(0);
				if($totalproducts>$split){
					$j = floor($totalproducts/$split);
					$rem = $totalproducts-($j*$split);
					for($i=1; $i<=$j; $i++){
						$from = (($i-1)*$split)+1;
						$to = $i*$split;
						$data['data_feed'] .= ($i>1?'^':'').HTTP_CATALOG.'index.php?route=feed/uksb_google_merchant&send='.$from.'-'.$to;
						$data['data_bingfeed'] .= ($i>1?'^':'').HTTP_CATALOG.'index.php?route=feed/uksb_bing_shopping&send='.$from.'-'.$to;
					}
					if($rem>0){
						$data['data_feed'] .= '^'.HTTP_CATALOG.'index.php?route=feed/uksb_google_merchant&send='.($to+1).'-'.($to+$split);
						$data['data_bingfeed'] .= '^'.HTTP_CATALOG.'index.php?route=feed/uksb_bing_shopping&send='.($to+1).'-'.($to+$split);
					}
				}else{
					$data['data_feed'] = HTTP_CATALOG.'index.php?route=feed/uksb_google_merchant';
					$data['data_bingfeed'] = HTTP_CATALOG.'index.php?route=feed/uksb_bing_shopping';
				}
			}else{
				$data['data_feed'] = HTTP_CATALOG.'index.php?route=feed/uksb_google_merchant';
				$data['data_bingfeed'] = HTTP_CATALOG.'index.php?route=feed/uksb_bing_shopping';
			}
		}else{
			$data['data_feed'] = HTTP_CATALOG.'uksb_feeds/';
			$data['data_cron_path'] = HTTP_CATALOG.'index.php?route=feed/uksb_google_merchant&mode=cron';
			
			$data['data_bingfeed'] = HTTP_CATALOG.'index.php?route=feed/uksb_bing_shopping';
		}
		
		$this->load->model('setting/store');

		if($this->model_setting_store->getTotalStores()>0){
			$stores = $this->model_setting_store->getStores();
			$stores = array_reverse($stores);
			
			foreach($stores as $store){
				if(!$this->config->get('uksb_google_merchant_cron')){
					if($this->config->get('uksb_google_merchant_split')>0){
						$split = $this->config->get('uksb_google_merchant_split');
						$totalproducts = $this->model_feed_uksb_google_merchant->getTotalProductsByStore($store['store_id']);
						if($totalproducts>$split){
							$j = floor($totalproducts/$split);
							$rem = $totalproducts-($j*$split);
							for($i=1; $i<=$j; $i++){
								$from = (($i-1)*$split)+1;
								$to = $i*$split;
								$data['data_feed'] .= '^'.$store['url'].'index.php?route=feed/uksb_google_merchant&send='.$from.'-'.$to;
								$data['data_bingfeed'] .= '^'.$store['url'].'index.php?route=feed/uksb_bing_shopping&send='.$from.'-'.$to;
							}
							if($rem>0){
								$data['data_feed'] .= '^'.$store['url'].'index.php?route=feed/uksb_google_merchant&send='.($to+1).'-'.($to+$split);
								$data['data_bingfeed'] .= '^'.$store['url'].'index.php?route=feed/uksb_bing_shopping&send='.($to+1).'-'.($to+$split);
							}
						}else{
							$data['data_feed'] .= '^'.$store['url'].'index.php?route=feed/uksb_google_merchant';
							$data['data_bingfeed'] .= '^'.$store['url'].'index.php?route=feed/uksb_bing_shopping';
						}
					}else{
						$data['data_feed'] .= '^'.$store['url'].'index.php?route=feed/uksb_google_merchant';
						$data['data_bingfeed'] .= '^'.$store['url'].'index.php?route=feed/uksb_bing_shopping';
					}
				}else{
					$data['data_feed'] .= '^'.$store['url'].'uksb_feeds/';
					$data['data_cron_path'] .= '^'.$store['url'].'index.php?route=feed/uksb_google_merchant&mode=cron';
			
					$data['data_bingfeed'] .= '^'.$store['url'].'index.php?route=feed/uksb_bing_shopping';
				}
			}
		}
		
		$data['state'] = $this->model_feed_uksb_google_merchant->checkInstallState();
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('feed/uksb_google_merchant.tpl', $data));
	} 
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'feed/uksb_google_merchant')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (isset($this->request->post['uksb_google_merchant_status'])&&$this->request->post['uksb_google_merchant_mpn']==$this->request->post['uksb_google_merchant_g_gtin']) {
			$this->error['duplicate'] = $this->language->get('error_duplicate');
		}

		return !$this->error;
	}	

	public function uksb_install() {
		$this->load->model('feed/uksb_google_merchant');
		$this->model_feed_uksb_google_merchant->uksbInstall();
		$this->response->redirect($this->url->link('feed/uksb_google_merchant', 'token=' . $this->session->data['token'], 'SSL'));
	}
	
	public function install() {
		
		$this->load->model('feed/uksb_google_merchant');
		
		$this->model_feed_uksb_google_merchant->install();
		/*
		$this->load->model('tool/event');

		$this->model_tool_event->addEvent('uksbgm_ap', 'post.admin.add.product', 'feed/uksb_google_merchant/addProduct');
		$this->model_tool_event->addEvent('uksbgm_ep', 'post.admin.edit.product', 'feed/uksb_google_merchant/editProduct');
		$this->model_tool_event->addEvent('uksbgm_dp', 'post.admin.delete.product', 'feed/uksb_google_merchant/deleteProduct');
		$this->model_tool_event->addEvent('uksbgm_ac', 'post.admin.add.category', 'feed/uksb_google_merchant/addCategory');
		$this->model_tool_event->addEvent('uksbgm_ec', 'post.admin.edit.category', 'feed/uksb_google_merchant/editCategory');
		$this->model_tool_event->addEvent('uksbgm_dc', 'post.admin.delete.category', 'feed/uksb_google_merchant/deleteCategory');
		*/
	}	

	/*public function uninstall() {
		//$this->load->model('tool/event');

		//$this->model_tool_event->deleteEvent('uksbgm_ap');
		//$this->model_tool_event->deleteEvent('uksbgm_ep');
		//$this->model_tool_event->deleteEvent('uksbgm_dp');
		//$this->model_tool_event->deleteEvent('uksbgm_ac');
		//$this->model_tool_event->deleteEvent('uksbgm_ec');
		//$this->model_tool_event->deleteEvent('uksbgm_dc');
		
	}*/
}