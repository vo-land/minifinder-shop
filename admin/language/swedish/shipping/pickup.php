<?php
// Heading
$_['heading_title']    = 'Avhämtning';

// Text 
$_['text_shipping']    = 'Frakt';
$_['text_success']     = 'Klart: Du har ändrat i Avhämtning!';
$_['text_edit']        = 'Ändra i Avhämtnings modulen';

// Entry
$_['entry_geo_zone']   = 'GeoZon:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sorteringsordning:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra!';
?>