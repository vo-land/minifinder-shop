<?php
// Heading
$_['heading_title']     = 'Användare';

// Text
$_['text_success']      = 'Klart: Du har ändrat i Användare!';
$_['text_list']         = 'Användar Lista';
$_['text_add']          = 'Lägg till Användare';
$_['text_edit']         = 'Ändra i Användare';

// Column
$_['column_username']   = 'Användarnamn';
$_['column_status']     = 'Status';
$_['column_date_added'] = 'Skapad datum';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_username']   = 'Användarnamn:';
$_['entry_user_group'] = 'Kundgrupp';
$_['entry_password']   = 'Lösenord:';
$_['entry_confirm']    = 'Bekräfta:';
$_['entry_firstname']  = 'Förnamn:';
$_['entry_lastname']   = 'Efernamn:';
$_['entry_email']      = 'E-postadress:';
$_['entry_image']      = 'Bild';
$_['entry_status']     = 'Status:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i modulen!';
$_['error_account']    = 'Varning: Du kan inte radera ditt eget konto!';
$_['error_exists']     = 'Varning: ANvändarnamnet finns redan!';
$_['error_username']   = 'Användarnamnetmåste innehålla mellan 3 och 20 tecken!';
$_['error_password']   = 'Lösenordet måste innehålla mellan 4 och 20 tecken!';
$_['error_confirm']    = 'Lösenord och bekräftelse stämmer inte överens!';
$_['error_firstname']  = 'Förnamnet måste innehålla mellan 1 och 32 tecken!';
$_['error_lastname']   = 'Efternamnet måste innehålla mellan 1 och 32 tecken!';
?>
