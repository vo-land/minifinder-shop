<?php
// Heading
$_['heading_title']     = 'Rapport köpta produkter';

// Text
$_['text_list']         = 'Köpta Produkterd Lista';
$_['text_all_status']   = 'Alla statusar';

// Column
$_['column_date_start'] = 'Startdatum';
$_['column_date_end']   = 'Slutdatum';
$_['column_name']       = 'Produktnamn';
$_['column_model']      = 'Artikelnummer';
$_['column_quantity']   = 'Antal';
$_['column_total']      = 'Summa';

// Entry
$_['entry_date_start']  = 'Startdatum:';
$_['entry_date_end']    = 'Slutdatum:';
$_['entry_status']      = 'Orderstatus:';
?>