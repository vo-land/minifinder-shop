<?php
// Heading
$_['heading_title']      = 'Extended Banners';

// Text
$_['text_success']       = 'Klart: Du har ändrat i Banners!';
$_['text_list']          = 'Alternativ grupplista';
$_['text_add']           = 'Lägg till Banner';
$_['text_edit']          = 'Ändra i Banner';
$_['text_default']       = 'Förvalt';
$_['text_left']             = 'Kvar: <span class="left_symbols error">%s</span>';
$_['text_max_length']       = 'Den maximala längden på %s tecken';

// Column
$_['column_name']        = 'Bannernamn';
$_['column_status']      = 'Status';
$_['column_action']      = 'Ändra';

// Entry
$_['entry_name']         = 'Bannernamn:';
$_['entry_title']        = 'Titel:';
$_['entry_button_ena']   = 'Knappen Aktivera';
$_['entry_link']         = 'Länk:';
$_['entry_button_title'] = 'Knappen Titel:';
$_['entry_button_link']  = 'Knappen Länk:';
$_['entry_description']  = 'Beskrivning:';
$_['entry_description_ena'] = 'Beskrivning Aktivera';
$_['entry_image']        = 'Bild:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sorteringsordning';

// Error
$_['error_permission']   = 'Varning: Du har inte behörighet att ändra i Banners!';
$_['error_name']         = 'Bannernamnet måste vara mellan 3 och 64 tecken!';
$_['error_title']        = 'Bannertitel måste vara mellan 2 och 64 tecken!';
$_['error_button_title'] = 'Knappen Titel måste vara mellan 2 och 64 tecken!';
$_['error_button_link']  = 'Knappen Länk får inte vara tomt!';
$_['error_description']  = 'Bannerbeskrivning måste vara mellan %s och %s tecken!';
?>