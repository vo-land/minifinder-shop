<?php
// Text
$_['text_upload']    = 'Din upload är nu klar!';

// Error
$_['error_filename'] = 'Filnamn måste vara mellan 3-64 tecken.';
$_['error_filetype'] = 'Invalid fil typ!';
$_['error_upload']   = 'Upload krävs!';
?>
