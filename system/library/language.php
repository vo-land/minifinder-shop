<?php
class Language {
	private $default = 'english';
	private $directory;
	private $data = array();

	public function __construct($directory = '') {
		$this->directory = $directory;
	}

	public function get($key) {
		return (isset($this->data[$key]) ? $this->data[$key] : $key);
	}

	public function load($filename, $store_id = 0) {
		$_ = array();

        $oldfilename = $filename;

		if ($store_id) {
		    $filename .= $store_id;
        }

		$file = DIR_LANGUAGE . $this->default . '/' . $filename . '.php';
		$oldfile = DIR_LANGUAGE . $this->default . '/' . $oldfilename . '.php';

		if (file_exists($file)) {
			require($file);
		} elseif (file_exists($oldfile)) {
            require($oldfile);
        }

		$file = DIR_LANGUAGE . $this->directory . '/' . $filename . '.php';
        $oldfile = DIR_LANGUAGE . $this->directory . '/' . $oldfilename . '.php';

		if (file_exists($file)) {
			require($file);
		} elseif (file_exists($oldfile)) {
            require($oldfile);
        }

		$this->data = array_merge($this->data, $_);

		return $this->data;
	}
}