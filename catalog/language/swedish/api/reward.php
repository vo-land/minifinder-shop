<?php
// Text
$_['text_success']     = 'Klart: Dina bonuspoäng är registrerat!';

// Error
$_['error_permission'] = 'Varning: Du har inte tillåtelse att ändra i API!!';
$_['error_reward']     = 'Varning: Ange hur många poäng du viil använda!';
$_['error_points']     = 'Varning: Du har inte %s poäng!';
$_['error_maximum']    = 'Varning: Maximum poäng du kan använda är %s!';
?>