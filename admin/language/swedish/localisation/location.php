<?php
// Heading
$_['heading_title']    = 'Butikens Inställningar';

// Text
$_['text_success']     = 'Klart: Du har nu ändrat i Butikens information!';
$_['text_list']        = 'Layout Lista';
$_['text_add']         = 'Lägg till Butikens plats';
$_['text_edit']        = 'Ändra i Butikens plats';
$_['text_default']     = 'Default';
$_['text_time']        = 'Öppettider';
$_['text_geocode']     = 'Geocode är inte godkänd:';

// Column
$_['column_name']      = 'Butikens Namn';
$_['column_address']   = 'Adress';
$_['column_action']    = 'Ändra';

// Entry
$_['entry_name']       = 'Butikens Namn';
$_['entry_address']    = 'Adress';
$_['entry_geocode']    = 'Geocode';
$_['entry_telephone']  = 'Telefon';
$_['entry_fax']        = 'Fax';
$_['entry_image']      = 'Bild';
$_['entry_open']       = 'Öppettider';
$_['entry_comment']    = 'Kommentar';

// Help
$_['help_geocode']     = 'Ange din geocode manuellt.';
$_['help_open']        = 'Änge dina öppettider.';
$_['help_comment']     = 'Detta fält används för special texter som visas för kunderna.';

// Error
$_['error_permission'] = 'Varning: Du har inte tillåtelse att ändra!';
$_['error_name']       = 'Butikens Namn måate vara mer än 1 tecken.';
$_['error_address']    = 'Adressen måste vara mellan 3-128 tecken!';
$_['error_telephone']  = 'Telefon måste vara mellan 3-32 tecken!';
?>