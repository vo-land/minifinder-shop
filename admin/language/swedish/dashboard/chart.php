<?php
// Heading
$_['heading_title'] = ' Försäljning Analys';

// Text
$_['text_order']    = 'Order';
$_['text_customer'] = 'Kund';
$_['text_day']      = 'Idag';
$_['text_week']     = 'Vecka';
$_['text_month']    = 'Månad';
$_['text_year']     = 'År';
?>