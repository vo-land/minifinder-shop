<?php
// Heading 
$_['heading_title']      = 'Transaktioner';

// Column
$_['column_date_added']  = 'Datum';
$_['column_description'] = 'Beskrivning';
$_['column_amount']      = 'Summa (%s)';

// Text
$_['text_account']       = 'Affiliatekonto';
$_['text_transaction']   = 'Transaktioner';
$_['text_balance']       = 'Nuvarande saldo är:';
$_['text_empty']         = 'Du har inga transaktioner!';
?>