<form action="<?php echo $action; ?>" method="post" id="checkout-form">
  <?php foreach ($fields as $key => $value) { ?>
    <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>" />
  <?php } ?>
</form>
<div class="buttons">
  <table>
    <tr>
      <td align="left"><a onclick="location='<?php echo $back; ?>'" class="button"><span><?php echo $button_back; ?></span></a></td>
      <td align="right"><a onclick="document.forms['checkout-form'].submit();" class="button"><span><?php echo $button_continue; ?></span></a></td>
    </tr>
  </table>
</div>