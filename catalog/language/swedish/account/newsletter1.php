<?php
// Heading 
$_['heading_title']    = 'Prenumerera på nyhetsbrev';

// Text
$_['text_account']     = 'Konto';
$_['text_newsletter']  = 'Nyhetsbrev';
$_['text_success']     = 'Klart: Din nyhetsbrevsprenumeration har uppdaterats!';

// Entry
$_['entry_newsletter'] = 'Prenumerera:';

//button
$_['button_new_save'] = 'Behålla';

?>
