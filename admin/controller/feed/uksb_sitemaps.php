<?php 
class ControllerFeedUksbSitemaps extends Controller {
	private $error = array(); 
	
	public function index() {
		$this->load->language('uksb_licensing/uksb_licensing');
		
		$data['regerror_email'] = $this->language->get('regerror_email');
		$data['regerror_orderid'] = $this->language->get('regerror_orderid');
		$data['regerror_noreferer'] = $this->language->get('regerror_noreferer');
		$data['regerror_localhost'] = $this->language->get('regerror_localhost');
		$data['regerror_licensedupe'] = $this->language->get('regerror_licensedupe');
		$data['regerror_quote_msg'] = $this->language->get('regerror_quote_msg');
		$data['license_purchase_thanks'] = $this->language->get('license_purchase_thanks');
		$data['license_registration'] = $this->language->get('license_registration');
		$data['license_opencart_email'] = $this->language->get('license_opencart_email');
		$data['license_opencart_orderid'] = $this->language->get('license_opencart_orderid');
		$data['check_email'] = $this->language->get('check_email');
		$data['check_orderid'] = $this->language->get('check_orderid');
		$data['server_error_curl'] = $this->language->get('server_error_curl');

		$this->load->language('feed/uksb_sitemaps');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('uksb_sitemaps', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_general_settings'] = $this->language->get('heading_general_settings');
		$data['heading_sitemap_content'] = $this->language->get('heading_sitemap_content');
		$data['heading_sitemap_urls'] = $this->language->get('heading_sitemap_urls');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_always'] = $this->language->get('text_always');
		$data['text_hourly'] = $this->language->get('text_hourly');
		$data['text_daily'] = $this->language->get('text_daily');
		$data['text_weekly'] = $this->language->get('text_weekly');
		$data['text_monthly'] = $this->language->get('text_monthly');
		$data['text_yearly'] = $this->language->get('text_yearly');
		$data['text_pg_home'] = $this->language->get('text_pg_home');
		$data['text_pg_specials'] = $this->language->get('text_pg_specials');
		
		$data['entry_free_support'] = $this->language->get('entry_free_support');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_image_sitemap'] = $this->language->get('entry_image_sitemap');
		$data['entry_in_sitemap'] = $this->language->get('entry_in_sitemap');
		$data['entry_products'] = $this->language->get('entry_products');
		$data['entry_categories'] = $this->language->get('entry_categories');
		$data['entry_manufacturers'] = $this->language->get('entry_manufacturers');
		$data['entry_pages'] = $this->language->get('entry_pages');
		$data['entry_pages_omit'] = $this->language->get('entry_pages_omit');
		$data['entry_extras'] = $this->language->get('entry_extras');
		$data['entry_extras_omit'] = $this->language->get('entry_extras_omit');
		$data['entry_split'] = $this->language->get('entry_split');
		$data['entry_info'] = $this->language->get('entry_info');
		$data['entry_sitemap_content'] = $this->language->get('entry_sitemap_content');
		$data['entry_frequency'] = $this->language->get('entry_frequency');
		$data['entry_priority'] = $this->language->get('entry_priority');
		$data['entry_google'] = $this->language->get('entry_google');
		$data['entry_yahoo'] = $this->language->get('entry_yahoo');
		$data['entry_bing'] = $this->language->get('entry_bing');
		$data['entry_data_feed1'] = $this->language->get('entry_data_feed1');
		$data['entry_data_feed2'] = $this->language->get('entry_data_feed2');
		$data['help_split'] = $this->language->get('help_split');
		$data['help_content'] = $this->language->get('help_content');
		$data['help_extras'] = $this->language->get('help_extras');
		$data['help_urls'] = $this->language->get('help_urls');
		$data['help_info'] = $this->language->get('help_info');

		$data['warning_submit'] = $this->language->get('warning_submit');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');

 		if(isset($this->request->get['emailmal'])){
			$data['emailmal'] = true;
		}

		if(isset($this->request->get['regerror'])){
		    if($this->request->get['regerror']=='emailmal'){
		    	$this->error['warning'] = $this->language->get('regerror_email');
		    }elseif($this->request->get['regerror']=='orderidmal'){
		    	$this->error['warning'] = $this->language->get('regerror_orderid');
		    }elseif($this->request->get['regerror']=='noreferer'){
		    	$this->error['warning'] = $this->language->get('regerror_noreferer');
		    }elseif($this->request->get['regerror']=='localhost'){
		    	$this->error['warning'] = $this->language->get('regerror_localhost');
		    }elseif($this->request->get['regerror']=='licensedupe'){
		    	$this->error['warning'] = $this->language->get('regerror_licensedupe');
		    }
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_feed'),
			'href'      => $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href'      => $this->url->link('feed/uksb_sitemaps', 'token=' . $this->session->data['token'], 'SSL')
		);
				
		$data['action'] = $this->url->link('feed/uksb_sitemaps', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['uksb_sitemaps_status'])) {
			$data['uksb_sitemaps_status'] = $this->request->post['uksb_sitemaps_status'];
		} else {
			$data['uksb_sitemaps_status'] = $this->config->get('uksb_sitemaps_status');
		}
		
		if (isset($this->request->post['uksb_sitemaps_image'])) {
			$data['uksb_sitemaps_image'] = $this->request->post['uksb_sitemaps_image'];
		} else {
			$data['uksb_sitemaps_image'] = $this->config->get('uksb_sitemaps_image');
		}
				
		if (isset($this->request->post['uksb_sitemaps_split'])) {
			$data['uksb_sitemaps_split'] = $this->request->post['uksb_sitemaps_split'];
		} else {
			$data['uksb_sitemaps_split'] = $this->config->get('uksb_sitemaps_split');
		}
				
		if (isset($this->request->post['uksb_sitemaps_products_on'])) {
			$data['uksb_sitemaps_products_on'] = $this->request->post['uksb_sitemaps_products_on'];
		} else {
			$data['uksb_sitemaps_products_on'] = $this->config->get('uksb_sitemaps_products_on');
		}
	
		if (isset($this->request->post['uksb_sitemaps_products_fr'])) {
			$data['uksb_sitemaps_products_fr'] = $this->request->post['uksb_sitemaps_products_fr'];
		} else {
			$data['uksb_sitemaps_products_fr'] = $this->config->get('uksb_sitemaps_products_fr');
		}
	
		if (isset($this->request->post['uksb_sitemaps_products_pr'])) {
			$data['uksb_sitemaps_products_pr'] = $this->request->post['uksb_sitemaps_products_pr'];
		} else {
			$data['uksb_sitemaps_products_pr'] = $this->config->get('uksb_sitemaps_products_pr');
		}
	
		if (isset($this->request->post['uksb_sitemaps_categories_on'])) {
			$data['uksb_sitemaps_categories_on'] = $this->request->post['uksb_sitemaps_categories_on'];
		} else {
			$data['uksb_sitemaps_categories_on'] = $this->config->get('uksb_sitemaps_categories_on');
		}
	
		if (isset($this->request->post['uksb_sitemaps_categories_fr'])) {
			$data['uksb_sitemaps_categories_fr'] = $this->request->post['uksb_sitemaps_categories_fr'];
		} else {
			$data['uksb_sitemaps_categories_fr'] = $this->config->get('uksb_sitemaps_categories_fr');
		}
	
		if (isset($this->request->post['uksb_sitemaps_categories_pr'])) {
			$data['uksb_sitemaps_categories_pr'] = $this->request->post['uksb_sitemaps_categories_pr'];
		} else {
			$data['uksb_sitemaps_categories_pr'] = $this->config->get('uksb_sitemaps_categories_pr');
		}
	
		if (isset($this->request->post['uksb_sitemaps_manufacturers_on'])) {
			$data['uksb_sitemaps_manufacturers_on'] = $this->request->post['uksb_sitemaps_manufacturers_on'];
		} else {
			$data['uksb_sitemaps_manufacturers_on'] = $this->config->get('uksb_sitemaps_manufacturers_on');
		}
	
		if (isset($this->request->post['uksb_sitemaps_manufacturers_fr'])) {
			$data['uksb_sitemaps_manufacturers_fr'] = $this->request->post['uksb_sitemaps_manufacturers_fr'];
		} else {
			$data['uksb_sitemaps_manufacturers_fr'] = $this->config->get('uksb_sitemaps_manufacturers_fr');
		}
	
		if (isset($this->request->post['uksb_sitemaps_manufacturers_pr'])) {
			$data['uksb_sitemaps_manufacturers_pr'] = $this->request->post['uksb_sitemaps_manufacturers_pr'];
		} else {
			$data['uksb_sitemaps_manufacturers_pr'] = $this->config->get('uksb_sitemaps_manufacturers_pr');
		}
	
		if (isset($this->request->post['uksb_sitemaps_pages_on'])) {
			$data['uksb_sitemaps_pages_on'] = $this->request->post['uksb_sitemaps_pages_on'];
		} else {
			$data['uksb_sitemaps_pages_on'] = $this->config->get('uksb_sitemaps_pages_on');
		}
	
		if (isset($this->request->post['uksb_sitemaps_pages_fr'])) {
			$data['uksb_sitemaps_pages_fr'] = $this->request->post['uksb_sitemaps_pages_fr'];
		} else {
			$data['uksb_sitemaps_pages_fr'] = $this->config->get('uksb_sitemaps_pages_fr');
		}
	
		if (isset($this->request->post['uksb_sitemaps_pages_pr'])) {
			$data['uksb_sitemaps_pages_pr'] = $this->request->post['uksb_sitemaps_pages_pr'];
		} else {
			$data['uksb_sitemaps_pages_pr'] = $this->config->get('uksb_sitemaps_pages_pr');
		}
	
		if (isset($this->request->post['uksb_sitemaps_pages_omit_a'])) {
			$data['uksb_sitemaps_pages_omit_a'] = $this->request->post['uksb_sitemaps_pages_omit_a'];
		} else {
			$data['uksb_sitemaps_pages_omit_a'] = $this->config->get('uksb_sitemaps_pages_omit_a');
		}
	
		if (isset($this->request->post['uksb_sitemaps_pages_omit_b'])) {
			$data['uksb_sitemaps_pages_omit_b'] = $this->request->post['uksb_sitemaps_pages_omit_b'];
		} else {
			$data['uksb_sitemaps_pages_omit_b'] = $this->config->get('uksb_sitemaps_pages_omit_b');
		}
	
		if (isset($this->request->post['uksb_sitemaps_extras_on'])) {
			$data['uksb_sitemaps_extras_on'] = $this->request->post['uksb_sitemaps_extras_on'];
		} else {
			$data['uksb_sitemaps_extras_on'] = $this->config->get('uksb_sitemaps_extras_on');
		}
	
		if (isset($this->request->post['uksb_sitemaps_extras_fr'])) {
			$data['uksb_sitemaps_extras_fr'] = $this->request->post['uksb_sitemaps_extras_fr'];
		} else {
			$data['uksb_sitemaps_extras_fr'] = $this->config->get('uksb_sitemaps_extras_fr');
		}
	
		if (isset($this->request->post['uksb_sitemaps_extras_pr'])) {
			$data['uksb_sitemaps_extras_pr'] = $this->request->post['uksb_sitemaps_extras_pr'];
		} else {
			$data['uksb_sitemaps_extras_pr'] = $this->config->get('uksb_sitemaps_extras_pr');
		}
	
		if (isset($this->request->post['uksb_sitemaps_extras_omit'])) {
			$data['uksb_sitemaps_extras_omit'] = $this->request->post['uksb_sitemaps_extras_omit'];
		} else {
			$data['uksb_sitemaps_extras_omit'] = $this->config->get('uksb_sitemaps_extras_omit');
		}
			

		$this->load->model('feed/uksb_sitemaps');

		$data['manufacturers_total'] = $this->model_feed_uksb_sitemaps->countManufacturers();

		$data['informations'] = array();
	
		$results = $this->model_feed_uksb_sitemaps->getInformationList();
 
	    	foreach ($results as $result) {
			$data['informations'][] = array(
				'information_id' => $result['information_id'],
				'title'          => $result['title']
			);
			
			if (isset($this->request->post['uksb_sitemaps_pages_omit_' . $result['information_id']])) {
				$data['uksb_sitemaps_pages_omit_' . $result['information_id']] = $this->request->post['uksb_sitemaps_pages_omit_' . $result['information_id']];
			} else {
				$data['uksb_sitemaps_pages_omit_' . $result['information_id']] = $this->config->get('uksb_sitemaps_pages_omit_' . $result['information_id']);
			}
		}

		$data['extra_pages'] = array();

		$extras = $this->model_feed_uksb_sitemaps->getExtraPages();

		foreach ($extras as $extra) {
			$data['extra_pages'][] = $extra['keyword'];
		}

		$data['extras_total'] = count($data['extra_pages']);

		$data['data_feed_url'] = HTTP_CATALOG;
		$data['data_feed'] = 'index.php?route=feed/uksb_sitemaps/google&store=0';
		$data['data_feed2'] = 'index.php?route=feed/uksb_sitemaps/all&store=0';

		$this->load->model('setting/store');

		if($this->model_setting_store->getTotalStores()>0){
			$stores = $this->model_setting_store->getStores();
			$stores = array_reverse($stores);
			
			foreach($stores as $store){
				$data['data_feed_url'] .= '^'.$store['url'];
				$data['data_feed'] .= '^index.php?route=feed/uksb_sitemaps/google&store='.$store['store_id'];
				$data['data_feed2'] .= '^index.php?route=feed/uksb_sitemaps/all&store='.$store['store_id'];
			}
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		$data['home'] = 'https://www.secureserverssl.co.uk/opencart-extensions/google-merchant/';
		$domainssl = explode("//", HTTPS_SERVER);
		$domainnonssl = explode("//", HTTP_SERVER);
		$domain = ($domainssl[1] != '' ? $domainssl[1] : $domainnonssl[1]);

		$data['licensed'] = @file_get_contents($data['home'] . 'licensed.php?domain=' . $domain . '&extension=2881');

		if(!$data['licensed'] || $data['licensed'] == ''){
			if(extension_loaded('curl')) {
		        $post_data = array('domain' => $domain, 'extension' => '2881');
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_HEADER, false);
		        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
		        curl_setopt($curl, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
		        $follow_allowed = ( ini_get('open_basedir') || ini_get('safe_mode')) ? false : true;
		        if ($follow_allowed) {
		            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		        }
		        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 9);
		        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
		        curl_setopt($curl, CURLOPT_AUTOREFERER, true); 
		        curl_setopt($curl, CURLOPT_VERBOSE, 1);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_FORBID_REUSE, false);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_URL, $data['home'] . 'licensed.php');
		        curl_setopt($curl, CURLOPT_POST, true);
		        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_data));
		        $data['licensed'] = curl_exec($curl);
		        curl_close($curl);
		    }else{
		        $data['licensed'] = 'curl';
		    }
		}

		$order_date = @file_get_contents($data['home'] . 'order_date.php?domain=' . $domain . '&extension=2881');

		if(!$order_date || $order_date == ''){
			if(extension_loaded('curl')) {
		        $post_data2 = array('domain' => $domain, 'extension' => '2881');
		        $curl2 = curl_init();
		        curl_setopt($curl2, CURLOPT_HEADER, false);
		        curl_setopt($curl2, CURLINFO_HEADER_OUT, true);
		        curl_setopt($curl2, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
		        $follow_allowed2 = ( ini_get('open_basedir') || ini_get('safe_mode')) ? false : true;
		        if ($follow_allowed2) {
		            curl_setopt($curl2, CURLOPT_FOLLOWLOCATION, 1);
		        }
		        curl_setopt($curl2, CURLOPT_CONNECTTIMEOUT, 9);
		        curl_setopt($curl2, CURLOPT_TIMEOUT, 60);
		        curl_setopt($curl2, CURLOPT_AUTOREFERER, true); 
		        curl_setopt($curl2, CURLOPT_VERBOSE, 1);
		        curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, false);
		        curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl2, CURLOPT_FORBID_REUSE, false);
		        curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl2, CURLOPT_URL, $data['home'] . 'order_date.php');
		        curl_setopt($curl2, CURLOPT_POST, true);
		        curl_setopt($curl2, CURLOPT_POSTFIELDS, http_build_query($post_data2));
		        $order_date = curl_exec($curl2);
		        curl_close($curl2);
		    }else{
		        $order_date = 'unknown';
		    }
		}

		if($order_date == '' || $order_date == 'unknown'){
			$data['text_free_support_remaining'] = sprintf($this->language->get('text_free_support_remaining'), 'unknown');
		}else{
			$isSecure = false;
			if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {
				$isSecure = true;
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
				$isSecure = true;
			}

			if((time() - $order_date) > 31536000){
				$data['text_free_support_remaining'] = sprintf($this->language->get('text_free_support_expired'), 1, ($isSecure ? 1 : 0), urlencode($domain), '2881', $this->session->data['token']);
			}else{
				$data['text_free_support_remaining'] = sprintf($this->language->get('text_free_support_remaining'), 366 - ceil((time() - $order_date) / 86400));
			}
		}

		$this->response->setOutput($this->load->view('feed/uksb_sitemaps.tpl', $data));
	} 
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'feed/uksb_sitemaps')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}	
}