<footer>
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3 footer_col">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
          <li><a href="http://www.tracktor.se/">Tracktor login</a></li>
          <li><a href="http://minifinder.se/">Minifinder</a></li>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3 footer_col">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <!--li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li-->
        </ul>
        <h5 class="p_top"><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <!--li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li-->
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <!--li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li-->
        </ul>
      </div>
      <div class="col-sm-3 footer_col">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
	      <div class="footer_logos">
	      	<a href="http://www.pricerunner.se/kopskydd/registrera?merchant=53964" target="_blank"><img src="catalog/view/theme/minifinder/image/logo2.png" alt="Pricerunner Köpskydd" title="Pricerunner Köpskydd" class="socs_logos"/></a>
	      	&nbsp;&nbsp;<img src="catalog/view/theme/minifinder/image/logo1.png" alt="Certifierad E-handel" title="Certifierad E-handel" class="socs_logos"/>
	      </div>
	      <br />
      </div>
      <div class="col-sm-3 footer_col">
          <div class="soc_networks">
              <a href="https://www.facebook.com/minifinder" class="fb_icon"><!--<img src="/image/catalog/www/footer_facebook.png" alt="Facebook Logotype" title="Facebook" class="soc_logos" />--></a>
              <a href="https://twitter.com/minifinder" class="twitter_icon"><!--<img src="/image/catalog/www/footer_twitter.png" alt="Twitter Logotype" title="Twitter" class="soc_logos" />--></a>
              <a href="https://www.youtube.com/channel/UCsxZSz1Krppkd8sUnXOCFtw" class="youtube_icon"><!--<img src="/image/catalog/www/footer_youtube.png" alt="Youtube Logotype" title="Youtube" class="soc_logos" />--></a>
              <a href="https://www.instagram.com/minifinder/" class="instagram_icon"></a>
              <!--<a href="https://plus.google.com/+Xxxxx/" class="google_icon">--><!--<img src="/image/catalog/www/footer_googleplus.png" alt="Google+ Logotype" title="Google+" class="soc_logos" />--><!--</a>-->
              <!--<img src="/image/catalog/www/footer_pinterest.png" alt="Pinterest Logotype" title="Pinterest" class="soc_logos" />-->
          </div>
      	<h5><?php echo $text_accepted_payment; ?></h5>
      	<img src="catalog/view/theme/minifinder/image/logos.png" alt="Payment Logotype" title="Betalningsalternativen som vi accepterar" class="soc_logos img-responsive"/>
      </div>
    </div>
  </div>
  <div class="container2">
  	<div class="container">
  		<img src="/image/catalog/www/footer_gpser_logo.png" alt="GPSER Logotype" title="Gpser" class="logo" />
  		<img src="/image/catalog/www/footer_minifinder_logo.png" alt="MiniFinder Logotype" title="MiniFinder" class="logo" />
  		<img src="/image/catalog/www/footer_tracktor_logo.png" alt="Tracktor Logotype" title="Tracktor" class="logo" />
  	</div>
      <div class="container">
          <div id="powered">
              <?php echo $powered; ?>
          </div>
      </div>
  </div>
</footer>

<a id="toTop" href="#"><img src="catalog/view/theme/minifinder/image/up.png" alt="To-the-top" /></a>
<script type="text/javascript">
$(document).ready(function()
{
// Hide the toTop button when the page loads.
$("#toTop").css("display", "none");

// This function runs every time the user scrolls the page.
$(window).scroll(function(){

	// Check weather the user has scrolled down (if "scrollTop()"" is more than 0)
	if($(window).scrollTop() > 0){

		// If it's more than or equal to 0, show the toTop button. console.log("is more");
		$("#toTop").fadeIn("slow");

	} else {

		// If it's less than 0 (at the top), hide the toTop button. console.log("is less");
		$("#toTop").fadeOut("slow");
	}
});

// When the user clicks the toTop button, we want the page to scroll to the top.
$("#toTop").click(function(){

	// Disable the default behaviour when a user clicks an empty anchor link.
	// (The page jumps to the top instead of // animating)
	event.preventDefault();

	// Animate the scrolling motion.
	$("html, body").animate({
	scrollTop:0
	},"slow");
});


});
</script>

<!-- Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- / Facebook -->

<!-- Start of Zopim Live Chat Script 
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?31uloCDKZwm6nU0kyiljzynPGNHcTiCy";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>-->
<!--End of Zopim Live Chat Script-->


</body></html>