<?php
// Text
$_['text_title']           = 'Klarna Konto - Delbetala från %s/mån';
$_['text_terms']		   = '<span id="klarna_account_toc"></span><script type="text/javascript">var terms = new Klarna.Terms.Account({el: \'klarna_account_toc\', eid: \'%s\', country: \'%s\'});</script>';
$_['text_information']     = 'Klarna Konto Information';
$_['text_additional']      = 'Klarna Konto - information om dig';
$_['text_male']           = 'Herr';
$_['text_female']         = 'Dam';
$_['text_year']           = 'År';
$_['text_month']          = 'Månad';
$_['text_day']            = 'Dag';
$_['text_payment_option']  = 'Betalningsalternativ';
$_['text_single_payment']  = 'En betalning';
$_['text_monthly_payment'] = '%s - %s per manad';
$_['text_comment']        = "Klarna's faktura Nr: %s\n%s/%s: %.4f";

// Entry
$_['entry_gender']         = 'Kön:';
$_['entry_pno']            = 'Personnummer:<br /><span class="help">ÅÅÅÅMMDD-XXXX</span>';
$_['entry_dob']            = 'Datum du föddes:';
$_['entry_phone_no']       = 'Telefonnummer:<br /><span class="help">Ange ditt telefonnummer.</span>';
$_['entry_street']         = 'Adress:<br /><span class="help">Leverans kan endast ske till registrerad adress.</span>';
$_['entry_house_no']       = 'Husnummer:<br /><span class="help">Ange husnummer.</span>';
$_['entry_house_ext']      = 'Husvåning:<br /><span class="help">Ange våningsplan</span>';
$_['entry_company']        = 'Organisationsnummer:<br /><span class="help">Företagets Organisationsnummer</span>';

// Help
$_['help_pno']					= 'Ange personnummer.';
$_['help_phone_no']				= 'Ange telefonnummer.';
$_['help_street']				= 'Ange gatuadress, gatan.';
$_['help_house_no']				= 'Ange husnummer.';
$_['help_house_ext']			= 'Ange husvåning, trappa. E.g. A, B, C, Red, Blue ect.';
$_['help_company']				= 'Ange firmans organisationsnummer';

// Error
$_['error_deu_terms']     = 'Du måste acceptera Klarnas villkor';
$_['error_address_match'] = 'Faktura - och Leveransadress måste vara samma';
$_['error_network']       = 'Error medans Klarna jobbar, snälla försök igen.';
?>
