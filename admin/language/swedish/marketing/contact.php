<?php
// Heading
$_['heading_title']        = 'Mail';

// Text
$_['text_success']         = 'Ditt mail har skickats!';
$_['text_sent']            = 'Ditt mail har skickats till %s av %s mottagare!';
$_['text_list']            = 'Layout Lista';
$_['text_default']         = 'Default';
$_['text_newsletter']      = 'Alla Nyhetsbrev Mottagare';
$_['text_customer_all']    = 'Alla kunder';
$_['text_customer_group']  = 'Kundgrupp';
$_['text_customer']        = 'Kunder';
$_['text_affiliate_all']   = 'Alla återförsäljare';
$_['text_affiliate']       = 'Återförsäljare';
$_['text_product']         = 'Produkter';

// Entry
$_['entry_store']          = 'Från';
$_['entry_to']             = 'Till';
$_['entry_customer_group'] = 'Kundgrupp';
$_['entry_customer']       = 'Kunder';
$_['entry_affiliate']      = 'Återförsäljare';
$_['entry_product']        = 'Produkter';
$_['entry_subject']        = 'Ämne, rubrik';
$_['entry_message']        = 'Meddelande';

// Help
$_['help_customer']       = 'Autocomplete';
$_['help_affiliate']      = 'Autocomplete';
$_['help_product']        = 'Skicka endast till kunder som köpt produkten innan.(Autocomplete)';

// Error
$_['error_permission']     = 'Varning: Du har inte tillåtelse att skicka E-Mail\'s!';
$_['error_subject']        = 'E-Mail ämne krävs!';
$_['error_message']        = 'E-Mail meddelande krävs!';
?>