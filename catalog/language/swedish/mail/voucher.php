<?php
// Text
$_['text_subject']  = 'Du har fått ett presentkort från %s';
$_['text_greeting'] = 'Grattis! Du har fått ett presentkort med ett värde av %s';
$_['text_from']     = 'Detta presentkort har skickats till dig från %s';
$_['text_message']  = 'med följande meddelande';
$_['text_redeem']   = 'För att utnyttja detta presentkort, anteckna presentkortskoden som är <b>%s</b>. Klicka därefter på länken nedan och välj den produkt du vill använda presentkortet för. Ange presentkortskoden i varukorgen innan du klickar på "Till kassan".';
$_['text_footer']   = 'Var vänlig svara på detta e-postmeddelande om du har några frågor.';
?>