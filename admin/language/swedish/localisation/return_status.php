<?php
// Heading
$_['heading_title']    = 'Returstatus';

// Text
$_['text_success']     = 'Klart: Du har ändrat i Returstatus!';
$_['text_list']        = 'Layout Lista';
$_['text_add']         = 'Lägg till Returstatus';
$_['text_edit']        = 'Ändra i Returstatus';

// Column
$_['column_name']      = 'Returstatus';
$_['column_action']    = 'Ändra';

// Entry
$_['entry_name']       = 'Returstatus:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i Returstatus!';
$_['error_name']       = 'Namnet måste innehålla mellan 3 och 32 tecken!';
$_['error_default']    = 'Varning: Denna kan inte raderas eftersom den är kopplad till butikens förvalda!';
$_['error_return']     = 'Varning: Denna kan inte raderas eftersom den är kopplad till %s returer!';
?>