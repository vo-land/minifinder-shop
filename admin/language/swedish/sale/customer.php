<?php
// Heading
$_['heading_title']         = 'Kunder';

// Text
$_['text_success']          = 'Klart: Du har ändrat i Kunder!';
$_['text_list']             = 'Kund Lista';
$_['text_add']              = 'Lägg till Kund';
$_['text_edit']             = 'Ändra i Kund';
$_['text_default']          = 'Förvald';
$_['text_balance']          = 'Summa:';
$_['text_add_ban_ip']       = 'Lägg till en banned IP';
$_['text_remove_ban_ip']    = 'Ta bort en banned IP';

// Column
$_['column_name']           = 'Kundnamn';
$_['column_email']          = 'E-postadress';
$_['column_customer_group'] = 'Kundgrupp';
$_['column_status']         = 'Status'; 
$_['column_date_added']     = 'Registrerad';
$_['column_comment']        = 'Kommentar';
$_['column_description']    = 'Beskrivning';
$_['column_amount']         = 'Summa';
$_['column_points']         = 'Poäng';
$_['column_ip']             = 'IP-adress';
$_['column_total']          = 'Antal kundkonton';
$_['column_action']         = 'Ändra';

// Entry
$_['entry_customer_group']  = 'Kundgrupp';
$_['entry_firstname']       = 'Förnamn:';
$_['entry_lastname']        = 'Efternamn:';
$_['entry_email']           = 'E-postadress:';
$_['entry_telephone']       = 'Mobiltelefon:';
$_['entry_fax']             = 'Telefon:';
$_['entry_newsletter']      = 'Nyhetsbrev:';
$_['entry_status']          = 'Status:';
$_['entry_safe']            = 'Säkerhet';
$_['entry_password']        = 'Lösenord:';
$_['entry_confirm']         = 'Bekräfta:';
$_['entry_company']         = 'Company:';
$_['entry_address_1']       = 'Adress 1:';
$_['entry_address_2']       = 'Adress 2:';
$_['entry_city']            = 'Postort:';
$_['entry_postcode']        = 'Postnummer:';
$_['entry_country']         = 'Land:';
$_['entry_zone']            = 'Län:';
$_['entry_default']         = 'Standardadress:';
$_['entry_comment']         = 'Kommentar:';
$_['entry_description']     = 'Beskrivning:';
$_['entry_amount']          = 'Summa:';
$_['entry_points']          = 'Poäng:';
$_['entry_name']            = 'Kunder Namn';
$_['entry_approved']        = 'Godkänd';
$_['entry_ip']              = 'IP';
$_['entry_date_added']      = 'Datum';

// Help
$_['help_safe']             = 'Sätt kunden på ja:säker för att undvika problem.';
$_['help_points']           = 'Använd minus för att ta bort poäng';

// Error
$_['error_warning']         = 'Varning: Kontrollera felmeddelanden!';
$_['error_permission']      = 'Varning: Du har inte behörighet att ändra i modulen!';
$_['error_exists']          = 'Varning: E-postadressen är redan registrerad!';
$_['error_firstname']       = 'Förnamnet måste innehålla mellan 1 och 32 tecken!';
$_['error_lastname']        = 'Efternamnet måste innehålla mellan 1 och 32 tecken!';
$_['error_email']           = 'E-postadressen är inte giltig!';
$_['error_telephone']       = 'Telefonnummer måste innehålla mellan 3 och 32 tecken!';
$_['error_password']        = 'Lösenordet måste innehålla mellan 4 och 20 tecken!';
$_['error_confirm']         = 'Lösenord och bekräftelse stämmer inte överens!';
$_['error_address_1']       = 'Adress 1 måste innehålla mellan 3 och 128 tecken!';
$_['error_city']            = 'Stad måste innehålla mellan 2 och 128 tecken!';
$_['error_postcode']        = 'Postnummer måste innehålla mellan 2 och 10 tecken!';
$_['error_country']         = 'Välj ett land!';
$_['error_zone']            = 'Välj ett län!';
$_['error_custom_field']    = '%s krävs!';
$_['error_comment']         = 'Du måste ange en kommentar.';
?>