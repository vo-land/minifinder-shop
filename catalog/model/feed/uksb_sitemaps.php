<?php
class ModelFeedUksbSitemaps extends Model {
	public function getTotalProductsByStore($store_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_store p2s LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$store_id . "' Order By p2s.store_id ASC");

		return $query->row['total'];
	}			

	public function getExtraPages() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` NOT LIKE 'category_id%' AND `query` NOT LIKE 'product_id%' AND `query` NOT LIKE 'manufacturer_id%' AND `query` NOT LIKE 'information_id%' ORDER BY `query` DESC");

		$extra_data = $query->rows;

		return $extra_data;			
	}
}