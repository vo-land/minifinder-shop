<?php
// Heading
$_['heading_title']    = 'Returneringsorsaker';

// Text
$_['text_success']     = 'Klart: Du har ändrat i Returneringsorsaker!';
$_['text_list']        = 'Layout Lista';
$_['text_add']         = 'Lägg till Returneringsorsak';
$_['text_edit']        = 'Ändra i Returneringsorsak';

// Column
$_['column_name']      = 'Returneringsorsak';
$_['column_action']    = 'Ändra';

// Entry
$_['entry_name']       = 'Returneringsorsak:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i Returneringsorsaker!';
$_['error_name']       = 'Namnet måste innehålla mellan 3 och 32 tecken!';
$_['error_return']     = 'Varning: Denna orsak kan inte raderas eftersom den är kopplad till %s returnerade produkter!';
?>