<div id="slideshow<?php echo $module; ?>" class="owl-carousel extended_slider">
    <?php foreach ($banners as $banner) { ?>
    <?php if (!$banner['button_ena'] && $banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>">
        <?php } ?>
        <div class="item" style="background-image: url(<?php echo $banner['image']; ?>);">
            <div class="container">
                <div class="slide_txt_anim">
                    <div class="slide_title"><?php echo $banner['title']; ?></div>
                    <?php if ($banner['description_ena'] && $banner['description']) { ?>
                        <div class="slide_desc"><?php echo $banner['description']; ?></div>
                    <?php } ?>
                </div>
                <?php if ($banner['button_ena']) { ?>
                    <?php if ($banner['button_title'] && $banner['button_link']) { ?>
                        <div class="slide_btn_anim">
                            <a href="<?php echo $banner['button_link']; ?>" class="red_button">
                                <span><?php echo $banner['button_title']; ?></span>
                                <span><?php echo $banner['button_title']; ?></span>
                            </a>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
        <?php if (!$banner['button_ena'] && $banner['link']) { ?>
            </a>
        <?php } ?>
    <?php } ?>
</div>
<link href="catalog/view/javascript/jquery/owl-carousel/owl.transitions.css" rel="stylesheet" type="text/css" />
<script type="text/javascript"><!--
    <?php echo $slider_move; ?> && $('#slideshow<?php echo $module; ?>').detach().insertAfter('<?php echo $slider_after; ?>');
    function slider_init () {
        var options = {
            autoPlay: true,
            singleItem: true,
            navigation: false,
            pagination: true,
            transitionStyle : "fade",
            afterInit: function(){
                $('#slideshow<?php echo $module; ?> .slide_txt_anim').addClass('anim-text');
                setTimeout(function(){
                    $('#slideshow<?php echo $module; ?> .slide_btn_anim').addClass('anim-btn');
                }, 300);
            },
            beforeMove: function(){
                $('#slideshow<?php echo $module; ?> .slide_txt_anim').removeClass('anim-text');
                $('#slideshow<?php echo $module; ?> .slide_btn_anim').removeClass('anim-btn');
            },
            afterMove: function(){
                setTimeout(function(){
                    $('#slideshow<?php echo $module; ?> .slide_txt_anim').addClass('anim-text');
                }, 100);
                setTimeout(function(){
                    $('#slideshow<?php echo $module; ?> .slide_btn_anim').addClass('anim-btn');
                }, 300);
            }
        };
        $('#slideshow<?php echo $module; ?>').owlCarousel(options);
    }
    slider_init();
    --></script>