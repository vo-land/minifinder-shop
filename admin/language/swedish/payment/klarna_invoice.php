<?php
// Heading
$_['heading_title']         = 'Klarna Faktura';

// Text
$_['text_payment']          = 'Betalning';
$_['text_success']          = 'Klart: Du kan modifiera Klarna Faktura!';
$_['text_edit']             = 'Ändra i Klarna Faktura';
$_['text_klarna_invoice']   = '<a href="https://merchants.klarna.com/signup?locale=en&partner_id=d5c87110cebc383a826364769047042e777da5e8&utm_campaign=Platform&utm_medium=Partners&utm_source=Opencart" target="_blank"><img src="https://cdn.klarna.com/public/images/global/logos/v1/basic/global_basic_logo_std_blue-black.png?width=60&eid=opencart" alt="Klarna Invoice" title="Klarna Invoice" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']             = 'Live';
$_['text_beta']             = 'Beta';
$_['text_sweden']           = 'Sverige';
$_['text_norway']           = 'Norge';
$_['text_finland']          = 'Finland';
$_['text_denmark']          = 'Danmark';
$_['text_germany']          = 'Deutschland';
$_['text_netherlands']      = 'The Netherlands';

// Entry
$_['entry_merchant']        = 'Klarna Merchant ID:';
$_['entry_secret']          = 'Klarna Secret:';
$_['entry_server']          = 'Server:';
$_['entry_total']           = 'Totalt:';
$_['entry_pending_status']  = 'Order Status:';
$_['entry_accepted_status'] = 'Accepterad Status:';
$_['entry_geo_zone']        = 'Zon:';
$_['entry_status']          = 'Status:';
$_['entry_sort_order']      = 'Sorteringsordning:';

// Help
$_['help_merchant']					= '(butiks id) för att använda mot Klarna.';
$_['help_secret']					= 'Säkerhetskod eller email som används mot Klarna.';
$_['help_total']					= 'Totalsumman måste uppnå viss ordersumma för att detta alternativ är giltigt.';

// Error
$_['error_permission']      = 'Varning: Du kan inte modifiera Klarna fakturor i din webshop!';
?>
