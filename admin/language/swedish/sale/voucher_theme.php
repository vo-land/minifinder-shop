<?php
// Heading
$_['heading_title']      = 'Presentkortstema';

// Text
$_['text_success']      = 'Klart: Du har ändrat i Presentkortstema!';
$_['text_list']         = 'Presentkortstema Lista';
$_['text_add']          = 'Lägg till Presentkortstema';
$_['text_edit']         = 'Ändra i Presentkortstema';

// Column
$_['column_name']        = 'Temats namn';
$_['column_action']      = 'Ändra';

// Entry
$_['entry_name']         = 'Temats namn:';
$_['entry_description']  = 'Temabeskrivning:';
$_['entry_image']        = 'Bild:';

// Error
$_['error_permission']  = 'Varning: Du har inte behörighet att ändra i teman!';
$_['error_name']        = 'Namnet måste innehålla mellan 3 och 32 tecken!';
$_['error_image']       = 'Bild krävs!';
$_['error_voucher']     = 'Varning: Temat kan inte raderas eftersom det är kopplat till %s presentkort!';
?>