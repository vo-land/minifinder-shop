<?php
// Text
$_['text_new_subject']          = '%s - Order %s';
$_['text_new_greeting']         = 'Tack för din beställning!';
$_['text_new_received']         = 'Du har mottagit en order.';
$_['text_new_link']             = 'För att visa din order klicka på länken nedan:';
$_['text_new_order_detail']     = 'Orderinfo';
$_['text_new_instruction']      = 'Instruktioner';
$_['text_new_order_id']         = 'Ordernummer:';
$_['text_new_date_added']       = 'Datum:';
$_['text_new_order_status']     = 'Orderstatus:';
$_['text_new_payment_method']   = 'Betalsätt:';
$_['text_new_shipping_method']  = 'Fraktsätt:';
$_['text_new_email']  		    = 'E-postadress:';
$_['text_new_telephone']     	= 'Mobiltelefon:';
$_['text_new_ip']  		        = 'IP-adress:';
$_['text_new_payment_address']  = 'Betalningsadress';
$_['text_new_shipping_address'] = 'Leveransadress';
$_['text_new_products']         = 'Produkter';
$_['text_new_product']          = 'Produkt';
$_['text_new_model']            = 'Artikelnummer';
$_['text_new_quantity']         = 'Antal';
$_['text_new_price']            = 'Pris';
$_['text_new_order_total']      = 'Ordertotal';	
$_['text_new_total']            = 'Totalt';	
$_['text_new_download']         = 'När din betalning har bekräftats kan du klicka på länken nedan för att få åtkomst till dina nedladdningsbara produkter:';
$_['text_new_comment']          = 'Kommentarer för denna order:';
$_['text_new_footer']           = 'Svara på detta e-postmeddelande om du har några frågor.';
$_['text_update_subject']       = '%s - Order-uppdatering %s';
$_['text_update_order']         = 'Orderid:';
$_['text_update_date_added']    = 'Orderdatum:';
$_['text_update_order_status']  = 'Din order har uppdaterats till följande status:';
$_['text_update_comment']       = 'Kommentarer för din order:';
$_['text_update_link']          = 'För att visa din order klicka på länken nedan:';
$_['text_update_footer']        = 'Var vänlig svara på detta e-postmeddelande om du har några frågor.';
?>