<?php
// Heading 
$_['heading_title']             = 'Affiliateprogram';

// Text
$_['text_account']              = 'Affiliatekonto';
$_['text_register']             = 'Registrera ett Affiliatekonto';
$_['text_account_already']      = 'Om du redan har ett konto hos oss kan du logga in via <a href="%s">login page</a>.';
$_['text_signup']               = 'Fyll i formuläret för att skapa ett Affiliatekonto. Var noga med att ange uppgifterna i de obligatoriska fälten:';
$_['text_your_details']         = 'Personuppgifter';
$_['text_your_address']         = 'Adressuppgifter';
$_['text_payment']              = 'Betalningsuppgifter';
$_['text_your_password']        = 'Lösenord';
$_['text_cheque']               = 'Utbetalningskort';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Banköverföring';
$_['text_agree']                = 'Jag har läst och godkänt <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_firstname']           = 'Förnamn:';
$_['entry_lastname']            = 'Efternamn:';
$_['entry_email']               = 'E-post:';
$_['entry_telephone']           = 'Telefon:';
$_['entry_fax']                 = 'Fax:';
$_['entry_company']             = 'Företag:';
$_['entry_website']             = 'Hemsida:';
$_['entry_address_1']           = 'Adress 1:';
$_['entry_address_2']           = 'Adress 2:';
$_['entry_postcode']            = 'Postnr:';
$_['entry_city']                = 'Stad:';
$_['entry_country']             = 'Land:';
$_['entry_zone']                = 'Län:';
$_['entry_tax']                 = 'Momsregistreringsnummer (VAT-number):';
$_['entry_payment']             = 'Betalsätt:';
$_['entry_cheque']              = 'Mottagarens namn:';
$_['entry_paypal']              = 'PayPal-kopplad e-postadress:';
$_['entry_bank_name']           = 'Banknamn:';
$_['entry_bank_branch_number']  = 'Önskad referenstext:';
$_['entry_bank_swift_code']     = 'Clearingnummer:';
$_['entry_bank_account_name']   = 'Mottagarnamn:';
$_['entry_bank_account_number'] = 'Kontonummer:';
$_['entry_password']            = 'Lösenord:';
$_['entry_confirm']             = 'Bekräfta lösenord:';

// Error
$_['error_exists']              = 'Fel: E-postadressen är redan registrerad!';
$_['error_firstname']           = 'Förnamn måste bestå av minst 1 och högst 32 tecken!';
$_['error_lastname']            = 'Efternamn måste bestå av minst 1 och högst 32 tecken!';
$_['error_email']               = 'E-postadressen verkar inte vara giltig!';
$_['error_telephone']           = 'Telefon måste bestå av minst 3 och högst 32 tecken!';
$_['error_password']            = 'Lösenord måste bestå av minst 4 och högst 20 tecken!';
$_['error_confirm']             = 'Lösenordsbekräftelsen stämmer inte med lösenordet!';
$_['error_address_1']           = 'Adress 1 måste bestå av minst 3 och högst 128 tecken!';
$_['error_city']                = 'Stad måste bestå av minst 2 och högst 128 tecken!';
$_['error_country']             = 'Välj ett land!';
$_['error_zone']                = 'Välj ett län!';
$_['error_postcode']            = 'Postnummer måste bestå av minst 2 och högst 10 tecken!';
$_['error_agree']               = 'Fel: Du måste godkänna %s!';
?>