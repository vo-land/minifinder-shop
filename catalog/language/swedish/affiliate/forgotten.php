<?php
// Heading 
$_['heading_title']   = 'Glömt ditt lösenord?';

// Text
$_['text_account']    = 'Konto';
$_['text_forgotten']  = 'Glömt lösenord?';
$_['text_your_email'] = 'Din e-postadress';
$_['text_email']      = 'Ange den e-postadress som är kopplad till ditt konto och klicka därefter på fortsätt. Lösenordet skickas då med e-post till dig.';
$_['text_success']    = 'Klart: Ett nytt lösenord har skickats till din e-postadress.';

// Entry
$_['entry_email']     = 'E-postadress:';

// Error
$_['error_email']     = 'Fel: E-postadressen kunde inte hittas i våra register. Var vänlig försök igen!';
?>