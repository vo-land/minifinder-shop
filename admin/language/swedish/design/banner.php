<?php
// Heading
$_['heading_title']      = 'Banners';

// Text
$_['text_success']       = 'Klart: Du har ändrat i Banners!';
$_['text_list']          = 'Alternativ grupplista';
$_['text_add']           = 'Lägg till Banner';
$_['text_edit']          = 'Ändra i Banner';
$_['text_default']       = 'Förvalt';

// Column
$_['column_name']        = 'Bannernamn';
$_['column_status']      = 'Status';
$_['column_action']      = 'Ändra';

// Entry
$_['entry_name']         = 'Bannernamn:';
$_['entry_title']        = 'Titel:';
$_['entry_link']         = 'Länk:';
$_['entry_image']        = 'Bild:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sorteringsordning';

// Error
$_['error_permission']   = 'Varning: Du har inte behörighet att ändra i Banners!';
$_['error_name']         = 'Bannernamnet måste vara mellan 3 och 64 tecken!';
$_['error_title']        = 'Bannertitel måste vara mellan 2 och 64 tecken!';
?>