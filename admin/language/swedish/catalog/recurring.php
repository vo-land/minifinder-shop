<?php
// Heading
$_['heading_title']			= 'Återkommande Betalningar';

// Text
$_['text_success']          = 'Klart: Dina ändringar är sparade.';
$_['text_add']              = 'Lägg till Återkommande Betalningar';
$_['text_list']             = 'Alternativ grupplista';
$_['text_edit']             = 'Ändra i Återkommande Betalningar';
$_['text_day']				= 'Dag';
$_['text_week']				= 'Vecka';
$_['text_semi_month']		= 'Halv månad';
$_['text_month']			= 'Månad';
$_['text_year']				= 'År';
$_['text_recurring']	    = '<p><i class="fa fa-info-circle"></i> Recurring amounts are calculated by the frequency and cycles.</p><p>For example if you use a frequency of "week" and a cycle of "2", then the user will be billed every 2 weeks.</p><p>The duration is the number of times the user will make a payment, set this to 0 if you want payments until they are cancelled.</p>';
$_['text_profile']			= 'Återkommande betalning.';
$_['text_trial']			= 'Trial Profil';

// Entry
$_['entry_name']			= 'Namn';
$_['entry_price']			= 'Pris';
$_['entry_duration']		= 'Varaktighet';
$_['entry_cycle']			= 'Hur ofta';
$_['entry_frequency']		= 'Frekvens';
$_['entry_trial_price']		= 'Trial pris';
$_['entry_trial_duration']	= 'Trial varaktighet';
$_['entry_trial_status']	= 'Trial status';
$_['entry_trial_cycle']	    = 'Trial hur ofta';
$_['entry_trial_frequency']	= 'Trial frekvens';
$_['entry_status']			= 'Status';
$_['entry_sort_order']		= 'Sorteringsordning';

// Column
$_['column_name']			= 'Namn';
$_['column_sort_order']	    = 'Sorteringsordning';
$_['column_action']         = 'Klart';

// Error
$_['error_warning']         = 'Varning: Kolla igenom inställningarna för fel.';
$_['error_permission']		= 'Varning: Du har inte tillåtelse att ändra i profilen.';
$_['error_name']			= 'Profil Namnet måste vara mer än 3 tecken och mindre än 255.';
$_['error_product']			= 'Varning: Denna profil kan inte raderas då den används på %s produkter!';