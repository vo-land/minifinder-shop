<?php
//licensing check
$home = 'https://www.secureserverssl.co.uk/opencart-extensions/google-merchant/';
$domain = str_replace("https://", "", str_replace("http://", "", HTTPS_SERVER));

if (extension_loaded('curl')) {
    $curl = curl_init();
    
   	curl_setopt($curl, CURLOPT_URL, $home . 'licensed.php?domain=' . $domain . '&extension=19110');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    
    $licensed = curl_exec($curl);
    
    curl_close($curl);

    $curl = curl_init();
}else{
	$curl = 'n';
    $licensed = 'curl';
}

if(isset($this->request->get['regerror'])&&$this->request->get['regerror']=='emailmal'){
	$error_warning = $regerror_email;
}

if(isset($this->request->get['regerror'])&&$this->request->get['regerror']=='orderidmal'){
	$error_warning = $regerror_orderid;
}

if(isset($this->request->get['regerror'])&&$this->request->get['regerror']=='noreferer'){
	$error_warning = $regerror_noreferer;
}

if(isset($this->request->get['regerror'])&&$this->request->get['regerror']=='localhost'){
	$error_warning = $regerror_localhost;
}

if(isset($this->request->get['regerror'])&&$this->request->get['regerror']=='licensedupe'){
	$error_warning = $regerror_licensedupe;
}
?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><?php if(md5($licensed)=='e9dc924f238fa6cc29465942875fe8f0' && $state=='complete'){ ?>
        <button type="submit" form="form-uksb-google-merchant" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button><?php } ?>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
    </div>
    <div class="panel-body">
        <?php if ($error_warning || $error_duplicate) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo ($error_warning?$error_warning:$error_duplicate); ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
      <?php if(md5($licensed)=='e9dc924f238fa6cc29465942875fe8f0'){ ?>
      <?php if($state!='complete'){ ?>
      <div id="create_data">
      <h2><?php echo $text_initialise_data; ?></h2>
      <?php echo $text_initialise_data_text; ?>
      <a onclick="$('#create_data').hide();$('#creating_data').show();location = '<?php echo $uksb_install_link; ?>';"><button type="button" id="loading" class="btn btn-primary">
 <?php echo $button_run; ?>
</button></a>
      </div>
      <div id="creating_data" style="display:none;">
      <p><img src="view/image/create_data.gif"></p>
      </div>
      <?php } else { ?>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-uksb-google-merchant" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general_settings; ?></a></li>
            <li><a href="#tab-google-settings" data-toggle="tab"><?php echo $tab_google_settings; ?></a></li>
            <li><a href="#tab-google-feeds" data-toggle="tab"><?php echo $tab_google_feeds; ?></a></li>
            <li><a href="#tab-bing-feeds" data-toggle="tab"><?php echo $tab_bing_feeds; ?></a></li>
            <li><a href="#tab-utilities" data-toggle="tab"><?php echo $tab_utilities; ?></a></li>
            <li><a href="#tab-videos" data-toggle="tab"><?php echo $tab_videos; ?></a></li>
          </ul>
      <div class="tab-content">    
    <div class="tab-pane active" id="tab-general">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status1"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
            	<div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary<?php if ($uksb_google_merchant_status) { ?> active<?php } ?>">
                        <input type="radio" name="uksb_google_merchant_status" id="input-status1" value="1"<?php if ($uksb_google_merchant_status) { ?> checked<?php } ?>><?php echo $text_enabled; ?>
                    </label>
                    <label class="btn btn-primary<?php if (!$uksb_google_merchant_status) { ?> active<?php } ?>">
                        <input type="radio" name="uksb_google_merchant_status" id="input-status2" value="0"<?php if (!$uksb_google_merchant_status) { ?> checked<?php } ?>><?php echo $text_disabled; ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-characters1"><span data-toggle="tooltip" title="<?php echo $help_characters; ?>"><?php echo $entry_characters; ?></span></label>
            <div class="col-sm-10">
            	<div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary<?php if ($uksb_google_merchant_characters) { ?> active<?php } ?>">
                        <input type="radio" name="uksb_google_merchant_characters" id="input-characters1" value="1"<?php if ($uksb_google_merchant_characters) { ?> checked<?php } ?>><?php echo $text_enabled; ?>
                    </label>
                    <label class="btn btn-primary<?php if (!$uksb_google_merchant_characters) { ?> active<?php } ?>">
                        <input type="radio" name="uksb_google_merchant_characters" id="input-characters2" value="0"<?php if (!$uksb_google_merchant_characters) { ?> checked<?php } ?>><?php echo $text_disabled; ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="split"><span data-toggle="tooltip" title="<?php echo $help_split; ?>"><?php echo $entry_split; ?></span></label>
            <div class="col-sm-10">
              <select name="uksb_google_merchant_split" id="split" class="form-control">
                <option value="0"<?php if (!$uksb_google_merchant_split) { ?> selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                <option value="50"<?php if ($uksb_google_merchant_split=='50') { ?> selected="selected"<?php } ?>>50</option>
                <option value="100"<?php if ($uksb_google_merchant_split=='100') { ?> selected="selected"<?php } ?>>100</option>
                <option value="250"<?php if ($uksb_google_merchant_split=='250') { ?> selected="selected"<?php } ?>>250</option>
                <option value="500"<?php if ($uksb_google_merchant_split=='500') { ?> selected="selected"<?php } ?>>500</option>
                <option value="750"<?php if ($uksb_google_merchant_split=='750') { ?> selected="selected"<?php } ?>>750</option>
                <option value="1000"<?php if ($uksb_google_merchant_split=='1000') { ?> selected="selected"<?php } ?>>1000</option>
                <option value="1500"<?php if ($uksb_google_merchant_split=='1500') { ?> selected="selected"<?php } ?>>1500</option>
                <option value="2000"<?php if ($uksb_google_merchant_split=='2000') { ?> selected="selected"<?php } ?>>2000</option>
                <option value="2500"<?php if ($uksb_google_merchant_split=='2500') { ?> selected="selected"<?php } ?>>2500</option>
                <option value="3000"<?php if ($uksb_google_merchant_split=='3000') { ?> selected="selected"<?php } ?>>3000</option>
                <option value="3500"<?php if ($uksb_google_merchant_split=='3500') { ?> selected="selected"<?php } ?>>3500</option>
                <option value="4000"<?php if ($uksb_google_merchant_split=='4000') { ?> selected="selected"<?php } ?>>4000</option>
                <option value="4500"<?php if ($uksb_google_merchant_split=='4500') { ?> selected="selected"<?php } ?>>4500</option>
                <option value="5000"<?php if ($uksb_google_merchant_split=='5000') { ?> selected="selected"<?php } ?>>5000</option>
                <option value="6000"<?php if ($uksb_google_merchant_split=='6000') { ?> selected="selected"<?php } ?>>6000</option>
                <option value="7000"<?php if ($uksb_google_merchant_split=='7000') { ?> selected="selected"<?php } ?>>7000</option>
                <option value="8000"<?php if ($uksb_google_merchant_split=='8000') { ?> selected="selected"<?php } ?>>8000</option>
                <option value="9000"<?php if ($uksb_google_merchant_split=='9000') { ?> selected="selected"<?php } ?>>9000</option>
                <option value="10000"<?php if ($uksb_google_merchant_split=='10000') { ?> selected="selected"<?php } ?>>10000</option>
                <option value="12500"<?php if ($uksb_google_merchant_split=='12500') { ?> selected="selected"<?php } ?>>12500</option>
                <option value="15000"<?php if ($uksb_google_merchant_split=='15000') { ?> selected="selected"<?php } ?>>15000</option>
                <option value="20000"<?php if ($uksb_google_merchant_split=='20000') { ?> selected="selected"<?php } ?>>20000</option>
                <option value="25000"<?php if ($uksb_google_merchant_split=='25000') { ?> selected="selected"<?php } ?>>25000</option>
                <option value="30000"<?php if ($uksb_google_merchant_split=='30000') { ?> selected="selected"<?php } ?>>30000</option>
                <option value="40000"<?php if ($uksb_google_merchant_split=='40000') { ?> selected="selected"<?php } ?>>40000</option>
                <option value="50000"<?php if ($uksb_google_merchant_split=='50000') { ?> selected="selected"<?php } ?>>50000</option>
              </select></div>
          </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-cron1"><span data-toggle="tooltip" title="<?php echo $help_cron; ?>"><?php echo $entry_cron; ?></span></label>
            <div class="col-sm-10">
            	<div class="btn-group" data-toggle="buttons">
                    <label onclick="$('#split_help').fadeOut().fadeIn();$( '#split' ).val('0');" class="btn btn-primary<?php if ($uksb_google_merchant_cron) { ?> active<?php } ?>">
                        <input type="radio" name="uksb_google_merchant_cron" id="input-cron1" value="1"<?php if ($uksb_google_merchant_cron) { ?> checked<?php } ?>><?php echo $text_enabled; ?>
                    </label>
                    <label onclick="$('#split_help').fadeOut().fadeIn();" class="btn btn-primary<?php if (!$uksb_google_merchant_cron) { ?> active<?php } ?>">
                        <input type="radio" name="uksb_google_merchant_cron" id="input-cron2" value="0"<?php if (!$uksb_google_merchant_cron) { ?> checked<?php } ?>><?php echo $text_disabled; ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group" id="split_help" style="display:none;">
            <div class="col-sm-2">&nbsp;</div>
            <div class="col-sm-10"><span style="color:red;"><br /><?php echo $help_split_help; ?></span></div>
          </div>
        
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_info; ?></label>
            <div class="col-sm-10"><?php echo $help_info; ?></div>
          </div>
        </div>
    <div class="tab-pane" id="tab-google-settings">
       <div class="form-group">
            <label class="col-sm-2 control-label" for="select-gpc"><span data-toggle="tooltip" title="<?php echo $help_google_category; ?><br /><br /><?php echo $entry_choose_google_category_xml; ?>"><?php echo $entry_google_category; ?></span></label>
            <div class="col-sm-8"><div style="overflow:auto; height:400px;"><div class="input-group"><span class="input-group-addon"><img src="view/image/flags/gb.png" /></span><input id="select-gpc" type="text" name="uksb_google_merchant_google_category_gb" value="<?php echo $uksb_google_merchant_google_category_gb; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/us.png" /></span><input type="text" name="uksb_google_merchant_google_category_us" value="<?php echo $uksb_google_merchant_google_category_us; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=en-US','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/au.png" /></span><input type="text" name="uksb_google_merchant_google_category_au" value="<?php echo $uksb_google_merchant_google_category_au; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=en-AU','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/fr.png" /></span><input type="text" name="uksb_google_merchant_google_category_fr" value="<?php echo $uksb_google_merchant_google_category_fr; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=fr-FR','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/de.png" /></span><input type="text" name="uksb_google_merchant_google_category_de" value="<?php echo $uksb_google_merchant_google_category_de; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=de-DE','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/it.png" /></span><input type="text" name="uksb_google_merchant_google_category_it" value="<?php echo $uksb_google_merchant_google_category_it; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=it-IT','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/nl.png" /></span><input type="text" name="uksb_google_merchant_google_category_nl" value="<?php echo $uksb_google_merchant_google_category_nl; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=nl-NL','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
           <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/es.png" /></span><input type="text" name="uksb_google_merchant_google_category_es" value="<?php echo $uksb_google_merchant_google_category_es; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=es-ES','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/br.png" /></span><input type="text" name="uksb_google_merchant_google_category_pt" value="<?php echo $uksb_google_merchant_google_category_pt; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=pt-BR','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/cz.png" /></span><input type="text" name="uksb_google_merchant_google_category_cz" value="<?php echo $uksb_google_merchant_google_category_cz; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=cs-CZ','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/jp.png" /></span><input type="text" name="uksb_google_merchant_google_category_jp" value="<?php echo $uksb_google_merchant_google_category_jp; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=ja-JP','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/dk.png" /></span><input type="text" name="uksb_google_merchant_google_category_dk" value="<?php echo $uksb_google_merchant_google_category_dk; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=da-DK','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/no.png" /></span><input type="text" name="uksb_google_merchant_google_category_no" value="<?php echo $uksb_google_merchant_google_category_no; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=no-NO','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/pl.png" /></span><input type="text" name="uksb_google_merchant_google_category_pl" value="<?php echo $uksb_google_merchant_google_category_pl; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=pl-PL','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/ru.png" /></span><input type="text" name="uksb_google_merchant_google_category_ru" value="<?php echo $uksb_google_merchant_google_category_ru; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=ru-RU','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/se.png" /></span><input type="text" name="uksb_google_merchant_google_category_sv" value="<?php echo $uksb_google_merchant_google_category_sv; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=sv-SE','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br />
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/tr.png" /></span><input type="text" name="uksb_google_merchant_google_category_tr" value="<?php echo $uksb_google_merchant_google_category_tr; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $home; ?>taxonomy.php?lang=tr-TR','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div></div></div>
          </div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_condition1"><span data-toggle="tooltip" title="<?php echo $help_condition; ?>"><?php echo $entry_condition; ?></span></label>
			<div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
					<label class="btn btn-primary<?php if (!$uksb_google_merchant_condition||$uksb_google_merchant_condition=='new') { ?> active<?php } ?>">
						<input type="radio" name="uksb_google_merchant_condition" id="input-g_condition1" value="new"<?php if (!$uksb_google_merchant_condition||$uksb_google_merchant_condition=='new') { ?> checked<?php } ?>><?php echo $text_condition_new; ?>
					</label>
					<label class="btn btn-primary<?php if ($uksb_google_merchant_condition=='used') { ?> active<?php } ?>">
						<input type="radio" name="uksb_google_merchant_condition" id="input-g_condition2" value="used"<?php if ($uksb_google_merchant_condition=='used') { ?> checked<?php } ?>><?php echo $text_condition_used; ?>
					</label>
					<label class="btn btn-primary<?php if ($uksb_google_merchant_condition=='refurbished') { ?> active<?php } ?>">
						<input type="radio" name="uksb_google_merchant_condition" id="input-g_condition3" value="refurbished"<?php if ($uksb_google_merchant_condition=='refurbished') { ?> checked<?php } ?>><?php echo $text_condition_ref; ?>
					</label>
				</div>
			</div>
		</div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="select-mpn"><span data-toggle="tooltip" title="<?php echo $help_mpn; ?>"><?php echo $entry_mpn; ?></span></label>
            <div class="col-sm-10">
              <select name="uksb_google_merchant_mpn" id="select-mpn" class="form-control">
                <option value="sku"<?php if ($uksb_google_merchant_mpn=='sku') { ?> selected="selected"<?php } ?>><?php echo $text_sku; ?></option>
                <option value="model"<?php if (!$uksb_google_merchant_mpn||$uksb_google_merchant_mpn=='model') { ?> selected="selected"<?php } ?>><?php echo $text_model; ?></option>
                <option value="mpn"<?php if ($uksb_google_merchant_mpn=='mpn') { ?> selected="selected"<?php } ?>><?php echo $text_mpn; ?></option>
                <option value="location"<?php if ($uksb_google_merchant_mpn=='location') { ?> selected="selected"<?php } ?>><?php echo $text_location; ?></option>
              </select></div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="select-gtin"><span data-toggle="tooltip" title="<?php echo $help_gtin; ?>"><?php echo $entry_gtin; ?></span></label>
            <div class="col-sm-10">
              <select name="uksb_google_merchant_g_gtin" id="select-gtin" class="form-control">
                <option value="default"<?php if (!$uksb_google_merchant_g_gtin||$uksb_google_merchant_g_gtin=='default') { ?> selected="selected"<?php } ?>><?php echo $text_default; ?></option>
                <option value="sku"<?php if ($uksb_google_merchant_g_gtin=='sku') { ?> selected="selected"<?php } ?>><?php echo $text_sku; ?></option>
                <option value="gtin"<?php if ($uksb_google_merchant_g_gtin=='gtin') { ?> selected="selected"<?php } ?>><?php echo $text_gtin; ?></option>
                <option value="location"<?php if ($uksb_google_merchant_g_gtin=='location') { ?> selected="selected"<?php } ?>><?php echo $text_location; ?></option>
              </select></div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="select-gender"><span data-toggle="tooltip" title="<?php echo $help_gender; ?>"><?php echo $entry_gender; ?></span></label>
            <div class="col-sm-10">
              <select name="uksb_google_merchant_gender" id="select-gender" class="form-control">
                <option value="0"<?php if (!$uksb_google_merchant_gender||$uksb_google_merchant_gender=='0') { ?> selected="selected"<?php } ?>><?php echo $text_none; ?></option>
                <option value="male"<?php if ($uksb_google_merchant_gender=='male') { ?> selected="selected"<?php } ?>><?php echo $text_male; ?></option>
                <option value="female"<?php if ($uksb_google_merchant_gender=='female') { ?> selected="selected"<?php } ?>><?php echo $text_female; ?></option>
                <option value="unisex"<?php if ($uksb_google_merchant_gender=='unisex') { ?> selected="selected"<?php } ?>><?php echo $text_unisex; ?></option>
              </select></div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="select-age_agroup"><span data-toggle="tooltip" title="<?php echo $help_age_group; ?>"><?php echo $entry_age_group; ?></span></label>
            <div class="col-sm-10">
              <select name="uksb_google_merchant_age_group" id="select-age_agroup" class="form-control">
                <option value="0"<?php if (!$uksb_google_merchant_age_group||$uksb_google_merchant_age_group=='0') { ?> selected="selected"<?php } ?>><?php echo $text_none; ?></option>
                <option value="newborn"<?php if ($uksb_google_merchant_age_group=='newborn') { ?> selected="selected"<?php } ?>><?php echo $text_newborn; ?></option>
                <option value="toddler"<?php if ($uksb_google_merchant_age_group=='toddler') { ?> selected="selected"<?php } ?>><?php echo $text_toddler; ?></option>
                <option value="infant"<?php if ($uksb_google_merchant_age_group=='infant') { ?> selected="selected"<?php } ?>><?php echo $text_infant; ?></option>
                <option value="kids"<?php if ($uksb_google_merchant_age_group=='kids') { ?> selected="selected"<?php } ?>><?php echo $text_kids; ?></option>
                <option value="adult"<?php if ($uksb_google_merchant_age_group=='adult') { ?> selected="selected"<?php } ?>><?php echo $text_adult; ?></option>
              </select></div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_info; ?></label>
            <div class="col-sm-10"><?php echo $help_info; ?></div>
          </div>
        </div>
         <div class="tab-pane" id="tab-google-feeds">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="google-site"><span data-toggle="tooltip" title="<?php echo $help_site; ?>"><?php echo $entry_site; ?></span></label>
            <div class="col-sm-10">
              <select name="uksb_google_merchant_site" id="google_site" class="form-control">
                <option value="default" selected="selected">Store Default</option>
                <option value="gb">United Kingdom</option>
                <option value="us">United States of America</option>
                <option value="ca">Canada (English)</option>
                <option value="ca_fr">Canada (Français)</option>
                <option value="mx">México</option>
                <option value="au">Australia</option>
                <option value="fr">France</option>
                <option value="de">Deutschland</option>
                <option value="it">Italia</option>
                <option value="nl">Nederlands</option>
                <option value="es">España</option>
                <option value="be_nl">België (Nederlands)</option>
                <option value="be_fr">Belgique (Français)</option>
                <option value="at">Österreich</option>
                <option value="dk">Danmark</option>
                <option value="no">Norge</option>
                <option value="sv">Sverige</option>
                <option value="pl">Polska</option>
                <option value="cz">Československo</option>
                <option value="ch">Switzerland (English)</option>
                <option value="ch_fr">Suisse (Français)</option>
                <option value="ch_de">Schweiz (Deutsch)</option>
                <option value="ch_it">Svizzera (Italiano)</option>
                <option value="ru">Россия</option>
                <option value="tr">Türkiye</option>
                <option value="br">Brasil</option>
                <option value="in">India (English)</option>
                <option value="ja">日本</option>
              </select></div>
          </div>
          <?php if($config_cron){ ?>
          <div class="form-group">
            <div class="col-sm-2">&nbsp;</div>
            <div class="col-sm-10"><?php echo $help_cron_code; ?></div>
          </div>
          <?php } ?>
          <?php
          $feeds = explode("^", $data_feed);
          $crons = ($config_cron?explode("^", $data_cron_path):'');
          $i=0;
          foreach (array_keys($feeds) as $key) {
          ?>
          <div class="form-group">
          <label class="col-sm-2 control-label" for="feed_url_<?php echo $i; ?>"><?php echo $entry_data_feed; ?></label>
          <div class="col-sm-10"><textarea id="feed_url_<?php echo $i; ?>" rows="2" class="form-control" readonly onClick="$(this).select();"><?php echo $feeds[$key]; ?></textarea></div>
          </div>
          <?php if($config_cron){ ?>
          <div class="form-group">
          <label class="col-sm-2 control-label" for="cron_code_<?php echo $i; ?>"><?php echo $entry_cron_code; ?></label>
          <div class="col-sm-10"><textarea id="cron_code_<?php echo $i; ?>" rows="2" class="form-control" readonly onClick="$(this).select();">curl -L "<?php echo $crons[$key]; ?>" >/dev/null 2>&1</textarea></div>
          </div>
          <?php } ?>
          <?php
          $i++;
          } ?>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_info; ?></label>
            <div class="col-sm-10"><?php echo $help_info; ?></div>
          </div>
        </div>
        <div class="tab-pane" id="tab-bing-feeds">
          <?php
          $bingfeeds = explode("^", $data_bingfeed);
          $i=0;
          foreach($bingfeeds as $bingfeed){
          ?>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="bingfeed_url_<?php echo $i; ?>"><?php echo $entry_data_feed; ?></label>
            <div class="col-sm-10"><textarea id="bingfeed_url_<?php echo $i; ?>" rows="2" class="form-control" readonly onClick="$(this).select();"><?php echo $bingfeed; ?></textarea></div>
            </div>
          <?php
          $i++;
          } ?>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_info; ?></label>
            <div class="col-sm-10"><?php echo $help_info; ?></div>
          </div>
        </div>
        
        
        
        <div class="tab-pane" id="tab-utilities">

          <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $utilities1; ?></label>
            <div class="col-sm-7"> <a onclick="if(confirm('<?php echo $utilities_confirm; ?>')){return window.open('model/feed/uksb_google_merchant_utilities.php?run=1','utilities','menubar=0, toolbar=0, resizable=1, scrollable=0, width=350, height=250');}"><button type="button" id="utilities1" class="btn btn-primary">
 <?php echo $button_run; ?>
</button></a></div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $utilities2; ?></label>
            <div class="col-sm-7"> <a onclick="if(confirm('<?php echo $utilities_confirm; ?>')){return window.open('model/feed/uksb_google_merchant_utilities.php?run=2','utilities','menubar=0, toolbar=0, resizable=1, scrollable=0, width=350, height=250');}"><button type="button" id="utilities2" class="btn btn-primary">
 <?php echo $button_run; ?>
</button></a></div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $utilities3; ?></label>
            <div class="col-sm-7"> <a onclick="if(confirm('<?php echo $utilities_confirm; ?>')){return window.open('model/feed/uksb_google_merchant_utilities.php?run=3','utilities','menubar=0, toolbar=0, resizable=1, scrollable=0, width=350, height=250');}"><button type="button" id="utilities3" class="btn btn-primary">
 <?php echo $button_run; ?>
</button></a></div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $utilities4; ?></label>
            <div class="col-sm-7"> <a onclick="if(confirm('<?php echo $utilities_confirm; ?>')){return window.open('model/feed/uksb_google_merchant_utilities.php?run=4','utilities','menubar=0, toolbar=0, resizable=1, scrollable=0, width=350, height=250');}"><button type="button" id="utilities4" class="btn btn-primary">
 <?php echo $button_run; ?>
</button></a></div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $utilities5; ?></label>
            <div class="col-sm-7"> <a onclick="if(confirm('<?php echo $utilities_confirm; ?>')){return window.open('model/feed/uksb_google_merchant_utilities.php?run=5','utilities','menubar=0, toolbar=0, resizable=1, scrollable=0, width=350, height=250');}"><button type="button" id="utilities5" class="btn btn-primary">
 <?php echo $button_run; ?>
</button></a></div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $utilities6; ?></label>
            <div class="col-sm-7"> <a onclick="if(confirm('<?php echo $utilities_confirm; ?>')){return window.open('model/feed/uksb_google_merchant_utilities.php?run=6','utilities','menubar=0, toolbar=0, resizable=1, scrollable=0, width=350, height=250');}"><button type="button" id="utilities6" class="btn btn-primary">
 <?php echo $button_run; ?>
</button></a></div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $utilities7; ?></label>
            <div class="col-sm-7"> <a onclick="if(confirm('<?php echo $utilities_confirm; ?>')){return window.open('model/feed/uksb_google_merchant_utilities.php?run=7','utilities','menubar=0, toolbar=0, resizable=1, scrollable=0, width=350, height=250');}"><button type="button" id="utilities7" class="btn btn-primary">
 <?php echo $button_run; ?>
</button></a></div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $utilities8; ?></label>
            <div class="col-sm-7"> <a onclick="if(confirm('<?php echo $utilities_confirm; ?>')){return window.open('model/feed/uksb_google_merchant_utilities.php?run=8','utilities','menubar=0, toolbar=0, resizable=1, scrollable=0, width=350, height=250');}"><button type="button" id="utilities8" class="btn btn-primary">
 <?php echo $button_run; ?>
</button></a></div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $utilities9; ?></label>
            <div class="col-sm-7"> <a onclick="if(confirm('<?php echo $utilities_confirm; ?>')){return window.open('model/feed/uksb_google_merchant_utilities.php?run=9','utilities','menubar=0, toolbar=0, resizable=1, scrollable=0, width=350, height=250');}"><button type="button" id="utilities9" class="btn btn-primary">
 <?php echo $button_run; ?>
</button></a></div>
          </div>


          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_info; ?></label>
            <div class="col-sm-10"><?php echo $help_info; ?></div>
          </div>
        </div>
        
        
        <div class="tab-pane" id="tab-videos">
        <div class="form-group">
        <div class="embed-responsive embed-responsive-16by9"><iframe src="//www.youtube.com/embed/videoseries?list=SPzQz7G36iOiZsePOZPhA8band-1rxZ9ae" frameborder="0" allowfullscreen></iframe></div>
        </div>
        
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_info; ?></label>
            <div class="col-sm-10"><?php echo $help_info; ?></div>
          </div>
        </div>
</div>        
        </form>
    <?php } ?>
    <?php } ?>
    <?php if($licensed=='none'){ ?>
    <?php echo $license_purchase_thanks; ?>
    <?php if(isset($this->request->get['regerror'])){ echo $regerror_quote_msg; } ?>
    <?php if(isset($this->request->get['regerror'])){ ?><p style="color:red;">error msg: <?php echo $this->request->get['regerror']; ?></p><?php } ?>
      <h2><?php echo $license_registration; ?></h2>
    <form name="reg" method="post" action="<?php echo $home; ?>register.php" id="reg" class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="opencart_email"><?php echo $license_opencart_email; ?></label>
            <div class="col-sm-10">
          	  <input name="opencart_email" type="text" autofocus required id="opencart_email" form="reg" class="form-control"></div>
          </div>
	<?php if(isset($this->request->get['emailmal'])&&$this->request->get['regerror']=='emailmal'){ ?><p style="color:red;"><?php echo $check_email; ?></p><?php } ?>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="order_id"><?php echo $license_opencart_orderid; ?></label>
            <div class="col-sm-10">
          	  <input name="order_id" type="text" autofocus required id="order_id" form="reg" class="form-control"></div>
          </div>
	<?php if(isset($this->request->get['regerror'])&&$this->request->get['regerror']=='orderid'){ ?><p style="color:red;"><?php echo $check_orderid; ?></p><?php } ?>
        <div class="form-group">
            <div class="col-sm-12">
          	  <button type="submit" form="reg" data-toggle="tooltip" title="<?php echo $license_registration; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button><input name="extension_id" type="hidden" id="extension_id" form="reg" value="19110"></div>
          </div>
    </form>
    <?php } ?>
    <?php if($licensed=='curl'){ ?>
    <?php echo $server_error_curl; ?>
    <?php } ?>
     
    </div>
  </div>
  </div>
</div>
<?php echo $footer; ?>

<?php if(md5($licensed)=='e9dc924f238fa6cc29465942875fe8f0'){ ?>
<?php if($state=='complete'){ ?>
<script type="text/javascript"><!--
$(document).ready(function(){ 
<?php
$i = 0;
reset($feeds);
if($config_cron){
	reset($crons);
}
foreach (array_keys($feeds) as $key) {
?>
		
	$("textarea#feed_url_<?php echo $i; ?>").text('<?php echo $feeds[$key] . ($config_cron ? 'google_' . $config_language . '-' . $config_currency . '.xml' : '&language=' . $config_language . '&currency=' . $config_currency); ?>');
		
		<?php if($config_cron){ ?>$("textarea#cron_code_<?php echo $i; ?>").text('<?php echo 'curl -L "' . $crons[$key] . '&language='.$config_language.'&currency='.$config_currency.'" >/dev/null 2>&1'; ?>');
		<?php } ?>
<?php $i++; } ?>
			
	$("#split").change(function(){
		$("#split_help").fadeOut().fadeIn();
		if($( "#split" ).val() != '0'){
			$( "#input-cron2" ).trigger( "click" );
		};
	});

	$("#google_site").change(function(){
		var site;
		site = $("#google_site").val();
		var cron_lang_curr;
		var feed_lang_curr;
		
		switch (site) {
			case 'gb':
				cron_lang_curr = 'en-GBP';
				feed_lang_curr = '&language=en&currency=GBP';
				break;
			case 'us':
				cron_lang_curr = 'en-USD';
				feed_lang_curr = '&language=en&currency=USD';
				break;
			case 'ca':
				cron_lang_curr = 'en-CAD';
				feed_lang_curr = '&language=en&currency=CAD';
				break;
			case 'ca_fr':
				cron_lang_curr = 'fr-CAD';
				feed_lang_curr = '&language=fr&currency=CAD';
				break;
			case 'au':
				cron_lang_curr = 'en-AUD';
				feed_lang_curr = '&language=en&currency=AUD';
				break;
			case 'fr':
			case 'be_fr':
				cron_lang_curr = 'fr-EUR';
				feed_lang_curr = '&language=fr&currency=EUR';
				break;
			case 'de':
			case 'at':
				cron_lang_curr = 'de-EUR';
				feed_lang_curr = '&language=de&currency=EUR';
				break;
			case 'it':
				cron_lang_curr = 'it-EUR';
				feed_lang_curr = '&language=it&currency=EUR';
				break;
			case 'nl':
			case 'be_nl':
				cron_lang_curr = 'nl-EUR';
				feed_lang_curr = '&language=nl&currency=EUR';
				break;
			case 'es':
				cron_lang_curr = 'es-EUR';
				feed_lang_curr = '&language=es&currency=EUR';
				break;
			case 'dk':
				cron_lang_curr = 'dk-DKK';
				feed_lang_curr = '&language=dk&currency=DKK';
				break;
			case 'no':
				cron_lang_curr = 'no-NOK';
				feed_lang_curr = '&language=no&currency=NOK';
				break;
			case 'sv':
				cron_lang_curr = 'se-SEK';
				feed_lang_curr = '&language=se&currency=SEK';
				break;
			case 'pl':
				cron_lang_curr = 'pl-PLN';
				feed_lang_curr = '&language=pl&currency=PLN';
				break;
			case 'cz':
				cron_lang_curr = 'cz-CZK';
				feed_lang_curr = '&language=cz&currency=CZK';
				break;
			case 'ru':
				cron_lang_curr = 'ru-RUB';
				feed_lang_curr = '&language=ru&currency=RUB';
				break;
			case 'tr':
				cron_lang_curr = 'tr-TRY';
				feed_lang_curr = '&language=tr&currency=TRY';
				break;
			case 'in':
				cron_lang_curr = 'en-INR';
				feed_lang_curr = '&language=en&currency=INR';
				break;
			case 'ja':
				cron_lang_curr = 'ja-JPY';
				feed_lang_curr = '&language=ja&currency=JPY';
				break;
			case 'br':
				cron_lang_curr = 'pt-BRL';
				feed_lang_curr = '&language=pt&currency=BRL';
				break;
			case 'mx':
				cron_lang_curr = 'es-MXN';
				feed_lang_curr = '&language=es&currency=MXN';
				break;
			case 'ch':
				cron_lang_curr = 'en-CHF';
				feed_lang_curr = '&language=en&currency=CHF';
				break;
			case 'ch-fr':
				cron_lang_curr = 'fr-CHF';
				feed_lang_curr = '&language=fr&currency=CHF';
				break;
			case 'ch-de':
				cron_lang_curr = 'de-CHF';
				feed_lang_curr = '&language=de&currency=CHF';
				break;
			case 'ch-it':
				cron_lang_curr = 'it-CHF';
				feed_lang_curr = '&language=it&currency=CHF';
				break;
			case 'default':
			default:
				cron_lang_curr = '<?php echo $config_currency.'-'.$config_language; ?>';
				feed_lang_curr = '<?php echo '&language='.$config_language.'&currency='.$config_currency; ?>';
		}
		
			<?php $i = 0; reset($feeds); if($config_cron){reset($crons);}
    
			foreach (array_keys($feeds) as $key) {
			
				if($config_cron){ ?>
				$("textarea#feed_url_<?php echo $i; ?>").text('<?php echo $feeds[$key]; ?>' + 'google_' + cron_lang_curr + '.xml');
				$("textarea#cron_code_<?php echo $i; ?>").text('curl -L "' + '<?php echo $crons[$key]; ?>' + feed_lang_curr + '" >/dev/null 2>&1');
				<?php }else{ ?>
				$("textarea#feed_url_<?php echo $i; ?>").text('<?php echo $feeds[$key]; ?>' + feed_lang_curr);
				<?php } ?>
				
				
			<?php $i++; } ?>
		
	});
});
//--></script>
<?php } ?>
<?php } ?>