<?php echo $header; ?><?php echo $column_left; ?>
    <div id="content">
        <div class="page-header">
            <div class="container-fluid">
                <div class="pull-right">
                    <button type="submit" form="form-banner" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
                <h1><?php echo $heading_title; ?></h1>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
                </div>
                <div class="panel-body">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-banner" class="form-horizontal">
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                                <?php if ($error_name) { ?>
                                    <div class="text-danger"><?php echo $error_name; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                            <div class="col-sm-10">
                                <select name="status" id="input-status" class="form-control">
                                    <?php if ($status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <table id="images" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-left"><?php echo $entry_title; ?></td>
                                <td class="text-left"><?php echo $entry_description; ?></td>
                                <td class="text-left"><?php echo $entry_image; ?></td>
                                <td class="text-right"><?php echo $entry_sort_order; ?></td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $image_row = 0; ?>
                            <?php foreach ($banner_images as $banner_image) { ?>
                                <tr id="image-row<?php echo $image_row; ?>">
                                    <td class="text-left" style="width: 40%; vertical-align: top;">
                                        <input type="hidden" name="banner_image[<?php echo $image_row; ?>][button_ena]" value="<?php echo $banner_image['button_ena']; ?>">
                                        <input
                                                id="banner-image<?php echo $image_row; ?>-button"
                                                type="checkbox"
                                                onclick="toggleFields(this);"
                                            <?php echo $banner_image['button_ena'] ? 'checked' : ''; ?>
                                        />
                                        <label for="banner-image<?php echo $image_row; ?>-button"><?php echo $entry_button_ena; ?></label>
                                        <?php foreach ($languages as $language) { ?>
                                            <div class="input-group pull-left"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> </span>
                                                <input type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($banner_image['banner_image_description'][$language['language_id']]) ? $banner_image['banner_image_description'][$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />
                                                <?php if (isset($error_banner_image[$image_row][$language['language_id']]['title'])) { ?>
                                                    <div class="text-danger"><?php echo $error_banner_image[$image_row][$language['language_id']]['title']; ?></div>
                                                <?php } ?>
                                                <div class="banner_link<?php echo $banner_image['button_ena'] ? '' : ' banner_active_field'; ?>" >
                                                    <input type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][slide_link]" value="<?php echo $banner_image['banner_image_description'][$language['language_id']]['slide_link']; ?>" placeholder="<?php echo $entry_link; ?>" class="form-control" />
                                                </div>
                                                <div class="banner_button<?php echo $banner_image['button_ena'] ? ' banner_active_field' : ''; ?>">
                                                    <input type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][button_title]" value="<?php echo $banner_image['banner_image_description'][$language['language_id']]['button_title']; ?>" placeholder="<?php echo $entry_button_title; ?>" class="form-control" />
                                                    <?php if (isset($error_banner_image[$image_row][$language['language_id']]['button_title'])) { ?>
                                                        <div class="text-danger"><?php echo $error_banner_image[$image_row][$language['language_id']]['button_title']; ?></div>
                                                    <?php } ?>
                                                    <input type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][button_link]" value="<?php echo $banner_image['banner_image_description'][$language['language_id']]['button_link']; ?>" placeholder="<?php echo $entry_button_link; ?>" class="form-control" />
                                                    <?php if (isset($error_banner_image[$image_row][$language['language_id']]['button_link'])) { ?>
                                                        <div class="text-danger"><?php echo $error_banner_image[$image_row][$language['language_id']]['button_link']; ?></div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left" style="width: 30%; vertical-align: top;">
                                        <input type="hidden" name="banner_image[<?php echo $image_row; ?>][description_ena]" value="<?php echo $banner_image['description_ena']; ?>">
                                        <input
                                                id="banner-image<?php echo $image_row; ?>-description"
                                                type="checkbox"
                                                onclick="toggleDescription(this);"
                                                <?php echo $banner_image['description_ena'] ? 'checked' : ''; ?>
                                        />
                                        <label for="banner-image<?php echo $image_row; ?>-description"><?php echo $entry_description_ena; ?></label>
                                        <div class="banner_description<?php echo $banner_image['description_ena'] ? ' banner_active_field' : ''; ?>">
                                            <!--<div class="length_info"><?php /*echo $text_max_length; */?></div>-->
                                            <?php foreach ($languages as $language) { ?>
                                                <div class="description_area">
                                                    <div class="input-group pull-left"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> </span>
                                                        <textarea
                                                                name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][description]"
                                                                cols="30"
                                                                rows="5"
                                                                onkeyup="checkDescriptionLength(this, <?php echo $image_row; ?>, <?php echo $language['language_id']; ?>);"
                                                        ><?php echo $banner_image['banner_image_description'][$language['language_id']]['description']; ?></textarea>
                                                    </div>
                                                    <div class="length<?php echo $image_row . '_' . $language['language_id']; ?>"><?php echo sprintf($text_left, ''); ?></div>
                                                    <?php if (isset($error_banner_image[$image_row][$language['language_id']]['description'])) { ?>
                                                        <div class="text-danger"><?php echo $error_banner_image[$image_row][$language['language_id']]['description']; ?></div>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </td>
                                    <td class="text-left"><a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $banner_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                        <input type="hidden" name="banner_image[<?php echo $image_row; ?>][image]" value="<?php echo $banner_image['image']; ?>" id="input-image<?php echo $image_row; ?>" />
                                    </td>
                                    <td class="text-right"><input type="text" name="banner_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $banner_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                                    <td class="text-left"><button type="button" onclick="$('#image-row<?php echo $image_row; ?>, .tooltip').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                    <script>
                                        $(document).ready(function () {
                                            $('.left_symbols').each(function (index, item) {
                                                var text = $(item).closest('.description_area').find('textarea').val();
                                                $(item).text(<?php echo $max_length; ?> - text.length);
                                                if ((text.length < <?php echo $min_length; ?>) || (text.length > <?php echo $max_length; ?>)) {
                                                    $(item).addClass('error');
                                                } else {
                                                    $(item).removeClass('error');
                                                }
                                            });
                                        });
                                        function toggleFields(context) {
                                            $(context).closest('td').find('.banner_link').toggleClass('banner_active_field');
                                            $(context).closest('td').find('.banner_button').toggleClass('banner_active_field');
                                            var prev = context.previousElementSibling;
                                            prev.value = 1 - prev.value;
                                        }
                                        function toggleDescription(context) {
                                            $(context).closest('td').find('.banner_description').toggleClass('banner_active_field');
                                            var prev = context.previousElementSibling;
                                            prev.value = 1 - prev.value;
                                        }
                                        function checkDescriptionLength(context, row, lang) {
                                            var text = context.value;
                                            var symbols = $('.length'+row+'_'+lang+' .left_symbols');
                                            symbols.text(<?php echo $max_length; ?> - text.length);
                                            if ((text.length < <?php echo $min_length; ?>) || (text.length > <?php echo $max_length; ?>)) {
                                                $(context).addClass('error');
                                                symbols.addClass('error');
                                            } else {
                                                $(context).removeClass('error');
                                                symbols.removeClass('error');
                                            }
                                        }
                                    </script>
                                </tr>
                                <?php $image_row++; ?>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="4"></td>
                                <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_banner_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                            </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript"><!--
            var image_row = <?php echo $image_row; ?>;

            function addImage() {
                html  = '<tr id="image-row' + image_row + '">';
                html += '  <td class="text-left" style="width: 40%; vertical-align: top;">';
                html += '    <input type="hidden" name="banner_image[' + image_row + '][button_ena]" value="0">';
                html += '    <input id="banner-image' + image_row + '-button" type="checkbox" onclick="toggleFields(this);"/>';
                html += '    <label for="banner-image' + image_row + '-button"><?php echo $entry_button_ena; ?></label>';
                <?php foreach ($languages as $language) { ?>
                html += '    <div class="input-group pull-left">';
                html += '      <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
                html += '      <input type="text" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][title]" value="" placeholder="<?php echo $entry_title; ?>" class="form-control" />';
                html += '      <div class="banner_link banner_active_field" >';
                html += '        <input type="text" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][slide_link]" value="" placeholder="<?php echo $entry_link; ?>" class="form-control" />';
                html += '      </div>';
                html += '      <div class="banner_button">';
                html += '        <input type="text" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][button_title]" value="" placeholder="<?php echo $entry_button_title; ?>" class="form-control" />';
                html += '        <input type="text" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][button_link]" value="" placeholder="<?php echo $entry_button_link; ?>" class="form-control" />';
                html += '      </div>';
                html += '    </div>';
                <?php } ?>
                html += '  </td>';
                html += '  <td class="text-left" style="width: 30%; vertical-align: top;">';
                html += '    <input type="hidden" name="banner_image[' + image_row + '][description_ena]" value="0">';
                html += '    <input id="banner-image' + image_row + '-description" type="checkbox" onclick="toggleDescription(this);" />';
                html += '    <label for="banner-image' + image_row + '-description"><?php echo $entry_description_ena; ?></label>';
                html += '    <div class="banner_description">';
                <?php foreach ($languages as $language) { ?>
                html += '      <div class="description_area">';
                html += '        <div class="input-group pull-left"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> </span>';
                html += '          <textarea name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][description]" cols="30" rows="5" onkeyup="checkDescriptionLength(this, ' + image_row + ', <?php echo $language['language_id']; ?>);"></textarea>';
                html += '        </div>';
                html += '      <div class="length' + image_row + '_<?php echo $language['language_id']; ?>"><?php echo sprintf($text_left, $max_length); ?></div>';
                <?php } ?>
                html += '    </div>';
                html += '  </td>';
                html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="banner_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';
                html += '  <td class="text-right"><input type="text" name="banner_image[' + image_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
                html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
                html += '</tr>';

                $('#images tbody').append(html);

                image_row++;
            }
            //--></script></div>
<style>
    .banner_link, .banner_button, .banner_description {
        display: none;
    }
    .banner_link.banner_active_field, .banner_button.banner_active_field, .banner_description.banner_active_field {
        display: block;
    }
    .left_symbols.error {
        color: #cc0000;
    }
    .description_area textarea.error {
        border: 1px solid #cc0000;
    }
</style>
<?php echo $footer; ?>