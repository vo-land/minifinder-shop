<?php
// Heading
$_['heading_title']     = 'Produktjämförelse';
 
// Text
$_['text_product']      = 'Produktdetaljer';
$_['text_name']         = 'Produkt';
$_['text_image']        = 'Bild';
$_['text_price']        = 'Pris';
$_['text_model']        = 'Artikelnummer';
$_['text_manufacturer'] = 'Tillverkare';
$_['text_availability'] = 'Tillgänglighet';
$_['text_instock']      = 'I lager';
$_['text_rating']       = 'Betyg';
$_['text_reviews']      = 'Baserad på %s recensioner.';
$_['text_summary']      = 'Summering';
$_['text_weight']       = 'Vikt';
$_['text_dimension']    = 'Storlek: (L x B x H)';
$_['text_compare']      = 'Produktjämförelse (%s)';
$_['text_success']      = 'Klart: Du har lagt till <a href="%s">%s</a> till din <a href="%s">produktjämförelse</a>!';
$_['text_remove']       = 'Ta bort';
$_['text_empty']        = 'Du har inte valt några produkter att jämföra.';
?>