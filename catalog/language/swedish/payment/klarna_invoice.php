<?php
// Text
$_['text_title']                = 'Klarna Faktura';
$_['text_terms_fee']			= '<span id="klarna_invoice_toc"></span> (+%s)<script type="text/javascript">var terms = new Klarna.Terms.Invoice({el: \'klarna_invoice_toc\', eid: \'%s\', country: \'%s\', charge: %s});</script>';
$_['text_terms_no_fee']			= '<span id="klarna_invoice_toc"></span><script type="text/javascript">var terms = new Klarna.Terms.Invoice({el: \'klarna_invoice_toc\', eid: \'%s\', country: \'%s\'});</script>';
$_['text_additional']     = 'Klarna Faktura - information om dig';
$_['text_male']           = 'Herr';
$_['text_female']         = 'Dam';
$_['text_year']           = 'År';
$_['text_month']          = 'Månad';
$_['text_day']            = 'Dag';
$_['text_comment']        = "Klarna's fakturanr.: %s\n%s/%s: %.4f";

// Entry
$_['entry_gender']         = 'Kön:';
$_['entry_pno']            = 'Personnummer:<br /><span class="help">ÅÅÅÅMMDD-XXXX</span>';
$_['entry_dob']            = 'Födelsedatum:';
$_['entry_phone_no']       = 'Telefonnummer:<br /><span class="help"></span>';
$_['entry_street']         = 'Adress:<br /><span class="help">Leverans kan endast ske till registrerad adress.</span>';
$_['entry_house_no']       = 'Husnummer:<br /><span class="help">Ange ditt husnummer.</span>';
$_['entry_house_ext']      = 'Husvåning:<br /><span class="help">Ange din våningsplan</span>';
$_['entry_company']        = 'Org.nr.:<br /><span class="help">Företagets org.nr.</span>';

// Help
$_['help_pno']					= 'Ange personnummer.';
$_['help_phone_no']				= 'Ange telefonnummer.';
$_['help_street']				= 'Ange gatuadress, gatan.';
$_['help_house_no']				= 'Ange husnummer.';
$_['help_house_ext']			= 'Ange husvåning, trappa. E.g. A, B, C, Red, Blue ect.';
$_['help_company']				= 'Ange firmans organisationsnummer';

// Error
$_['error_deu_terms']     = 'Du måste acceptera Klarnas villkor';
$_['error_address_match'] = 'Faktura - och Leveransadress måste vara samma';
$_['error_network']       = 'Error medans Klarna jobbar, snälla försök igen.';
?>
