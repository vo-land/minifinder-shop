<?php
// Heading 
$_['heading_title']                  = 'Kassa';

// Text
$_['text_cart']                      = 'Varukorg';
$_['text_checkout_option']           = 'Steg 1: Alternativ';
$_['text_checkout_account']          = 'Steg 2: Personuppgifter och adress';
$_['text_checkout_payment_address']  = 'Steg 2: Personuppgifter och adress';
$_['text_checkout_shipping_address'] = 'Steg 3: Avvikande leveransadress';
$_['text_checkout_shipping_method']  = 'Steg 4: Frakt';
$_['text_checkout_payment_method']   = 'Steg 5: Betalsätt';
$_['text_checkout_confirm']          = 'Steg 6: Bekräfta beställningen';
$_['text_modify']                    = 'Ändra &raquo;';
$_['text_new_customer']              = 'Ny kund';
$_['text_returning_customer']        = 'Återkommande kund';
$_['text_checkout']                  = 'Alternativ:';
$_['text_i_am_returning_customer']   = 'Jag är kund sedan tidigare.';
$_['text_register']                  = 'Registrera konto';
$_['text_guest']                     = 'Handla som gäst';
$_['text_register_account']          = 'Genom att skapa ett konto kan du handla snabbare, se din orderstatus och få tillgång till din orderhistorik. Du kan också välja att handla som gäst utan att registrera dig.';
$_['text_forgotten']                 = 'Glömt lösenord?';
$_['text_your_details']              = 'Kontaktuppgifter';
$_['text_your_address']              = 'Adress';
$_['text_your_password']             = 'Lösenord';
$_['text_agree']                     = 'Jag har läst och godkänner <a href="%s" class="agree"><b>%s</b></a>';
$_['text_address_new']               = 'Jag vill ange en ny adress';
$_['text_address_existing']          = 'Jag vill använda en befintlig adress';
$_['text_shipping_method']           = 'Ange önskat leveranssätt.';
$_['text_payment_method']            = 'Ange hur du vill betala.';
$_['text_comments']                  = 'Meddelande till butiken';
$_['text_recurring']                 = 'Återkommande produkter';
$_['text_payment_recurring']         = 'Betalnings Profil';
$_['text_trial_description']         = '%s alla %d %s(s) för %d betalningar då';
$_['text_payment_description']       = '%s alla %d %s(s) för %d betalningar';
$_['text_payment_until_canceled_description'] = '%s alla %d %s(s) förrän avslutad';
$_['text_day']                       = 'dag';
$_['text_week']                      = 'vecka';
$_['text_semi_month']                = 'halv månad';
$_['text_month']                     = 'månad';
$_['text_year']                      = 'år';

// Column
$_['column_name']                    = 'Produktnamn';
$_['column_model']                   = 'Artikelnummer';
$_['column_quantity']                = 'Antal';
$_['column_price']                   = 'Pris';
$_['column_total']                   = 'Totalt';

// Entry
$_['entry_email_address']            = 'E-postadress:';
$_['entry_email']                    = 'E-post:';
$_['entry_password']                 = 'Lösenord:';
$_['entry_confirm']                  = 'Bekräfta lösenord:';
$_['entry_firstname']                = 'Förnamn:';
$_['entry_lastname']                 = 'Efternamn:';
$_['entry_telephone']                = 'Mobiltelefon:';
$_['entry_fax']                      = 'Fax:';
$_['entry_address']                  = 'Välj Adress';
$_['entry_company']                  = 'Företag:';
$_['entry_customer_group']           = 'Välj kundtyp:';
$_['entry_address_1']                = 'Adress:';
$_['entry_address_2']                = 'C/O:';
$_['entry_postcode']                 = 'Postnummer:';
$_['entry_city']                     = 'Stad:';
$_['entry_country']                  = 'Land:';
$_['entry_zone']                     = 'Län:';
$_['entry_newsletter']               = 'Skicka mig nyhetsbrev.';
$_['entry_shipping'] 	             = 'Min faktura- och leveransadress är samma.';

// Error
$_['error_warning']                  = 'Det uppstod ett problem i samband med hanteringen av din order! Om problemet återkommer var vänlig välj ett annat betalsätt eller kontakta oss genom att <a href="%s">klicka här</a>.';
$_['error_login']                    = 'Fel: Den angivna e-postadressen och/eller lösenordet kunde inte hittas i vårt register.';
$_['error_attempts']                 = 'Ditt konto är låst för 1 timme för du försökt logga in för många gånger.';
$_['error_approved']                 = 'Ditt konto kräver godkännande först innan du kan logga in.'; 
$_['error_exists']                   = 'Obs! Du är redan kund hos oss! Vänligen <a href="/login">logga in</a>.';
$_['error_firstname']                = 'Förnamn måste bestå av minst 1 och högst 32 tecken!';
$_['error_lastname']                 = 'Efternamn måste bestå av minst 1 och högst 32 tecken!';
$_['error_email']                    = 'E-postadressen verkar inte vara giltig!';
$_['error_telephone']                = 'Telefonnummer måste bestå av minst 6 och högst 18 tecken!';
$_['error_password']                 = 'Lösenord måste bestå av minst 3 och högst 20 tecken!';
$_['error_confirm']                  = 'Lösenordsbekräftelsen stämmer inte med angivet lösenord!';
$_['error_address_1']                = 'Adress måste bestå av minst 5 och högst 128 tecken!';
$_['error_city']                     = 'Stad måste bestå av minst 2 och högst 128 tecken!';
$_['error_postcode']                 = 'Postnr måste bestå av minst 5 och högst 10 tecken!';
$_['error_country']                  = 'Var vänlig välj ett land!';
$_['error_zone']                     = 'Var vänlig välj ett län!';
$_['error_agree']                    = 'Fel: Du måste godkänna %s!';
$_['error_address']                  = 'Fel: Du måste välja adress!';
$_['error_shipping']                 = 'Fel: Fraktsätt krävs!';
$_['error_no_shipping']              = 'Inga fraktsätt är tillgängliga. Var vänlig <a href="%s">kontakta oss</a> för hjälp!';
$_['error_payment']                  = 'Fel: Betalsätt krävs!';
$_['error_no_payment']               = 'Fel: Inga betalsätt är tillgängliga. Var vänlig <a href="%s">kontakta oss</a> för hjälp!';
$_['error_custom_field']             = 'Fältet måste fyllas i!';
?>