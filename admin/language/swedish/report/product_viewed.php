<?php
// Heading
$_['heading_title']  = 'Rapport visade produkter';

// Text
$_['text_list']      = 'Visade Produkters Lista';
$_['text_success']   = 'Klart du har ändrat i rapporten!';

// Column
$_['column_name']    = 'Produktnamn';
$_['column_model']   = 'Artikelnummer';
$_['column_viewed']  = 'Visad';
$_['column_percent'] = 'Procent';

// Error
$_['error_permission'] = 'Varning: Du har inte tillåtelse att nollställa rapporten!';
?>