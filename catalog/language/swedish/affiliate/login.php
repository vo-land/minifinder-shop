<?php
// Heading 
$_['heading_title']                 = 'Affiliateprogram';

// Text
$_['text_account']                  = 'Affiliatekonto';
$_['text_login']                    = 'Logga in';
$_['text_description']              = '<p>%s erbjuder gratis affiliateprogram som ger dig möjlighet att tjäna extra pengar. Om du blir medlem i programmet kan du få provision genom att placera en eller flera länkar på din hemsida eller blogg som marknadsför produkter från %s. Alla genomförda inköp från kunder som har klickat på dina länkar kommer att ge dig en affiliate-provision. Just nu är provisionen %s.</p>';
$_['text_new_affiliate']            = 'Ny affiliate';
$_['text_register_account']         = '<p>Jag är inte en affiliate än.</p><p>Klicka på Fortsätt här nedan för att skapa ett nytt affiliate-konto. Observera att detta konto inte på något sätt är anslutet till ditt kundkonto.</p>';
$_['text_returning_affiliate']      = 'Logga in';
$_['text_i_am_returning_affiliate'] = 'Jag är en återkommande affiliate.';
$_['text_forgotten']                = 'Glömt lösenord?';

// Entry
$_['entry_email']                   = 'E-post:';
$_['entry_password']                = 'Lösenord:';

// Error
$_['error_login']                   = 'Fel: E-postadress och/eller lösenord hittades inte. Alternativt är ditt konto ännu inte godkänt.';
$_['error_attempts']                = 'Varning: Ditt konto är stängt under 1 timme för att du försökt logga in för många gånger.';
$_['error_approved']                = 'Vänligen vänta tills vi uppdaterat ditt konto.';
?>