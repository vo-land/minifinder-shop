<?php
// Heading
$_['heading_title']         = 'Administration';

// Text
$_['text_order']           = 'Order';
$_['text_order_status']    = 'Pågår';
$_['text_complete_status'] = 'Klart';
$_['text_customer']        = 'Kunder';
$_['text_online']          = 'Kunder online';
$_['text_approval']        = 'Väntar godkännande';
$_['text_product']         = 'Produkter';
$_['text_stock']           = 'Ej i lager';
$_['text_review']          = 'Omdöme';
$_['text_return']          = 'Returer';
$_['text_affiliate']       = 'Återförsäljare';
$_['text_store']           = 'Butiker';
$_['text_front']           = 'Butik framsida';
$_['text_help']            = 'Hjälp';
$_['text_homepage']        = 'Hemsida';
$_['text_support']         = 'Support';
$_['text_documentation']   = 'Dokumentation';
$_['text_logout']          = 'Logga ut';
?>