<?php
// Heading
$_['heading_title']       = 'Kategorier'; 

// Text
$_['text_module']         = 'Moduler';
$_['text_success']        = 'Klart: Du har ändrat i Kategorier!';
$_['text_edit']           = 'Ändra i Kategorier.';

// Entry
$_['entry_status']        = 'Status:';

// Error
$_['error_permission']    = 'Varning: Du har inte behörighet att ändra i Kategorier!';
?>