<?php
$_['lang_title']                    = 'OpenBay Pro för eBay';
$_['lang_heading']                  = 'eBay Översikt';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_heading_settings']         = 'Settings';
$_['lang_heading_sync']             = 'Syncronisera';
$_['lang_heading_account']          = 'Mitt Konto';
$_['lang_heading_links']            = 'Produkt Länkar';
$_['lang_heading_item_import']      = 'Importera produkter';
$_['lang_heading_order_import']     = 'Importera orders';
$_['lang_heading_adds']             = 'Installera tillägg';
$_['lang_heading_summary']          = 'eBay resultat';
$_['lang_heading_profile']          = 'Profiler';
$_['lang_heading_template']         = 'Mallar';
$_['lang_heading_ebayacc']          = 'eBay konto';
$_['lang_heading_register']         = 'Registrera dig här';
?>
