<?php
// Text
$_['text_success']     = 'Klart: Ditt presenntkort är registrerat!';
$_['text_cart']        = 'Klart: Du har ändrat din varukorg!';

$_['text_for']         = '%s Presentkort till %s';

// Error
$_['error_permission'] = 'Varning: Du har inte tillåtelse att ändra i API!';
$_['error_voucher']    = 'Varning: Presentkortet är antingen felaktigt eller använt!';
$_['error_to_name']    = 'Mottagare måste vara mellan 1-64 tecken!';
$_['error_from_name']  = 'Ditt namn måste vara mellan 1-64 tecken.';
$_['error_email']      = 'E-Mail Adress går ej att hitta.';
$_['error_theme']      = 'Du måste ange ett tema.';
$_['error_amount']     = 'Summan måste vara mellan %s och %s!';
?>