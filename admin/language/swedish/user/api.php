<?php
// Heading
$_['heading_title']        = 'API\'s';

// Text
$_['text_success']         = 'Klart: Du har ändrat i API\'s!';
$_['text_list']            = 'API Lista';
$_['text_add']             = 'Lägg till API';
$_['text_edit']            = 'Ändra i API';

// Column
$_['column_username']      = 'Användarnamn';
$_['column_status']        = 'Status';
$_['column_date_added']    = 'Datum';
$_['column_date_modified'] = 'Datum ändrat';
$_['column_action']        = 'Ändra';

// Entry
$_['entry_username']       = 'Användarnamn';
$_['entry_password']       = 'Password';
$_['entry_status']         = 'Status';

// Error
$_['error_permission']     = 'Varning: Du har inte tillåtelse att ändra!';
$_['error_username']       = 'Användarnamn måste vara mellan 3-20 tecken!';
$_['error_password']       = 'API password måste vara mellan 3-256 tecken!';
?>