<?php
// Heading 
$_['heading_title']      = 'Dina Bonuspoäng';

// Column
$_['column_date_added']  = 'Datum';
$_['column_description'] = 'Beskrivning';
$_['column_points']      = 'Bonuspoäng';

// Text
$_['text_account']       = 'Konto';
$_['text_reward']        = 'Bonuspoäng';
$_['text_total']         = 'Din totala Bonuspoäng är:';
$_['text_empty']         = 'Du har för närvarande inga Bonuspoäng!';
?>