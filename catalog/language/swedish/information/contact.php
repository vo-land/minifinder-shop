<?php
// Heading
$_['heading_title']  = 'Kontakta oss';

// Text 
$_['text_location']  = 'Kontaktuppgifter';
$_['text_store']     = 'Våra butiker';
$_['text_contact']   = 'Kontaktformulär';
$_['text_address']   = 'Adress:';
$_['text_telephone'] = 'Telefon:';
$_['text_fax']       = 'Fax:';
$_['text_open']      = 'Butikens öppettider';
$_['text_comment']   = 'Kommentar';
$_['text_success']   = '<p>Ditt meddelande har nu skickats till oss!<br />Vi besvarar din fråga så snart vi kan.</p>';

// Entry Fields
$_['entry_name']     = 'Ditt namn:';
$_['entry_email']    = 'E-postadress:';
$_['entry_enquiry']  = 'Meddelande:';
$_['entry_captcha']  = 'Ange koden i rutan nedan:';

// Email
$_['email_subject']  = 'Meddelande: %s';

// Errors
$_['error_name']     = 'Namnet måste bestå av minst 3 och högst 32 tecken!';
$_['error_email']    = 'E-postadress verkar inte vara giltig!';
$_['error_enquiry']  = 'Förfrågan måste bestå av minst 10 och högst 3000 tecken!';
$_['error_captcha']  = 'Verifikationskoden matchar inte bilden!';
?>
