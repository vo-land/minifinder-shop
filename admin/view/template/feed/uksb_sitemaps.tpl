<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><?php if(md5($licensed)=='e9dc924f238fa6cc29465942875fe8f0'){ ?>
        <button type="submit" form="form-uksb-sitemaps" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button><?php } ?>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
    </div>
    <div class="panel-body">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
     <?php if(md5($licensed)=='e9dc924f238fa6cc29465942875fe8f0'){ ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-uksb-sitemaps" class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-12"><h2><?php echo $heading_general_settings; ?></h2></div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="free_support"><?php echo $entry_free_support; ?></label>
            <div class="col-sm-10">
              <span id="free_support"><?php echo $text_free_support_remaining; ?></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status1"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
            	<div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary<?php if ($uksb_sitemaps_status) { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_status" id="input-status1" value="1"<?php if ($uksb_sitemaps_status) { ?> checked<?php } ?>><?php echo $text_enabled; ?>
                    </label>
                    <label class="btn btn-primary<?php if (!$uksb_sitemaps_status) { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_status" id="input-status2" value="0"<?php if (!$uksb_sitemaps_status) { ?> checked<?php } ?>><?php echo $text_disabled; ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-image_sitemap1"><?php echo $entry_image_sitemap; ?></label>
            <div class="col-sm-10">
            	<div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary<?php if ($uksb_sitemaps_image) { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_image" id="input-image_sitemap1" value="1"<?php if ($uksb_sitemaps_image) { ?> checked<?php } ?>><?php echo $text_enabled; ?>
                    </label>
                    <label class="btn btn-primary<?php if (!$uksb_sitemaps_image) { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_image" id="input-image_sitemap2" value="0"<?php if (!$uksb_sitemaps_image) { ?> checked<?php } ?>><?php echo $text_disabled; ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="split"><span data-toggle="tooltip" title="<?php echo $help_split; ?>"><?php echo $entry_split; ?></span></label>
            <div class="col-sm-10">
              <select name="uksb_sitemaps_split" id="split" class="form-control">
                <option value="0"<?php if (!$uksb_sitemaps_split) { ?> selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                <option value="50"<?php if ($uksb_sitemaps_split=='50') { ?> selected="selected"<?php } ?>>50</option>
                <option value="100"<?php if ($uksb_sitemaps_split=='100') { ?> selected="selected"<?php } ?>>100</option>
                <option value="250"<?php if ($uksb_sitemaps_split=='250') { ?> selected="selected"<?php } ?>>250</option>
                <option value="500"<?php if ($uksb_sitemaps_split=='500') { ?> selected="selected"<?php } ?>>500</option>
                <option value="750"<?php if ($uksb_sitemaps_split=='750') { ?> selected="selected"<?php } ?>>750</option>
                <option value="1000"<?php if ($uksb_sitemaps_split=='1000') { ?> selected="selected"<?php } ?>>1000</option>
                <option value="1500"<?php if ($uksb_sitemaps_split=='1500') { ?> selected="selected"<?php } ?>>1500</option>
                <option value="2000"<?php if ($uksb_sitemaps_split=='2000') { ?> selected="selected"<?php } ?>>2000</option>
                <option value="2500"<?php if ($uksb_sitemaps_split=='2500') { ?> selected="selected"<?php } ?>>2500</option>
                <option value="3000"<?php if ($uksb_sitemaps_split=='3000') { ?> selected="selected"<?php } ?>>3000</option>
                <option value="3500"<?php if ($uksb_sitemaps_split=='3500') { ?> selected="selected"<?php } ?>>3500</option>
                <option value="4000"<?php if ($uksb_sitemaps_split=='4000') { ?> selected="selected"<?php } ?>>4000</option>
                <option value="4500"<?php if ($uksb_sitemaps_split=='4500') { ?> selected="selected"<?php } ?>>4500</option>
                <option value="5000"<?php if ($uksb_sitemaps_split=='5000') { ?> selected="selected"<?php } ?>>5000</option>
                <option value="6000"<?php if ($uksb_sitemaps_split=='6000') { ?> selected="selected"<?php } ?>>6000</option>
                <option value="7000"<?php if ($uksb_sitemaps_split=='7000') { ?> selected="selected"<?php } ?>>7000</option>
                <option value="8000"<?php if ($uksb_sitemaps_split=='8000') { ?> selected="selected"<?php } ?>>8000</option>
                <option value="9000"<?php if ($uksb_sitemaps_split=='9000') { ?> selected="selected"<?php } ?>>9000</option>
                <option value="10000"<?php if ($uksb_sitemaps_split=='10000') { ?> selected="selected"<?php } ?>>10000</option>
                <option value="12500"<?php if ($uksb_sitemaps_split=='12500') { ?> selected="selected"<?php } ?>>12500</option>
                <option value="15000"<?php if ($uksb_sitemaps_split=='15000') { ?> selected="selected"<?php } ?>>15000</option>
                <option value="20000"<?php if ($uksb_sitemaps_split=='20000') { ?> selected="selected"<?php } ?>>20000</option>
                <option value="25000"<?php if ($uksb_sitemaps_split=='25000') { ?> selected="selected"<?php } ?>>25000</option>
                <option value="30000"<?php if ($uksb_sitemaps_split=='30000') { ?> selected="selected"<?php } ?>>30000</option>
                <option value="40000"<?php if ($uksb_sitemaps_split=='40000') { ?> selected="selected"<?php } ?>>40000</option>
                <option value="50000"<?php if ($uksb_sitemaps_split=='50000') { ?> selected="selected"<?php } ?>>50000</option>
              </select></div>
          </div>
        <div class="form-group">
            <div class="col-sm-12"><h2><?php echo $heading_sitemap_content; ?></h2><br><?php echo $help_content; ?></div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_products; ?></label>
            <div class="col-sm-3">
            	<div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary<?php if ($uksb_sitemaps_products_on || $uksb_sitemaps_products_on=='') { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_products_on" id="input-products1" value="1"<?php if ($uksb_sitemaps_products_on || $uksb_sitemaps_products_on=='') { ?> checked<?php } ?>><?php echo $text_enabled; ?>
                    </label>
                    <label class="btn btn-primary<?php if ($uksb_sitemaps_products_on=='0') { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_products_on" id="input-products2" value="0"<?php if ($uksb_sitemaps_products_on=='0') { ?> checked<?php } ?>><?php echo $text_disabled; ?>
                    </label>
                </div>
            </div>
              <label class="col-sm-2 control-label" for="uksb_sitemaps_products_fr"><?php echo $entry_frequency; ?></label>
            <div class="col-sm-2 text-right">
              <select name="uksb_sitemaps_products_fr" id="uksb_sitemaps_products_fr" class="form-control">
                <option value="always"<?php if ($uksb_sitemaps_products_fr=='always') { ?> selected="selected"<?php } ?>><?php echo $text_always; ?></option>
                <option value="hourly"<?php if ($uksb_sitemaps_products_fr=='hourly') { ?> selected="selected"<?php } ?>><?php echo $text_hourly; ?></option>
                <option value="daily"<?php if ($uksb_sitemaps_products_fr=='daily' || !$uksb_sitemaps_products_fr) { ?> selected="selected"<?php } ?>><?php echo $text_daily; ?></option>
                <option value="weekly"<?php if ($uksb_sitemaps_products_fr=='weekly') { ?> selected="selected"<?php } ?>><?php echo $text_weekly; ?></option>
                <option value="monthly"<?php if ($uksb_sitemaps_products_fr=='monthly') { ?> selected="selected"<?php } ?>><?php echo $text_monthly; ?></option>
                <option value="yearly"<?php if ($uksb_sitemaps_products_fr=='yearly') { ?> selected="selected"<?php } ?>><?php echo $text_yearly; ?></option>
              </select></div>
              <label class="col-sm-1 control-label" for="uksb_sitemaps_products_pr"><?php echo $entry_priority; ?></label>
            <div class="col-sm-2 text-right">
              <select name="uksb_sitemaps_products_pr" id="uksb_sitemaps_products_pr" class="form-control">
                <option value="1.0"<?php if ($uksb_sitemaps_products_pr=='1.0' || !$uksb_sitemaps_products_pr) { ?> selected="selected"<?php } ?>>1.0</option>
                <option value="0.9"<?php if ($uksb_sitemaps_products_pr=='0.9') { ?> selected="selected"<?php } ?>>0.9</option>
                <option value="0.8"<?php if ($uksb_sitemaps_products_pr=='0.8') { ?> selected="selected"<?php } ?>>0.8</option>
                <option value="0.7"<?php if ($uksb_sitemaps_products_pr=='0.7') { ?> selected="selected"<?php } ?>>0.7</option>
                <option value="0.6"<?php if ($uksb_sitemaps_products_pr=='0.6') { ?> selected="selected"<?php } ?>>0.6</option>
                <option value="0.5"<?php if ($uksb_sitemaps_products_pr=='0.5') { ?> selected="selected"<?php } ?>>0.5</option>
                <option value="0.4"<?php if ($uksb_sitemaps_products_pr=='0.4') { ?> selected="selected"<?php } ?>>0.4</option>
                <option value="0.3"<?php if ($uksb_sitemaps_products_pr=='0.3') { ?> selected="selected"<?php } ?>>0.3</option>
                <option value="0.2"<?php if ($uksb_sitemaps_products_pr=='0.2') { ?> selected="selected"<?php } ?>>0.2</option>
                <option value="0.1"<?php if ($uksb_sitemaps_products_pr=='0.1') { ?> selected="selected"<?php } ?>>0.1</option>
                <option value="0.0"<?php if ($uksb_sitemaps_products_pr=='0.0') { ?> selected="selected"<?php } ?>>0.0</option>
              </select></div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_categories; ?></label>
            <div class="col-sm-3">
            	<div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary<?php if ($uksb_sitemaps_categories_on || $uksb_sitemaps_categories_on=='') { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_categories_on" id="input-categories1" value="1"<?php if ($uksb_sitemaps_categories_on || $uksb_sitemaps_categories_on=='') { ?> checked<?php } ?>><?php echo $text_enabled; ?>
                    </label>
                    <label class="btn btn-primary<?php if ($uksb_sitemaps_categories_on=='0') { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_categories_on" id="input-categories2" value="0"<?php if ($uksb_sitemaps_categories_on=='0') { ?> checked<?php } ?>><?php echo $text_disabled; ?>
                    </label>
                </div>
            </div>
              <label class="col-sm-2 control-label" for="uksb_sitemaps_categories_fr"><?php echo $entry_frequency; ?></label>
            <div class="col-sm-2 text-right">
              <select name="uksb_sitemaps_categories_fr" id="uksb_sitemaps_categories_fr" class="form-control">
                <option value="always"<?php if ($uksb_sitemaps_categories_fr=='always') { ?> selected="selected"<?php } ?>><?php echo $text_always; ?></option>
                <option value="hourly"<?php if ($uksb_sitemaps_categories_fr=='hourly') { ?> selected="selected"<?php } ?>><?php echo $text_hourly; ?></option>
                <option value="daily"<?php if ($uksb_sitemaps_categories_fr=='daily') { ?> selected="selected"<?php } ?>><?php echo $text_daily; ?></option>
                <option value="weekly"<?php if ($uksb_sitemaps_categories_fr=='weekly' || !$uksb_sitemaps_categories_fr) { ?> selected="selected"<?php } ?>><?php echo $text_weekly; ?></option>
                <option value="monthly"<?php if ($uksb_sitemaps_categories_fr=='monthly') { ?> selected="selected"<?php } ?>><?php echo $text_monthly; ?></option>
                <option value="yearly"<?php if ($uksb_sitemaps_categories_fr=='yearly') { ?> selected="selected"<?php } ?>><?php echo $text_yearly; ?></option>
              </select></div>
              <label class="col-sm-1 control-label" for="uksb_sitemaps_categories_pr"><?php echo $entry_priority; ?></label>
            <div class="col-sm-2 text-right">
              <select name="uksb_sitemaps_categories_pr" id="uksb_sitemaps_categories_pr" class="form-control">
                <option value="1.0"<?php if ($uksb_sitemaps_categories_pr=='1.0') { ?> selected="selected"<?php } ?>>1.0</option>
                <option value="0.9"<?php if ($uksb_sitemaps_categories_pr=='0.9' || !$uksb_sitemaps_categories_pr) { ?> selected="selected"<?php } ?>>0.9</option>
                <option value="0.8"<?php if ($uksb_sitemaps_categories_pr=='0.8') { ?> selected="selected"<?php } ?>>0.8</option>
                <option value="0.7"<?php if ($uksb_sitemaps_categories_pr=='0.7') { ?> selected="selected"<?php } ?>>0.7</option>
                <option value="0.6"<?php if ($uksb_sitemaps_categories_pr=='0.6') { ?> selected="selected"<?php } ?>>0.6</option>
                <option value="0.5"<?php if ($uksb_sitemaps_categories_pr=='0.5') { ?> selected="selected"<?php } ?>>0.5</option>
                <option value="0.4"<?php if ($uksb_sitemaps_categories_pr=='0.4') { ?> selected="selected"<?php } ?>>0.4</option>
                <option value="0.3"<?php if ($uksb_sitemaps_categories_pr=='0.3') { ?> selected="selected"<?php } ?>>0.3</option>
                <option value="0.2"<?php if ($uksb_sitemaps_categories_pr=='0.2') { ?> selected="selected"<?php } ?>>0.2</option>
                <option value="0.1"<?php if ($uksb_sitemaps_categories_pr=='0.1') { ?> selected="selected"<?php } ?>>0.1</option>
                <option value="0.0"<?php if ($uksb_sitemaps_categories_pr=='0.0') { ?> selected="selected"<?php } ?>>0.0</option>
              </select></div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_manufacturers; ?></label>
            <div class="col-sm-3">
            	<div class="btn-group" data-toggle="buttons">
                    <?php if($manufacturers_total > 0){ ?><label class="btn btn-primary<?php if (($uksb_sitemaps_manufacturers_on || $uksb_sitemaps_manufacturers_on=='') && $manufacturers_total > 0) { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_manufacturers_on" id="input-manufacturers1" value="1"<?php if (($uksb_sitemaps_manufacturers_on || $uksb_sitemaps_manufacturers_on=='') && $manufacturers_total > 0) { ?> checked<?php } ?>><?php echo $text_enabled; ?>
                    </label><?php } ?>
                    <label class="btn btn-primary<?php if ($uksb_sitemaps_manufacturers_on=='0' || $manufacturers_total == '0') { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_manufacturers_on" id="input-manufacturers2" value="0"<?php if ($uksb_sitemaps_manufacturers_on=='0' || $manufacturers_total == '0') { ?> checked<?php } ?>><?php echo $text_disabled; ?>
                    </label>
                </div>
            </div>
              <label class="col-sm-2 control-label" for="uksb_sitemaps_manufacturers_fr"><?php echo $entry_frequency; ?></label>
            <div class="col-sm-2 text-right">
              <select name="uksb_sitemaps_manufacturers_fr" id="uksb_sitemaps_manufacturers_fr" class="form-control">
                <option value="always"<?php if ($uksb_sitemaps_manufacturers_fr=='always') { ?> selected="selected"<?php } ?>><?php echo $text_always; ?></option>
                <option value="hourly"<?php if ($uksb_sitemaps_manufacturers_fr=='hourly') { ?> selected="selected"<?php } ?>><?php echo $text_hourly; ?></option>
                <option value="daily"<?php if ($uksb_sitemaps_manufacturers_fr=='daily') { ?> selected="selected"<?php } ?>><?php echo $text_daily; ?></option>
                <option value="weekly"<?php if ($uksb_sitemaps_manufacturers_fr=='weekly' || !$uksb_sitemaps_manufacturers_fr) { ?> selected="selected"<?php } ?>><?php echo $text_weekly; ?></option>
                <option value="monthly"<?php if ($uksb_sitemaps_manufacturers_fr=='monthly') { ?> selected="selected"<?php } ?>><?php echo $text_monthly; ?></option>
                <option value="yearly"<?php if ($uksb_sitemaps_manufacturers_fr=='yearly') { ?> selected="selected"<?php } ?>><?php echo $text_yearly; ?></option>
              </select></div>
              <label class="col-sm-1 control-label" for="uksb_sitemaps_manufacturers_pr"><?php echo $entry_priority; ?></label>
            <div class="col-sm-2 text-right">
              <select name="uksb_sitemaps_manufacturers_pr" id="uksb_sitemaps_manufacturers_pr" class="form-control">
                <option value="1.0"<?php if ($uksb_sitemaps_manufacturers_pr=='1.0') { ?> selected="selected"<?php } ?>>1.0</option>
                <option value="0.9"<?php if ($uksb_sitemaps_manufacturers_pr=='0.9') { ?> selected="selected"<?php } ?>>0.9</option>
                <option value="0.8"<?php if ($uksb_sitemaps_manufacturers_pr=='0.8' || !$uksb_sitemaps_manufacturers_pr) { ?> selected="selected"<?php } ?>>0.8</option>
                <option value="0.7"<?php if ($uksb_sitemaps_manufacturers_pr=='0.7') { ?> selected="selected"<?php } ?>>0.7</option>
                <option value="0.6"<?php if ($uksb_sitemaps_manufacturers_pr=='0.6') { ?> selected="selected"<?php } ?>>0.6</option>
                <option value="0.5"<?php if ($uksb_sitemaps_manufacturers_pr=='0.5') { ?> selected="selected"<?php } ?>>0.5</option>
                <option value="0.4"<?php if ($uksb_sitemaps_manufacturers_pr=='0.4') { ?> selected="selected"<?php } ?>>0.4</option>
                <option value="0.3"<?php if ($uksb_sitemaps_manufacturers_pr=='0.3') { ?> selected="selected"<?php } ?>>0.3</option>
                <option value="0.2"<?php if ($uksb_sitemaps_manufacturers_pr=='0.2') { ?> selected="selected"<?php } ?>>0.2</option>
                <option value="0.1"<?php if ($uksb_sitemaps_manufacturers_pr=='0.1') { ?> selected="selected"<?php } ?>>0.1</option>
                <option value="0.0"<?php if ($uksb_sitemaps_manufacturers_pr=='0.0') { ?> selected="selected"<?php } ?>>0.0</option>
              </select></div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_pages; ?></label>
            <div class="col-sm-3">
            	<div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary<?php if ($uksb_sitemaps_pages_on || $uksb_sitemaps_pages_on=='') { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_pages_on" id="input-pages1" value="1"<?php if ($uksb_sitemaps_pages_on || $uksb_sitemaps_pages_on=='') { ?> checked<?php } ?>><?php echo $text_enabled; ?>
                    </label>
                    <label class="btn btn-primary<?php if ($uksb_sitemaps_pages_on=='0') { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_pages_on" id="input-pages2" value="0"<?php if ($uksb_sitemaps_pages_on=='0') { ?> checked<?php } ?>><?php echo $text_disabled; ?>
                    </label>
                </div>
            </div>
              <label class="col-sm-2 control-label text-right" for="uksb_sitemaps_pages_fr"><?php echo $entry_frequency; ?></label>
            <div class="col-sm-2 text-right">
              <select name="uksb_sitemaps_pages_fr" id="uksb_sitemaps_pages_fr" class="form-control">
                <option value="always"<?php if ($uksb_sitemaps_pages_fr=='always') { ?> selected="selected"<?php } ?>><?php echo $text_always; ?></option>
                <option value="hourly"<?php if ($uksb_sitemaps_pages_fr=='hourly') { ?> selected="selected"<?php } ?>><?php echo $text_hourly; ?></option>
                <option value="daily"<?php if ($uksb_sitemaps_pages_fr=='daily') { ?> selected="selected"<?php } ?>><?php echo $text_daily; ?></option>
                <option value="weekly"<?php if ($uksb_sitemaps_pages_fr=='weekly' || !$uksb_sitemaps_pages_fr) { ?> selected="selected"<?php } ?>><?php echo $text_weekly; ?></option>
                <option value="monthly"<?php if ($uksb_sitemaps_pages_fr=='monthly') { ?> selected="selected"<?php } ?>><?php echo $text_monthly; ?></option>
                <option value="yearly"<?php if ($uksb_sitemaps_pages_fr=='yearly') { ?> selected="selected"<?php } ?>><?php echo $text_yearly; ?></option>
              </select></div>
              <label class="col-sm-1 control-label text-right" for="uksb_sitemaps_pages_pr"><?php echo $entry_priority; ?></label>
            <div class="col-sm-2 text-right">
              <select name="uksb_sitemaps_pages_pr" id="uksb_sitemaps_pages_pr" class="form-control">
                <option value="1.0"<?php if ($uksb_sitemaps_pages_pr=='1.0') { ?> selected="selected"<?php } ?>>1.0</option>
                <option value="0.9"<?php if ($uksb_sitemaps_pages_pr=='0.9') { ?> selected="selected"<?php } ?>>0.9</option>
                <option value="0.8"<?php if ($uksb_sitemaps_pages_pr=='0.8' || !$uksb_sitemaps_pages_pr) { ?> selected="selected"<?php } ?>>0.8</option>
                <option value="0.7"<?php if ($uksb_sitemaps_pages_pr=='0.7') { ?> selected="selected"<?php } ?>>0.7</option>
                <option value="0.6"<?php if ($uksb_sitemaps_pages_pr=='0.6') { ?> selected="selected"<?php } ?>>0.6</option>
                <option value="0.5"<?php if ($uksb_sitemaps_pages_pr=='0.5') { ?> selected="selected"<?php } ?>>0.5</option>
                <option value="0.4"<?php if ($uksb_sitemaps_pages_pr=='0.4') { ?> selected="selected"<?php } ?>>0.4</option>
                <option value="0.3"<?php if ($uksb_sitemaps_pages_pr=='0.3') { ?> selected="selected"<?php } ?>>0.3</option>
                <option value="0.2"<?php if ($uksb_sitemaps_pages_pr=='0.2') { ?> selected="selected"<?php } ?>>0.2</option>
                <option value="0.1"<?php if ($uksb_sitemaps_pages_pr=='0.1') { ?> selected="selected"<?php } ?>>0.1</option>
                <option value="0.0"<?php if ($uksb_sitemaps_pages_pr=='0.0') { ?> selected="selected"<?php } ?>>0.0</option>
              </select></div>
        </div>
        
        
        
        <div class="form-group">
            <label class="col-sm-2 control-label" for="uksb_sitemaps_pages_omit_a"><?php echo $entry_pages_omit; ?></label>
            <div class="col-sm-10">
                <div class="btn-group" data-toggle="buttons" style="margin-bottom:2px;">
                  <label class="btn btnx btn-success<?php if($uksb_sitemaps_pages_omit_a!=''){ ?> active<?php } ?>">
                    <input type="checkbox" id="uksb_sitemaps_pages_omit_a" name="uksb_sitemaps_pages_omit_a" value="1"<?php if($uksb_sitemaps_pages_omit_a!=''){ ?> checked="checked"<?php } ?> /> <?php echo $text_pg_home; ?>
                  </label>
                </div> 
                <div class="btn-group" data-toggle="buttons" style="margin-bottom:2px;">
                  <label class="btn btnx btn-success<?php if($uksb_sitemaps_pages_omit_b!=''){ ?> active<?php } ?>">
                    <input type="checkbox" id="uksb_sitemaps_pages_omit_b" name="uksb_sitemaps_pages_omit_b" value="1"<?php if($uksb_sitemaps_pages_omit_b!=''){ ?> checked="checked"<?php } ?> /> <?php echo $text_pg_specials; ?>
                  </label>
                </div> 
            <?php foreach($informations as $information){  ?>
                <div class="btn-group" data-toggle="buttons" style="margin-bottom:2px;">
                  <label class="btn btnx btn-success<?php if(${'uksb_sitemaps_pages_omit_'.$information['information_id']}!=''){ ?> active<?php } ?>">
                    <input type="checkbox" id="uksb_sitemaps_pages_omit_<?php echo $information['information_id']; ?>" name="uksb_sitemaps_pages_omit_<?php echo $information['information_id']; ?>" value="1"<?php if(${'uksb_sitemaps_pages_omit_'.$information['information_id']}!=''){ ?> checked="checked"<?php } ?> /> <?php echo $information['title']; ?>
                  </label>
                </div> 
            <?php } ?>
            </div>       
        </div> 
        <div class="form-group">
            <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_extras; ?>"><?php echo $entry_extras; ?></span></label>
            <div class="col-sm-3">
                <div class="btn-group" data-toggle="buttons">
                    <?php if($extras_total > 0){ ?><label class="btn btn-primary<?php if (($uksb_sitemaps_extras_on || $uksb_sitemaps_extras_on=='') && $extras_total > 0) { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_extras_on" id="input-extras1" value="1"<?php if (($uksb_sitemaps_extras_on || $uksb_sitemaps_extras_on=='') && $extras_total > 0) { ?> checked<?php } ?>><?php echo $text_enabled; ?>
                    </label><?php } ?>
                    <label class="btn btn-primary<?php if ($uksb_sitemaps_extras_on=='0' || $extras_total == '0') { ?> active<?php } ?>">
                        <input type="radio" name="uksb_sitemaps_extras_on" id="input-extras2" value="0"<?php if ($uksb_sitemaps_extras_on=='0') { ?> checked<?php } ?>><?php echo $text_disabled; ?>
                    </label>
                </div>
            </div>
            <label class="col-sm-2 control-label text-right" for="uksb_sitemaps_extras_fr"><?php echo $entry_frequency; ?></label>
            <div class="col-sm-2 text-right">
              <select name="uksb_sitemaps_extras_fr" id="uksb_sitemaps_extras_fr" class="form-control">
                <option value="always"<?php if ($uksb_sitemaps_extras_fr=='always') { ?> selected="selected"<?php } ?>><?php echo $text_always; ?></option>
                <option value="hourly"<?php if ($uksb_sitemaps_extras_fr=='hourly') { ?> selected="selected"<?php } ?>><?php echo $text_hourly; ?></option>
                <option value="daily"<?php if ($uksb_sitemaps_extras_fr=='daily') { ?> selected="selected"<?php } ?>><?php echo $text_daily; ?></option>
                <option value="weekly"<?php if ($uksb_sitemaps_extras_fr=='weekly' || !$uksb_sitemaps_extras_fr) { ?> selected="selected"<?php } ?>><?php echo $text_weekly; ?></option>
                <option value="monthly"<?php if ($uksb_sitemaps_extras_fr=='monthly') { ?> selected="selected"<?php } ?>><?php echo $text_monthly; ?></option>
                <option value="yearly"<?php if ($uksb_sitemaps_extras_fr=='yearly') { ?> selected="selected"<?php } ?>><?php echo $text_yearly; ?></option>
              </select></div>
            <label class="col-sm-1 control-label text-right" for="uksb_sitemaps_extras_pr"><?php echo $entry_priority; ?></label>
            <div class="col-sm-2 text-right">
              <select name="uksb_sitemaps_extras_pr" id="uksb_sitemaps_extras_pr" class="form-control">
                <option value="1.0"<?php if ($uksb_sitemaps_extras_pr=='1.0') { ?> selected="selected"<?php } ?>>1.0</option>
                <option value="0.9"<?php if ($uksb_sitemaps_extras_pr=='0.9') { ?> selected="selected"<?php } ?>>0.9</option>
                <option value="0.8"<?php if ($uksb_sitemaps_extras_pr=='0.8' || !$uksb_sitemaps_extras_pr) { ?> selected="selected"<?php } ?>>0.8</option>
                <option value="0.7"<?php if ($uksb_sitemaps_extras_pr=='0.7') { ?> selected="selected"<?php } ?>>0.7</option>
                <option value="0.6"<?php if ($uksb_sitemaps_extras_pr=='0.6') { ?> selected="selected"<?php } ?>>0.6</option>
                <option value="0.5"<?php if ($uksb_sitemaps_extras_pr=='0.5') { ?> selected="selected"<?php } ?>>0.5</option>
                <option value="0.4"<?php if ($uksb_sitemaps_extras_pr=='0.4') { ?> selected="selected"<?php } ?>>0.4</option>
                <option value="0.3"<?php if ($uksb_sitemaps_extras_pr=='0.3') { ?> selected="selected"<?php } ?>>0.3</option>
                <option value="0.2"<?php if ($uksb_sitemaps_extras_pr=='0.2') { ?> selected="selected"<?php } ?>>0.2</option>
                <option value="0.1"<?php if ($uksb_sitemaps_extras_pr=='0.1') { ?> selected="selected"<?php } ?>>0.1</option>
                <option value="0.0"<?php if ($uksb_sitemaps_extras_pr=='0.0') { ?> selected="selected"<?php } ?>>0.0</option>
              </select></div>
        </div>
        <?php if($extras_total > 0){ ?>
        <div class="form-group">
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
            <ul style="list-style:none;">
            <?php foreach ($extra_pages as $extra_page) { ?>
                <li><a href="<?php echo HTTP_CATALOG . $extra_page; ?>" target="_blank"><i class="fa fa-eye"></i> <?php echo $extra_page; ?></a></li>
            <?php } ?>
            </ul>
            </div>
        </div><?php } ?>

        <div class="form-group">
            <div class="col-sm-12"><h2><?php echo $heading_sitemap_urls; ?></h2><br><?php echo $help_urls; ?></div>
        </div>
        
        
          <?php
          $feeds = explode("^", $data_feed);
          $feed_urls = explode("^", $data_feed_url);
          $i = 0;
          foreach (array_keys($feeds) as $key) {
          ?>
          <div class="form-group">
          <label class="col-sm-2 control-label" for="feed1_url_<?php echo $i; ?>"><?php echo $entry_data_feed1; ?></label>
          <div class="col-sm-10"><strong><?php echo $feed_urls[$key]; ?></strong> <a href="<?php echo $feed_urls[$key] . $feeds[$key]; ?>" target="_blank"><i class="fa fa-eye"></i></a><br><textarea id="feed1_url_<?php echo $i; ?>" rows="2" class="form-control" readonly onClick="$(this).select();"><?php echo $feeds[$key]; ?></textarea></div>
          </div>
          <?php
          $i++;
          } ?>
          <?php
          $feeds2 = explode("^", $data_feed2);
          $i = 0;
          foreach (array_keys($feeds2) as $key) {
          ?>
          <div class="form-group">
          <label class="col-sm-2 control-label" for="feed2_url_<?php echo $i; ?>"><?php echo $entry_data_feed2; ?></label>
          <div class="col-sm-10"><strong><?php echo $feed_urls[$key]; ?></strong> <a href="<?php echo $feed_urls[$key] . $feeds2[$key]; ?>" target="_blank"><i class="fa fa-eye"></i></a><br><textarea id="feed2_url_<?php echo $i; ?>" rows="2" class="form-control" readonly onClick="$(this).select();"><?php echo $feeds2[$key]; ?></textarea></div>
          </div>
          <?php
          $i++;
          } ?>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_info; ?></label>
            <div class="col-sm-10"><?php echo $help_info; ?></div>
          </div>
      </form>
      
    <?php } ?>
    <?php if($licensed=='none'){ ?>
    <?php echo $license_purchase_thanks; ?>
    <?php if(isset($regerror)){ echo $regerror_quote_msg; } ?>
    <?php if(isset($regerror)){ ?><p style="color:red;">error msg: <?php echo $regerror; ?></p><?php } ?>
      <h2><?php echo $license_registration; ?></h2>
    <form name="reg" method="post" action="<?php echo $home; ?>register.php" id="reg" class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="opencart_email"><?php echo $license_opencart_email; ?></label>
            <div class="col-sm-10">
          	  <input name="opencart_email" type="text" autofocus required id="opencart_email" form="reg" class="form-control"></div>
          </div>
	<?php if(isset($emailmal)&&$regerror=='emailmal'){ ?><p style="color:red;"><?php echo $check_email; ?></p><?php } ?>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="order_id"><?php echo $license_opencart_orderid; ?></label>
            <div class="col-sm-10">
          	  <input name="order_id" type="text" autofocus required id="order_id" form="reg" class="form-control"></div>
          </div>
	<?php if(isset($regerror)&&$regerror=='orderid'){ ?><p style="color:red;"><?php echo $check_orderid; ?></p><?php } ?>
        <div class="form-group">
            <div class="col-sm-12">
          	  <button type="submit" form="reg" data-toggle="tooltip" title="<?php echo $license_registration; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button><input name="extension_id" type="hidden" id="extension_id" form="reg" value="2881"></div>
          </div>
    </form>
    <?php } ?>
    <?php if($licensed=='curl'){ ?>
    <?php echo $server_error_curl; ?>
    <?php } ?>
     
    </div>
  </div>
  </div>
<?php echo $footer; ?>
<script type="text/javascript">
<!--
$(document).ready(function(){ 
  $(".btn.active").removeClass("btn-primary").addClass("btn-success");
  $(".btnx.active").removeClass("btn-success").addClass("btn-danger");
  $(document).on("click", function(){
    $(".btn").removeClass("btn-success").addClass("btn-primary");
    $(".btnx").removeClass("btn-danger").addClass("btn-success");
    $(".btn.active").removeClass("btn-primary").addClass("btn-success");
    $(".btnx.active").removeClass("btn-success").addClass("btn-danger");
  });
});
//-->
</script>