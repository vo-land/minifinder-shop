<?php
// Heading
$_['heading_title']     = 'Tillverkare';

// Text
$_['text_brand']        = 'Märke';
$_['text_index']        = 'Tillverkarregister:';
$_['text_error']        = 'Tillverkaren hittades inte!';
$_['text_empty']        = 'Det finns inga tillverkare att visa.';
$_['text_quantity']     = 'Antal:';
$_['text_manufacturer'] = 'Tillverkare:';
$_['text_model']        = 'Produktkod:'; 
$_['text_points']       = 'Bonuspoäng:'; 
$_['text_price']        = 'Pris:'; 
$_['text_tax']          = 'Exkl moms:'; 
$_['text_compare']      = 'Produktjämförelse (%s)'; 
$_['text_sort']         = 'Sortera efter:';
$_['text_default']      = 'Standard';
$_['text_name_asc']     = 'Namn (A - Ö)';
$_['text_name_desc']    = 'Namn (Ö - A)';
$_['text_price_asc']    = 'Pris (Lågt &gt; Högt)';
$_['text_price_desc']   = 'Pris (Högt &gt; Lågt)';
$_['text_rating_asc']   = 'Betyg (Lägsta)';
$_['text_rating_desc']  = 'Betyg (Högsta)';
$_['text_model_asc']    = 'Artikelnummer(A - Ö, 1-99999)';
$_['text_model_desc']   = 'Artikelnummer (Ö - A, 99999-1)';
$_['text_limit']        = 'Visa:';
?>