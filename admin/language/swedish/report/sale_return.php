<?php
// Heading
$_['heading_title']     = 'Rapport returer';

// Text
$_['text_list']         = 'Retur Lista';
$_['text_year']         = 'År';
$_['text_month']        = 'Månader';
$_['text_week']         = 'Veckor';
$_['text_day']          = 'Dagar';
$_['text_all_status']   = 'Alla statusar';

// Column
$_['column_date_start'] = 'Stardatum';
$_['column_date_end']   = 'Slutdatum';
$_['column_returns']    = 'Antal returer';

// Entry
$_['entry_date_start']  = 'Startdatum:';
$_['entry_date_end']    = 'Slutdatum:';
$_['entry_group']       = 'Gruppera efter:';
$_['entry_status']      = 'Returstatus:';
?>