<?php
// Heading
$_['heading_title']      = 'Postförskott';

// Text
$_['text_payment']       = 'Betalningar';
$_['text_success']       = 'Klart: Du har ändrat i Postförskott!';
$_['text_edit']          = 'Ändra i Postförskott modulen';

// Entry
$_['entry_total']        = 'Totalt:';
$_['entry_order_status'] = 'Orderstatus:';
$_['entry_geo_zone']     = 'Geozon:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sorteringsordning:';

// Help
$_['help_total']		= 'Totalsumman måste uppnå viss ordersumma för att detta alternativ är giltigt.';

// Error
$_['error_permission']   = 'Varning: Du har inte behörighet att ändra!';
?>