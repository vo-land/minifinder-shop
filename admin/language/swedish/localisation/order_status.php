<?php
// Heading
$_['heading_title']    = 'Orderstatus';

// Text
$_['text_success']     = 'Klart: Du har ändrat i Orderstatus!';
$_['text_list']        = 'Layout Lista';
$_['text_add']         = 'Lägg till Orderstatus';
$_['text_edit']        = 'Ändra i Orderstatus';

// Column
$_['column_name']      = 'Orderstatus';
$_['column_action']    = 'Ändra';

// Entry
$_['entry_name']       = 'Orderstatus:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i Orderstatus!';
$_['error_name']       = 'Namnet måste innehålla mellan 3 och 32 tecken!';
$_['error_default']    = 'Varning: Denna orderstatus kan inte raderas eftersom den är förvald!';
$_['error_download']   = 'Varning: Denna orderstatus kan inte raderas eftersom den är butikens förvalda nerladdningsstatus!';
$_['error_store']      = 'Varning: Denna orderstatus kan inte raderas eftersom den är kopplad till %s butiker!';
$_['error_order']      = 'Varning: Denna orderstatus kan inte raderas eftersom den är kopplad till %s ordrar!';
?>