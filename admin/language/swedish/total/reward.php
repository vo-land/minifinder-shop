<?php
// Heading
$_['heading_title']    = 'Bonuspoäng';

// Text
$_['text_total']       = 'Ordertotal';
$_['text_success']     = 'Klart: Du har ändrat!';
$_['text_edit']        = 'Änddra i Bonuspoäng';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sorteringsordning:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra!';
?>