<?php
// Heading
$_['heading_title']      = 'Banköverföring';

// Text 
$_['text_payment']       = 'Betalningar';
$_['text_success']       = 'Klart: Du har ändrat i modulen Banköverföring!';
$_['text_edit']          = 'Ändra i Banköverföring';

// Entry
$_['entry_bank']         = 'Instruktioner för banköverföring:';
$_['entry_total']        = 'Totalt:';
$_['entry_order_status'] = 'Orderstatus:';
$_['entry_geo_zone']     = 'Geozon:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sorteringsordning:';

// Help
$_['help_total']	    = 'Totalsumman måste uppnå viss ordersumma för att detta alternativ är giltigt.';

// Error
$_['error_permission']   = 'Varning: Du har inte behörighet att ändra i Banköverföring!';
$_['error_bank']         = 'Instruktioner för Banköverföring krävs!';
?>