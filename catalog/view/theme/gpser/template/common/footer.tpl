<footer>
  <div class="container" style="padding-bottom:20px;">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
          <li><a href="//www.tracktor.se/">Tracktor login</a></li>
          <li><a href="https://minifinder.se/">Minifinder</a></li>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
        <div style="height:10px;"></div>
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <!--li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li-->
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
        <br />
	      <div>
	      	<a href="//www.pricerunner.se/kopskydd/registrera?merchant=53964" target="_blank"><img src="/image/catalog/www/pricerunner_kopskydd.png" alt="Pricerunner Köpskydd" title="Pricerunner Köpskydd" class="socs_logos" style="opacity: 0.9;"/></a>
	      	&nbsp;&nbsp;<a href="https://www.ehandelscertifiering.se/rapport.php?url=www.gpser.se" target="_blank"><img src="/image/catalog/www/certifierad_ehandel.png" alt="Certifierad E-handel" title="Certifierad E-handel" class="socs_logos" style="opacity: 0.9;"/></a>
	      </div>
	      <br />
      </div>
      <div class="col-sm-3">
      	<a href="https://www.facebook.com/gpser"><img src="/image/catalog/www/footer_facebook.png" alt="Facebook Logotype" title="Facebook" class="soc_logos" /></a>
      	<a href="https://twitter.com/gpserab"><img src="/image/catalog/www/footer_twitter.png" alt="Twitter Logotype" title="Twitter" class="soc_logos" /></a>
      	<a href="https://www.youtube.com/c/GpserSeAB"><img src="/image/catalog/www/footer_youtube.png" alt="Youtube Logotype" title="Youtube" class="soc_logos" /></a>
      	<a href="https://plus.google.com/+GpserSeAB/"><img src="/image/catalog/www/footer_googleplus.png" alt="Google+ Logotype" title="Google+" class="soc_logos" /></a>
      	<img src="/image/catalog/www/footer_pinterest.png" alt="Pinterest Logotype" title="Pinterest" class="soc_logos" />
      	<br /><br /><br />
      	<h5>Vi accepterar</h5>
      	<img src="/image/catalog/www/footer_payment_logos.png" alt="Payment Logotype" title="Betalningsalternativen som vi accepterar" class="soc_logos img-responsive" style="opacity: 0.7;"/>
      </div>
    </div>
  </div>
  <div class="container2">
  	<div class="container">
  		<a href="//www.gpser.se/"><img src="/image/catalog/www/footer_gpser_logo.png" alt="GPSER Logotype" title="Gpser" class="logo" /></a>
  		<a href="https://minifinder.se/" target="_blank"><img src="/image/catalog/www/footer_minifinder_logo.png" alt="MiniFinder Logotype" title="MiniFinder" class="logo" /></a>
  		<a href="//www.tracktor.se/" target="_blank"><img src="/image/catalog/www/footer_tracktor_logo.png" alt="Tracktor Logotype" title="Tracktor" class="logo" /></a>
  	</div>
  	
  </div>
  <div class="container">
  	<div id="powered" style="padding:20px 0;">
    	<?php echo $powered; ?>
    </div>
  </div>
</footer>

<a id="toTop" href="#" class="hidden-xs hidden-sm"><img src="/image/catalog/www/totop.png" alt="To-the-top" /></a>
<script type="text/javascript">
$(document).ready(function()
{
// Hide the toTop button when the page loads.
$("#toTop").css("display", "none");

// This function runs every time the user scrolls the page.
$(window).scroll(function(){

	// Check weather the user has scrolled down (if "scrollTop()"" is more than 0)
	if($(window).scrollTop() > 0){

		// If it's more than or equal to 0, show the toTop button. console.log("is more");
		$("#toTop").fadeIn("slow");

	} else {

		// If it's less than 0 (at the top), hide the toTop button. console.log("is less");
		$("#toTop").fadeOut("slow");
	}
});

// When the user clicks the toTop button, we want the page to scroll to the top.
$("#toTop").click(function(){

	// Disable the default behaviour when a user clicks an empty anchor link.
	// (The page jumps to the top instead of // animating)
	event.preventDefault();

	// Animate the scrolling motion.
	$("html, body").animate({
	scrollTop:0
	},"slow");
});


});
</script>

<!-- Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- / Facebook -->

<!-- Start of Zopim Live Chat Script --> 
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?31uloCDKZwm6nU0kyiljzynPGNHcTiCy";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->


</body></html>