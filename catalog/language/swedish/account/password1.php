<?php
// Heading 
$_['heading_title']  = 'Ändra lösenord';

// Text
$_['text_account']   = 'Konto';
$_['text_password']  = 'Lösenord';
$_['text_success']   = 'Klart: Ditt lösenord har uppdaterats.';

// Entry
$_['entry_password'] = 'Lösenord:';
$_['entry_confirm']  = 'Bekräfta lösenord:';

// Error
$_['error_password'] = 'Lösenordet måste bestå av minst 4 och högst 20 tecken!';
$_['error_confirm']  = 'Lösenordet och bekräftelsen stämmer inte överens!';

//button
$_['button_new_save'] = 'Behålla';
?>