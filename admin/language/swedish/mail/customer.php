<?php
// Text
$_['text_approve_subject']      = '%s - Ditt konto är nu aktiverat!';
$_['text_approve_welcome']      = 'Välkommen och tack för att du registrerade dig hos %s!';
$_['text_approve_login']        = 'Ditt konto är nu skapat och du kan logga in med din epostadress och lösenord på vår hemsida som du hittar på denna URL:';
$_['text_approve_services']     = 'Efter att du loggat in får du åtkomst till fler tjänster som tex tidigare ordrar, skriva ut kvitton/fakturor och ändra dina kontouppgifter.';
$_['text_approve_thanks']       = 'Tack,';
$_['text_transaction_subject']  = '%s - Poäng';
$_['text_transaction_received'] = 'Du har mottagit %s poäng!';
$_['text_transaction_total']    = 'Din totala poäng är nu %s.' . "\n\n" . 'Dina lojalitetspoäng kommer att dras ifrån automatiskt vid ditt nästa inköp.';
$_['text_reward_subject']       = '%s - Bonuspoäng';
$_['text_reward_received']      = 'Du har emottagit %s Bonuspoäng!';
$_['text_reward_total']         = 'Dina totala poäng är nu %s.';
?>