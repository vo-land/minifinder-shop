<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php $last_breadcrumb = array_pop($breadcrumbs); ?>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
    <li><?php echo $last_breadcrumb['text']; ?></li>
  </ul>
  <div class="row contact_us"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <h1><?php echo $heading_title; ?></h1>
  </div>
</div>
<!-- Goolge Maps - Our location -->
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<!--<script>
    function initialize() {
        var mapCanvas = document.getElementById('map');
        var marker;
        var mapOptions = {
            center: new google.maps.LatLng(56.878375, 14.780221),
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.HYBRID
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);

        // Map Marker
        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: {lat: 56.878460, lng: 14.780201},
        });
        marker.addListener('click', toggleBounce);
        // EOF Map Marker
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>-->
<!--<div id="map" style="width:100%;height:200px;margin-bottom:30px;"></div>-->
<div class="wi-contacts-main">
    <div class="container">
        <div class="wi-contacts-pulse">
            <div class="wi-contacts-pulse-1"></div>
            <div class="wi-contacts-pulse-2"></div>
            <div class="wi-contacts-pulse-3"></div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row contact_us">
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      <!--h3><?php echo $text_location; ?></h3 -->
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <!--<?php if ($image) { ?>
            <div class="col-sm-3"><img src="<?php echo $image; ?>" alt="<?php echo $store; ?>" title="<?php echo $store; ?>" class="img-thumbnail" /></div>
            <?php } ?>-->
            <div class="pull-left contact_info" itemscope itemtype="http://schema.org/Organization">
              <div class="pull-left cont_left_info">
                  <strong itemprop="name"><?php echo $store; ?></strong>
                  <address itemprop="address">
                      <?php echo $address; ?>
                  </address>
                  <strong><?php echo $text_tax; ?></strong><br />
                  VAT: SE556909569701<br />
                  BG: 122-0102<br />
                  BG (2): 5003-6722<br />
                  <?php /*if ($geocode) { ?>
                  <strong>We are here</strong><br />
                  <a href="https://maps.google.com/maps?q=<?php echo urlencode($geocode); ?>&hl=en&t=m&z=15" target="_blank"><img src="/image/catalog/www/gpser-honnoersgatan-6.jpg" alt="GPSER Sweden AB location image" /></a>
                  <?php } */?>
                  <!--p>Lat: 56.878375, Long: 14.780221</p-->
               </div>
              <div class="pull-right cont_right_info">
                  <strong><?php echo $text_telephone; ?></strong><br>
                  <div class="tel" itemprop="telephone"><?php echo $telephone; ?></div>
                  <?php if ($fax) { ?>
                  <strong><?php echo $text_fax; ?></strong><br>
                  <div class="tel" itemprop="faxNumber"><?php echo $fax; ?></div>
                  <?php } ?>
                  <strong><?php echo $text_hours; ?></strong><br />
                  <?php echo $text_hours_work; ?><br />
                  <?php echo $text_hours_lunch; ?><br />
                  <?php echo $text_hours_weekend; ?><br /><br />

                  <?php if ($open) { ?>
                  <strong><?php echo $text_open; ?></strong><br />
                  <?php echo $open; ?><br />
                  <br />
                  <?php } ?>
                  <?php if ($comment) { ?>
                  <strong><?php echo $text_comment; ?></strong><br />
                  <?php echo $comment; ?>
                  <?php } ?>

                  <span id='liveadmin'></span>
            </div>
            <div class="mail_us">
                <strong><?php echo $text_email; ?></strong><br />
                <table>
                    <tr>
                        <td><?php echo $text_email_enquiries; ?></td> <td><a href="mailto:info@minifinder.se" itemprop="email">info@minifinder.se</a></td>
                    </tr>
                    <tr>
                        <td><?php echo $text_email_support; ?></td> <td><a href="mailto:support@minifinder.se" itemprop="email">support@minifinder.se</a></td>
                    </tr>
                    <tr>
                        <td><?php echo $text_email_orders; ?></td> <td><a href="mailto:order@minifinder.se" itemprop="email">order@minifinder.se</a></td>
                    </tr>
                    <tr>
                        <td><?php echo $text_email_economy; ?></td> <td><a href="mailto:ekonomi@minifinder.se" itemprop="email">ekonomi@minifinder.se</a></td>
                    </tr>
                    <tr>
                        <td><?php echo $text_email_market; ?></td> <td><a href="mailto:market@minifinder.se" itemprop="email">market@minifinder.se</a></td>
                    </tr>
                </table>
            </div>
            </div>
            <div class="pull-right contact_form">
              <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
		        <fieldset>
		          <h2><?php echo $text_contact; ?></h2>
		          <div class="form-group required">
		            <div class="row_form">
		            <!--<label class="coll-sm-3 control-label" for="input-name"><?php echo $entry_name; ?></label>-->
		              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
		              <?php if ($error_name) { ?>
		              <div class="text-danger"><?php echo $error_name; ?></div>
		              <?php } ?>
		            </div>
		          </div>
		          <div class="form-group required">
		            <div class="row_form">
	                 <!--<label class="coll-sm-3 control-label" for="input-email"><?php echo $entry_email; ?></label>-->
		              <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
		              <?php if ($error_email) { ?>
		              <div class="text-danger"><?php echo $error_email; ?></div>
		              <?php } ?>
		            </div>
		          </div>
		          <div class="form-group required">
		            <div class="row_form">
		              <!--<label class="coll-sm-3 control-label" for="input-enquiry"><?php echo $entry_enquiry; ?></label>-->
		              <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control" placeholder="<?php echo $entry_enquiry; ?>"><?php echo $enquiry; ?></textarea>
		              <?php if ($error_enquiry) { ?>
		              <div class="text-danger"><?php echo $error_enquiry; ?></div>
		              <?php } ?>
		            </div>
		          </div>
		          <?php if ($site_key) { ?>
		            <div class="form-group">
		              <div class="row_form">
		                <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
		                <?php if ($error_captcha) { ?>
		                  <div class="text-danger"><?php echo $error_captcha; ?></div>
		                <?php } ?>
		              </div>
				        <div class="buttons">
				          <div class="pull-left">
                              <div class="red_form_btn">
                                  <input type="submit" value="<?php echo $button_submit; ?>" />
                              </div>
				          </div>
				        </div>
		            </div>
		          <?php } else { ?>
		          
		          <div class="form-group">
				        <div class="buttons">
				          <div class="pull-left">
                              <div class="red_form_btn">
                                  <input type="submit" value="<?php echo $button_submit; ?>" />
                              </div>
				          </div>
				        </div>
		            </div>
		          
		          <?php } ?>
		          
		        </fieldset>
		      </form>
            </div>
          </div>
        </div>
      </div>
      
      <?php if ($locations) { ?>
      <h3><?php echo $text_store; ?></h3>
      <div class="panel-group" id="accordion">
        <?php foreach ($locations as $location) { ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><a href="#collapse-location<?php echo $location['location_id']; ?>" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><?php echo $location['name']; ?> <i class="fa fa-caret-down"></i></a></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-location<?php echo $location['location_id']; ?>">
            <div class="panel-body">
              <div class="row">
                <?php if ($location['image']) { ?>
                <div class="col-sm-3"><img src="<?php echo $location['image']; ?>" alt="<?php echo $location['name']; ?>" title="<?php echo $location['name']; ?>" class="img-thumbnail" /></div>
                <?php } ?>
                <div class="col-sm-3"><strong><?php echo $location['name']; ?></strong><br />
                  <address>
                  <?php echo $location['address']; ?>
                  </address>
                  <?php if ($location['geocode']) { ?>
                  <a href="https://maps.google.com/maps?q=<?php echo urlencode($location['geocode']); ?>&hl=en&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
                  <?php } ?>
                </div>
                <div class="col-sm-3"> <strong><?php echo $text_telephone; ?></strong><br>
                  <?php echo $location['telephone']; ?><br />
                  <br />
                  <?php if ($location['fax']) { ?>
                  <strong><?php echo $text_fax; ?></strong><br>
                  <?php echo $location['fax']; ?>
                  <?php } ?>
                </div>
                <div class="col-sm-3">
                  <?php if ($location['open']) { ?>
                  <strong><?php echo $text_open; ?></strong><br />
                  <?php echo $location['open']; ?><br />
                  <br />
                  <?php } ?>
                  <?php if ($location['comment']) { ?>
                  <strong><?php echo $text_comment; ?></strong><br />
                  <?php echo $location['comment']; ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
