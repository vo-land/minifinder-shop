<?php
// Heading
$_['heading_title']    = 'Lagerstatus';

// Text
$_['text_success']     = 'Klart: Du har ändrat i Lagerstatus!';
$_['text_list']        = 'Layout Lista';
$_['text_add']         = 'Lägg till Lagerstatus';
$_['text_edit']        = 'Ändra i Lagerstatus';

// Column
$_['column_name']      = 'Lagerstatus Namn';
$_['column_action']    = 'Ändra';

// Entry
$_['entry_name']       = 'Lagerstatus Namn:';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i Lagerstatus!';
$_['error_name']       = 'Namnet måste innehålla mellan 3 och 32 tecken!';
$_['error_product']    = 'Varning: Denna kan inte raderas eftersom den är kopplad till %s produkter!';
?>
