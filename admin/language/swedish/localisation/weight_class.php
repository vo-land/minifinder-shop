<?php
// Heading
$_['heading_title']    = 'Vikter';

// Text
$_['text_success']     = 'Klart: Du har ändrat i Vikter!';
$_['text_list']        = 'Layout Lista';
$_['text_add']         = 'Lägg till Vikter';
$_['text_edit']        = 'Ändra i Vikter';

// Column
$_['column_title']     = 'Namn';
$_['column_unit']      = 'Viktenhet';
$_['column_value']     = 'Värde';
$_['column_action']    = 'Ändra';

// Entry
$_['entry_title']      = 'Namn:';
$_['entry_unit']       = 'Viktenhet:';
$_['entry_value']      = 'Värde:';

// Help
$_['help_value']       = 'Sätt 1.00000 om det är din standard vikt.';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra i Vikter!';
$_['error_title']      = 'Namnet måste innehålla mellan 3 och 32 tecken!';
$_['error_unit']       = 'Enhet måste innehålla mellan 1 och 4 tecken!';
$_['error_default']    = 'Varning: Denna kan inte raderas eftersom den är butikens förvalda!';
$_['error_product']    = 'Varning: Denna kan inte raderas eftersom den är kopplad till %s produkter!';
?>