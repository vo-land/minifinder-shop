<?php
// Text
$_['text_home']            = 'Hem';
$_['text_wishlist']        = 'Önskelista (%s)';
$_['text_shopping_cart']   = 'Varukorg';
$_['text_category']        = 'Kategorier';
$_['text_reseller']        = 'Återförsäljare';
$_['text_account']         = 'Mitt konto';
$_['text_register']      = 'Registrera';
$_['text_login']         = 'Logga in';
$_['text_order']         = 'Orderhistorik';
$_['text_transaction']   = 'Transaktioner';
$_['text_download']      = 'Nedladdningar';
$_['text_logout']        = 'Logga ut';
$_['text_checkout']      = 'Kassa';
$_['text_search']        = 'Sök';
$_['text_all']           = 'Se alla';
$_['text_contact']		= 'Kontakta oss'; // custom
$_['text_terms']		= 'Köpvillkor'; // custom
$_['text_help']			= 'Hjälp'; // custom
$_['text_faq']			= 'FAQ'; // custom
$_['text_tax']			= 'Priser inkl. moms'; // custom
?>