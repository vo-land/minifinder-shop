<?php
// Heading
$_['heading_title']       = 'Bästsäljare';

// Text
$_['text_module']         = 'Moduler';
$_['text_success']        = 'Klart: Du har ändrat i Bästsäljare!';
$_['text_edit']           = 'Ändra i Bästsäljare';

// Entry
$_['entry_name']       = 'Modul Namn';
$_['entry_limit']         = 'Antal:';
$_['entry_image']         = 'Bild (B x H):';
$_['entry_width']         = 'Bredd';
$_['entry_height']        = 'Höjd';
$_['entry_status']        = 'Status:';

// Error
$_['error_permission']    = 'Varning: Du har inte behörighet att ändra i Bästsäljare!';
$_['error_name']       = 'Modul Namn måste vara mellan 3-64 tecken.!';
$_['error_width']      = 'Bredd krävs!';
$_['error_height']     = 'Höjd krävs!';
?>