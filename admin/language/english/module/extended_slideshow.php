<?php
// Heading
$_['heading_title']    = 'Extended Slideshow';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified extended slideshow module!';
$_['text_edit']        = 'Edit Extended Slideshow Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_banner']     = 'Banner';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify extended slideshow module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';