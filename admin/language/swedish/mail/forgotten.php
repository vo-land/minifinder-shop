<?php
// Text
$_['text_subject']  = '%s - Återställning av lösenord';
$_['text_greeting'] = 'Ett nytt lösenord begärdes för %s administration.';
$_['text_change']   = 'För att återställa ditt lösenord, klicka på länken nedan:';
$_['text_ip']       = 'IP-adressen som begärde ett nytt lösenord är: %s';
?>