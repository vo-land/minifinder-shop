<?php
// Heading
$_['heading_title']     = 'Information';

// Text
$_['text_success']      = 'Klart: Du har ändrat i "Information"!';
$_['text_list']         = 'Alternativ grupplista';
$_['text_add']               = 'Lägg till Information';
$_['text_edit']              = 'Ändra i Information';
$_['text_default']      = 'Förvalt';

// Column
$_['column_title']      = 'Rubrik';
$_['column_sort_order']	= 'Sorteringsordning';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_title']            = 'Rubrik:';
$_['entry_description']      = 'Beskrivning:';
$_['entry_store']            = 'Butiker:';
$_['entry_meta_title'] 	     = 'Meta Tag Namn';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Beskrivning';
$_['entry_keyword']          = 'SEO Keyword:';
$_['entry_bottom']           = 'Bottom: Visas i footern.';
$_['entry_status']           = 'Status:';
$_['entry_sort_order']       = 'Sorteringsordning:';
$_['entry_layout']           = 'Layout Override:';

// Help
$_['help_keyword']           = 'Avnänd inte mellanslag. Använd istället - tecken mellan orden.';
$_['help_bottom']            = 'Visas i nedre menyn sk.footer.';

// Error 
$_['error_warning']     = 'Varning: Kontrollera felmeddelanden!';
$_['error_permission']  = 'Varning: Du har inte behörighet att ändra i "Information"!';
$_['error_title']       = 'Rubriken måste innehålla mellan 3 och 64 tecken!';
$_['error_description'] = 'Beskrivningen måste innehålla minst 3 tecken!';
$_['error_meta_title']  = 'Meta Tag Namn måste vara mer än 3 tecken och max 255.';
$_['error_keyword']     = 'SEO keyword används redan!';
$_['error_account']     = 'Varning: Den här informaitonssidan kan inte raderas eftersom den är kopplad till butikens kontovillkor!';
$_['error_checkout']    = 'Varning: Den här informaitonssidan kan inte raderas eftersom den är kopplad till butikens köpvillkor!';
$_['error_affiliate']   = 'Varning: Den här informaitonssidan kan inte raderas eftersom den är kopplad till butikens affiliatevillkor!';
$_['error_return']      = 'Varning: Den här informaitonssidan kan inte raderas eftersom den är kopplad till butikens återbetalning sida!';
$_['error_store']       = 'Varning: Den här informaitonssidan kan inte raderas eftersom den är kopplad till %s stores!';
?>