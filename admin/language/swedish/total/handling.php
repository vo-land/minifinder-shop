<?php
// Heading
$_['heading_title']    = 'Expeditionsavgift';

// Text
$_['text_total']       = 'Ordertotal';
$_['text_success']     = 'Klart: Du har ändrat!';
$_['text_edit']        = 'Ändra i Expeditionsavgift';

// Entry
$_['entry_total']      = 'Summa:';
$_['entry_fee']        = 'Avgift:';
$_['entry_tax_class']  = 'Moms:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sorteringsordning:';

// Help
$_['help_total']       = 'Totalsumman måste uppnås innan det är giltigt.';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra!';
?>