<?php
// Heading
$_['heading_title']        = 'Valuta';  

// Text
$_['text_success']         = 'Klart: Du har ändrat i Valuta!';
$_['text_list']            = 'Layout Lista';
$_['text_add']             = 'Lägg till Valuta';
$_['text_edit']            = 'Ändra i Valuta';

// Column
$_['column_title']         = 'Valutanamn';
$_['column_code']          = 'Kod'; 
$_['column_value']         = 'Värde';
$_['column_date_modified'] = 'Senast uppdaterad';
$_['column_action']        = 'Ändra';

// Entry
$_['entry_title']          = 'Valutanamn:';
$_['entry_code']           = 'Kod:';
$_['entry_value']          = 'Värde:';
$_['entry_symbol_left']    = 'Symbol till vänster:';
$_['entry_symbol_right']   = 'Symbol till höger:';
$_['entry_decimal_place']  = 'Decimalplacering:';
$_['entry_status']         = 'Status:';

// Help
$_['help_code']            = 'Ändra inte detta om det är din standard valuta. Måste vara <a href="http://www.xe.com/iso4217.php" target="_blank">ISO code</a>.';
$_['help_value']           = 'Sätt 1.00000 om det är din standard valuta.';

// Error
$_['error_permission']     = 'Varning: Du har inte behörighet att ändra i Valuta!';
$_['error_title']          = 'Valutanamnet måste innehålla mellan 3 och 32 tecken!';
$_['error_code']           = 'Valutakoden måste innehålla 3 tecken!';
$_['error_default']        = 'Varning: Denna valtua kan inte raderas eftersom den är kopplad till butikens förvalda valuta!';
$_['error_store']          = 'Varning: Denna valtua kan inte raderas eftersom den är kopplad till %s butiker!';
$_['error_order']          = 'Varning: Denna valtua kan inte raderas eftersom den är kopplad till %s ordrar!';
?>