<?php if (!$carousel) { ?>
    <?php if ($gtitle) { ?><h2 class="shheading"
                               style="<?php echo ($sh_h1_font_size) ? 'font-size:' . $sh_h1_font_size . '; ' : ''; ?><?php echo ($sh_h1_color) ? 'color:' . $sh_h1_color . '; ' : ''; ?><?php echo ($sh_h1_border_color) ? 'border-bottom-color:' . $sh_h1_border_color . '; ' : ''; ?><?php echo ($sh_h1_bg_color) ? 'background:' . $sh_h1_bg_color . '; ' : ''; ?><?php echo ($sh_h1_fontl) ? "font-family:'" . $sh_h1_fontl . "'; " : ""; ?>"><?php echo $title; ?></h2><?php } ?>
    <?php $isfirst = 0;

// Custom added to hide it in mobile devices
// Only when two columns
    if ($nocol == 2) {
        $mob_class = 'hidden-xs hidden-sm hidden-md';
    } else {
        $mob_class = '';
    }
    ?>

    <div class="shrow shrow<?php echo $nocol; ?> <?php echo $mob_class; ?> <?php echo ($nocol > 1) ? 'multiple_rows' : 'single_row'; ?>"
         id="shinstance<?php echo $module; ?>">
        <?php foreach ($columns as $column) { ?>
            <div <?php if (!$isfirst) { ?>class="firstone shcolumn"<?php $isfirst++;
                                                                   } else { ?>class="shcolumn"<?php } ?>>
            <?php if ($ctitle && $column['name']) { ?><?php echo $column['url'] ? '<a href="' . $column['url'] . '">' : ''; ?><h3
                    class="shname"><?php echo $column['name'] ?></h3><?php echo $column['url'] ? '</a>' : ''; ?>
            <?php } ?>
            <?php if ($column['image']) { ?>
                <div class="shimage">
                    <?php echo $column['url'] ? '<a href="' . $column['url'] . '">' : ''; ?><img
                            src="<?php echo $column['image'] ?>"
                            alt="image<?php echo $column['name']; ?>"/><?php echo $column['url'] ? '</a>' : ''; ?>
                </div>
            <?php } ?>
            <?php if ($column['youtube']) { ?>
                <div class="shyoutube">
                    <iframe src="<?php echo str_replace("watch?v=", "embed/", $column['youtube']); ?>" frameborder="0"
                            width="560" height="315"></iframe>
                </div>
            <?php } ?>
            <?php if ($column['text']) { ?>
                <div class="shtext"><?php echo $column['text'] ?></div>
            <?php } ?>
            </div><?php } ?>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            function resizesh() {
                var min_item_w = <?php echo $min_width; ?> +20;
                var items_per_row = <?php echo $nocol; ?>;
                var shrow_width = $('#shinstance<?php echo $module; ?>').width();
                var item_w = (shrow_width / Math.floor(items_per_row));
                if (item_w < min_item_w) {
                    $('#shinstance<?php echo $module; ?>').removeClass('shrow' + Math.floor(items_per_row));
                    items_per_row = shrow_width / min_item_w;
                    $('#shinstance<?php echo $module; ?>').addClass('shrow' + Math.floor(items_per_row));
                }
            }

            resizesh();
            var waitforresize;
            $(window).resize(function () {
                for (i = 0; i < 6; i++) {
                    $('#shinstance<?php echo $module; ?>').removeClass('shrow' + i);
                }
                $('#shinstance<?php echo $module; ?>').addClass('shrow<?php echo $nocol; ?>');
                clearTimeout(waitforresize);
                waitforresize = setTimeout(resizesh, 500);
            });
        });
    </script>
<?php } else { ?>
    <?php if ($gtitle) { ?><h2 class="shheading"
                               style="<?php echo ($sh_h1_font_size) ? 'font-size:' . $sh_h1_font_size . '; ' : ''; ?><?php echo ($sh_h1_color) ? 'color:' . $sh_h1_color . '; ' : ''; ?><?php echo ($sh_h1_border_color) ? 'border-bottom-color:' . $sh_h1_border_color . '; ' : ''; ?><?php echo ($sh_h1_bg_color) ? 'background:' . $sh_h1_bg_color . '; ' : ''; ?><?php echo ($sh_h1_fontl) ? "font-family:'" . $sh_h1_fontl . "'; " : ""; ?>"><?php echo $title; ?></h2><?php } ?>
    <?php $isfirst = 0; ?>
    <div class="owl-carousel owl-sh itm<?php echo $nocol; ?>" id="shinstance<?php echo $module; ?>" style="">
        <?php foreach ($columns as $column) { ?>
            <div <?php if (!$isfirst) { ?>id="firstone"<?php $isfirst++;
        } ?>>
            <?php if ($ctitle && $column['name']) { ?><?php echo $column['url'] ? '<a href="' . $column['url'] . '">' : ''; ?>
                <h3 class="shname"
                    style="<?php echo ($sh_h2_fontl) ? "font-family:'" . $sh_h2_fontl . "'; " : ""; ?><?php echo ($sh_h2_font_size) ? 'font-size:' . $sh_h2_font_size . '; ' : ''; ?><?php echo ($sh_h2_color) ? 'color:' . $sh_h2_color . '; ' : ''; ?>"><?php echo $column['name'] ?></h3><?php echo $column['url'] ? '</a>' : ''; ?><?php } ?>
            <?php if ($column['image']) { ?>
                <div class="shimage">
                    <?php echo $column['url'] ? '<a href="' . $column['url'] . '">' : ''; ?><img
                            src="<?php echo $column['image'] ?>"
                            alt="<?php echo $column['name']; ?>"/><?php echo $column['url'] ? '</a>' : ''; ?>
                </div>
            <?php } ?>
            <?php if ($column['youtube']) { ?>
                <div class="shyoutube">
                    <iframe src="<?php echo str_replace("watch?v=", "embed/", $column['youtube']); ?>" frameborder="0"
                            width="560" height="315"></iframe>
                </div>
            <?php } ?>
            <?php if ($column['text']) { ?>
                <div class="shtext"
                     style="<?php echo ($content_h2_fontl) ? "font-family:'" . $content_h2_fontl . "'; " : ""; ?><?php echo ($sh_content_font_size) ? 'font-size:' . $sh_content_font_size . '; ' : ''; ?><?php echo ($sh_content_color) ? 'color:' . $sh_content_color . '; ' : ''; ?>"><?php echo $column['text'] ?></div>
            <?php } ?>
            </div><?php } ?>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#shinstance<?php echo $module; ?>").owlCarousel({
                navigation: true,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                items: <?php echo $car_itm_desk; ?>,
                itemsDesktop: [1199,<?php echo $car_itm_desk; ?>],
                itemsDesktopSmall: [1000,<?php echo $car_itm_tab; ?>],
                itemsTablet: [700,<?php echo $car_itm_mob; ?>],
                itemsTabletSmall: [700,<?php echo $car_itm_mob; ?>],
                itemsMobile: false,
                autoPlay: <?php echo ($carousel_autoplay) ? $car_speed : 'false'; ?>,
                stopOnHover: true
            });
        });
    </script>
<?php } ?>
<?php if ($sh_h1_font || $sh_h2_font || $content_h2_font) { ?>
    <script type="text/javascript">
        if (typeof fontisadded === 'undefined') {
            WebFontConfig = {
                google: {
                    families: [ <?php echo ($sh_h1_font) ? "'" . $sh_h1_font . "'" : "";
                        echo ($sh_h2_font && $sh_h2_font != $sh_h1_font) ? ($sh_h1_font ? "," : "") . "'" . $sh_h2_font . "'" : "";
                        echo ($content_h2_font && $content_h2_font != $sh_h1_font && $content_h2_font != $sh_h2_font) ? (($sh_h1_font || $sh_h2_font) ? "," : "") . "'" . $content_h2_font . "'" : "";
                        ?> ]
                }
            };
            (function () {
                var wf = document.createElement('script');
                wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                    '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
                wf.type = 'text/javascript';
                wf.async = 'true';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(wf, s);
            })();
            fontisadded = true;
        }
    </script>
<?php } ?>