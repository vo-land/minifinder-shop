<?php
// Heading
$_['heading_title']     = 'Specifikationsgrupper';

// Text
$_['text_success']      = 'Klart: Du har ändrat i modulen Specifikationsgrupper!';
$_['text_list']         = 'Alternativ grupplista';
$_['text_add']          = 'Lägg till Specifikationsgrupper';
$_['text_edit']         = 'Ändra i Specifikationsgrupper';

// Column
$_['column_name']       = 'Namn';
$_['column_sort_order'] = 'Sorteringsordning';
$_['column_action']     = 'Ändra';

// Entry
$_['entry_name']        = 'Namn:';
$_['entry_sort_order']  = 'Sorteringsordning:';

// Error
$_['error_permission']  = 'Varning: Du har inte behörighet att ändra!';
$_['error_name']        = 'Namnet måste innehålla mellan 3 och 64 tecken!';
$_['error_attribute']   = 'Varning: Denna grupp kan inte raderas eftersom den är kopplad till %s specifikationer!';
$_['error_product']     = 'Varning: Denna grupp kan inte raderas eftersom den är kopplad till %s produkter!';
?>