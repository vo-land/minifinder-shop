<?php
// Heading
$_['heading_title']          = 'Produkter'; 

// Text  
$_['text_success']           = 'Klart: Du har ändrat i Produkter!';
$_['text_list']              = 'Alternativ grupplista';
$_['text_add']               = 'Lägg till Produkt';
$_['text_edit']              = 'Ändra i Produkt';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Förvalt';
$_['text_option']            = 'Alternativ';
$_['text_option_value']      = 'Alternativvärde';
$_['text_percent']           = 'Procent';
$_['text_amount']            = 'Fast summa';

// Column
$_['column_name']            = 'Produktnamn';
$_['column_model']           = 'Artikelnummer';
$_['column_image']           = 'Bild';
$_['column_price']           = 'Pris';
$_['column_quantity']        = 'Antal';
$_['column_status']          = 'Status';
$_['column_action']          = 'Ändra';

// Entry
$_['entry_name']             = 'Produktnamn:';
$_['entry_description']      = 'Beskrivning:';
$_['entry_meta_title'] 	     = 'Meta Tag namn';
$_['entry_meta_keyword'] 	 = 'Meta Tag-sökord:';
$_['entry_meta_description'] = 'Meta Tag-beskrivning:';
$_['entry_keyword']          = 'SEO-url-ord:';
$_['entry_model']            = 'Artikelnummer:';
$_['entry_sku']              = 'Stock Keeping Unit:';
$_['entry_upc']              = 'Universal Product Code:';
$_['entry_ean']              = 'European Article Number:';
$_['entry_jan']              = 'Japanese Article Number:';
$_['entry_isbn']             = 'International Standard Book Number:';
$_['entry_mpn']              = 'Manufacturer Part Number:';
$_['entry_location']         = 'Lagerplats:';
$_['entry_manufacturer']     = 'Tillverkare:';
$_['entry_store']            = 'Butiker';
$_['entry_shipping']         = 'Kräver frakt:'; 
$_['entry_date_available']   = 'Tillgänglig (datum):';
$_['entry_quantity']         = 'Antal:';
$_['entry_minimum']          = 'Minimum antal:';
$_['entry_stock_status']     = 'Ej i lager:';
$_['entry_price']            = 'Pris:';
$_['entry_tax_class']        = 'Momsklass:';
$_['entry_points']           = 'Bonuspoäng:';
$_['entry_option_points']    = 'Poäng:';
$_['entry_subtract']         = 'Dra ifrån lager:';
$_['entry_weight_class']     = 'Viktklass:';
$_['entry_weight']           = 'Vikt:';
$_['entry_dimension']        = 'Dimensioner (L x B x H):';
$_['entry_length_class']     = 'Längdklass';
$_['entry_length']           = 'Längd';
$_['entry_width']            = 'Bredd';
$_['entry_height']           = 'Höjd';
$_['entry_image']            = 'Bild:';
$_['entry_customer_group']   = 'Kundgrupp:';
$_['entry_date_start']       = 'Startdatum:';
$_['entry_date_end']         = 'Slutdatum:';
$_['entry_priority']         = 'Prioriteringsordning:';
$_['entry_attribute']        = 'Specifikation:';
$_['entry_attribute_group']  = 'Specifikationsgrupp:';
$_['entry_text']             = 'Text:';
$_['entry_option']           = 'Alternativ:';
$_['entry_option_value']     = 'Alternativvärde:';
$_['entry_required']         = 'Krävs:';
$_['entry_status']           = 'Status:';
$_['entry_sort_order']       = 'Sorteringsordning:';
$_['entry_category']         = 'Kategorier:';
$_['entry_filter']           = 'Filters:';
$_['entry_download']         = 'Nerladdningar:';
$_['entry_related']          = 'Relaterade produkter:';
$_['entry_tag']          	 = 'Produkt-tags:';
$_['entry_reward']           = 'Lojalitetspoäng produkten ger vid köp:';
$_['entry_layout']           = 'Annan layout än standardlayout:';
$_['entry_recurring']        = 'Återbetalnings Profil';

// Help
$_['help_keyword']           = 'Använd inte mellanslag. Använd istället - tecken.';
$_['help_sku']               = 'Stock Keeping Unit';
$_['help_upc']               = 'Universal Product Code';
$_['help_ean']               = 'European Article Number';
$_['help_jan']               = 'Japanese Article Number';
$_['help_isbn']              = 'International Standard Book Number';
$_['help_mpn']               = 'Manufacturer Part Number';
$_['help_manufacturer']      = '(Autocomplete)';
$_['help_minimum']           = 'Kräv minimum order antal';
$_['help_stock_status']      = 'Visas när produkten inte är i lager.';
$_['help_points']            = 'Antal poäng som krävs för att köpa produkten.';
$_['help_category']          = '(Autocomplete)';
$_['help_filter']            = '(Autocomplete)';
$_['help_download']          = '(Autocomplete)';
$_['help_related']           = '(Autocomplete)';
$_['help_tag']          	 = 'Använd kommatecken för att dela ord';

// Error
$_['error_warning']          = 'Varning: Kontrollera markerade uppgifter och justera dessa!';
$_['error_permission']       = 'Varning: Du har inte behörighet att ändra i Produkter!';
$_['error_name']             = 'Produktnamnet måste innheålla mer än 3 och mindre än 255 tecken!';
$_['error_meta_title']       = 'Meta Tag namn måste vara mer än 3 tecken men max 255.!';
$_['error_model']            = 'Produktens artikelnummer måste innehålla mer än 3 och mindre än 64 tecken!';
$_['error_keyword']          = 'SEO keyword används redan!';

// Custom added
$_['entry_name2']            = 'Produktnamn2:'; // Custom Added
$_['entry_price2']           = 'Rek. Pris:'; // Custom Added
$_['entry_comment_private']  = 'Kommentar (intern):'; // Custom Added

?>