<?php
// Heading
$_['heading_title']         = 'Rapport lojalitetspoäng';

// Text
$_['text_list']             = 'Kunders Lojalitetspoäng Lista';

// Column
$_['column_customer']       = 'Kundnamn';
$_['column_email']          = 'E-post';
$_['column_customer_group'] = 'Kundgrupp';
$_['column_status']         = 'Status';
$_['column_points']         = 'Lojalitetspoäng';
$_['column_orders']         = 'Antal ordrar';
$_['column_total']          = 'Summa';
$_['column_action']         = 'Ändra';

// Entry
$_['entry_date_start']      = 'Startdatum:';
$_['entry_date_end']        = 'Slutdatum:';
?>