<?php
// Heading
$_['heading_title']                 = 'Betalnings Profiler';

// Text
$_['text_success']                  = 'Klart: Du har sparat ändringar i Betalningsprofilen!';
$_['text_list']                            = 'Betalnings Profiler Lista';
$_['text_add']                             = 'Lägg till Betalnings Profiler';
$_['text_edit']                            = 'Ändra i Betalnings Profiler';
$_['text_payment_profiles']         = 'Betalning profil';
$_['text_status_active']            = 'Aktiverad';
$_['text_status_inactive']          = 'Inaktiverad';
$_['text_status_cancelled']         = 'Avbruten';
$_['text_status_suspended']         = 'Avstängd';
$_['text_status_expired']           = 'Utgått';
$_['text_status_pending']           = 'Behandlas';
$_['text_transactions']             = 'Transaktion';
$_['text_cancel_confirm']           = 'Om du avbryter en Profil går det inte att komma tillbaka. Är du säker?';
$_['text_transaction_created']             = 'Skapad';
$_['text_transaction_payment']             = 'Betalning';
$_['text_transaction_outstanding_payment'] = 'Utestående betalning';
$_['text_transaction_skipped']             = 'Skippa betalning';
$_['text_transaction_failed']              = 'Betalning misslyckades';
$_['text_transaction_cancelled']           = 'Avbruten';
$_['text_transaction_suspended']           = 'Avstängd';
$_['text_transaction_suspended_failed']    = 'Avstängd för misslyckad betalning';
$_['text_transaction_outstanding_failed']  = 'Utestående betalning misslyckades';
$_['text_transaction_expired']             = 'Utgår';

// Entry
$_['entry_cancel_payment']      = 'Avbryt betalning:';
$_['entry_order_recurring']     = 'ID:';
$_['entry_order_id']            = 'Order ID:';
$_['entry_reference']           = 'Betalningsreferens';
$_['entry_customer']            = 'Kund';
$_['entry_date_created']        = 'Skapad datum';
$_['entry_status']              = 'Status';
$_['entry_type']                = 'Typ';
$_['entry_action']              = 'Ändra';
$_['entry_email']               = 'Email';
$_['entry_description']         = 'Profil beskrivning';
$_['entry_product']             = "Produkt:";
$_['entry_quantity']            = "Antal:";
$_['entry_amount']              = "Mängd:";
$_['entry_recurring']           = 'Profil';
$_['entry_payment_method']      = 'Betalnings sätt:';

// Error / Success
$_['error_not_cancelled'] = 'Error: %s';
$_['error_not_found']     = 'Kunde inte avbryta profil.';
$_['success_cancelled']   = 'Återkommande betalning avbruten.';
?>